<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.LabOpenPlanService" %>
<%@ page import="com.hgd.laboratory.po.OpenLabSchedule" %>

<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>
</head>
<%!
    String labOpenPlanStatusToString(int status) {
        if (status == LabOpenPlanService.SCRATCH)
            return "草稿";
        else
            return "已发布";
    }
%>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#defaultNavbar1"
                                aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span
                                class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">值班安排</a>
                    </div>
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>

                    <form class="navbar-form" role="form" action="dutySchedule_workoutByLabId">
                        <input type="text" name="term" value="${sessionScope.currentTerm}" hidden/>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span>开放实验室</span>
                            </div>
<%--                            <select class="form-control" id="openLab" name="srch_labId"--%>
<%--                                    required>--%>
<%--                                <%--%>
<%--                                    List<String> openLabList = new TestService().getAllLabList();--%>
<%--                                    for (int i = 0; i < openLabList.size(); i++) {--%>
<%--                                        out.print("<option>");--%>
<%--                                        out.print(openLabList.get(i));--%>
<%--                                        out.print("</option>");--%>
<%--                                    }--%>
<%--                                %>--%>
<%--                            </select>--%>
                            <select class="form-control" id="openLab" name="srch_labId" required>
                                <%
                                    String term = (String) session.getAttribute("currentTerm");
                                    List<String> openLabList = new TestService().getOpenLabList(term);
                                    OpenLabSchedule openLabSchedule = (OpenLabSchedule) request.getAttribute("openLabSchedule");
                                    if (openLabList != null) {
                                        for (int i = 0; i < openLabList.size(); i++) {
                                            String selected = "";
                                            if ((openLabSchedule != null) && (openLabList.get(i).equals(openLabSchedule.getLabId())))
                                                selected = "selected";
                                            out.print(String.format("<option  %s>", selected));
                                            out.print(openLabList.get(i));
                                            out.print("</option>");
                                        }
                                    }
                                %>
                            </select>





                        </div>
                        <button type="submit" class="btn btn-default">安排值班</button>
                    </form>

                </div>
                <!-- /.container-fluid -->
            </nav>
            <div class="clearfloat"></div>
            <h3>安排本学期开放实验室的值班</h3>
            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>#</th>
                    <th>实验室编号</th>
                    <th>说明</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="labOpenPlanList">
                    <tr>
                        <td><s:property value="openPlanId"/></td>
                        <td><s:property value="labId"/><a
                                href="dutySchedule_view?srch_openPlanId=<s:property value='openPlanId'/>">查看</a></td>
                        <td><s:property value="desp"/></td>
                        <%
                            ValueStack vs = (ValueStack) request.getAttribute("struts.valueStack");
                            int status_value = 0;
                            try {
                                status_value = (int) vs.findValue("status");
                            } catch (Exception e) {
                                ;
                            }

                        %>

                        <td><%= labOpenPlanStatusToString(status_value) %>
                        </td>

                        <% if (status_value == LabOpenPlanService.PUBLISH) { %>
                        <td></td>
                        <% } else { %>
                        <td><a href="dutySchedule_workoutById?srch_openPlanId=<s:property value='openPlanId'/>">拟订</a>
                            <a href="dutySchedule_delete?srch_openPlanId=<s:property value='openPlanId'/>">删除</a>
                        </td>
                        <% } %>

                    </tr>
                </s:iterator>
                </tbody>
            </table>

        </div>

        <!-- InstanceEndEditable -->
    </div>
</div>
<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>