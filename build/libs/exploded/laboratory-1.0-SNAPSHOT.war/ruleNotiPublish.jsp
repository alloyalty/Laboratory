<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        #groupbtn-layout {
            display: flex;
            justify-content: space-around;
        }
    </style>
</head>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>

<h3>规则 通知 审定</h3>
<div id="middle-part">
    <form class="form-horizontal" role="form">
        <input name="id" hidden value="<s:property value='id'/>">
        <input name="category" hidden value="<s:property value='category'/>">
        <input name="status" hidden value="<s:property value='status'/>" id="status">
        <div class="form-group">
            <label for="title" class="col-sm-2 control-label">标题</label>
            <div class="col-sm-10">
                <input type="text" class="form-control text-center input-lg title" id="title" name="title" required
                       value="<s:property value='title'/>"
                       placeholder="标题">
            </div>
        </div>
        <div class="form-group">
            <label for="副标题" class="col-sm-2 control-label">副标题</label>
            <div class="col-sm-10">
                <input type="text" class="form-control text-center input-sm subtitle" id="subtitle" name="subTitle"
                       value="<s:property value='subTitle'/>"
                       placeholder="副标题">
            </div>
        </div>
        <div class="form-group">
            <label for="正文" class="col-sm-2 control-label">正文</label>
            <div class="col-sm-10">
                <textarea name="text" class="form-control" rows="20" id="text" required> <s:property
                        value='text'/></textarea>
            </div>
        </div>
        <% User currUser = (User) session.getAttribute("user");
        %>
        <input type="text" name="drawupId" value="<%= currUser.getId() %>" hidden>
        <div class="form-group">
            <label for="正文" class="col-sm-2 control-label">附注(作者)</label>
            <div class="col-sm-10">
                <input type="text" class="form-control text-right" id="authorName" name="author"
                       value="<s:property value='author'/>"
                       placeholder="作 者">
            </div>
        </div>
        <div class="form-group">
            <label for="正文" class="col-sm-2 control-label"></label>
            <div class="col-sm-10 btn-group">
                <div id="groupbtn-layout">
                    <button type="submit" class="btn btn-primary" formaction="rulesAndNotice_publish">发布</button>
                    <button type="submit" class="btn btn-primary" formaction="rulesAndNotice_cancel">取消</button>
                </div>
            </div>
        </div>
    </form>
</div>

<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>

