<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
</div>
<div id="middle-part">
    <div id="contents"
         style="padding-top: 10px 10px 10px 10px; width: 1024px; border: rgba(0, 0, 0, 1.00) thin solid">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <h3 style="margin-left:30px">临时上机/实验登记</h3>
        <hr/>
        <form class="form-horizontal" role="form">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <label for="stuNum" class="col-sm-4 control-label">学号/工号</label>
                            <div class="col-sm-6">
                                <input name="stuNum" type="text" class="form-control"
                                       id="stuNum" placeholder="请输入学生学号/教师工号">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">姓名</label>
                            <div class="col-sm-6">
                                <p class="form-control-static"><s:property value="#applyForLab.userName"/></p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-4 control-label">预约时段</label>
                            <div class="col-sm-6">
                                <p class="form-control-static">
                                    <s:if test="#applyForLab!=null">
                                        <s:if test="#applyForLab.timeslotStart==#applyForLab.timeslotFinish">
                                            第<s:property value="#applyForLab.timeslotStart"/>时段
                                        </s:if>
                                        <s:else>
                                            第<s:property value="#applyForLab.timeslotStart"/>~<s:property
                                                value="#applyForLab.timeslotFinish"/>时段
                                        </s:else>
                                    </s:if>
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">实验室</label>
                            <input name="labId" type="text" hidden value="<%= session.getAttribute("labId")%>">
                            <div class="col-sm-6">
                                <p class="form-control-static"><%= session.getAttribute("labId")%>
                                </p>
                            </div>
                        </div>

                        <hr/>
                        <div class="form-group">
                            <label for="machNo" class="col-sm-4 control-label">分配机器编号</label>
                            <div class="col-sm-6">
                                <input name="machNo" type="text" class="form-control"
                                       id="machNo">
                            </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label class="col-sm-6 control-label">核对相关信息无误</label>
                            <div class="col-sm-6">
                                <button type="submit" class="btn btn-primary btn-lg">确认</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">## 临时上机说明 &nbsp;##</div>
                </div>
            </div>
        </form>
        <br/>
        <hr/>
        <br/>
        <table class="table table-hover">
            <caption>
                <p>预约与上机情况</p>
            </caption>
            <thead>
            <tr>
                <th>学号</th>
                <th>姓名</th>
                <th>申请时段</th>
                <th>状态</th>
                <th>提醒</th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="#paginationData.dataList">
                <tr>
                    <td><s:property value="applyNumber"/></td>
                    <td><s:property value="userName"/></td>
                    <s:if test="timeslotStart==timeslotStart">
                        <td>第<s:property value="timeslotStart"/>时段</td>
                    </s:if>
                    <s:else>
                        <td>第<s:property value="timeslotStart"/>~<s:property value="timeslotFinish"/>时段</td>
                    </s:else>
                    <td><s:property value="applyNumber"/></td>
                    <td></td>
                </tr>
            </s:iterator>
            </tbody>
        </table>
        <hr/>
        <s:debug/>
        <!-- InstanceEndEditable -->
    </div>
</div>
<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>