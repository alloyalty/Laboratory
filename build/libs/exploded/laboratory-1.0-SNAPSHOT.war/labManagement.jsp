
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


        h3 {
            text-align: center;
        }

        #contents {
            margin: 0 auto;
        }

        .clearfloat {
            clear: both;
            height: 0;
            font-size: 1px;
            line-height: 0px;
        }

        #leftpart {
            width: 100%;
            font-family: "宋体";
            font-size: 20px;
            float: left;
        }

        #addStu {
            width: 300px;
            font-family: "宋体";
            font-size: 16px;
            float: right;
            border: rgba(52, 31, 235, 1.00) thin solid;
            margin-left: 50px;
            margin-right: 50px;
            padding-left: 8px;
            padding-right: 8px;
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div>
            <h3>实验室人员管理</h3>
            <hr style="size: 2px; width: 99%; line-height: 10px">
            <div id="leftpart">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <!-- Brand and toggle get grouped for better mobile display -->

                        <!-- Collect the nav links, forms, and other content for toggling -->
                        <div class="collapse navbar-collapse" id="defaultNavbar1">
                            <ul class="nav navbar-nav">
                                <li><a data-toggle="modal" href="#addStuModal">新增实验室人员</a></li>

                            </ul>
                            <form class="navbar-form navbar-right" role="search"
                                  id="searchForm" action="labManaAction_query" method="post">
                                <input type="text" name="delflag" value="0" hidden id="delflag"/>
                            </form>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <div class="btn-group navbar-btn">
                                        <button type="button" class="btn btn-default" onclick="delflagFilter(0)"
                                                form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>在值
                                        </button>
                                        <button type="button" class="btn btn-default" onclick="delflagFilter(1)"
                                                form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>曾经
                                        </button>
                                        <button type="button" class="btn btn-default" onclick="delflagFilter(-1)"
                                                form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>全部
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->

                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
            <div class="modal fade" id="addStuModal" tabindex="-1" role="dialog"
                 aria-labelledby="addStuModalLable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content container" style="width: 35em">
                        <form class="form-horizontal" role="form" action="labManaAction_addLabManager" method="post">
                            <div class="modal-header">
                                <h3 class="modal-title" id="addStuModalLable">增加 实验室值班人员</h3>
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="teaName" class="col-sm-4 control-label ">姓名：</label>
                                        <div class="col-sm-4">
                                            <input class="form-control" id="teaName" name="teaNameOrId"
                                                   placeholder="请输入工号或姓名" type="text">
                                        </div>
                                        <div class="col-sm-3">
                                            <button type="button" class="btn btn-primary" id="queryByNameOrId">查询
                                            </button>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tId" class="col-sm-4 control-label">工号：</label>
                                        <div class="col-sm-8">
                                            <p id="tId" class="form-control-static"></p>
                                            <input name="code" hidden id="teaId">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="tName" class="col-sm-4 control-label">姓名：</label>
                                        <div class="col-sm-8">
                                            <p id="tName" class="form-control-static"></p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="collage" class="col-sm-4 control-label">单位：</label>
                                        <div class="col-sm-8">
                                            <p id="collage" class="form-control-static"></p>
                                        </div>
                                    </div>
                                </fieldset>
                                <br/>
                            </div>
                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-default close"
                                                data-dismiss="modal">关闭
                                        </button>
                                    </div>
                                    <div class="col-sm-offset-4 col-sm-4">
                                        <button type="submit" class="btn btn-primary">指定为实验室人员</button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfloat">
            <h3 style="text-align: left;">实验室人员列表</h3>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>工号</th>
                    <th>姓名</th>
                    <th>单位</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="#labTeacherList">
                    <tr>
                        <td><s:property value="code"/></td>
                        <td><s:property value="name"/></td>
                        <td><s:property value="collage"/></td>
                        <td>
                            <s:if test="delflag==0">在值</s:if>
                            <s:else>不在值</s:else>
                        </td>
                        <td><a href="labManaAction_delLabManager?id=<s:property value='id'/>">
                            <s:if test="delflag==0">删除</s:if>
                        </a></td>
                    </tr>
                </s:iterator>


                </tbody>
            </table>
        </div>

        <!-- InstanceEndEditable -->
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    $(document).ready(function () {
        $("#queryByNameOrId").click(function () {
            $.post("labManaAction_queryByNameOrId", $("#teaName").serializeArray(),
                function (data, status) {
                    //alert("数据: \n" + data + "\n状态: " + status);
                    let arr = data.teacherList;
                    if (arr.length == 1) {
                        let teacher = arr[0];
                        $("#tId").text(teacher["tId"]);
                        $("#teaId").val(teacher["tId"]);
                        $("#tName").text(teacher["tName"]);
                        $("#collage").text(teacher["collage"]);
                    }

                }, "json"
            );
        });
    });

    function delflagFilter(delflag) {
        $("#delflag").val(delflag);
        $("#searchForm").action = "labManaAction_query";
        $("#searchForm").submit();
    }
</script>
</body>
<!-- InstanceEnd -->
</html>