<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container" style="width:300px">
            <form class="form-signin" action="termAction_init" method="post">
                <h3 class="form-signin-heading text-center">初始化新学期</h3>
                <br/>
                <div class="input-group"><span class="input-group-addon"> <span>学年</span></span>

                    <input
                            name="year" type="text" id="inputTermYear" class="form-control"
                            placeholder="xxxx-xxxx 例如2018-2019" pattern="2[0-9]{3}-2[0-9]{3}"
                            required autofocus>
                </div>

                <div class="input-group"><span class="input-group-addon"> <span>学期</span></span>

                    <input
                            name="term" type="number" id="inputTerm" class="form-control" placeholder="1或2"
                            pattern="[1-4]"
                            required>
                </div>

                <div class="input-group"><span class="input-group-addon"> <span>周数</span></span>

                    <input
                            name="weeksOfTerm" type="number" id="inputWeeks" class="form-control"
                            pattern="[0-9]{1,2}"
                            required>
                </div>


                <div class="input-group"><span class="input-group-addon"> <span>第一周星期一</span></span>

                    <input
                            name="dayOfFirstWeekMonday" type="date" id="inputFirstDate" class="form-control"
                            required>
                </div>


                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="submit">期初始化
                </button>
                <a class="btn btn-lg btn-primary btn-block" type="button" href="user_blank">取消
                </a>
            </form>
        </div>

    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>