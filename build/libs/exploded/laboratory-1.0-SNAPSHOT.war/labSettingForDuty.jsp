<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="com.hgd.laboratory.dto.ApplyForLabAudit" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ page import="java.util.Calendar" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>


<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <h3 align="center">设置管理实验室</h3>
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <form class="navbar-form" role="form" id="auditForm"
                          action="dutyAction_LabSetting">
                        <input type="hidden" name="state" value="0" id="stateId"/>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <span>开放实验室</span>
                            </div>
                            <select class="form-control" id="openLab" name="labId" required>
                                <%
                                    String term = (String) session.getAttribute("currentTerm");
                                    String lastSelected = (String) request.getParameter("labId");
                                    if (lastSelected == null) lastSelected = "";
                                    List<String> openLabList = new TestService().getOpenLabList(term);
                                    for (int i = 0; i < openLabList.size(); i++) {
                                        String selected = openLabList.get(i).equalsIgnoreCase(lastSelected) ? "selected" : "";
                                        out.print(String.format("<option %s>", selected));
                                        out.print(openLabList.get(i));
                                        out.print("</option>");
                                    }
                                    String selected = lastSelected.isEmpty() ? "selected" : "";
                                %>

                                <%--                                <option value=""  <%=selected%>>不限实验室</option>--%>
                            </select>

                        </div>
                        <button type="submit" class="btn btn-default">确认
                        </button>
                    </form>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>


        </div>

        <s:debug/>
        <!-- InstanceEndEditable -->
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script src="js/mydefFilter.js"></script>
</body>
<!-- InstanceEnd -->
</html>
<%!
    private String calYuyueDefaultDate(Date theDate) {
        if (theDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 1);
            theDate = calendar.getTime();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(theDate);
    }
%>