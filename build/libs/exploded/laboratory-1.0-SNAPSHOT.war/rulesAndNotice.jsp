<%@ page import="com.hgd.laboratory.util.Page" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ page import="com.hgd.laboratory.po.Notice" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


    </style>

</head>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<%!
    String activeItem(int state, int activeState) {
        if (state == activeState)
            return " active ";
        else
            return "";
    }
%>
<div id="middle-part">
    <div id="contents">

        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="defaultNavbar2">
                    <%
                        String category = (String) request.getAttribute("category");
                        if (category == null) {
                            category = "announce";
                        }
                        String announce_selected = "";
                        String rules_selected = "";
                        if (category.equals("announce")) {
                            announce_selected = "selected";
                        } else {
                            rules_selected = "selected";
                        }
                    %>
                    <form class="navbar-form navbar-left" role="search" action="rulesAndNotice_toDrawup" method="post">
                        <div class="form-group">
                            <select class="form-control" name="category"
                                    style="display: inline" autofocus required>
                                <option <%= announce_selected%> value="announce">通知</option>
                                <option <%= rules_selected%> value="rules">规章</option>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default navbar-btn">起草</button>
                    </form>

                    <form class="navbar-form navbar-right" role="search"
                          id="searchForm" action="rulesAndNotice_query" method="post">
                        <div class="form-group">
                            <select class="form-control" name="category"
                                    style="display: inline" autofocus required form="searchForm">
                                <option  <%= announce_selected%> value="announce">通知</option>
                                <option  <%= rules_selected%> value="rules">规章</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input name="keyword" type="text" class="form-control" required
                                   value="<s:property value='keyword' />"
                                   placeholder="请输入关键字搜索...">
                        </div>
                        <input type="text" name="status" hidden id="status" value="<s:property value='status' />"/>
                        <input type="text" name="pageNo" value="<s:property value='pageNo' />" hidden id="pageNo"/>
                        <button type="button" class="btn btn-default" onclick="statusFilter(-1,1)">搜索</button>
                    </form>
                    <%
                        int status = (Integer) request.getAttribute("status");
                    %>
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <div class="btn-group navbar-btn">
                                <button type="button" class="btn btn-default <%= activeItem(status,1)%>"
                                        onclick="statusFilter(1,1)"
                                        form="searchForm">
                                    <span class="glyphicon glyphicon-filter"></span>草稿
                                </button>
                                <button type="button" class="btn btn-default<%= activeItem(status,2)%>"
                                        onclick="statusFilter(2,1)"
                                        form="searchForm">
                                    <span class="glyphicon glyphicon-filter"></span>待审定
                                </button>
                                <button type="button" class="btn btn-default<%= activeItem(status,4)%>"
                                        onclick="statusFilter(4,1)"
                                        form="searchForm">
                                    <span class="glyphicon glyphicon-filter"></span>待发布
                                </button>
                                <button type="button" class="btn btn-default<%= activeItem(status,8)%>"
                                        onclick="statusFilter(8,1)"
                                        form="searchForm">
                                    <span class="glyphicon glyphicon-filter"></span>已发布
                                </button>
                                <button type="button" class="btn btn-default<%= activeItem(status,-1)%>"
                                        onclick="statusFilter(-1,1)"
                                        form="searchForm">
                                    <span class="glyphicon glyphicon-filter"></span>全部
                                </button>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
        <table class="table">
            <caption>
                <% String cate = (String) request.getAttribute("category");
                    if (cate == null)
                        cate = "通知";
                    else if (cate.equals("announce"))
                        cate = "通知";
                    else
                        cate = "规章";
                %>
                <b><%= cate%>
                </b>
            </caption>
            <thead>
            <tr>
                <th>标题</th>
                <th>起草日期</th>
                <th>起草人</th>
                <th>状态</th>
                <th>处理</th>
                <th>备注</th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="#paginationData.dataList">
                <tr>
                    <td><s:property value="title"/></td>
                    <td><s:date name="drawupDate" format="yyyy-MM-dd"/>
                    </td>
                    <td><s:property value="author"/></td>
                    <td>
                        <s:if test="status==1">
                            草稿
                        </s:if>
                        <s:elseif test="status==2">
                            待审核
                        </s:elseif>
                        <s:elseif test="status==4">
                            待发布
                        </s:elseif>
                        <s:else>
                            发布
                        </s:else>
                    </td>
                    <s:if test="status==1">
                        <td><a href="rulesAndNotice_toDrawup?specId=<s:property value='id'/> ">修改</a>
                            <a href="rulesAndNotice_delete?specId=<s:property value='id'/> ">删除</a>
                        </td>
                    </s:if>
                    <s:elseif test="status==2">
                        <td><a href="rulesAndNotice_toAudit?specId=<s:property value='id'/> ">审核</a></td>
                    </s:elseif>
                    <s:elseif test="status==4">
                        <td><a href="rulesAndNotice_toPublish?specId=<s:property value='id'/> ">发布</a>
                        </td>
                    </s:elseif>
                    <s:else>
                        <td>
                            <a href="rulesAndNotice_recallPublish?specId=<s:property value='id'/> ">撤回发布</a>
                        </td>
                    </s:else>
                    <td><s:property value="comment"/></td>
                </tr>
            </s:iterator>
            </tbody>
        </table>

        <hr/>
        <nav>
            <!-- Add class .pagination-lg for larger blocks or .pagination-sm for smaller blocks-->
            <%--            入口参数： form=searchForm, 类型Class<T> PaginationData<Notice>,statusFilter --%>
            <s:if test="#paginationData!=null and #paginationData.page.totalPages>=2">
                <ul class="pagination pagination-lg">
                    <% PaginationData<Notice> paginationData = (PaginationData<Notice>) request.getAttribute("paginationData");
                        Page pageNav = paginationData.getPage();
                        String prevDisabled = "";
                        if (pageNav.getCurrentPage() == 1) prevDisabled = "disabled";
                        String nextDisabled = "";
                        if (pageNav.getCurrentPage() == pageNav.getTotalPages()) nextDisabled = "disabled";
                    %>

                    <li>
                        <button type="button" class="btn btn-default"  <%= prevDisabled%>
                                onclick="statusFilter(null,<%=  pageNav.getPrevPage()%>)"
                                form="searchForm">
                            &laquo;
                        </button>
                    </li>

                    <s:bean name="org.apache.struts2.util.Counter" var="counter">
                        <s:param name="first" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                        <s:param name="last" value="#paginationData.page.lastPageNoInCurrentPanel"/>
                        <s:iterator>
                            <s:set var="current"/>
                            <s:set var="firstPageNo" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                            <s:if test="#current==#paginationData.page.currentPage">
                                <s:set var="active" value="'active'"/>
                            </s:if>
                            <s:else>
                                <s:set var="active" value="''"/>
                            </s:else>

                            <li>
                                <button type="button" class="btn btn-default <s:property value='#active' />"
                                        onclick="statusFilter(null,
                                            <s:property/> )"
                                        form="searchForm">
                                    <s:property/>
                                </button>
                            </li>
                        </s:iterator>
                    </s:bean>

                    <li>
                        <button type="button" class="btn btn-default"  <%= nextDisabled %>
                                onclick="statusFilter(null,<%=  pageNav.getNextPage()%>)"
                                form="searchForm">
                            &raquo;
                        </button>
                    </li>
                </ul>
            </s:if>
        </nav>
    </div>
</div>
<s:debug/>

<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function statusFilter(status, pageNo) {
        // !null 参数，表示此参数代替表单参数。
        // null 表示不更改表单参数，使用表单参数
        if (status != null) {
            $("#status").val(status);
        }
        if (pageNo != null) {
            $("#pageNo").val(pageNo);
        }
        $("#searchForm").attr("action", "rulesAndNotice_query");
        $("#searchForm").submit();
    }
</script>
</body>
</html>
