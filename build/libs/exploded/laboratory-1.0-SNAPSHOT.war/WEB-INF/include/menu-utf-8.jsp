﻿<%@ page import="com.hgd.laboratory.po.User" %>
<%@ page import="com.hgd.laboratory.po.User" %>
<%@ page pageEncoding="UTF-8"%>
<div id="header">
	<div id="brand">
		<img src="images/logo.png" width="475" height="130" alt="河南工业大学" /> <span
			class="systitle">实验室开放管理系统</span>
	</div>
	<%
		//String role="labManager";
		User user = (User) session.getAttribute("user");
		if (user==null) {
			response.sendRedirect("login.jsp");
			return;
		}
		String role = user.acqSystemRole().toLowerCase();
		if ("student".equalsIgnoreCase(role)) {
	%>
	<nav class="navbar navbar-inverse  navbar-light bg-primary ">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">选择操作</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="openLabSchedule_view.action">开放时间表</a></li>
					 
					<li><a href="makeAnAppointmentStep1.jsp">预约申请</a></li>
<%--					 --%>
<%--					<li><a href="anAppointmentQuery.jsp">查询预约</a></li>--%>

					<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-haspopup="true"
											aria-expanded="false">查询预约 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="anAppointmentQuery_queryHistory.action">查询历史预约</a></li>
							<li><a href="anAppointmentQuery_queryCurrent.action">查询当前预约</a></li>
						</ul></li>

					<li><a href="announce_toView">查看通知</a></li>
					 
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">账号管理 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="modifyPassword.jsp">修改密码</a></li>
							<li><a href="bindingEmail.jsp">绑定邮箱</a></li>
							<li><a href="modifyEmail.jsp">修改邮箱</a></li>
						</ul></li>
					 
						<li><a href="user_logout">退出登录</a></li>
						<li><a href="login.jsp">登录</a></li>
					 
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<%
		}
	%>
	<%
		if ("teacher".equalsIgnoreCase(role)) {
	%>
	<nav class="navbar navbar-inverse  navbar-light bg-primary ">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">选择操作</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="openLabSchedule_view.action">开放时间表</a></li>
					 
					<li><a href="makeAnAppointmentStep1.jsp">预约申请</a></li>
					 
					<li><a href="anAppointmentQuery.jsp">查询预约</a></li>
					 
					<li><a href="announce_toView">查看通知</a></li>
					 
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">账号管理 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="modifyPassword.jsp">修改密码</a></li>
							<li><a href="bindingEmail.jsp">绑定邮箱</a></li>
							<li><a href="modifyEmail.jsp">修改邮箱</a></li>
						</ul></li>
					 
						<li><a href="user_logout">退出登录</a></li>
						<li><a href="login.jsp">登录</a></li>
					 
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<%
		}
	%>
	<%
		if ("labmanager".equalsIgnoreCase(role)) {
	%>
	<nav class="navbar navbar-inverse  navbar-light bg-primary ">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">选择操作</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<!-- #BeginLibraryItem "/Library/实验室开放Item.lbi" -->
					<li><a href="openLabSchedule_view.action">开放时间表</a></li>
					 
					<li><a href="appAppointmentAudit.jsp">审核预约</a></li>

					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">开放计划<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="labOpenPlan_manage.action">拟订学期开放计划</a></li>
							<li><a href="tempOpenPlan_managetp.action">安排临时计划</a></li>
							<li><a href="openLabSchedule_view.action">开放时间表</a></li>

						</ul></li> 


					<li><a href="equipmentMaintenance.jsp">设备维修日志</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">值班人员<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="dutySchedule_manage">安排值班</a></li>
							<li><a href="dutyStudentAction_query?delflag=0">值班人员管理</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">规章/通知 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="announce_toView">查看通知</a></li>
							<li><a href="rulesAndNotice_query">发布规章/通知</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">账号管理 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="modifyPassword.jsp">修改密码</a></li>
							<li><a href="bindingEmail.jsp">绑定邮箱</a></li>
							<li><a href="modifyEmail.jsp">修改邮箱</a></li>
						</ul></li>
					 
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">系统维护<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="baseDataImport.jsp#">导入基数数据</a></li>
							<li><a href="termInit.jsp">初始化学期</a></li>
							<li><a href="closeCurrTerm.jsp">关闭当前学期</a></li>
						</ul></li>
						<li><a href="user_logout">退出登录</a></li>
						<li><a href="login.jsp">登录</a></li>
					 
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<%
		}
	%>
	<%
		if ("duty".equalsIgnoreCase(role)) {
	%>
	<nav class="navbar navbar-inverse  navbar-light bg-primary ">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">选择操作</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="openLabSchedule_view.action">开放时间表</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							学生上机
							<b class="caret"></b>
						</a>
						<ul class="dropdown-menu">
							<li><a href="dutyAction_checkLabSetting?target=studentExp.jsp">学生预约上机</a></li>
							<li><a href="dutyAction_checkLabSetting?target=studentTempExp.jsp">学生临时上机</a></li>
						</ul>
					</li>
					<li><a href="faultRepair_reqfill.action">设备报修</a></li>
					<li><a href="announce_toView">查看通知</a></li>
					 
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">账号管理 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="modifyPassword.jsp">修改密码</a></li>
							<li><a href="bindingEmail.jsp">绑定邮箱</a></li>
							<li><a href="modifyEmail.jsp">修改邮箱</a></li>
						</ul></li>
					 
						<li><a href="user_logout">退出登录</a></li>
						<li><a href="login.jsp">登录</a></li>
					 
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
	<%
		}
	%>
	<%
		if ("admin".equalsIgnoreCase(role)) {
	%>
	<nav class="navbar navbar-inverse  navbar-light bg-primary ">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#">选择操作</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="openLabSchedule_view.action">开放时间表</a></li>
					 
					<li><a href="announce_toView">查看通知</a></li>
					 
					<li><a href="labManaAction_query">实验室人员管理</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">账号管理 <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="modifyPassword.jsp">修改密码</a></li>
							<li><a href="bindingEmail.jsp">绑定邮箱</a></li>
							<li><a href="modifyEmail.jsp">修改邮箱</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
											data-toggle="dropdown" role="button" aria-haspopup="true"
											aria-expanded="false">系统维护<span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="baseDataImport.jsp#">导入基数数据</a></li>
							<li><a href="termInit.jsp">初始化学期</a></li>
							<li><a href="closeCurrTerm.jsp">关闭当前学期</a></li>
						</ul></li>
						<li><a href="user_logout">退出登录</a></li>
					 
						<li><a href="login.jsp">登录</a></li>
					 
				</ul>
			</div>
		</div>
	</nav>
	<%
		}
	%>

</div>
