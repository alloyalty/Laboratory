<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="orginId" required="true" %>
<%@ attribute name="orginValue" required="true" %>
<%@ attribute name="tempId" required="true" %>
<%@ attribute name="tempValue" required="true" %>

<%!
    String tempPlanDisplay(String orgin,String temp ) {
        if (temp.trim().isEmpty())
            return "";
        if (orgin.trim().equals(temp.trim()))
            return "";
        if (temp.trim().equals("0"))
            return "关闭";
        else if (temp.trim().equals("1"))
            return "开放";
        else
            return "错误";


    }
%>

<div class="tempPlan_box">
    <span><%= orginValue.trim().equals("0")?"关闭":"开放"  %></span>
    <button type="button">临时</button>
    <span  ><%= tempPlanDisplay(orginValue,tempValue)%></span>
    <input id= "<%= orginId%>" name="<%= orginId%>" value="<%= orginValue%>" type="text"  hidden/>
    <input id="<%= tempId%>" name="<%= tempId%>" value="<%= tempValue%>"  type="text" hidden/>

</div>