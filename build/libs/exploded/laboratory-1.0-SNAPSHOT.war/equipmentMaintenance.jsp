<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.FaultRepairService" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


    </style>

    <%!
        String statusToString(int status) {
            if (status == FaultRepairService.FAULT)
                return "故障";
            else
                return "正常";
        }
    %>
</head>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <main>
            <section class="jumbotron">
                <h3>维修设备管理</h3>
                <hr>
                <form class="form-inline" action="faultRepair_queryFaultByLid" method="post">
                    <div class="form-group">
                        <label style="display: inline">实验室: </label>
                        <select class="form-control" id="openLab" name="lid" required>
                            <%
                                String term = (String) session.getAttribute("currentTerm");
                                List<String> openLabList = new TestService().getOpenLabList(term);
                                String lastSelected = (String) request.getParameter("lid");
                                if (lastSelected == null) lastSelected = "";

                                for (int i = 0; i < openLabList.size(); i++) {
                                    String selected = openLabList.get(i).equalsIgnoreCase(lastSelected) ? "selected" : "";
                                    out.print(String.format("<option %s>", selected));
                                    out.print(openLabList.get(i));
                                    out.print("</option>");
                                }
                                //String selected = lastSelected.isEmpty() ? "selected" : "";
                            %>

<%--                            <option value=""  <%=selected%>>不限实验室</option>--%>
                        </select>
                    </div>



                    <div>
                        <button id="query" type="submit" class="btn btn-primary">查询报修记录</button>
                    </div>
                </form>
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="id">标识</th>
                        <th scope="col">实验室</th>
                        <th scope="col">设备标识</th>
                        <th scope="col">故障说明</th>
                        <th scope="col">设备状态</th>
                        <th scope="col">故障处理</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                    </tr>
                    <s:iterator value="faultFillList">
                        <tr class="myclass">
                            <td scope="row"><s:property value="id"/></td>
                            <td><s:property value="lid"/></td>
                            <td><s:property value="fid"/></td>
                            <td><s:property value="faultDFesp"/></td>
                            <%
                                ValueStack vs = (ValueStack) request.getAttribute("struts.valueStack");
                                int fstate_value = 0;
                                try {
                                    fstate_value = (int) vs.findValue("fstate");
                                } catch (Exception e) {
                                    ;
                                }

                            %>
                            <td><%= statusToString(fstate_value)     %>
                            </td>

                            <td>
                                <button type="button" class="btn btn-primary " onclick="openAddModal(this);">故障处理
                                </button>
                            </td>

                        </tr>
                    </s:iterator>

                    </tbody>
                </table>
            </section>
            <br>

            <!-- 模态框（Modal） -->
            <div class="modal fade" id="faultHandleModal" tabindex="-1" role="dialog"
                 aria-labelledby="faultHandleModalLable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content container" style="width:35em">
                        <form class="form-horizontal" role="form" method="post" action="faultRepair_updateMaintance">

                            <div class="modal-header">
                                <h3 class="modal-title" id="faultHandleModalLable">维修记录</h3>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="modal-body">
                                    <p id="lid">实验室:#6431#</p>
                                    <hr>
                                    <p id="fid">设备ID:##BS121323##</p>
                                    <hr>
                                    <p id="faultDFesp">故障:##fAULT DESCRIPTION##</p>
                                    <hr>
                                </div>
                                <input type="number" name="id" hidden/>


                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text">处理:</span></div>
                                    <textarea name="maintenceRecs" class="form-control"
                                              aria-label="With textarea"></textarea>
                                </div>
                                <hr/>
                                <div class="radio" style="flex-direction: row-reverse;">
                                    <label>
                                        <input type="radio" name="fstate" id="optionsRadios1" value="正常" checked>
                                        维修处理后恢复正常 </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input type="radio" name="fstate" id="optionsRadios2" value="不正常">
                                        维修处理后仍然异常 </label>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
                                <button type="submit" class="btn btn-primary">提交更改</button>
                            </div>
                        </form>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal -->
            </div>
        </main>

        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<%-- <script src="js/acqCurrData.js" />--%>
<script>
    function openAddModal(obj) {
        $("#faultHandleModal").modal('show');
        //获取当前点击行的id
        var $td = $(obj).parents('tr').children('td');
        var id = $td.eq(0).text();
        $("#itemmodalid").val(id); //将获取的该行的id值填充到模态框的文框中，文本框的ID为itemmodalid，其他的数据也是如此处理}
        $("#lid").text("实验室:" + $td.eq(1).text());
        $("#fid").text("设备ID:" + $td.eq(2).text());
        $("#faultDFesp").text("故障:" + $td.eq(3).text());


    }

</script>
</body>
<!-- InstanceEnd -->
</html>