<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div id="anAppointment" class="container" style="width: 800px">
            <div class="alert alert-success">##成功或者失败##</div>
            <div class="alert alert-info">##成功时预约的摘要信息##</div>
            <div class="alert alert-warning">##失败时的提示信息##</div>
            <div class="alert alert-danger">##错误时的信息##</div>
            <form class="form-signin">

                <!-- 成功时显示关闭，失败时显示上一步 -->
                <br/>

                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-3">
                        <button class="btn btn-lg btn-primary " type="button" onclick=""
                        >关闭
                        </button>
                    </div>
                    <div class="col-lg-2"></div>
                </div>


            </form>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>