<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.TempOpenPlanService" %>
<%@ page import="com.hgd.laboratory.po.Term" %>
<%@ page import="com.hgd.laboratory.util.TermUtil" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
</head>
<%!
    String tempOpenPlanStatusToString(int status) {
        if (status == TempOpenPlanService.SCARTCH)
            return "草稿";
        else
            return "已发布";
    }
%>
<body>

<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed"
                                data-toggle="collapse" data-target="#defaultNavbar1"
                                aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span
                                class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">开放计划</a>
                    </div>
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>


                </div>
                <!-- /.container-fluid -->
            </nav>
            <div class="clearfloat"></div>
            <h3>临时开放计划安排</h3>
            <form class="form-inline" role="form" action="tempOpenPlan_workout" method="post">
                <input type="text" name="term" value="${sessionScope.currentTerm}" hidden/>

                <%--					//todo 计算当前周次--%>
                <% Term currentTermObj = (Term)session.getAttribute("currentTermObj");
                   java.util.Date today = new java.util.Date();
                   int currentWeekNo = TermUtil.calWeekOfTerm(today,currentTermObj.getDayOfFirstWeekMonday());
                %>
                <div class="form-group">
                    当前周次 <input type="number" class="form-control" id="name"
                                name="srch_weekOfTerm"
                                placeholder="请输入周次" required value=<%= currentWeekNo%> autofocus="autofocus" min="1" max="<%= currentTermObj.getWeeksOfTerm() %>">
                </div>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>开放实验室</span>
                    </div>
                    <select class="form-control" id="labId" name="srch_labId" required>
                        <%
                            String term = (String) session.getAttribute("currentTerm");
                            List<String> openLabList = new TestService().getOpenLabList(term);
                            for (int i = 0; i < openLabList.size(); i++) {
                                out.print("<option>");
                                out.print(openLabList.get(i));
                                out.print("</option>");
                            }
                        %>
                    </select>
                </div>
                <button type="submit" class="btn btn-default">拟订计划</button>
            </form>


            <table class="table table-bordered ">
                <thead>
                <tr>
                    <th>#</th>
                    <th>周次</th>
                    <th>实验室编号</th>
                    <th>说明</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>

                <s:iterator value="tempOpenPlanList">
                    <tr>
                        <td><s:property value="tempPlanId"/></td>
                        <td><s:property value="weekOfTerm"/></td>
                        <td><s:property value="labId"/><a
                                href="tempOpenPlan_view?srch_tempPlanId=<s:property value='tempPlanId'/>">查看</a></td>
                        <td><s:property value="desp"/></td>
                        <%
                            ValueStack vs = (ValueStack) request.getAttribute("struts.valueStack");
                            int status_value = 0;
                            try {
                                status_value = (int) vs.findValue("status");
                            } catch (Exception e) {
                                ;
                            }

                        %>

                        <td><%= tempOpenPlanStatusToString(status_value) %>
                        </td>

                        <% if (status_value == TempOpenPlanService.PUBLISH) { %>
                        <td></td>
                        <% } else { %>
                        <td>
                            <a href="tempOpenPlan_workout?srch_tempPlanId=<s:property value='tempPlanId'/> ">拟订</a>
                            <a href="tempOpenPlan_delete?srch_tempPlanId=<s:property value='tempPlanId'/> ">删除临时计划</a>

                        </td>
                        <% } %>

                    </tr>
                </s:iterator>

                </tbody>
            </table>

        </div>

        <!-- InstanceEndEditable -->
    </div>
</div>

<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>