<%@ page import="com.hgd.laboratory.util.Page" %>
<%@ page import="com.hgd.laboratory.dto.PaginatingData" %>
<%@ page import="com.hgd.laboratory.po.ApplyForLab" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .box {
            border: rgb(48, 0, 0) solid thin;
            padding: 5px;
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">

        <div style="width: 900px">
            <div>
                <h3>个人历史预约信息</h3>
                <s:iterator value="#paginationData.dataList">
                    <div class="container">
                        <div class="row ">
                            <div class="col-lg-6 box"><s:property value="applyNumber"/></div>
                            <div class="col-lg-6 box">&nbsp;</div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6 box ">学号/工号：<s:property value="userId"/></div>
                            <div class="col-lg-6 box ">姓名：<s:property value="userName"/></div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6 box">申请日期：<s:property value="applyTime"/></div>
                            <div class="col-lg-6 box">申请时段：<s:property value="timeslotStart"/>-<s:property
                                    value="timeslotFinish"/></div>
                        </div>
                        <div class="row ">
                            <div class="col-lg-6 box">实验室:<s:property value="labId"/></div>
                            <div class="col-lg-6 box">状态：<s:property
                                    value="@com.hgd.laboratory.po.ApplyForLab@stateDisplay(#this.state)"/></div>
                        </div>
                        <br/>
                    </div>
                </s:iterator>


                <nav>
                    <!-- Add class .pagination-lg for larger blocks or .pagination-sm for smaller blocks-->
                    <%--            入口参数： form=searchForm, 类型Class<T> PaginationData<Notice>,statusFilter --%>
                    <s:if test="#paginationData!=null and #paginationData.page.totalPages>=2">
                        <ul class="pagination pagination-lg">
                            <% PaginationData<ApplyForLab> paginationData = (PaginationData<ApplyForLab>) request.getAttribute("paginationData");
                                Page pageNav = paginationData.getPage();
                                String prevDisabled = "";
                                if (pageNav.getCurrentPage() == 1) prevDisabled = "disabled";
                                String nextDisabled = "";
                                if (pageNav.getCurrentPage() == pageNav.getTotalPages()) nextDisabled = "disabled";
                            %>

                            <li>
                                <button type="button" class="btn btn-default"  <%= prevDisabled%>
                                        onclick="statusFilter(<%=  pageNav.getPrevPage()%>)"
                                        form="searchForm">
                                    &laquo;
                                </button>
                            </li>

                            <s:bean name="org.apache.struts2.util.Counter" var="counter">
                                <s:param name="first" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                                <s:param name="last" value="#paginationData.page.lastPageNoInCurrentPanel"/>
                                <s:iterator>
                                    <s:set var="current"/>
                                    <s:set var="firstPageNo" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                                    <s:if test="#current==#paginationData.page.currentPage">
                                        <s:set var="active" value="'active'"/>
                                    </s:if>
                                    <s:else>
                                        <s:set var="active" value="''"/>
                                    </s:else>

                                    <li>
                                        <button type="button" class="btn btn-default <s:property value='#active' />"
                                                onclick="statusFilter(<s:property/> )"
                                                form="searchForm">
                                            <s:property/>
                                        </button>
                                    </li>
                                </s:iterator>
                            </s:bean>

                            <li>
                                <button type="button" class="btn btn-default"  <%= nextDisabled %>
                                        onclick="statusFilter(<%=  pageNav.getNextPage()%>)"
                                        form="searchForm">
                                    &raquo;
                                </button>
                            </li>
                        </ul>
                    </s:if>
                </nav>
                <form action="anAppointmentQuery_queryHistory" method="post" id="searchForm">
                    <input type="text" name="pageNo" id="pageNo" hidden value ="<s:property value='pageNo' />">
                    <input type="text" name="perPageRows"  hidden value ="4">
                </form>
            </div>
        </div>

    </div>
</div>

<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function statusFilter(pageNo) {
        // !null 参数，表示此参数代替表单参数。
        // null 表示不更改表单参数，使用表单参数
        if (pageNo != null) {
            $("#pageNo").val(pageNo);
        }
        $("#searchForm").attr("action", "anAppointmentQuery_queryHistory");
        $("#searchForm").submit();
    }
</script>
<s:debug/>
</body>
</html>