<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .errorMessage li {
            list-style: none;
            text-align: left;
            vertical-align: bottom;
            padding: 0px 10px 0 0px;
            color: #ee0000;
        }
    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <div id="login" class="container">
            <form class="form-signin" action="user_changePwd" method="post">
                <h3 class="form-signin-heading text-center">修改密码</h3>
                <br/>
                <div class="input-group"> <span class="input-group-addon"> <span
                        class="glyphicon glyphicon-user"></span> </span>
                    <label for="inputUsername" class="sr-only">用户名</label>
                    <input readonly="true"
                           name="logName" type="text" id="inputUsername" class="form-control"
                           value="<%= ((User)session.getAttribute("user")).getName()%>"
                           placeholder="用户名" style="width: 300px">
                </div>
                <label class="sr-only" for="oldPassword">密码</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                    <input class="form-control" id="oldPassword" name="oldPassword" placeholder="旧密码"
                           type="password" placeholder="密码" required style="width: 300px" required autofocus>
                </div>
                <div>
                    <div class="input-group"><span id="addon1" class="input-group-addon">新密码</span>
                        <input class="form-control" id="newPassword" name="newPassword"
                               type="password" placeholder="新密码" required style="width: 272px" required>
                    </div>
                    <div>
                        <div class="input-group"><span id="addon2" class="input-group-addon">新密码</span>
                            <input class="form-control" id="reNewPassword" name="reNewPassword"
                                   type="password" placeholder="再输一次新密码" required style="width: 272px" required>
                        </div>
                        <br/>
                        <s:actionerror cssClass="errorMessage" />
                        <button class="btn btn-lg btn-primary btn-block" type="submit" id="btnSubmit"
                                style="width: 345px"> 修改
                        </button>
            </form>
        </div>

    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    $(document).ready(function () {
        $("#btnSubmit").click(function () {
            var p1 = $("#newPassword").val();
            var p2 = $("#reNewPassword").val();
            if (p1 !== p2) {
                alert("二次输入密码不同。");
                return false;
            }
            return true;
        });

    });

</script>
</body>
<!-- InstanceEnd -->
</html>