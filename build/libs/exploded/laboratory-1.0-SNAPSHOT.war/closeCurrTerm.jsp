<%@ page import="com.hgd.laboratory.po.Term" %>
<%@ page import="com.hgd.laboratory.util.TermUtil" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>

    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">

        <div class="container" style="width:300px">
            <form class="form-signin" action="termAction_closure" method="post">
                <h3 class="form-signin-heading">
                    <h3 class="text-center">关闭当前学期</h3>
                    <br/>
                        <%  Term cTerm  = (Term)session.getAttribute("currentTermObj");  %>
                        <% if (cTerm!=null) {  %>
                    <div class="input-group"><span class="input-group-addon"> <span>学年</span></span>
                        <input readonly
                               name="termYear" type="text" id="inputTermYear" class="form-control"
                               value="<%= cTerm.getYear()%>"
                               required autofocus>
                    </div>
                    <div class="input-group"><span class="input-group-addon"> <span>学期</span></span>
                        <input readonly
                               name="term" type="number" id="inputTerm" class="form-control"
                               value="<%= cTerm.getTerm()%>"
                               required>
                    </div>
                    <div class="input-group"><span class="input-group-addon"> <span>周数</span></span>
                        <input readonly
                               name="termWeeks" type="number" id="inputWeeks" class="form-control"
                               value="<%= cTerm.getWeeksOfTerm()%>"
                               required>
                    </div>
                    <div class="input-group"><span class="input-group-addon"> <span>第一周星期一</span></span>
                        <input readonly
                               name="termFirstDate" type="date" id="inputFirstDate" class="form-control"
                               value="<%= cTerm.getDayOfFirstWeekMonday()%>"
                               required>
                    </div>
                    <div class="input-group"><span class="input-group-addon"> <span>学期状态</span></span>
                        <input
                                name="termStatus" type="text" id="inputStatus" class="form-control" value="正在进行......"
                                readonly="readonly">
                    </div>
                    <br/>
                    <button class="btn btn-lg btn-primary btn-block" type="submit"
                    >当前学期关闭
                    </button>
                    <a class="btn btn-lg btn-primary btn-block" type="button" href="user_blank"
                    >取消
                    </a>
                        <% } else { %>
                    <h3 class="text-center">没有学期在运行。</h3>
                        <% }%>
            </form>
        </div>

    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>