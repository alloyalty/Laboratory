<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>

</head>
<body>

<div id="middle-part">
    <div id="contents">
        <div id="login" class="container">
            <form class="form-signin" action="findBackPwdAsyn_validAndchangePwd" method="post" id="chPwdForm">
                <h3 class="form-signin-heading text-center">修改密码</h3>
                <br/>
                <div class="input-group"> <span class="input-group-addon"> <span
                        class="glyphicon glyphicon-user"></span> </span>
                    <label for="inputUsername" class="sr-only">用户名</label>
                    <input required
                           name="id" type="text" id="inputUsername" class="form-control"
                           value="<s:property value='id' />"
                           placeholder="用户名" style="width: 300px">
                </div>
                <div>
                    <div class="input-group"><span id="addon1" class="input-group-addon">新密码</span>
                        <input class="form-control" id="newPassword" name="newPassword"
                               type="password" placeholder="新密码" required style="width: 272px" required>
                    </div>
                    <input hidden name="sid" value="<s:property value='sid' />" >
                    <div>
                        <div class="input-group"><span id="addon2" class="input-group-addon">新密码</span>
                            <input class="form-control" id="reNewPassword" name="reNewPassword"
                                   type="password" placeholder="再输一次新密码" required style="width: 272px" required>
                        </div>
                        <br/>
                        <button class="btn btn-lg btn-primary btn-block" type="button" id="btnSubmit"
                                style="width: 345px"> 修改
                        </button>
            </form>
        </div>

    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>

    //同步请求
    // $(document).ready(function () {
    //     $("#btnSubmit").click(function () {
    //         var p1 = $("#newPassword").val();
    //         var p2 = $("#reNewPassword").val();
    //         if (p1 !== p2) {
    //             alert("二次输入密码不同。");
    //             return false;
    //         }
    //          $("#btnSubmit").submit();
    //     });
    //
    // });

    //异步请求
    $(document).ready(function () {
        $("#btnSubmit").click(function () {
            var p1 = $("#newPassword").val();
            var p2 = $("#reNewPassword").val();
            if (p1 !== p2) {
                alert("二次输入密码不同。");
                return false;
            }
            $.post("findBackPwdAsyn_validAndchangePwd",$("#chPwdForm").serializeArray(),
                function(data,status){
                    // alert("数据: \n" + data + "\n状态: " + status);
                    if (data["succ"]=="succ") {
                        alert(data["backMsg"]);
                        window.location.assign("user_toLogin.action");
                    } else {
                        alert(data["backMsg"]);
                    }
                },"json");
        });

    });



</script>
</body>
<!-- InstanceEnd -->
</html>