<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="com.hgd.laboratory.dto.TempItem" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.TempOpenPlanService" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="m" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt"%>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    .tempPlan_box{
            display:-webkit-flex;
            display:flex;

            flex-direction:row;
            flex-wrap: nowrap;
            justify-content:flex-start;
            align-items:center;

        }
        .tempPlan_close {
            color:red;
            text-align:justify;
            text-shadow:1px 1px #FF0000;
            font-family:黑体;
        }
        .tempPlan_open {
            color:blue;
            text-align:justify;
            text-shadow:1px 1px #FF0000;
            font-family:黑体;
        }
    </style>
</head>

<%!
    String tempOpenPlanStatusToString(int status) {
        if (status== TempOpenPlanService.SCARTCH)
            return "草稿";
        else
            return "已发布";
    }
%>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">开放计划</a></div>
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="defaultNavbar1">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="active"><a href="#">安排临时计划<span class="sr-only">(current)</span></a></li>
                        </ul>

                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <%
                ValueStack vs = (ValueStack)request.getAttribute("struts.valueStack");
                int status_value = 0;
                try {
                    status_value = (int)vs.findValue("status");
                } catch (Exception e) {
                    ;
                }
            %>
            <h3>实验室<strong><s:property value="tempOpenPlan.labId"/>临时开放计划安排{第<s:property value="tempOpenPlan.weekOfTerm"/>周}</strong></h3>
            <strong><%= "--"+tempOpenPlanStatusToString(status_value)%></strong>

            <form role="form" action="tempOpenPlan_save.action" method="post" id="editForm">
                <table class="table table-bordered">

                    <input name="tempOpenPlan.tempPlanId" value="<s:property value='tempOpenPlan.tempPlanId'/>" hidden/>
                    <input name="tempOpenPlan.labId"  value="<s:property value='tempOpenPlan.labId'/>" hidden/>
                    <input name="tempOpenPlan.desp"  value="<s:property value='tempOpenPlan.desp'/>" hidden/>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>星期一</th>
                        <th>星期二</th>
                        <th>星期三</th>
                        <th>星期四</th>
                        <th>星期五</th>
                        <th>星期六</th>
                        <th>星期日</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%
                        Map<String, TempItem> tempOpenPlanDetailMap = (Map<String, TempItem>) request.getAttribute("tempOpenPlanDetailMap");
                        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
                            out.println("<tr>");
                            out.println("<th>第" + i + "大节</th>");
                            for (int j = 1; j <= 7; j++) {
                                String key = "t" + i + "w" + j;
                                String orgin = tempOpenPlanDetailMap.get(key).getOrign();
                                String temp = tempOpenPlanDetailMap.get(key).getTemp();
                                out.print("<td>");
                    %>
                                <m:enabledTempPlan orginId="<%=key+\"_orgin\"%>"
                                                   orginValue="<%= orgin%>"
                                                   tempId="<%= key+\"_temp\"%>"
                                                   tempValue="<%= temp%>">
                                </m:enabledTempPlan>
                    <%

                                out.println("</td>");
                            }
                            out.println("</tr>");
                        }
                    %>


                    </tbody>
                </table>
                <div class="btn-group btn-group-justified" role="group" aria-label="btn-group"  >
                    <div class="btn-group">
                        <button type="submit" class="btn btn-info " onclick="save();">暂存</button>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary " onclick="publish();">确认并发布</button>
                    </div>
                </div>


                <p class="sj-div"><b>注：</b></p>
                <p class="sj-div">浅色显示非选中状态，深色显示选中状态</p>
                <p></p>
            </form>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function myToggle(orgin,temp) {
        if (temp.trim()=="1")
            return "0";
        if(temp.trim()=="0")
            return "1";
        if (temp.trim()==""  && orgin.trim()=="1")
            return "0";
        if (temp.trim()==""  && orgin.trim()=="0")
            return "1"
        return "";
    }

    function tempDisplay(orgin,new_temp) {
        if (new_temp.trim()=="")
            return "";
        if (orgin.trim()==new_temp.trim())
            return "";
        if (new_temp.trim()=="0")
            return "关闭";
        else if (new_temp.trim()=="1")
            return "开放";
        else
            return "错误";

    }
    $(function() {
        $(".tempPlan_box>button").click(
            function (){
                let new_temp;
                let orgin,temp;
                let input_orgin,input_temp,span_temp;

                let fa = $(this).parent();

                input_orgin = fa.children("input").eq(0);
                input_temp = fa.children("input").eq(1);
                span_temp = fa.children("span").eq(1);

                // 取得 orgin,temp 两个字段的值
                orgin =input_orgin.val();
                temp = input_temp.val();

                //计算 new_temp 值
                new_temp = myToggle(orgin,temp);
                //显示 new_temp 提示:tempDisplay()
                let d =tempDisplay(orgin,new_temp)
                if (d=="开放") {
                    span_temp.text(d).removeClass("tempPlan_close").addClass("tempPlan_open");
                } else {
                    span_temp.text(d).removeClass("tempPlan_open").addClass("tempPlan_close");
                }


                //更新 输入字段 temp 的值
                input_temp.val(myToggle(orgin,temp));
            }
        );
    });


</script>

<script>
    function save() {
        //取表单，设置action属性,提交
        var form = document.getElementById('editForm');
        form.action = "tempOpenPlan_save.action";
        form.submit;
    }
    function publish() {
        //取表单，设置action属性,提交
        var form = document.getElementById('editForm');
        form.action = "tempOpenPlan_publish.action";
        form.submit;
    }
</script>
</body>
<!-- InstanceEnd -->
</html>