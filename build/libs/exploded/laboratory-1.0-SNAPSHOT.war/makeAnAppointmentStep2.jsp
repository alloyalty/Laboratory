<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
    <script>
        function gotoStep1() {
            //document.getElementById("anAppointment").action = "makeAnAppointmentStep1.jsp";
            //document.getElementById("anAppointment").submit();
            history.back();
        }

    </script>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container" style="width: 800px">
            <form id="anAppointment" class="form-signin" action="makeAnAppointmentStep3.jsp">
                <h3 class="form-signin-heading">填写相关信息</h3>
                <br/>

                <div class="input-group">
                    <span class="input-group-addon"> <span>实验项目</span></span> <label
                        for="expItem" class="sr-only">实验项目</label> <input name="expItem"
                                                                          type="text" id="expItem" class="form-control"
                                                                          required autofocus>
                </div>
                <label class="sr-only" for="courseName">课程名称</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>课程名称</span>
                    </div>
                    <input class="form-control" id="courseName" name="courseName"
                           type="text" required>
                </div>


                <div class="input-group">
                    <span class="input-group-addon"> <span>软件</span></span> <label
                        for="sotewares" class="sr-only">软件</label> <input
                        name="sotewares" type="text" id="sotewares" class="form-control"
                        required>
                </div>
                <label class="sr-only" for="hardwares">硬件</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>硬件</span>
                    </div>
                    <input class="form-control" id="hardwares" name="hardwares"
                           type="text" required>
                </div>


                <br/>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <button class="btn btn-lg btn-primary " style="margin:0 auto" type="button"
                                onclick="gotoStep1();">上一步
                        </button>
                    </div>
                    <div class="col-lg-3">
                        <button class="btn btn-lg btn-primary " style="margin:0 auto" type="submit">提交申请</button>
                    </div>
                    <div class="col-lg-3">
                        <button class="btn btn-lg btn-primary " style="margin:0 auto" type="button"
                                onclick="window.location.href='blank.jsp';">取消
                        </button>
                    </div>
                    <div class="col-lg-1"></div>
                </div>

            </form>
        </div>

        <!-- InstanceEndEditable -->
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>

</body>
<!-- InstanceEnd -->
</html>