<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="com.hgd.laboratory.po.OpenLabSchedule" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>

</head>
<%!
    void outOptions(PrintWriter out, String term) {
        List<String> openLabList = new TestService().getOpenLabList(term);
        if (openLabList != null) {
            for (int i = 0; i < openLabList.size(); i++) {
                out.print("<option>");
                out.print(openLabList.get(i));
                out.print("</option>");
            }
        }
    }
%>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>
                    <form class="navbar-form" role="form" method="post">

                        <div class="input-group">
                            <div class="input-group-addon"><span>开放实验室</span></div>
                            <input type="text" hidden name="srch_term" value="${sessionScope.currentTerm}">
                            <select class="form-control" id="openLab" name="srch_openLabId" required>
                                <!--             <option>#6312</option>
                                            <option>#6320</option>
                                            <option>#6345</option>
                                            <option>#6311</option>
                                            <option>#6334</option> -->
                                <%
                                    String term = (String) session.getAttribute("currentTerm");
                                    List<String> openLabList = new TestService().getOpenLabList(term);
                                    OpenLabSchedule openLabSchedule = (OpenLabSchedule) request.getAttribute("openLabSchedule");
                                    if (openLabList != null) {
                                        for (int i = 0; i < openLabList.size(); i++) {
                                            String selected = "";
                                            if ((openLabSchedule != null) && (openLabList.get(i).equals(openLabSchedule.getLabId())))
                                                selected = "selected";
                                            out.print(String.format("<option  %s>", selected));
                                            out.print(openLabList.get(i));
                                            out.print("</option>");
                                        }
                                    }
                                %>
                            </select>
                        </div>
                        <button type="submit" class="btn btn-default">查看</button>
                    </form>
                </div>
                <!-- /.container-fluid -->
            </nav>
            <%
                Map<String, String> map = (Map<String, String>) request.getAttribute("openLabScheduleDetailMap");
                if (map == null || map.size() == 0) {
                    out.print("<h3>所选实验室<strong>开放计划不存在。</strong></h3> ");
                } else {
            %>
            <h3>实验室<s:property value="openLabSchedule.labId"/>开放计划</h3>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>星期一</th>
                    <th>星期二</th>
                    <th>星期三</th>
                    <th>星期四</th>
                    <th>星期五</th>
                    <th>星期六</th>
                    <th>星期日</th>
                </tr>
                </thead>
                <tbody>

                <%

                    for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
                        out.println("<tr>");
                        out.println("<th>第" + i + "大节</th>");
                        for (int j = 1; j <= 7; j++) {
                            String key = "t" + i + "w" + j;
                            String value = map.get(key);
                            String strtd = String.format("<td id='%s'>%s</td>", key, value);
                            out.println(strtd);

                        }
                        out.println("</tr>");

                    }
                %>
                </tbody>
            </table>
            <%
                }
            %>
        </div>
        <s:debug/>
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>