CREATE DATABASE  IF NOT EXISTS `reservation` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `reservation`;
-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: reservation
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `administrators`
--

DROP TABLE IF EXISTS `administrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `administrators` (
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`t_id`,`role`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `administrators`
--

LOCK TABLES `administrators` WRITE;
/*!40000 ALTER TABLE `administrators` DISABLE KEYS */;
/*!40000 ALTER TABLE `administrators` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `apply_for_lab`
--

DROP TABLE IF EXISTS `apply_for_lab`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `apply_for_lab` (
  `apply_number` bigint(30) NOT NULL AUTO_INCREMENT COMMENT '申请号/自增',
  `apply_time` datetime NOT NULL COMMENT '申请时间',
  `user_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人ID',
  `user_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人姓名',
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `faci_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '设备编号',
  `course_name` varchar(255) COLLATE utf8_bin DEFAULT NULL COMMENT '课程名称',
  `exp_name` char(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验名称',
  `hardware` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `software` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '使用软件',
  `timeslot_start` int(4) NOT NULL COMMENT '时间编号，申请时间的区间--起始点(含)',
  `timeslot_finish` int(4) NOT NULL COMMENT '时间编号，申请时间的区间--终止点(含)',
  `state` int(11) NOT NULL COMMENT '申请状态=ENUM(''待审核'', ''未通过'', ''通过未使用'', ''通过已使用'')',
  `auditor` char(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '审核人',
  `audit_time` datetime DEFAULT NULL COMMENT '审核日期',
  `term` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `applcatio_code` char(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '申请号',
  PRIMARY KEY (`apply_number`) USING BTREE,
  KEY `fk_userid` (`user_id`),
  KEY `fk_labID` (`lab_id`),
  KEY `applcatio_code` (`applcatio_code`),
  CONSTRAINT `fk_labID` FOREIGN KEY (`lab_id`) REFERENCES `laboratory` (`lab_id`),
  CONSTRAINT `fk_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1347 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apply_for_lab`
--

LOCK TABLES `apply_for_lab` WRITE;
/*!40000 ALTER TABLE `apply_for_lab` DISABLE KEYS */;
INSERT INTO `apply_for_lab` VALUES (4,'2018-08-30 00:00:00','student','TEST2','6312','','B','A','D','C',1,1,0,NULL,NULL,'2018-2019-1','20180830#6312#1001'),(5,'2018-08-30 00:00:00','student','TEST2','6312','','B','A','D','C',1,1,0,NULL,NULL,'2018-2019-1','20180830#6312#1002'),(6,'2018-08-30 00:00:00','student','TEST2','6312','','B','A','D','C',1,1,0,NULL,NULL,'2018-2019-1','20180830#6312#1003'),(7,'2019-08-31 00:00:00','student','TEST2','6312','','B','A','D','C',2,2,0,NULL,NULL,'2018-2019-1','20190831#6312#2001'),(8,'2019-09-02 00:00:00','student','TEST2','6312','','B','A','D','Dev-C,C++###Visual Studio Community 2017',1,1,0,NULL,NULL,'2018-2019-1','20190902#6312#1001'),(9,'2019-09-02 00:00:00','student','TEST2','6312','','DSDS','ASA','D','Visual C++###mysq###sql server ',3,3,0,NULL,NULL,'2018-2019-1','20190902#6312#3001'),(10,'2019-09-02 00:00:00','student','TEST2','6312','','sds','dfd','D','Visual Studio Community 2017###eclipse for web',1,1,0,NULL,NULL,'2018-2019-1','20190902#6312#1002'),(11,'2019-09-02 00:00:00','student','TEST2','6312','','ere','ere','asa','Dev-C,C++###eclipse for web',1,1,0,NULL,NULL,'2018-2019-1','20190902#6312#1003'),(804,'2019-09-25 00:00:00','student','TEST2','6312','','DDD','AAA','CCC','Dev-C,C++',1,1,0,NULL,NULL,'2018-2019-1','20190925#6312#1001'),(805,'2019-09-28 00:00:00','student','TEST2','6312','','B1','A1','C1','Dev-C,C++',2,2,0,NULL,NULL,'2018-2019-1','20190928#6312#2001'),(806,'2019-09-28 00:00:00','student','TEST2','6312','','B2','A2','X2','Dev-C,C++',5,7,4,NULL,NULL,'2018-2019-1','20190928#6312#7001'),(807,'2019-09-29 00:00:00','student','TEST2','6312','','B3','A3','C3','Visual Studio Community 2017',1,3,4,NULL,NULL,'2018-2019-1','20190929#6312#3001'),(808,'2019-09-29 00:00:00','student','TEST2','6312','','B1','A1','C1','Dev-C,C++',1,1,0,NULL,NULL,'2018-2019-1','20190929#6312#1002'),(809,'2019-09-29 00:00:00','student','TEST2','6312','','B2','A2','C2','Visual Studio Community 2017',2,2,0,NULL,NULL,'2018-2019-1','20190929#6312#2002'),(810,'2019-09-29 00:00:00','student','TEST2','6312','','B3','A3','C3','interllij idea 2019',3,4,0,NULL,NULL,'2018-2019-1','20190929#6312#4001'),(1115,'2019-10-04 00:00:00','student','TEST2','6312','','B1','A1','C1','Dev-C,C++',1,1,0,NULL,NULL,'2018-2019-1','20191004#6312#1001'),(1116,'2019-10-04 00:00:00','student','TEST2','6312','','B2','A2','C2','Visual Studio Community 2017',2,2,0,NULL,NULL,'2018-2019-1','20191004#6312#2001'),(1117,'2019-10-04 00:00:00','student','TEST2','6312','','B3','A3','C3','Visual C++###C-Free 5',3,3,0,NULL,NULL,'2018-2019-1','20191004#6312#3001'),(1118,'2019-10-04 00:00:00','student','TEST2','6312','','B4','A4','C4','Dev-C,C++###Visual Studio Community 2017###Visual C++###C-Free 5###eclise for  Java###eclipse for web###interllij idea 2019###mysq###sql server ###matlab',4,4,0,NULL,NULL,'2018-2019-1','20191004#6312#4001'),(1119,'2019-10-05 00:00:00','student','TEST2','6312','','b1','a1','c1','eclipse for web',1,1,0,NULL,NULL,'2018-2019-1','20191005#6312#1001'),(1120,'2019-10-05 00:00:00','student','TEST2','6312','','c3','c2','d3','interllij idea 2019',2,2,0,NULL,NULL,'2018-2019-1','20191005#6312#2001'),(1121,'2019-10-05 00:00:00','student','TEST2','6312','','d3','d2','c3','Visual Studio Community 2017###interllij idea 2019',3,4,4,NULL,NULL,'2018-2019-1','20191005#6312#4001'),(1122,'2019-10-05 00:00:00','student','TEST2','6312','','b4','a4','d4','mysq',5,5,0,NULL,NULL,'2018-2019-1','20191005#6312#5001'),(1123,'2019-10-06 00:00:00','student','TEST2','6312','','b1','a1','c1','Dev-C,C++',7,7,1,NULL,NULL,'2018-2019-1','20191006#6312#7001'),(1124,'2019-10-06 00:00:00','student','TEST2','6312','','d3','d3','dd','interllij idea 2019',5,6,2,NULL,NULL,'2018-2019-1','20191006#6312#6001'),(1125,'2019-10-06 00:00:00','student','TEST2','6312','','bb','aa','de','Dev-C,C++###eclipse for web',2,2,1,NULL,NULL,'2018-2019-1','20191006#6312#2001'),(1126,'2019-10-06 00:00:00','student','TEST2','6312','','AA','A','asa','',1,1,2,NULL,NULL,'2018-2019-1','20191006#6312#1001');
/*!40000 ALTER TABLE `apply_for_lab` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dutytable`
--

DROP TABLE IF EXISTS `dutytable`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `dutytable` (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学生姓名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `t_id` int(4) NOT NULL COMMENT '时间编号',
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dutytable`
--

LOCK TABLES `dutytable` WRITE;
/*!40000 ALTER TABLE `dutytable` DISABLE KEYS */;
/*!40000 ALTER TABLE `dutytable` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facility`
--

DROP TABLE IF EXISTS `facility`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `facility` (
  `f_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备编号',
  `f_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备名称',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '设备状态:\n''可预约'', ''已预约'', ''故障''\n{0,1,2}',
  `softName` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`f_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facility`
--

LOCK TABLES `facility` WRITE;
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;
INSERT INTO `facility` VALUES ('FACILITY001','DEC 计算机',2,'c++','6320'),('FACILITY002','DEC计算机',2,'C','6320'),('FACILITY003','DEC计算机',0,'java','6320'),('FACITEST001','DEC计算机',0,'c++','6312'),('FACITEST002','DEC计算机',1,'c++','6312'),('FACITEST003','DEC计算机',0,'c++','6313'),('FACITEST004','DEC计算机',0,'c++','6312'),('FACITEST005','DEC计算机',0,'c++','6312'),('FACITEST006','DEC计算机',0,'c++','6312'),('FACITEST007','DEC计算机',0,'c++','6312'),('FACITEST008','DEC计算机',0,'c++','6312'),('FACITEST009','DEC计算机',0,'c++','6312'),('FACITEST010','DEC计算机',0,'c++','6312'),('FACITEST011','DEC计算机',0,'c++','6312'),('FACITEST012','DEC计算机',0,'c++','6312'),('FACITEST013','DEC计算机',0,'c++','6312'),('FACITEST014','DEC计算机',0,'c++','6312'),('FACITEST015','DEC计算机',0,'c++','6312'),('FACITEST016','DEC计算机',0,'c++','6312'),('FACITEST017','DEC计算机',0,'c++','6312'),('FACITEST018','DEC计算机',0,'c++','6312'),('FACITEST019','DEC计算机',0,'c++','6312'),('FACITEST020','DEC计算机',0,'c++','6312'),('FACITEST021','DEC计算机',0,'c++','6312'),('FACITEST022','DEC计算机',0,'c++','6312'),('FACITEST023','DEC计算机',0,'c++','6312'),('FACITEST024','DEC计算机',0,'c++','6312'),('FACITEST025','DEC计算机',0,'c++','6312'),('FACITEST026','DEC计算机',0,'c++','6312'),('FACITEST027','DEC计算机',0,'c++','6312'),('FACITEST028','DEC计算机',0,'c++','6312'),('FACITEST029','DEC计算机',0,'c++','6312'),('FACITEST030','DEC计算机',0,'c++','6312'),('FACITEST031','DEC计算机',0,'c++','6312'),('FACITEST032','DEC计算机',0,'c++','6312'),('FACITEST033','DEC计算机',0,'c++','6312'),('FACITEST034','DEC计算机',0,'c++','6312'),('FACITEST035','DEC计算机',0,'c++','6312'),('FACITEST036','DEC计算机',0,'c++','6312'),('FACITEST037','DEC计算机',0,'c++','6312'),('FACITEST038','DEC计算机',0,'c++','6312'),('FACITEST039','DEC计算机',0,'c++','6312'),('FACITEST040','DEC计算机',0,'c++','6312');
/*!40000 ALTER TABLE `facility` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fault_repair`
--

DROP TABLE IF EXISTS `fault_repair`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `fault_repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Lid` char(10) NOT NULL,
  `Fid` char(20) NOT NULL,
  `FaultDFesp` varchar(300) DEFAULT NULL,
  `FillingDate` datetime NOT NULL,
  `FillingId` int(11) DEFAULT NULL,
  `Fstate` int(11) DEFAULT '0' COMMENT 'ENUM(''可预约'', ''已预约'', ''故障'')\n{0,1,2}\n',
  `MaintenceRecs` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=311 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fault_repair`
--

LOCK TABLES `fault_repair` WRITE;
/*!40000 ALTER TABLE `fault_repair` DISABLE KEYS */;
INSERT INTO `fault_repair` VALUES (2,'6321','FACILITY001','FAULT','2019-07-22 11:54:10',1000,2,'MAINTENCERECTEST'),(3,'6321','FACILITY001','FAULT TEST','2019-07-22 12:11:21',1000,2,NULL),(4,'6321','FACILITY002','FAULT002','2019-07-22 15:48:01',1000,2,NULL),(5,'6321','FACILITY003','FAULT 004','2019-07-22 17:45:32',1000,2,NULL),(54,'6321','FACILITY001','FAULT TEST 0004','2019-08-04 09:47:04',1000,2,NULL),(103,'6321','F123123112',' ','2019-08-10 16:51:56',1000,2,NULL),(172,'TESTLIB01','FYYY001','TESTDESP','2019-08-15 12:20:29',1000,2,NULL),(173,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 12:22:41',1000,2,NULL),(185,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 12:41:30',1000,2,NULL),(186,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 17:32:11',1000,2,NULL),(198,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 17:39:18',1000,2,NULL),(199,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 17:47:02',1000,2,NULL),(200,'TESTLIB01','FACILITY002','TESTDESP','2019-08-15 17:58:56',1000,2,NULL);
/*!40000 ALTER TABLE `fault_repair` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_open_plan`
--

DROP TABLE IF EXISTS `lab_open_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lab_open_plan` (
  `open_plan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) NOT NULL COMMENT '实验室标识',
  `status` int(11) DEFAULT '0' COMMENT '计划状态{0---草稿，1发布}',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '实验室开放计划' COMMENT '描述',
  PRIMARY KEY (`open_plan_id`),
  UNIQUE KEY `termLabID` (`term`,`labID`) USING BTREE,
  KEY `labID` (`labID`) /*!80000 INVISIBLE */
) ENGINE=InnoDB AUTO_INCREMENT=252 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_open_plan`
--

LOCK TABLES `lab_open_plan` WRITE;
/*!40000 ALTER TABLE `lab_open_plan` DISABLE KEYS */;
INSERT INTO `lab_open_plan` VALUES (101,'2018-2019-1','6311',1,'实验室开放计划'),(102,'2018-2019-1','6312',1,'实验室开放计划'),(182,'2018-2019-1','6320',1,'实验室开放计划'),(185,'2018-2019-1','6345',0,'实验室开放计划'),(240,'2018-2019-1','test002',0,'test bu wcq');
/*!40000 ALTER TABLE `lab_open_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lab_open_plan_detail`
--

DROP TABLE IF EXISTS `lab_open_plan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `lab_open_plan_detail` (
  `detailID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `open_plan_id` int(11) NOT NULL COMMENT '外键',
  `timeSlot` int(11) DEFAULT NULL COMMENT '一天内的时段',
  `dayOfWeek` int(11) DEFAULT NULL COMMENT '周几',
  `content` varchar(255) DEFAULT NULL COMMENT '开放计划的内容：周、或周的列表表示',
  PRIMARY KEY (`detailID`),
  UNIQUE KEY `open_plan_id` (`open_plan_id`,`timeSlot`,`dayOfWeek`) USING BTREE,
  CONSTRAINT `lab_open_plan_detail_ibfk_1` FOREIGN KEY (`open_plan_id`) REFERENCES `lab_open_plan` (`open_plan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5758 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lab_open_plan_detail`
--

LOCK TABLES `lab_open_plan_detail` WRITE;
/*!40000 ALTER TABLE `lab_open_plan_detail` DISABLE KEYS */;
INSERT INTO `lab_open_plan_detail` VALUES (2230,101,5,7,''),(2231,101,7,5,''),(2232,101,5,6,'1-12'),(2233,101,7,4,''),(2234,101,7,7,''),(2235,101,7,6,'1-12'),(2236,101,1,1,'1'),(2237,101,1,7,''),(2238,101,3,5,''),(2239,101,5,3,''),(2240,101,7,1,''),(2241,101,1,6,'1-12'),(2242,101,3,4,''),(2243,101,5,2,''),(2244,101,3,7,''),(2245,101,5,5,''),(2246,101,7,3,''),(2247,101,3,6,'1-12'),(2248,101,5,4,''),(2249,101,7,2,''),(2250,101,1,3,'3'),(2251,101,3,1,''),(2252,101,1,2,'2'),(2253,101,1,5,''),(2254,101,3,3,''),(2255,101,5,1,''),(2256,101,1,4,'4'),(2257,101,3,2,''),(2258,101,6,6,'1-12'),(2259,101,4,7,''),(2260,101,6,5,''),(2261,101,6,7,''),(2262,101,2,6,'1-12'),(2263,101,4,4,''),(2264,101,6,2,''),(2265,101,2,5,''),(2266,101,4,3,''),(2267,101,6,1,''),(2268,101,4,6,'1-12'),(2269,101,6,4,''),(2270,101,2,7,''),(2271,101,4,5,''),(2272,101,6,3,''),(2273,101,2,2,''),(2274,101,2,1,''),(2275,101,2,4,''),(2276,101,4,2,''),(2277,101,2,3,''),(2278,101,4,1,''),(2279,102,5,7,'1-10'),(2280,102,7,5,''),(2281,102,5,6,'1-10'),(2282,102,7,4,'1-10'),(2283,102,7,7,'1-10'),(2284,102,7,6,'1-10'),(2285,102,1,1,'1-10'),(2286,102,1,7,'1-10'),(2287,102,3,5,'1-10'),(2288,102,5,3,''),(2289,102,7,1,'1-10'),(2290,102,1,6,'1-10'),(2291,102,3,4,''),(2292,102,5,2,'1-10'),(2293,102,3,7,'1-10'),(2294,102,5,5,''),(2295,102,7,3,''),(2296,102,3,6,'1-10'),(2297,102,5,4,'1-10'),(2298,102,7,2,'1-10'),(2299,102,1,3,'1-10'),(2300,102,3,1,'1-10'),(2301,102,1,2,'1-10'),(2302,102,1,5,'1-10'),(2303,102,3,3,'1-10'),(2304,102,5,1,'1-10'),(2305,102,1,4,''),(2306,102,3,2,'1-10'),(2307,102,6,6,'1-10'),(2308,102,4,7,'1-10'),(2309,102,6,5,''),(2310,102,6,7,'1-10'),(2311,102,2,6,'1-10'),(2312,102,4,4,''),(2313,102,6,2,'1-10'),(2314,102,2,5,'1-10'),(2315,102,4,3,'1-10'),(2316,102,6,1,'1-10'),(2317,102,4,6,'1-10'),(2318,102,6,4,'1-10'),(2319,102,2,7,'1-10'),(2320,102,4,5,'1-10'),(2321,102,6,3,''),(2322,102,2,2,'1-10'),(2323,102,2,1,'1-10'),(2324,102,2,4,''),(2325,102,4,2,'1-10'),(2326,102,2,3,'1-10'),(2327,102,4,1,'1-10'),(3896,182,5,7,'1-15'),(3897,182,7,5,'1-15'),(3898,182,5,6,'1-15'),(3899,182,7,4,'1-15'),(3900,182,7,7,'1-15'),(3901,182,7,6,'1-15'),(3902,182,1,1,'1-15'),(3903,182,1,7,'1-15'),(3904,182,3,5,''),(3905,182,5,3,''),(3906,182,7,1,''),(3907,182,1,6,'1-15'),(3908,182,3,4,'1-15'),(3909,182,5,2,'1-15'),(3910,182,3,7,'1-15'),(3911,182,5,5,'1-15'),(3912,182,7,3,'1-15'),(3913,182,3,6,'1-15'),(3914,182,5,4,''),(3915,182,7,2,''),(3916,182,1,3,''),(3917,182,3,1,'1-15'),(3918,182,1,2,'1-15'),(3919,182,1,5,''),(3920,182,3,3,'1-15'),(3921,182,5,1,'1-15'),(3922,182,1,4,''),(3923,182,3,2,'1-15'),(3924,182,6,6,'1-15'),(3925,182,4,7,'1-15'),(3926,182,6,5,'1-15'),(3927,182,6,7,'1-15'),(3928,182,2,6,'1-15'),(3929,182,4,4,'1-15'),(3930,182,6,2,''),(3931,182,2,5,''),(3932,182,4,3,'1-15'),(3933,182,6,1,''),(3934,182,4,6,'1-15'),(3935,182,6,4,'1-15'),(3936,182,2,7,'1-15'),(3937,182,4,5,''),(3938,182,6,3,'1-15'),(3939,182,2,2,'1-15'),(3940,182,2,1,'1-15'),(3941,182,2,4,'1-15'),(3942,182,4,2,'1-15'),(3943,182,2,3,'1-15'),(3944,182,4,1,'1-15'),(4043,185,5,7,''),(4044,185,7,5,''),(4045,185,5,6,''),(4046,185,7,4,''),(4047,185,7,7,''),(4048,185,7,6,''),(4049,185,1,1,''),(4050,185,1,7,''),(4051,185,3,5,''),(4052,185,5,3,''),(4053,185,7,1,''),(4054,185,1,6,''),(4055,185,3,4,''),(4056,185,5,2,''),(4057,185,3,7,''),(4058,185,5,5,''),(4059,185,7,3,''),(4060,185,3,6,''),(4061,185,5,4,''),(4062,185,7,2,''),(4063,185,1,3,'3'),(4064,185,3,1,''),(4065,185,1,2,'1'),(4066,185,1,5,'5'),(4067,185,3,3,''),(4068,185,5,1,''),(4069,185,1,4,'4'),(4070,185,3,2,''),(4071,185,6,6,''),(4072,185,4,7,''),(4073,185,6,5,''),(4074,185,6,7,''),(4075,185,2,6,''),(4076,185,4,4,''),(4077,185,6,2,''),(4078,185,2,5,''),(4079,185,4,3,''),(4080,185,6,1,''),(4081,185,4,6,''),(4082,185,6,4,''),(4083,185,2,7,''),(4084,185,4,5,''),(4085,185,6,3,''),(4086,185,2,2,''),(4087,185,2,1,''),(4088,185,2,4,''),(4089,185,4,2,''),(4090,185,2,3,''),(4091,185,4,1,'');
/*!40000 ALTER TABLE `lab_open_plan_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laboratory`
--

DROP TABLE IF EXISTS `laboratory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `laboratory` (
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `lab_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室名称',
  `adress` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室地址',
  `state` int(4) NOT NULL DEFAULT '0' COMMENT '实验室状态=ENUM( ''关'',''开'', ''已预约'')',
  `lab_for` int(4) NOT NULL DEFAULT '0' COMMENT '实验室开放对象==SET(''学生'', ''教师'')',
  `installSoft` int(11) NOT NULL COMMENT '实验室所安装的软件集合名称',
  `hardware` varchar(300) COLLATE utf8_bin DEFAULT NULL COMMENT '实验室硬件类型',
  PRIMARY KEY (`lab_id`) USING BTREE,
  KEY `fk_softwareconfig` (`installSoft`),
  CONSTRAINT `fk_softwareconfig` FOREIGN KEY (`installSoft`) REFERENCES `software_config` (`configid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laboratory`
--

LOCK TABLES `laboratory` WRITE;
/*!40000 ALTER TABLE `laboratory` DISABLE KEYS */;
INSERT INTO `laboratory` VALUES ('6311','莲6311','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色'),('6312','莲6312','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色'),('6318','莲6318','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色'),('6320','莲6320','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色'),('6334','莲6334','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色'),('6345','莲6345','莲花街',0,0,1,'九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
/*!40000 ALTER TABLE `laboratory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notice`
--

DROP TABLE IF EXISTS `notice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `notice` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '通知编号/自增',
  `category` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知类别',
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知内容',
  `time` datetime(6) NOT NULL COMMENT '发布时间',
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '发布人员工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notice`
--

LOCK TABLES `notice` WRITE;
/*!40000 ALTER TABLE `notice` DISABLE KEYS */;
/*!40000 ALTER TABLE `notice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `open_lab_schedule`
--

DROP TABLE IF EXISTS `open_lab_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `open_lab_schedule` (
  `open_sche_id` int(11) NOT NULL COMMENT '自增主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) NOT NULL COMMENT '实验室标识',
  `status` int(11) DEFAULT '1' COMMENT '计划状态{0---草稿，1发布}',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '实验室开放计划' COMMENT '描述',
  PRIMARY KEY (`open_sche_id`),
  UNIQUE KEY `termLabID` (`term`,`labID`) USING BTREE,
  KEY `labID` (`labID`) /*!80000 INVISIBLE */
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `open_lab_schedule`
--

LOCK TABLES `open_lab_schedule` WRITE;
/*!40000 ALTER TABLE `open_lab_schedule` DISABLE KEYS */;
INSERT INTO `open_lab_schedule` VALUES (101,'2018-2019-1','6311',1,'实验室 开放计划'),(102,'2018-2019-1','6312',1,'实验室 开放计划'),(182,'2018-2019-1','6320',0,'实验室 开放计划');
/*!40000 ALTER TABLE `open_lab_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `open_lab_schedule_detail`
--

DROP TABLE IF EXISTS `open_lab_schedule_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `open_lab_schedule_detail` (
  `detailID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `open_sche_id` int(11) NOT NULL COMMENT '外键---从open_lab_plan中复制',
  `timeSlot` int(11) DEFAULT NULL COMMENT '一天内的时段',
  `dayOfWeek` int(11) DEFAULT NULL COMMENT '周几',
  `content` varchar(255) DEFAULT NULL COMMENT '开放计划的内容：周、或周的列表表示',
  PRIMARY KEY (`detailID`),
  UNIQUE KEY `open_sche_id` (`open_sche_id`,`timeSlot`,`dayOfWeek`) USING BTREE,
  CONSTRAINT `open_lab_schedule_detail_ibfk_1` FOREIGN KEY (`open_sche_id`) REFERENCES `open_lab_schedule` (`open_sche_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2349 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `open_lab_schedule_detail`
--

LOCK TABLES `open_lab_schedule_detail` WRITE;
/*!40000 ALTER TABLE `open_lab_schedule_detail` DISABLE KEYS */;
INSERT INTO `open_lab_schedule_detail` VALUES (1173,101,1,1,'1'),(1174,101,1,2,'2'),(1175,101,1,3,'3'),(1176,101,1,4,'4'),(1177,101,1,5,''),(1178,101,1,6,'1-12'),(1179,101,1,7,'7'),(1180,101,2,1,''),(1181,101,2,2,''),(1182,101,2,3,''),(1183,101,2,4,''),(1184,101,2,5,''),(1185,101,2,6,'1-12'),(1186,101,2,7,'7'),(1187,101,3,1,''),(1188,101,3,2,''),(1189,101,3,3,''),(1190,101,3,4,''),(1191,101,3,5,''),(1192,101,3,6,'1-12'),(1193,101,3,7,'7'),(1194,101,4,1,''),(1195,101,4,2,''),(1196,101,4,3,''),(1197,101,4,4,''),(1198,101,4,5,''),(1199,101,4,6,'1-12'),(1200,101,4,7,''),(1201,101,5,1,''),(1202,101,5,2,''),(1203,101,5,3,''),(1204,101,5,4,''),(1205,101,5,5,''),(1206,101,5,6,'1-12'),(1207,101,5,7,'7'),(1208,101,6,1,''),(1209,101,6,2,''),(1210,101,6,3,''),(1211,101,6,4,''),(1212,101,6,5,''),(1213,101,6,6,'1-12'),(1214,101,6,7,'7'),(1215,101,7,1,''),(1216,101,7,2,''),(1217,101,7,3,''),(1218,101,7,4,''),(1219,101,7,5,''),(1220,101,7,6,'1-12'),(1221,101,7,7,'7'),(1222,102,1,1,'1-10'),(1223,102,1,2,'1-10'),(1224,102,1,3,'1-10'),(1225,102,1,4,''),(1226,102,1,5,'1-10'),(1227,102,1,6,'1-10'),(1228,102,1,7,'1-10'),(1229,102,2,1,'1-10'),(1230,102,2,2,'1-10'),(1231,102,2,3,'1-10'),(1232,102,2,4,''),(1233,102,2,5,'1-10'),(1234,102,2,6,'1-10'),(1235,102,2,7,'1-10'),(1236,102,3,1,'1-10'),(1237,102,3,2,'1-10'),(1238,102,3,3,'1-10'),(1239,102,3,4,''),(1240,102,3,5,'1-10'),(1241,102,3,6,'1-10'),(1242,102,3,7,'1-10'),(1243,102,4,1,'1-10'),(1244,102,4,2,'1-10'),(1245,102,4,3,'1-10'),(1246,102,4,4,'3'),(1247,102,4,5,'1-2,4-10'),(1248,102,4,6,'1-10'),(1249,102,4,7,'1-10'),(1250,102,5,1,'1-10'),(1251,102,5,2,'1-10'),(1252,102,5,3,''),(1253,102,5,4,'1-10'),(1254,102,5,5,''),(1255,102,5,6,'1-10'),(1256,102,5,7,'1-10'),(1257,102,6,1,'1-10'),(1258,102,6,2,'1-10'),(1259,102,6,3,''),(1260,102,6,4,'1-10'),(1261,102,6,5,''),(1262,102,6,6,'1-10'),(1263,102,6,7,'1-10'),(1264,102,7,1,'1-10'),(1265,102,7,2,'1-10'),(1266,102,7,3,''),(1267,102,7,4,'1-10'),(1268,102,7,5,''),(1269,102,7,6,'1-10'),(1270,102,7,7,'1-10'),(1271,182,1,1,'1-15'),(1272,182,1,2,'1-15'),(1273,182,1,3,''),(1274,182,1,4,''),(1275,182,1,5,''),(1276,182,1,6,'1-15'),(1277,182,1,7,'1-15'),(1278,182,2,1,'1-15'),(1279,182,2,2,'1-15'),(1280,182,2,3,'1-15'),(1281,182,2,4,'1-15'),(1282,182,2,5,''),(1283,182,2,6,'1-15'),(1284,182,2,7,'1-15'),(1285,182,3,1,'1-15'),(1286,182,3,2,'1-15'),(1287,182,3,3,'1-15'),(1288,182,3,4,'1-15'),(1289,182,3,5,''),(1290,182,3,6,'1-15'),(1291,182,3,7,'1-15'),(1292,182,4,1,'1-15'),(1293,182,4,2,'1-15'),(1294,182,4,3,'1-15'),(1295,182,4,4,'1-15'),(1296,182,4,5,''),(1297,182,4,6,'1-15'),(1298,182,4,7,'1-15'),(1299,182,5,1,'1-15'),(1300,182,5,2,'1-15'),(1301,182,5,3,''),(1302,182,5,4,''),(1303,182,5,5,'1-15'),(1304,182,5,6,'1-15'),(1305,182,5,7,'1-15'),(1306,182,6,1,''),(1307,182,6,2,''),(1308,182,6,3,'1-15'),(1309,182,6,4,'1-15'),(1310,182,6,5,'1-15'),(1311,182,6,6,'1-15'),(1312,182,6,7,'1-15'),(1313,182,7,1,''),(1314,182,7,2,''),(1315,182,7,3,'1-15'),(1316,182,7,4,'1-15'),(1317,182,7,5,'1-15'),(1318,182,7,6,'1-15'),(1319,182,7,7,'1-15');
/*!40000 ALTER TABLE `open_lab_schedule_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `open_res`
--

DROP TABLE IF EXISTS `open_res`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `open_res` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_slot` int(4) NOT NULL COMMENT '时间编号，申请时间的区间',
  `open_date` date NOT NULL COMMENT '日期',
  `day_of_week` int(4) DEFAULT '0' COMMENT '星期几',
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `res_total_num` int(4) DEFAULT NULL COMMENT '资源总数',
  `res_consumed` int(4) DEFAULT NULL COMMENT '已用资源数',
  `res_rest` int(4) DEFAULT NULL COMMENT '资源余数',
  `current_avail_apply_code` char(20) COLLATE utf8_bin DEFAULT NULL COMMENT '当前可用的资源号码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `DWlT001` (`time_slot`,`open_date`,`lab_id`) USING BTREE COMMENT '在（OpenDate、Week、Lid、Tid）建立唯一索引'
) ENGINE=InnoDB AUTO_INCREMENT=629 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `open_res`
--

LOCK TABLES `open_res` WRITE;
/*!40000 ALTER TABLE `open_res` DISABLE KEYS */;
INSERT INTO `open_res` VALUES (475,6,'2019-09-29',7,'6320',1,0,1,'20190929#6320#6001'),(476,7,'2019-09-29',7,'6320',1,0,1,'20190929#6320#7001'),(553,1,'2019-10-04',5,'6312',39,1,38,'20191004#6312#1002'),(554,2,'2019-10-04',5,'6312',39,1,38,'20191004#6312#2002'),(555,3,'2019-10-04',5,'6312',39,1,38,'20191004#6312#3002'),(556,4,'2019-10-04',5,'6312',39,1,38,'20191004#6312#4002'),(557,5,'2019-10-04',5,'6320',1,0,1,'20191004#6320#5001'),(558,6,'2019-10-04',5,'6320',1,0,1,'20191004#6320#6001'),(559,7,'2019-10-04',5,'6320',1,0,1,'20191004#6320#7001'),(560,1,'2019-10-05',6,'6311',0,0,0,'20191005#6311#1001'),(561,2,'2019-10-05',6,'6311',0,0,0,'20191005#6311#2001'),(562,3,'2019-10-05',6,'6311',0,0,0,'20191005#6311#3001'),(563,4,'2019-10-05',6,'6311',0,0,0,'20191005#6311#4001'),(564,5,'2019-10-05',6,'6311',0,0,0,'20191005#6311#5001'),(565,6,'2019-10-05',6,'6311',0,0,0,'20191005#6311#6001'),(566,7,'2019-10-05',6,'6311',0,0,0,'20191005#6311#7001'),(567,1,'2019-10-05',6,'6312',39,1,38,'20191005#6312#1002'),(568,2,'2019-10-05',6,'6312',39,1,38,'20191005#6312#2002'),(569,3,'2019-10-05',6,'6312',39,1,38,'20191005#6312#3002'),(570,4,'2019-10-05',6,'6312',39,1,38,'20191005#6312#4002'),(571,5,'2019-10-05',6,'6312',39,1,38,'20191005#6312#5002'),(572,6,'2019-10-05',6,'6312',39,0,39,'20191005#6312#6001'),(573,7,'2019-10-05',6,'6312',39,0,39,'20191005#6312#7001'),(574,1,'2019-10-05',6,'6320',1,0,1,'20191005#6320#1001'),(575,2,'2019-10-05',6,'6320',1,0,1,'20191005#6320#2001'),(576,3,'2019-10-05',6,'6320',1,0,1,'20191005#6320#3001'),(577,4,'2019-10-05',6,'6320',1,0,1,'20191005#6320#4001'),(578,5,'2019-10-05',6,'6320',1,0,1,'20191005#6320#5001'),(579,6,'2019-10-05',6,'6320',1,0,1,'20191005#6320#6001'),(580,7,'2019-10-05',6,'6320',1,0,1,'20191005#6320#7001'),(581,1,'2019-10-06',7,'6312',39,1,38,'20191006#6312#1002'),(582,2,'2019-10-06',7,'6312',39,1,38,'20191006#6312#2002'),(583,3,'2019-10-06',7,'6312',39,0,39,'20191006#6312#3001'),(584,4,'2019-10-06',7,'6312',39,0,39,'20191006#6312#4001'),(585,5,'2019-10-06',7,'6312',39,1,38,'20191006#6312#5002'),(586,6,'2019-10-06',7,'6312',39,1,38,'20191006#6312#6002'),(587,7,'2019-10-06',7,'6312',39,1,38,'20191006#6312#7002'),(588,1,'2019-10-06',7,'6320',1,0,1,'20191006#6320#1001'),(589,2,'2019-10-06',7,'6320',1,0,1,'20191006#6320#2001'),(590,3,'2019-10-06',7,'6320',1,0,1,'20191006#6320#3001'),(591,4,'2019-10-06',7,'6320',1,0,1,'20191006#6320#4001'),(592,5,'2019-10-06',7,'6320',1,0,1,'20191006#6320#5001'),(593,6,'2019-10-06',7,'6320',1,0,1,'20191006#6320#6001'),(594,7,'2019-10-06',7,'6320',1,0,1,'20191006#6320#7001'),(596,1,'2019-10-07',1,'6312',39,0,39,'20191007#6312#1001'),(597,2,'2019-10-07',1,'6312',39,0,39,'20191007#6312#2001'),(598,3,'2019-10-07',1,'6312',39,0,39,'20191007#6312#3001'),(599,4,'2019-10-07',1,'6312',39,0,39,'20191007#6312#4001'),(600,5,'2019-10-07',1,'6312',39,0,39,'20191007#6312#5001'),(601,6,'2019-10-07',1,'6312',39,0,39,'20191007#6312#6001'),(602,7,'2019-10-07',1,'6312',39,0,39,'20191007#6312#7001'),(603,1,'2019-10-07',1,'6320',1,0,1,'20191007#6320#1001'),(604,2,'2019-10-07',1,'6320',1,0,1,'20191007#6320#2001'),(605,3,'2019-10-07',1,'6320',1,0,1,'20191007#6320#3001'),(606,4,'2019-10-07',1,'6320',1,0,1,'20191007#6320#4001'),(607,5,'2019-10-07',1,'6320',1,0,1,'20191007#6320#5001'),(608,1,'2019-10-08',2,'6312',39,0,39,'20191008#6312#1001'),(609,2,'2019-10-08',2,'6312',39,0,39,'20191008#6312#2001'),(610,3,'2019-10-08',2,'6312',39,0,39,'20191008#6312#3001'),(611,4,'2019-10-08',2,'6312',39,0,39,'20191008#6312#4001'),(612,5,'2019-10-08',2,'6312',39,0,39,'20191008#6312#5001'),(613,6,'2019-10-08',2,'6312',39,0,39,'20191008#6312#6001'),(614,7,'2019-10-08',2,'6312',39,0,39,'20191008#6312#7001'),(615,1,'2019-10-08',2,'6320',1,0,1,'20191008#6320#1001'),(616,2,'2019-10-08',2,'6320',1,0,1,'20191008#6320#2001'),(617,3,'2019-10-08',2,'6320',1,0,1,'20191008#6320#3001'),(618,4,'2019-10-08',2,'6320',1,0,1,'20191008#6320#4001'),(619,5,'2019-10-08',2,'6320',1,0,1,'20191008#6320#5001'),(620,1,'2019-10-09',3,'6312',39,0,39,'20191009#6312#1001'),(621,2,'2019-10-09',3,'6312',39,0,39,'20191009#6312#2001'),(622,3,'2019-10-09',3,'6312',39,0,39,'20191009#6312#3001'),(623,4,'2019-10-09',3,'6312',39,0,39,'20191009#6312#4001'),(624,2,'2019-10-09',3,'6320',1,0,1,'20191009#6320#2001'),(625,3,'2019-10-09',3,'6320',1,0,1,'20191009#6320#3001'),(626,4,'2019-10-09',3,'6320',1,0,1,'20191009#6320#4001'),(627,6,'2019-10-09',3,'6320',1,0,1,'20191009#6320#6001'),(628,7,'2019-10-09',3,'6320',1,0,1,'20191009#6320#7001');
/*!40000 ALTER TABLE `open_res` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_detail`
--

DROP TABLE IF EXISTS `resource_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resource_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增，主键',
  `open_res_id` bigint(20) DEFAULT NULL COMMENT '开放资源表id, fk',
  `faci_id` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '占用设备之ID',
  `applcation_code` char(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '申请号',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_res_id` (`open_res_id`),
  CONSTRAINT `fk_res_id` FOREIGN KEY (`open_res_id`) REFERENCES `open_res` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_detail`
--

LOCK TABLES `resource_detail` WRITE;
/*!40000 ALTER TABLE `resource_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `resource_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_generation_date`
--

DROP TABLE IF EXISTS `resource_generation_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resource_generation_date` (
  `generation_date` char(8) NOT NULL,
  PRIMARY KEY (`generation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_generation_date`
--

LOCK TABLES `resource_generation_date` WRITE;
/*!40000 ALTER TABLE `resource_generation_date` DISABLE KEYS */;
INSERT INTO `resource_generation_date` VALUES ('20190831'),('20190923'),('20190925'),('20190927'),('20191002'),('20191005');
/*!40000 ALTER TABLE `resource_generation_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `resource_spec_date`
--

DROP TABLE IF EXISTS `resource_spec_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `resource_spec_date` (
  `generation_date` char(8) NOT NULL,
  PRIMARY KEY (`generation_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `resource_spec_date`
--

LOCK TABLES `resource_spec_date` WRITE;
/*!40000 ALTER TABLE `resource_spec_date` DISABLE KEYS */;
INSERT INTO `resource_spec_date` VALUES ('20190928'),('20190929'),('20190930'),('20191004'),('20191005'),('20191006'),('20191007'),('20191008'),('20191009');
/*!40000 ALTER TABLE `resource_spec_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role-url-power`
--

DROP TABLE IF EXISTS `role-url-power`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role-url-power` (
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  `url` char(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'URL',
  PRIMARY KEY (`role`,`url`) USING BTREE,
  KEY `role` (`role`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role-url-power`
--

LOCK TABLES `role-url-power` WRITE;
/*!40000 ALTER TABLE `role-url-power` DISABLE KEYS */;
INSERT INTO `role-url-power` VALUES ('Admin','http://localhost:8080/expSys/welcome.jsp'),('DutyAdmin','duty'),('LabAdmin','lab'),('Student','http://localhost:8080/expSys/welcome.jsp'),('SystemAdmin','sys'),('Teacher','teacher');
/*!40000 ALTER TABLE `role-url-power` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_config`
--

DROP TABLE IF EXISTS `software_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `software_config` (
  `configid` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件集合名称（代号）====软件配置名',
  `desp` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`configid`),
  UNIQUE KEY `configname` (`config_name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_config`
--

LOCK TABLES `software_config` WRITE;
/*!40000 ALTER TABLE `software_config` DISABLE KEYS */;
INSERT INTO `software_config` VALUES (1,'software engineering','软件工程开放项目配置');
/*!40000 ALTER TABLE `software_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `software_details`
--

DROP TABLE IF EXISTS `software_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `software_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `configid` int(11) DEFAULT NULL,
  `sofware_name` varchar(80) NOT NULL,
  `verion` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unq_config_software` (`configid`,`sofware_name`),
  KEY `fk_config_idx` (`configid`),
  CONSTRAINT `fk_config` FOREIGN KEY (`configid`) REFERENCES `software_config` (`configid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `software_details`
--

LOCK TABLES `software_details` WRITE;
/*!40000 ALTER TABLE `software_details` DISABLE KEYS */;
INSERT INTO `software_details` VALUES (1,1,'Dev-C,C++','Dev-C++5.11'),(2,1,'Visual Studio Community 2017','VS Community 2017'),(3,1,'Visual C++','VC++ 6.00'),(4,1,'C-Free 5','C-Free 5'),(5,1,'eclise for  Java','4.00'),(6,1,'eclipse for web','4.10'),(7,1,'interllij idea 2019','社区版 2019'),(8,1,'mysq','mysql community 8.0'),(9,1,'sql server ','sql server express'),(10,1,'matlab','matlab 9.00');
/*!40000 ALTER TABLE `software_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `student`
--

DROP TABLE IF EXISTS `student`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `student` (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `specialty` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '专业',
  `s_class` char(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '班级',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  `s_AllDefault` int(3) NOT NULL COMMENT '累计违约次数',
  `s_NewDefault` int(1) NOT NULL COMMENT '新近违约次数',
  `spunish` int(1) NOT NULL COMMENT '惩罚天数',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `student`
--

LOCK TABLES `student` WRITE;
/*!40000 ALTER TABLE `student` DISABLE KEYS */;
INSERT INTO `student` VALUES ('201616040122','李达','信息科学与工程学院','软件专业','1602','',0,0,0),('201616040123','黄鹏','信息科学与工程学院','软件专业','1601','',0,0,3),('201616040124','宋炯炯','信息科学与工程学院','软件工程','1601','',0,0,3),('student','TEST2','信息科学与工程学院','软件工程','1601','',3,2,5);
/*!40000 ALTER TABLE `student` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studentduty`
--

DROP TABLE IF EXISTS `studentduty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studentduty` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '值班学生学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `week` char(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '星期几',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studentduty`
--

LOCK TABLES `studentduty` WRITE;
/*!40000 ALTER TABLE `studentduty` DISABLE KEYS */;
/*!40000 ALTER TABLE `studentduty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `teacher`
--

DROP TABLE IF EXISTS `teacher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `teacher` (
  `t_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  PRIMARY KEY (`t_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `teacher`
--

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;
/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_open_plan`
--

DROP TABLE IF EXISTS `temp_open_plan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `temp_open_plan` (
  `temp_plan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '临时开放计划主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实验室',
  `weekOfTerm` int(11) NOT NULL DEFAULT '1' COMMENT '周次',
  `status` int(11) DEFAULT '0' COMMENT '计划状态',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT '实验室临时开放计划' COMMENT '描述',
  PRIMARY KEY (`temp_plan_id`),
  UNIQUE KEY `term` (`term`,`labID`,`weekOfTerm`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=78 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_open_plan`
--

LOCK TABLES `temp_open_plan` WRITE;
/*!40000 ALTER TABLE `temp_open_plan` DISABLE KEYS */;
INSERT INTO `temp_open_plan` VALUES (7,'2018-2019-1','6312',3,1,''),(35,'2018-2019-1','6311',7,1,'');
/*!40000 ALTER TABLE `temp_open_plan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_open_plan_detail`
--

DROP TABLE IF EXISTS `temp_open_plan_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `temp_open_plan_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增，主键',
  `temp_plan_id` int(11) NOT NULL COMMENT '外键',
  `dayOfWeek` int(11) DEFAULT NULL COMMENT '星期几',
  `timeSlot` int(11) DEFAULT NULL COMMENT '一天内的时间段',
  `orign` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '原值',
  `temp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL COMMENT '临时值',
  PRIMARY KEY (`id`),
  KEY `fk_idx` (`temp_plan_id`),
  CONSTRAINT `fk` FOREIGN KEY (`temp_plan_id`) REFERENCES `temp_open_plan` (`temp_plan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2433 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_open_plan_detail`
--

LOCK TABLES `temp_open_plan_detail` WRITE;
/*!40000 ALTER TABLE `temp_open_plan_detail` DISABLE KEYS */;
INSERT INTO `temp_open_plan_detail` VALUES (1873,7,1,1,'0',''),(1874,7,2,1,'0',''),(1875,7,3,1,'1',''),(1876,7,4,1,'1',''),(1877,7,5,1,'1',''),(1878,7,6,1,'1',''),(1879,7,7,1,'1',''),(1880,7,1,2,'0',''),(1881,7,2,2,'0',''),(1882,7,3,2,'0',''),(1883,7,4,2,'0',''),(1884,7,5,2,'1',''),(1885,7,6,2,'1',''),(1886,7,7,2,'1',''),(1887,7,1,3,'0',''),(1888,7,2,3,'0',''),(1889,7,3,3,'0',''),(1890,7,4,3,'0',''),(1891,7,5,3,'1',''),(1892,7,6,3,'1',''),(1893,7,7,3,'1',''),(1894,7,1,4,'0','1'),(1895,7,2,4,'0','1'),(1896,7,3,4,'0','1'),(1897,7,4,4,'0','1'),(1898,7,5,4,'1','0'),(1899,7,6,4,'1',''),(1900,7,7,4,'1',''),(1901,7,1,5,'0',''),(1902,7,2,5,'0',''),(1903,7,3,5,'0',''),(1904,7,4,5,'0',''),(1905,7,5,5,'1',''),(1906,7,6,5,'1',''),(1907,7,7,5,'1',''),(1908,7,1,6,'0',''),(1909,7,2,6,'0',''),(1910,7,3,6,'0',''),(1911,7,4,6,'0',''),(1912,7,5,6,'1',''),(1913,7,6,6,'1',''),(1914,7,7,6,'1',''),(1915,7,1,7,'0',''),(1916,7,2,7,'0',''),(1917,7,3,7,'0',''),(1918,7,4,7,'0',''),(1919,7,5,7,'1',''),(1920,7,6,7,'1',''),(1921,7,7,7,'1',''),(2090,35,1,1,'0',''),(2091,35,2,1,'0',''),(2092,35,3,1,'0',''),(2093,35,4,1,'0',''),(2094,35,5,1,'0',''),(2095,35,6,1,'1',''),(2096,35,7,1,'0','1'),(2097,35,1,2,'0',''),(2098,35,2,2,'0',''),(2099,35,3,2,'0',''),(2100,35,4,2,'0',''),(2101,35,5,2,'0',''),(2102,35,6,2,'1',''),(2103,35,7,2,'0','1'),(2104,35,1,3,'0',''),(2105,35,2,3,'0',''),(2106,35,3,3,'0',''),(2107,35,4,3,'0',''),(2108,35,5,3,'0',''),(2109,35,6,3,'1',''),(2110,35,7,3,'0','1'),(2111,35,1,4,'0',''),(2112,35,2,4,'0',''),(2113,35,3,4,'0',''),(2114,35,4,4,'0',''),(2115,35,5,4,'0',''),(2116,35,6,4,'1',''),(2117,35,7,4,'0',''),(2118,35,1,5,'0',''),(2119,35,2,5,'0',''),(2120,35,3,5,'0',''),(2121,35,4,5,'0',''),(2122,35,5,5,'0',''),(2123,35,6,5,'1',''),(2124,35,7,5,'0','1'),(2125,35,1,6,'0',''),(2126,35,2,6,'0',''),(2127,35,3,6,'0',''),(2128,35,4,6,'0',''),(2129,35,5,6,'0',''),(2130,35,6,6,'1',''),(2131,35,7,6,'0','1'),(2132,35,1,7,'0',''),(2133,35,2,7,'0',''),(2134,35,3,7,'0',''),(2135,35,4,7,'0',''),(2136,35,5,7,'0',''),(2137,35,6,7,'1',''),(2138,35,7,7,'0','1');
/*!40000 ALTER TABLE `temp_open_plan_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `term`
--

DROP TABLE IF EXISTS `term`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `term` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学年 xxxx-yyyy ',
  `term` int(4) NOT NULL COMMENT '学期 1,2',
  `weeks_of_term` int(4) NOT NULL COMMENT '周数 1,2,...',
  `day_of_first_week_monday` date NOT NULL COMMENT '第一周星期一所在日期',
  `status` int(4) NOT NULL COMMENT '状态0：已结束学期\r\n状态1：当前学期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `index_year_term` (`year`,`term`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC COMMENT='学期表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `term`
--

LOCK TABLES `term` WRITE;
/*!40000 ALTER TABLE `term` DISABLE KEYS */;
INSERT INTO `term` VALUES (2,'2018-2019',1,22,'2019-08-26',0);
/*!40000 ALTER TABLE `term` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '账号',
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户姓名',
  `password` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('0124','囧囧','123456','teacher'),('201616040124','宋炯炯','123456','student'),('admin','TEST0','123456','labManager'),('duty','TEST4','123456','duty'),('labManager','TEST1','123456','labManager'),('student','TEST2','123456','student'),('teacher','TEST3','123456','teacher');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `watch`
--

DROP TABLE IF EXISTS `watch`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `watch` (
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  PRIMARY KEY (`semester`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `watch`
--

LOCK TABLES `watch` WRITE;
/*!40000 ALTER TABLE `watch` DISABLE KEYS */;
/*!40000 ALTER TABLE `watch` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'reservation'
--

--
-- Dumping routines for database 'reservation'
--
/*!50003 DROP PROCEDURE IF EXISTS `mockOpenPlanDetail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mockOpenPlanDetail`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
         if timeslot<wk then
			set ct = concat(" ",timeslot,"-",wk);
         end if;
         
         if timeslot>wk then
			set ct = concat(" ",wk,"-",timeslot);
         end if;
         
         if timeslot=wk then
			set ct = concat(" ",wk);
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mockOpenPlanDetailMode2` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mockOpenPlanDetailMode2`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
         if timeslot=1 then
			set ct = concat(" ",timeslot,"-",wk);
         end if;
         
         if timeslot=7 then
			set ct = concat(" ",wk);
         end if;
         
         if timeslot<>1 and timeslot<>7 then
			set ct = " ";
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `mockOpenPlanDetailMode3` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `mockOpenPlanDetailMode3`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
      
         if timeslot=wk then
			set ct = concat(" ",timeslot);
		 else
            set ct=" ";
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-08 19:11:05
