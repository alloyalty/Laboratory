/*
 Navicat MySQL Data Transfer

 Source Server         : wcq
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : reservation

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 31/08/2019 21:41:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrators
-- ----------------------------
DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators`  (
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`t_id`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for apply_for_lab
-- ----------------------------
DROP TABLE IF EXISTS `apply_for_lab`;
CREATE TABLE `apply_for_lab`  (
  `apply_number` bigint(30) NOT NULL AUTO_INCREMENT COMMENT '申请号/自增',
  `apply_time` datetime(0) NOT NULL COMMENT '申请时间',
  `user_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人ID',
  `user_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人姓名',
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `faci_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '设备编号',
  `course_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '课程名称',
  `exp_name` char(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验名称',
  `hardware` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `software` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '使用软件',
  `timeslot_start` int(4) NOT NULL COMMENT '时间编号，申请时间的区间--起始点(含)',
  `timeslot_finish` int(4) NOT NULL COMMENT '时间编号，申请时间的区间--终止点(含)',
  `state` int(11) NOT NULL COMMENT '申请状态=ENUM(\'待审核\', \'未通过\', \'通过未使用\', \'通过已使用\')',
  `auditor` char(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '审核人',
  `audit_time` datetime(0) NULL DEFAULT NULL COMMENT '审核日期',
  `term` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `applcatio_code` char(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请号',
  PRIMARY KEY (`apply_number`) USING BTREE,
  INDEX `fk_userid`(`user_id`) USING BTREE,
  INDEX `fk_labID`(`lab_id`) USING BTREE,
  INDEX `applcatio_code`(`applcatio_code`) USING BTREE,
  CONSTRAINT `fk_labID` FOREIGN KEY (`lab_id`) REFERENCES `laboratory` (`lab_id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_userid` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of apply_for_lab
-- ----------------------------
INSERT INTO `apply_for_lab` VALUES (4, '2018-08-30 00:00:00', 'student', 'TEST2', '6312', '', 'B', 'A', 'D', 'C', 1, 1, 0, NULL, NULL, '2018-2019-1', '20180830#6312#1001');
INSERT INTO `apply_for_lab` VALUES (5, '2018-08-30 00:00:00', 'student', 'TEST2', '6312', '', 'B', 'A', 'D', 'C', 1, 1, 0, NULL, NULL, '2018-2019-1', '20180830#6312#1002');
INSERT INTO `apply_for_lab` VALUES (6, '2018-08-30 00:00:00', 'student', 'TEST2', '6312', '', 'B', 'A', 'D', 'C', 1, 1, 0, NULL, NULL, '2018-2019-1', '20180830#6312#1003');
INSERT INTO `apply_for_lab` VALUES (7, '2019-08-31 00:00:00', 'student', 'TEST2', '6312', '', 'B', 'A', 'D', 'C', 2, 2, 0, NULL, NULL, '2018-2019-1', '20190831#6312#2001');
INSERT INTO `apply_for_lab` VALUES (8, '2019-09-02 00:00:00', 'student', 'TEST2', '6312', '', 'B', 'A', 'D', 'Dev-C,C++###Visual Studio Community 2017', 1, 1, 0, NULL, NULL, '2018-2019-1', '20190902#6312#1001');
INSERT INTO `apply_for_lab` VALUES (9, '2019-09-02 00:00:00', 'student', 'TEST2', '6312', '', 'DSDS', 'ASA', 'D', 'Visual C++###mysq###sql server ', 3, 3, 0, NULL, NULL, '2018-2019-1', '20190902#6312#3001');
INSERT INTO `apply_for_lab` VALUES (10, '2019-09-02 00:00:00', 'student', 'TEST2', '6312', '', 'sds', 'dfd', 'D', 'Visual Studio Community 2017###eclipse for web', 1, 1, 0, NULL, NULL, '2018-2019-1', '20190902#6312#1002');
INSERT INTO `apply_for_lab` VALUES (11, '2019-09-02 00:00:00', 'student', 'TEST2', '6312', '', 'ere', 'ere', 'asa', 'Dev-C,C++###eclipse for web', 1, 1, 0, NULL, NULL, '2018-2019-1', '20190902#6312#1003');

-- ----------------------------
-- Table structure for dutytable
-- ----------------------------
DROP TABLE IF EXISTS `dutytable`;
CREATE TABLE `dutytable`  (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学生姓名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `t_id` int(4) NOT NULL COMMENT '时间编号',
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for facility
-- ----------------------------
DROP TABLE IF EXISTS `facility`;
CREATE TABLE `facility`  (
  `f_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备编号',
  `f_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备名称',
  `state` int(11) NOT NULL DEFAULT 0 COMMENT '设备状态:\n\'可预约\', \'已预约\', \'故障\'\n{0,1,2}',
  `softName` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`f_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of facility
-- ----------------------------
INSERT INTO `facility` VALUES ('FACILITY001', 'DEC 计算机', 2, 'c++', '6320');
INSERT INTO `facility` VALUES ('FACILITY002', 'DEC计算机', 2, 'C', '6320');
INSERT INTO `facility` VALUES ('FACILITY003', 'DEC计算机', 0, 'java', '6320');
INSERT INTO `facility` VALUES ('FACITEST001', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST002', 'DEC计算机', 1, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST003', 'DEC计算机', 0, 'c++', '6313');
INSERT INTO `facility` VALUES ('FACITEST004', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST005', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST006', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST007', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST008', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST009', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST010', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST011', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST012', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST013', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST014', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST015', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST016', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST017', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST018', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST019', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST020', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST021', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST022', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST023', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST024', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST025', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST026', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST027', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST028', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST029', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST030', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST031', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST032', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST033', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST034', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST035', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST036', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST037', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST038', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST039', 'DEC计算机', 0, 'c++', '6312');
INSERT INTO `facility` VALUES ('FACITEST040', 'DEC计算机', 0, 'c++', '6312');

-- ----------------------------
-- Table structure for fault_repair
-- ----------------------------
DROP TABLE IF EXISTS `fault_repair`;
CREATE TABLE `fault_repair`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Lid` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `Fid` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `FaultDFesp` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `FillingDate` datetime(0) NOT NULL,
  `FillingId` int(11) NULL DEFAULT NULL,
  `Fstate` int(11) NULL DEFAULT 0 COMMENT 'ENUM(\'可预约\', \'已预约\', \'故障\')\n{0,1,2}\n',
  `MaintenceRecs` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 288 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of fault_repair
-- ----------------------------
INSERT INTO `fault_repair` VALUES (2, '6321', 'FACILITY001', 'FAULT', '2019-07-22 11:54:10', 1000, 2, 'MAINTENCERECTEST');
INSERT INTO `fault_repair` VALUES (3, '6321', 'FACILITY001', 'FAULT TEST', '2019-07-22 12:11:21', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (4, '6321', 'FACILITY002', 'FAULT002', '2019-07-22 15:48:01', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (5, '6321', 'FACILITY003', 'FAULT 004', '2019-07-22 17:45:32', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (54, '6321', 'FACILITY001', 'FAULT TEST 0004', '2019-08-04 09:47:04', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (103, '6321', 'F123123112', ' ', '2019-08-10 16:51:56', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (172, 'TESTLIB01', 'FYYY001', 'TESTDESP', '2019-08-15 12:20:29', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (173, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 12:22:41', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (185, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 12:41:30', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (186, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 17:32:11', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (198, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 17:39:18', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (199, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 17:47:02', 1000, 2, NULL);
INSERT INTO `fault_repair` VALUES (200, 'TESTLIB01', 'FACILITY002', 'TESTDESP', '2019-08-15 17:58:56', 1000, 2, NULL);

-- ----------------------------
-- Table structure for lab_open_plan
-- ----------------------------
DROP TABLE IF EXISTS `lab_open_plan`;
CREATE TABLE `lab_open_plan`  (
  `open_plan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实验室标识',
  `status` int(11) NULL DEFAULT 0 COMMENT '计划状态{0---草稿，1发布}',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '实验室开放计划' COMMENT '描述',
  PRIMARY KEY (`open_plan_id`) USING BTREE,
  UNIQUE INDEX `termLabID`(`term`, `labID`) USING BTREE,
  INDEX `labID`(`labID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 240 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lab_open_plan
-- ----------------------------
INSERT INTO `lab_open_plan` VALUES (101, '2018-2019-1', '6311', 1, '实验室开放计划');
INSERT INTO `lab_open_plan` VALUES (102, '2018-2019-1', '6312', 1, '实验室开放计划');
INSERT INTO `lab_open_plan` VALUES (182, '2018-2019-1', '6320', 1, '');
INSERT INTO `lab_open_plan` VALUES (185, '2018-2019-1', '6345', 0, '');

-- ----------------------------
-- Table structure for lab_open_plan_detail
-- ----------------------------
DROP TABLE IF EXISTS `lab_open_plan_detail`;
CREATE TABLE `lab_open_plan_detail`  (
  `detailID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `open_plan_id` int(11) NOT NULL COMMENT '外键',
  `timeSlot` int(11) NULL DEFAULT NULL COMMENT '一天内的时段',
  `dayOfWeek` int(11) NULL DEFAULT NULL COMMENT '周几',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开放计划的内容：周、或周的列表表示',
  PRIMARY KEY (`detailID`) USING BTREE,
  UNIQUE INDEX `open_plan_id`(`open_plan_id`, `timeSlot`, `dayOfWeek`) USING BTREE,
  CONSTRAINT `lab_open_plan_detail_ibfk_1` FOREIGN KEY (`open_plan_id`) REFERENCES `lab_open_plan` (`open_plan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5561 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lab_open_plan_detail
-- ----------------------------
INSERT INTO `lab_open_plan_detail` VALUES (2230, 101, 5, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2231, 101, 7, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2232, 101, 5, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2233, 101, 7, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2234, 101, 7, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2235, 101, 7, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2236, 101, 1, 1, '1');
INSERT INTO `lab_open_plan_detail` VALUES (2237, 101, 1, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2238, 101, 3, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2239, 101, 5, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2240, 101, 7, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2241, 101, 1, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2242, 101, 3, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2243, 101, 5, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2244, 101, 3, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2245, 101, 5, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2246, 101, 7, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2247, 101, 3, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2248, 101, 5, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2249, 101, 7, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2250, 101, 1, 3, '3');
INSERT INTO `lab_open_plan_detail` VALUES (2251, 101, 3, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2252, 101, 1, 2, '2');
INSERT INTO `lab_open_plan_detail` VALUES (2253, 101, 1, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2254, 101, 3, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2255, 101, 5, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2256, 101, 1, 4, '4');
INSERT INTO `lab_open_plan_detail` VALUES (2257, 101, 3, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2258, 101, 6, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2259, 101, 4, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2260, 101, 6, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2261, 101, 6, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2262, 101, 2, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2263, 101, 4, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2264, 101, 6, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2265, 101, 2, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2266, 101, 4, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2267, 101, 6, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2268, 101, 4, 6, '1-12');
INSERT INTO `lab_open_plan_detail` VALUES (2269, 101, 6, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2270, 101, 2, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (2271, 101, 4, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (2272, 101, 6, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2273, 101, 2, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2274, 101, 2, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2275, 101, 2, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2276, 101, 4, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2277, 101, 2, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2278, 101, 4, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2279, 102, 5, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2280, 102, 7, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2281, 102, 5, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2282, 102, 7, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2283, 102, 7, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2284, 102, 7, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2285, 102, 1, 1, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2286, 102, 1, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2287, 102, 3, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2288, 102, 5, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2289, 102, 7, 1, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2290, 102, 1, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2291, 102, 3, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2292, 102, 5, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2293, 102, 3, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2294, 102, 5, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2295, 102, 7, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2296, 102, 3, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2297, 102, 5, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2298, 102, 7, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2299, 102, 1, 3, '1-9');
INSERT INTO `lab_open_plan_detail` VALUES (2300, 102, 3, 1, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2301, 102, 1, 2, '2');
INSERT INTO `lab_open_plan_detail` VALUES (2302, 102, 1, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2303, 102, 3, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2304, 102, 5, 1, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2305, 102, 1, 4, '3');
INSERT INTO `lab_open_plan_detail` VALUES (2306, 102, 3, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2307, 102, 6, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2308, 102, 4, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2309, 102, 6, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2310, 102, 6, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2311, 102, 2, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2312, 102, 4, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2313, 102, 6, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2314, 102, 2, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2315, 102, 4, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2316, 102, 6, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2317, 102, 4, 6, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2318, 102, 6, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2319, 102, 2, 7, '1-10');
INSERT INTO `lab_open_plan_detail` VALUES (2320, 102, 4, 5, '1-5');
INSERT INTO `lab_open_plan_detail` VALUES (2321, 102, 6, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2322, 102, 2, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2323, 102, 2, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (2324, 102, 2, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (2325, 102, 4, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (2326, 102, 2, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (2327, 102, 4, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3896, 182, 5, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3897, 182, 7, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3898, 182, 5, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3899, 182, 7, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3900, 182, 7, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3901, 182, 7, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3902, 182, 1, 1, '1');
INSERT INTO `lab_open_plan_detail` VALUES (3903, 182, 1, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3904, 182, 3, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3905, 182, 5, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3906, 182, 7, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3907, 182, 1, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3908, 182, 3, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3909, 182, 5, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3910, 182, 3, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3911, 182, 5, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3912, 182, 7, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3913, 182, 3, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3914, 182, 5, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3915, 182, 7, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3916, 182, 1, 3, '3');
INSERT INTO `lab_open_plan_detail` VALUES (3917, 182, 3, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3918, 182, 1, 2, '2');
INSERT INTO `lab_open_plan_detail` VALUES (3919, 182, 1, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3920, 182, 3, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3921, 182, 5, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3922, 182, 1, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3923, 182, 3, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3924, 182, 6, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3925, 182, 4, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3926, 182, 6, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3927, 182, 6, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3928, 182, 2, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3929, 182, 4, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3930, 182, 6, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3931, 182, 2, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3932, 182, 4, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3933, 182, 6, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3934, 182, 4, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (3935, 182, 6, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3936, 182, 2, 7, '3,5,7');
INSERT INTO `lab_open_plan_detail` VALUES (3937, 182, 4, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (3938, 182, 6, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3939, 182, 2, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3940, 182, 2, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (3941, 182, 2, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (3942, 182, 4, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (3943, 182, 2, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (3944, 182, 4, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4043, 185, 5, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4044, 185, 7, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4045, 185, 5, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4046, 185, 7, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4047, 185, 7, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4048, 185, 7, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4049, 185, 1, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4050, 185, 1, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4051, 185, 3, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4052, 185, 5, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4053, 185, 7, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4054, 185, 1, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4055, 185, 3, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4056, 185, 5, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4057, 185, 3, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4058, 185, 5, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4059, 185, 7, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4060, 185, 3, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4061, 185, 5, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4062, 185, 7, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4063, 185, 1, 3, '3');
INSERT INTO `lab_open_plan_detail` VALUES (4064, 185, 3, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4065, 185, 1, 2, '1');
INSERT INTO `lab_open_plan_detail` VALUES (4066, 185, 1, 5, '5');
INSERT INTO `lab_open_plan_detail` VALUES (4067, 185, 3, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4068, 185, 5, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4069, 185, 1, 4, '4');
INSERT INTO `lab_open_plan_detail` VALUES (4070, 185, 3, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4071, 185, 6, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4072, 185, 4, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4073, 185, 6, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4074, 185, 6, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4075, 185, 2, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4076, 185, 4, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4077, 185, 6, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4078, 185, 2, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4079, 185, 4, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4080, 185, 6, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4081, 185, 4, 6, '');
INSERT INTO `lab_open_plan_detail` VALUES (4082, 185, 6, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4083, 185, 2, 7, '');
INSERT INTO `lab_open_plan_detail` VALUES (4084, 185, 4, 5, '');
INSERT INTO `lab_open_plan_detail` VALUES (4085, 185, 6, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4086, 185, 2, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4087, 185, 2, 1, '');
INSERT INTO `lab_open_plan_detail` VALUES (4088, 185, 2, 4, '');
INSERT INTO `lab_open_plan_detail` VALUES (4089, 185, 4, 2, '');
INSERT INTO `lab_open_plan_detail` VALUES (4090, 185, 2, 3, '');
INSERT INTO `lab_open_plan_detail` VALUES (4091, 185, 4, 1, '');

-- ----------------------------
-- Table structure for laboratory
-- ----------------------------
DROP TABLE IF EXISTS `laboratory`;
CREATE TABLE `laboratory`  (
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `lab_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室名称',
  `adress` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室地址',
  `state` int(4) NOT NULL DEFAULT 0 COMMENT '实验室状态=ENUM( \'关\',\'开\', \'已预约\')',
  `lab_for` int(4) NOT NULL DEFAULT 0 COMMENT '实验室开放对象==SET(\'学生\', \'教师\')',
  `installSoft` int(11) NOT NULL COMMENT '实验室所安装的软件集合名称',
  `hardware` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '实验室硬件类型',
  PRIMARY KEY (`lab_id`) USING BTREE,
  INDEX `fk_softwareconfig`(`installSoft`) USING BTREE,
  CONSTRAINT `fk_softwareconfig` FOREIGN KEY (`installSoft`) REFERENCES `software_config` (`configid`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of laboratory
-- ----------------------------
INSERT INTO `laboratory` VALUES ('6311', '莲6311', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
INSERT INTO `laboratory` VALUES ('6312', '莲6312', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
INSERT INTO `laboratory` VALUES ('6318', '莲6318', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
INSERT INTO `laboratory` VALUES ('6320', '莲6320', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
INSERT INTO `laboratory` VALUES ('6334', '莲6334', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');
INSERT INTO `laboratory` VALUES ('6345', '莲6345', '莲花街', 0, 0, 1, '九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色');

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '通知编号/自增',
  `category` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知类别',
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知内容',
  `time` datetime(6) NOT NULL COMMENT '发布时间',
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '发布人员工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for open_lab_schedule
-- ----------------------------
DROP TABLE IF EXISTS `open_lab_schedule`;
CREATE TABLE `open_lab_schedule`  (
  `open_sche_id` int(11) NOT NULL COMMENT '自增主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实验室标识',
  `status` int(11) NULL DEFAULT 1 COMMENT '计划状态{0---草稿，1发布}',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '实验室开放计划' COMMENT '描述',
  PRIMARY KEY (`open_sche_id`) USING BTREE,
  UNIQUE INDEX `termLabID`(`term`, `labID`) USING BTREE,
  INDEX `labID`(`labID`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of open_lab_schedule
-- ----------------------------
INSERT INTO `open_lab_schedule` VALUES (101, '2018-2019-1', '6311', 1, '实验室 开放计划');
INSERT INTO `open_lab_schedule` VALUES (102, '2018-2019-1', '6312', 1, '实验室 开放计划');
INSERT INTO `open_lab_schedule` VALUES (182, '2018-2019-1', '6320', 1, '实验室 开放计划');

-- ----------------------------
-- Table structure for open_lab_schedule_detail
-- ----------------------------
DROP TABLE IF EXISTS `open_lab_schedule_detail`;
CREATE TABLE `open_lab_schedule_detail`  (
  `detailID` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `open_sche_id` int(11) NOT NULL COMMENT '外键---从open_lab_plan中复制',
  `timeSlot` int(11) NULL DEFAULT NULL COMMENT '一天内的时段',
  `dayOfWeek` int(11) NULL DEFAULT NULL COMMENT '周几',
  `content` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '开放计划的内容：周、或周的列表表示',
  PRIMARY KEY (`detailID`) USING BTREE,
  UNIQUE INDEX `open_sche_id`(`open_sche_id`, `timeSlot`, `dayOfWeek`) USING BTREE,
  CONSTRAINT `open_lab_schedule_detail_ibfk_1` FOREIGN KEY (`open_sche_id`) REFERENCES `open_lab_schedule` (`open_sche_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2055 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of open_lab_schedule_detail
-- ----------------------------
INSERT INTO `open_lab_schedule_detail` VALUES (1173, 101, 1, 1, '1');
INSERT INTO `open_lab_schedule_detail` VALUES (1174, 101, 1, 2, '2');
INSERT INTO `open_lab_schedule_detail` VALUES (1175, 101, 1, 3, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1176, 101, 1, 4, '4');
INSERT INTO `open_lab_schedule_detail` VALUES (1177, 101, 1, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1178, 101, 1, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1179, 101, 1, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1180, 101, 2, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1181, 101, 2, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1182, 101, 2, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1183, 101, 2, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1184, 101, 2, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1185, 101, 2, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1186, 101, 2, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1187, 101, 3, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1188, 101, 3, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1189, 101, 3, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1190, 101, 3, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1191, 101, 3, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1192, 101, 3, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1193, 101, 3, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1194, 101, 4, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1195, 101, 4, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1196, 101, 4, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1197, 101, 4, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1198, 101, 4, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1199, 101, 4, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1200, 101, 4, 7, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1201, 101, 5, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1202, 101, 5, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1203, 101, 5, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1204, 101, 5, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1205, 101, 5, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1206, 101, 5, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1207, 101, 5, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1208, 101, 6, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1209, 101, 6, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1210, 101, 6, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1211, 101, 6, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1212, 101, 6, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1213, 101, 6, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1214, 101, 6, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1215, 101, 7, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1216, 101, 7, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1217, 101, 7, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1218, 101, 7, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1219, 101, 7, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1220, 101, 7, 6, '1-12');
INSERT INTO `open_lab_schedule_detail` VALUES (1221, 101, 7, 7, '7');
INSERT INTO `open_lab_schedule_detail` VALUES (1222, 102, 1, 1, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1223, 102, 1, 2, '2');
INSERT INTO `open_lab_schedule_detail` VALUES (1224, 102, 1, 3, '1-9');
INSERT INTO `open_lab_schedule_detail` VALUES (1225, 102, 1, 4, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1226, 102, 1, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1227, 102, 1, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1228, 102, 1, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1229, 102, 2, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1230, 102, 2, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1231, 102, 2, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1232, 102, 2, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1233, 102, 2, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1234, 102, 2, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1235, 102, 2, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1236, 102, 3, 1, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1237, 102, 3, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1238, 102, 3, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1239, 102, 3, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1240, 102, 3, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1241, 102, 3, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1242, 102, 3, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1243, 102, 4, 1, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1244, 102, 4, 2, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1245, 102, 4, 3, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1246, 102, 4, 4, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1247, 102, 4, 5, '1-2,4-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1248, 102, 4, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1249, 102, 4, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1250, 102, 5, 1, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1251, 102, 5, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1252, 102, 5, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1253, 102, 5, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1254, 102, 5, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1255, 102, 5, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1256, 102, 5, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1257, 102, 6, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1258, 102, 6, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1259, 102, 6, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1260, 102, 6, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1261, 102, 6, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1262, 102, 6, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1263, 102, 6, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1264, 102, 7, 1, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1265, 102, 7, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1266, 102, 7, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1267, 102, 7, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1268, 102, 7, 5, '1-5');
INSERT INTO `open_lab_schedule_detail` VALUES (1269, 102, 7, 6, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1270, 102, 7, 7, '1-10');
INSERT INTO `open_lab_schedule_detail` VALUES (1271, 182, 1, 1, '1');
INSERT INTO `open_lab_schedule_detail` VALUES (1272, 182, 1, 2, '2');
INSERT INTO `open_lab_schedule_detail` VALUES (1273, 182, 1, 3, '3');
INSERT INTO `open_lab_schedule_detail` VALUES (1274, 182, 1, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1275, 182, 1, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1276, 182, 1, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1277, 182, 1, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1278, 182, 2, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1279, 182, 2, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1280, 182, 2, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1281, 182, 2, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1282, 182, 2, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1283, 182, 2, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1284, 182, 2, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1285, 182, 3, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1286, 182, 3, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1287, 182, 3, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1288, 182, 3, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1289, 182, 3, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1290, 182, 3, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1291, 182, 3, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1292, 182, 4, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1293, 182, 4, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1294, 182, 4, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1295, 182, 4, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1296, 182, 4, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1297, 182, 4, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1298, 182, 4, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1299, 182, 5, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1300, 182, 5, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1301, 182, 5, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1302, 182, 5, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1303, 182, 5, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1304, 182, 5, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1305, 182, 5, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1306, 182, 6, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1307, 182, 6, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1308, 182, 6, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1309, 182, 6, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1310, 182, 6, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1311, 182, 6, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1312, 182, 6, 7, '3,5,7');
INSERT INTO `open_lab_schedule_detail` VALUES (1313, 182, 7, 1, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1314, 182, 7, 2, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1315, 182, 7, 3, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1316, 182, 7, 4, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1317, 182, 7, 5, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1318, 182, 7, 6, '');
INSERT INTO `open_lab_schedule_detail` VALUES (1319, 182, 7, 7, '3,5,7');

-- ----------------------------
-- Table structure for open_res
-- ----------------------------
DROP TABLE IF EXISTS `open_res`;
CREATE TABLE `open_res`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `time_slot` int(4) NOT NULL COMMENT '时间编号，申请时间的区间',
  `open_date` date NOT NULL COMMENT '日期',
  `day_of_week` int(4) NULL DEFAULT 0 COMMENT '星期几',
  `lab_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `res_total_num` int(4) NULL DEFAULT NULL COMMENT '资源总数',
  `res_consumed` int(4) NULL DEFAULT NULL COMMENT '已用资源数',
  `res_rest` int(4) NULL DEFAULT NULL COMMENT '资源余数',
  `current_avail_apply_code` char(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '当前可用的资源号码',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `DWlT001`(`time_slot`, `open_date`, `lab_id`) USING BTREE COMMENT '在（OpenDate、Week、Lid、Tid）建立唯一索引'
) ENGINE = InnoDB AUTO_INCREMENT = 423 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of open_res
-- ----------------------------
INSERT INTO `open_res` VALUES (432, 1, '2019-09-02', 1, '6312', 39, 3, 36, '20190902#6312#1004');
INSERT INTO `open_res` VALUES (433, 3, '2019-09-02', 1, '6312', 39, 1, 38, '20190902#6312#3002');
INSERT INTO `open_res` VALUES (434, 5, '2019-09-02', 1, '6312', 39, 0, 39, '20190902#6312#5001');
INSERT INTO `open_res` VALUES (435, 7, '2019-09-02', 1, '6312', 39, 0, 39, '20190902#6312#7001');
INSERT INTO `open_res` VALUES (436, 1, '2019-09-03', 2, '6311', 0, 0, 0, '20190903#6311#1001');
INSERT INTO `open_res` VALUES (437, 1, '2019-09-03', 2, '6312', 39, 0, 39, '20190903#6312#1001');
INSERT INTO `open_res` VALUES (438, 1, '2019-09-03', 2, '6320', 1, 0, 1, '20190903#6320#1001');
INSERT INTO `open_res` VALUES (439, 1, '2019-09-04', 3, '6312', 39, 0, 39, '20190904#6312#1001');

-- ----------------------------
-- Table structure for resource_detail
-- ----------------------------
DROP TABLE IF EXISTS `resource_detail`;
CREATE TABLE `resource_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增，主键',
  `open_res_id` bigint(20) NULL DEFAULT NULL COMMENT '开放资源表id, fk',
  `faci_id` char(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '占用设备之ID',
  `applcation_code` char(20) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT '申请号',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_res_id`(`open_res_id`) USING BTREE,
  CONSTRAINT `fk_res_id` FOREIGN KEY (`open_res_id`) REFERENCES `open_res` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for resource_generation_date
-- ----------------------------
DROP TABLE IF EXISTS `resource_generation_date`;
CREATE TABLE `resource_generation_date`  (
  `generation_date` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`generation_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of resource_generation_date
-- ----------------------------
INSERT INTO `resource_generation_date` VALUES ('20190831');

-- ----------------------------
-- Table structure for resource_spec_date
-- ----------------------------
DROP TABLE IF EXISTS `resource_spec_date`;
CREATE TABLE `resource_spec_date`  (
  `generation_date` char(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`generation_date`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of resource_spec_date
-- ----------------------------
INSERT INTO `resource_spec_date` VALUES ('20190902');
INSERT INTO `resource_spec_date` VALUES ('20190903');
INSERT INTO `resource_spec_date` VALUES ('20190904');

-- ----------------------------
-- Table structure for role-url-power
-- ----------------------------
DROP TABLE IF EXISTS `role-url-power`;
CREATE TABLE `role-url-power`  (
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  `url` char(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'URL',
  PRIMARY KEY (`role`, `url`) USING BTREE,
  INDEX `role`(`role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role-url-power
-- ----------------------------
INSERT INTO `role-url-power` VALUES ('Admin', 'http://localhost:8080/expSys/welcome.jsp');
INSERT INTO `role-url-power` VALUES ('DutyAdmin', 'duty');
INSERT INTO `role-url-power` VALUES ('LabAdmin', 'lab');
INSERT INTO `role-url-power` VALUES ('Student', 'http://localhost:8080/expSys/welcome.jsp');
INSERT INTO `role-url-power` VALUES ('SystemAdmin', 'sys');
INSERT INTO `role-url-power` VALUES ('Teacher', 'teacher');

-- ----------------------------
-- Table structure for software_config
-- ----------------------------
DROP TABLE IF EXISTS `software_config`;
CREATE TABLE `software_config`  (
  `configid` int(11) NOT NULL AUTO_INCREMENT,
  `config_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件集合名称（代号）====软件配置名',
  `desp` varchar(500) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`configid`) USING BTREE,
  UNIQUE INDEX `configname`(`config_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of software_config
-- ----------------------------
INSERT INTO `software_config` VALUES (1, 'software engineering', '软件工程开放项目配置');

-- ----------------------------
-- Table structure for software_details
-- ----------------------------
DROP TABLE IF EXISTS `software_details`;
CREATE TABLE `software_details`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `configid` int(11) NULL DEFAULT NULL,
  `sofware_name` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `verion` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `unq_config_software`(`configid`, `sofware_name`) USING BTREE,
  INDEX `fk_config_idx`(`configid`) USING BTREE,
  CONSTRAINT `fk_config` FOREIGN KEY (`configid`) REFERENCES `software_config` (`configid`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of software_details
-- ----------------------------
INSERT INTO `software_details` VALUES (1, 1, 'Dev-C,C++', 'Dev-C++5.11');
INSERT INTO `software_details` VALUES (2, 1, 'Visual Studio Community 2017', 'VS Community 2017');
INSERT INTO `software_details` VALUES (3, 1, 'Visual C++', 'VC++ 6.00');
INSERT INTO `software_details` VALUES (4, 1, 'C-Free 5', 'C-Free 5');
INSERT INTO `software_details` VALUES (5, 1, 'eclise for  Java', '4.00');
INSERT INTO `software_details` VALUES (6, 1, 'eclipse for web', '4.10');
INSERT INTO `software_details` VALUES (7, 1, 'interllij idea 2019', '社区版 2019');
INSERT INTO `software_details` VALUES (8, 1, 'mysq', 'mysql community 8.0');
INSERT INTO `software_details` VALUES (9, 1, 'sql server ', 'sql server express');
INSERT INTO `software_details` VALUES (10, 1, 'matlab', 'matlab 9.00');

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `specialty` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '专业',
  `s_class` char(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '班级',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  `s_AllDefault` int(3) NOT NULL COMMENT '累计违约次数',
  `s_NewDefault` int(1) NOT NULL COMMENT '新近违约次数',
  `spunish` int(1) NOT NULL COMMENT '惩罚天数',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('201616040122', '李达', '信息科学与工程学院', '软件专业', '1602', '', 0, 0, 0);
INSERT INTO `student` VALUES ('201616040123', '黄鹏', '信息科学与工程学院', '软件专业', '1601', '', 0, 0, 3);
INSERT INTO `student` VALUES ('201616040124', '宋炯炯', '信息科学与工程学院', '软件工程', '1601', '', 0, 0, 3);

-- ----------------------------
-- Table structure for studentduty
-- ----------------------------
DROP TABLE IF EXISTS `studentduty`;
CREATE TABLE `studentduty`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '值班学生学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `week` char(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '星期几',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `t_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  PRIMARY KEY (`t_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for temp_open_plan
-- ----------------------------
DROP TABLE IF EXISTS `temp_open_plan`;
CREATE TABLE `temp_open_plan`  (
  `temp_plan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '临时开放计划主键',
  `term` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '学期',
  `labID` char(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '实验室',
  `weekOfTerm` int(11) NOT NULL DEFAULT 1 COMMENT '周次',
  `status` int(11) NULL DEFAULT 0 COMMENT '计划状态',
  `desp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '实验室临时开放计划' COMMENT '描述',
  PRIMARY KEY (`temp_plan_id`) USING BTREE,
  UNIQUE INDEX `term`(`term`, `labID`, `weekOfTerm`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 65 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of temp_open_plan
-- ----------------------------
INSERT INTO `temp_open_plan` VALUES (7, '2018-2019-1', '6312', 3, 1, '');
INSERT INTO `temp_open_plan` VALUES (35, '2018-2019-1', '6311', 7, 1, '');

-- ----------------------------
-- Table structure for temp_open_plan_detail
-- ----------------------------
DROP TABLE IF EXISTS `temp_open_plan_detail`;
CREATE TABLE `temp_open_plan_detail`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增，主键',
  `temp_plan_id` int(11) NOT NULL COMMENT '外键',
  `dayOfWeek` int(11) NULL DEFAULT NULL COMMENT '星期几',
  `timeSlot` int(11) NULL DEFAULT NULL COMMENT '一天内的时间段',
  `orign` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '原值',
  `temp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '临时值',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_idx`(`temp_plan_id`) USING BTREE,
  CONSTRAINT `fk` FOREIGN KEY (`temp_plan_id`) REFERENCES `temp_open_plan` (`temp_plan_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2348 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of temp_open_plan_detail
-- ----------------------------
INSERT INTO `temp_open_plan_detail` VALUES (1873, 7, 1, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1874, 7, 2, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1875, 7, 3, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1876, 7, 4, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1877, 7, 5, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1878, 7, 6, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1879, 7, 7, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1880, 7, 1, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1881, 7, 2, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1882, 7, 3, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1883, 7, 4, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1884, 7, 5, 2, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1885, 7, 6, 2, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1886, 7, 7, 2, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1887, 7, 1, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1888, 7, 2, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1889, 7, 3, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1890, 7, 4, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1891, 7, 5, 3, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1892, 7, 6, 3, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1893, 7, 7, 3, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1894, 7, 1, 4, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (1895, 7, 2, 4, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (1896, 7, 3, 4, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (1897, 7, 4, 4, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (1898, 7, 5, 4, '1', '0');
INSERT INTO `temp_open_plan_detail` VALUES (1899, 7, 6, 4, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1900, 7, 7, 4, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1901, 7, 1, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1902, 7, 2, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1903, 7, 3, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1904, 7, 4, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1905, 7, 5, 5, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1906, 7, 6, 5, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1907, 7, 7, 5, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1908, 7, 1, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1909, 7, 2, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1910, 7, 3, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1911, 7, 4, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1912, 7, 5, 6, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1913, 7, 6, 6, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1914, 7, 7, 6, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1915, 7, 1, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1916, 7, 2, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1917, 7, 3, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1918, 7, 4, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (1919, 7, 5, 7, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1920, 7, 6, 7, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (1921, 7, 7, 7, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2090, 35, 1, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2091, 35, 2, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2092, 35, 3, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2093, 35, 4, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2094, 35, 5, 1, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2095, 35, 6, 1, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2096, 35, 7, 1, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (2097, 35, 1, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2098, 35, 2, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2099, 35, 3, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2100, 35, 4, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2101, 35, 5, 2, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2102, 35, 6, 2, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2103, 35, 7, 2, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (2104, 35, 1, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2105, 35, 2, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2106, 35, 3, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2107, 35, 4, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2108, 35, 5, 3, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2109, 35, 6, 3, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2110, 35, 7, 3, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (2111, 35, 1, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2112, 35, 2, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2113, 35, 3, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2114, 35, 4, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2115, 35, 5, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2116, 35, 6, 4, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2117, 35, 7, 4, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2118, 35, 1, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2119, 35, 2, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2120, 35, 3, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2121, 35, 4, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2122, 35, 5, 5, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2123, 35, 6, 5, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2124, 35, 7, 5, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (2125, 35, 1, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2126, 35, 2, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2127, 35, 3, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2128, 35, 4, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2129, 35, 5, 6, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2130, 35, 6, 6, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2131, 35, 7, 6, '0', '1');
INSERT INTO `temp_open_plan_detail` VALUES (2132, 35, 1, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2133, 35, 2, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2134, 35, 3, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2135, 35, 4, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2136, 35, 5, 7, '0', '');
INSERT INTO `temp_open_plan_detail` VALUES (2137, 35, 6, 7, '1', '');
INSERT INTO `temp_open_plan_detail` VALUES (2138, 35, 7, 7, '0', '1');

-- ----------------------------
-- Table structure for term
-- ----------------------------
DROP TABLE IF EXISTS `term`;
CREATE TABLE `term`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学年 xxxx-yyyy ',
  `term` int(4) NOT NULL COMMENT '学期 1,2',
  `weeks_of_term` int(4) NOT NULL COMMENT '周数 1,2,...',
  `day_of_first_week_monday` date NOT NULL COMMENT '第一周星期一所在日期',
  `status` int(4) NOT NULL COMMENT '状态0：已结束学期\r\n状态1：当前学期',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `index_year_term`(`year`, `term`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '学期表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of term
-- ----------------------------
INSERT INTO `term` VALUES (2, '2018-2019', 1, 22, '2019-08-26', 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '账号',
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户姓名',
  `password` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('0124', '囧囧', '123456', 'teacher');
INSERT INTO `user` VALUES ('201616040124', '宋炯炯', '123456', 'student');
INSERT INTO `user` VALUES ('admin', 'TEST0', '123456', 'labManager');
INSERT INTO `user` VALUES ('duty', 'TEST4', '123456', 'duty');
INSERT INTO `user` VALUES ('labManager', 'TEST1', '123456', 'labManager');
INSERT INTO `user` VALUES ('student', 'TEST2', '123456', 'student');
INSERT INTO `user` VALUES ('teacher', 'TEST3', '123456', 'teacher');

-- ----------------------------
-- Table structure for watch
-- ----------------------------
DROP TABLE IF EXISTS `watch`;
CREATE TABLE `watch`  (
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  PRIMARY KEY (`semester`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Procedure structure for mockOpenPlanDetail
-- ----------------------------
DROP PROCEDURE IF EXISTS `mockOpenPlanDetail`;
delimiter ;;
CREATE PROCEDURE `mockOpenPlanDetail`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
         if timeslot<wk then
			set ct = concat(" ",timeslot,"-",wk);
         end if;
         
         if timeslot>wk then
			set ct = concat(" ",wk,"-",timeslot);
         end if;
         
         if timeslot=wk then
			set ct = concat(" ",wk);
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mockOpenPlanDetailMode2
-- ----------------------------
DROP PROCEDURE IF EXISTS `mockOpenPlanDetailMode2`;
delimiter ;;
CREATE PROCEDURE `mockOpenPlanDetailMode2`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
         if timeslot=1 then
			set ct = concat(" ",timeslot,"-",wk);
         end if;
         
         if timeslot=7 then
			set ct = concat(" ",wk);
         end if;
         
         if timeslot<>1 and timeslot<>7 then
			set ct = " ";
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END
;;
delimiter ;

-- ----------------------------
-- Procedure structure for mockOpenPlanDetailMode3
-- ----------------------------
DROP PROCEDURE IF EXISTS `mockOpenPlanDetailMode3`;
delimiter ;;
CREATE PROCEDURE `mockOpenPlanDetailMode3`(IN planid INT)
BEGIN

DECLARE timeslot int unsigned default 1; 
DECLARE wk int unsigned default 1;
DECLARE ct varchar(30) default '';


  set timeslot =1 ; 
  while timeslot <=7 do 
	  set wk = 1;
      while wk <=7 do 
      
         if timeslot=wk then
			set ct = concat(" ",timeslot);
		 else
            set ct=" ";
         end if;
         
         
	     INSERT INTO `reservation`.`lab_open_plan_detail`
	         (`open_plan_id`,`timeSlot`,`dayOfWeek`,`content`)
	       VALUES(planid,timeslot,wk ,ct);
           set wk = wk+1;
      end while; 
	  set timeslot = timeslot + 1;
  end while;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
