/*
 Navicat MySQL Data Transfer

 Source Server         : admin
 Source Server Type    : MySQL
 Source Server Version : 80013
 Source Host           : localhost:3306
 Source Schema         : reservation

 Target Server Type    : MySQL
 Target Server Version : 80013
 File Encoding         : 65001

 Date: 12/07/2019 17:31:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for administrators
-- ----------------------------
DROP TABLE IF EXISTS `administrators`;
CREATE TABLE `administrators`  (
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`t_id`, `role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for dutytable
-- ----------------------------
DROP TABLE IF EXISTS `dutytable`;
CREATE TABLE `dutytable`  (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学生姓名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `t_id` int(4) NOT NULL COMMENT '时间编号',
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for facility
-- ----------------------------
DROP TABLE IF EXISTS `facility`;
CREATE TABLE `facility`  (
  `f_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备编号',
  `f_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备名称',
  `state` int(11) NOT NULL DEFAULT '0' COMMENT '设备状态:\n''可预约'', ''已预约'', ''故障''\n{0,1,2}',
  `softName` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`f_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of facility
-- ----------------------------
INSERT INTO `facility` VALUES ('FACILITY001', 'DEC 计算机', '0', 'c++', '6320');
INSERT INTO `facility` VALUES ('FACILITY002', 'DEC计算机', '2', 'C', '6320');
INSERT INTO `facility` VALUES ('FACILITY003', 'DEC计算机', '0', 'java', '6320');

-- ----------------------------
-- Table structure for fault_repair
-- ----------------------------
DROP TABLE IF EXISTS `fault_repair`;
CREATE TABLE `fault_repair` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Lid` char(5) NOT NULL,
  `Fid` char(20) NOT NULL,
  `FaultDFesp` varchar(300) DEFAULT NULL,
  `FillingDate` datetime NOT NULL,
  `FillingId` int(11) DEFAULT NULL,
  `Fstate` int(11) DEFAULT '0' COMMENT 'ENUM(''可预约'', ''已预约'', ''故障'')\n{0,1,2}\n',
  `MaintenceRecs` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of fault_repair
-- ----------------------------
INSERT INTO `fault_repair` VALUES ('2', '6321', 'FACILITY001', 'FAULT', '2019-07-22 11:54:10', '1000', '2', 'MAINTENCERECTEST');
INSERT INTO `fault_repair` VALUES ('3', '6321', 'FACILITY001', 'FAULT TEST', '2019-07-22 12:11:21', '1000', '2', null);
INSERT INTO `fault_repair` VALUES ('4', '6321', 'FACILITY002', 'FAULT002', '2019-07-22 15:48:01', '1000', '2', null);
INSERT INTO `fault_repair` VALUES ('5', '6321', 'FACILITY003', 'FAULT 004', '2019-07-22 17:45:32', '1000', '2', null);

-- ----------------------------
-- Table structure for laboratory
-- ----------------------------
DROP TABLE IF EXISTS `laboratory`;
CREATE TABLE `laboratory`  (
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `l_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室名称',
  `adress` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室地址',
  `state` enum('开','关','已预约') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室状态',
  `l_for` set('学生','教师') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' COMMENT '实验室开放对象',
  `installSoft` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室所安装的软件集合名称',
  `hardword` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室硬件类型',
  PRIMARY KEY (`l_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for notice
-- ----------------------------
DROP TABLE IF EXISTS `notice`;
CREATE TABLE `notice`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '通知编号/自增',
  `category` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知类别',
  `text` varchar(3000) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '通知内容',
  `time` datetime(6) NOT NULL COMMENT '发布时间',
  `t_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '发布人员工号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for openres
-- ----------------------------
DROP TABLE IF EXISTS `openres`;
CREATE TABLE `openres`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `t_id` int(4) NOT NULL COMMENT '时间编号，申请时间的区间',
  `openDate` date NOT NULL COMMENT '日期',
  `week` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '星期几',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `f_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备ID',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `DWlT001`(`t_id`, `openDate`, `week`, `l_id`) USING BTREE COMMENT '在（OpenDate、Week、Lid、Tid）建立唯一索引'
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for request
-- ----------------------------
DROP TABLE IF EXISTS `request`;
CREATE TABLE `request`  (
  `number` bigint(30) NOT NULL AUTO_INCREMENT COMMENT '申请号/自增',
  `time` datetime(6) NOT NULL COMMENT '申请时间',
  `id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人ID',
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请人姓名',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  `f_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '设备编号',
  `software` varchar(0) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '使用软件',
  `l_name` char(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验名称',
  `state` enum('待审核','未通过','通过未使用','通过已使用') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '申请状态',
  `t_id` int(4) NOT NULL COMMENT '时间编号，申请时间的区间',
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `auditor` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '审核人',
  `a_id` datetime(6) NOT NULL COMMENT '审核日期',
  PRIMARY KEY (`number`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for role-url-power
-- ----------------------------
DROP TABLE IF EXISTS `role-url-power`;
CREATE TABLE `role-url-power`  (
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  `url` char(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'URL',
  PRIMARY KEY (`role`, `url`) USING BTREE,
  INDEX `role`(`role`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role-url-power
-- ----------------------------
INSERT INTO `role-url-power` VALUES ('Admin', 'http://localhost:8080/expSys/welcome.jsp');
INSERT INTO `role-url-power` VALUES ('DutyAdmin', 'duty');
INSERT INTO `role-url-power` VALUES ('LabAdmin', 'lab');
INSERT INTO `role-url-power` VALUES ('Student', 'http://localhost:8080/expSys/welcome.jsp');
INSERT INTO `role-url-power` VALUES ('SystemAdmin', 'sys');
INSERT INTO `role-url-power` VALUES ('Teacher', 'teacher');

-- ----------------------------
-- Table structure for software
-- ----------------------------
DROP TABLE IF EXISTS `software`;
CREATE TABLE `software`  (
  `softName` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件名',
  `softVersion` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件版本号',
  `installSoft` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '软件集合名称（代号）',
  PRIMARY KEY (`softName`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `specialty` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '专业',
  `s_class` char(4) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '班级',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  `s_AllDefault` int(3) NOT NULL COMMENT '累计违约次数',
  `s_NewDefault` int(1) NOT NULL COMMENT '新近违约次数',
  `spunish` int(1) NOT NULL COMMENT '惩罚天数',
  PRIMARY KEY (`s_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES ('201616040122', '李达', '信息科学与工程学院', '软件专业', '1602', '', 0, 0, 0);
INSERT INTO `student` VALUES ('201616040123', '黄鹏', '信息科学与工程学院', '软件专业', '1601', '', 0, 0, 3);
INSERT INTO `student` VALUES ('201616040124', '宋炯炯', '信息科学与工程学院', '软件工程', '1601', '', 0, 0, 3);

-- ----------------------------
-- Table structure for studentduty
-- ----------------------------
DROP TABLE IF EXISTS `studentduty`;
CREATE TABLE `studentduty`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '值班学生学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  `week` char(15) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '星期几',
  `l_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '实验室编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for teacher
-- ----------------------------
DROP TABLE IF EXISTS `teacher`;
CREATE TABLE `teacher`  (
  `t_id` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '工号',
  `t_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '教师姓名',
  `collage` char(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学院',
  `email` char(25) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '邮箱',
  PRIMARY KEY (`t_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for team
-- ----------------------------
DROP TABLE IF EXISTS `team`;
CREATE TABLE `team`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `year` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学年',
  `team` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `week` varchar(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '周数',
  `day` date NOT NULL COMMENT '第一周星期一所在日期',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '0' COMMENT '状态0：已结束学期\r\n状态1：当前学期',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '学期表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of team
-- ----------------------------
INSERT INTO `team` VALUES (1, '2018-2019', '1', '18', '2018-05-09', '1');
INSERT INTO `team` VALUES (2, '2018-2019', '2', '22', '2019-05-09', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '账号',
  `name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '用户姓名',
  `password` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '密码',
  `role` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('0124', '囧囧', '123456', 'teacher');
INSERT INTO `user` VALUES ('201616040124', '宋炯炯', '123456', 'student');
INSERT INTO `user` VALUES ('admin', 'TEST0', '123456', 'labManager');
INSERT INTO `user` VALUES ('duty', 'TEST4', '123456', 'duty');
INSERT INTO `user` VALUES ('labManager', 'TEST1', '123456', 'labManager');
INSERT INTO `user` VALUES ('student', 'TEST2', '123456', 'student');
INSERT INTO `user` VALUES ('teacher', 'TEST3', '123456', 'teacher');

-- ----------------------------
-- Table structure for watch
-- ----------------------------
DROP TABLE IF EXISTS `watch`;
CREATE TABLE `watch`  (
  `semester` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学期',
  `s_id` char(12) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '学号',
  `s_name` char(20) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '姓名',
  PRIMARY KEY (`semester`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
