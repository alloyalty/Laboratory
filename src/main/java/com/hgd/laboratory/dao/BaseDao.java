package com.hgd.laboratory.dao;

import java.util.List;

public interface BaseDao<T> {
    //添加
    void add(T t);

    //修改
    void update(T t);

    //删除
    void delete(T t);

    //根据id查询
    T findOne(int id);
    T findOne(short id);
    T findOne(Integer id);
    T findOne(Long id);
    T findOne(String id);

    // 根据ids查询部分
    List<T> findSome(Integer[] ids);

    //查询所有
    List<T> findAll();
}
