package com.hgd.laboratory.dao;


import com.hgd.laboratory.po.FaultRepair;
import java.util.List;

public interface FaultRepairDao extends BaseDao<FaultRepair> {
     List<FaultRepair> queryStatusFaultList();   //查找全部未修设备
     List<FaultRepair> queryFMAllList(String labId) ; //指定实验室，查找全部申报维修修记录
     List<FaultRepair> queryStatusFaultListByLid(String lid);


}
