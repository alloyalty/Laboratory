package com.hgd.laboratory.dao;

import com.hgd.laboratory.po.User;

public interface UserDao extends BaseDao<User>{
    //检索用户
    User checkLogin(String id, String password, String role);
    //修改密码在BaseDao接口中


}
