package com.hgd.laboratory.dao.impl;

import com.hgd.laboratory.dao.UserDao;
import com.hgd.laboratory.po.User;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class UserDaoImpl extends BaseDaoImpl<User> implements UserDao {

    /**
     * 实现用户登录接口
     * @param id
     * @param password
     * @param role
     * @return
     */
    @Override
    public User checkLogin(String id, String password, String role) {
        //此处的查询使用了hibernate5的参数新特性（如id=?1或者id=:id  id=?弃用，会报hql格式错误）
        // 同时使用了Spring继承HibernateDaoSupport的特性，直接调用即可
        Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        User user = (User) session.createQuery("from User where id = ?1 and password = ?2 and role = ?3")
                .setParameter(1,id)
                .setParameter(2,password)
                .setParameter(3,role)
                .uniqueResult();
        return user;
    }


}
