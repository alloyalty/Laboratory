package com.hgd.laboratory.dao.impl;

import com.hgd.laboratory.dao.FaultRepairDao;
import com.hgd.laboratory.po.FaultRepair;
import com.hgd.laboratory.service.FaultRepairService;
import org.hibernate.Session;

import javax.annotation.processing.SupportedOptions;
import java.util.List;

public class FaultRepairDaoImpl extends  BaseDaoImpl<FaultRepair> implements FaultRepairDao {
    @Override
    @SuppressWarnings("unchecked")
    public List<FaultRepair> queryStatusFaultList() {
        Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        String hql ="from FaultRepair where fstate = ?1 ";
        List<FaultRepair> fmList = (List<FaultRepair>) session.createQuery(hql)
                .setParameter(1,FaultRepairService.FAULT)
                .list();
        return fmList;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<FaultRepair> queryFMAllList(String labId) {
        Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        String hql ="from FaultRepair where lid = ?1 ";
        List<FaultRepair> fmList = (List<FaultRepair>) session.createQuery(hql)
                .setParameter(1,labId)
                .list();
        return fmList;
    }


    @Override
    @SuppressWarnings("unchecked")
    public List<FaultRepair> queryStatusFaultListByLid(String lid) {
        Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        String hql ="from FaultRepair where lid = ?1 and fstate = ?2 ";
        List<FaultRepair> fmList = (List<FaultRepair>) session.createQuery(hql)
                .setParameter(1,lid)
                .setParameter(2,FaultRepairService.FAULT)
                .list();
        return fmList;
    }

}
