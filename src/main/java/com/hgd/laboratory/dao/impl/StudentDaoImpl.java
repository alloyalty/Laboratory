package com.hgd.laboratory.dao.impl;


import com.hgd.laboratory.dao.StudentDao;
import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.util.CodeUtil;
import com.hgd.laboratory.util.MailUtil;
import com.opensymphony.xwork2.ActionContext;

public class StudentDaoImpl extends BaseDaoImpl<Student> implements StudentDao{


    /**
     * 发送验证码
     * @param email
     * @param codes
     * @return
     */
    @Override
    public boolean sendEmail(String email, String codes) {
        boolean flag = false;
        //获得验证码
        codes = CodeUtil.CreateCode();
        //将随机生成的code存入session中
        ActionContext.getContext().getSession().put("codes",codes);
        //发送邮件给用户
        try{
            MailUtil mailUtil = new MailUtil(email,codes);
            mailUtil.run();
            flag = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return flag;
    }

    /**
     * 绑定邮箱
     * 1.验证code是否正确
     * 2.如果验证成功，保存用户邮箱
     * @param email
     * @param codes
     * @return
     */
    @Override
    public boolean bandEmail(String email, String codes) {
        //获取session中codes的值
        String codes1 = (String) ActionContext.getContext().getSession().get("codes");
        if (codes.equals(codes1)){
            return true;
        }
        return false;
    }
}
