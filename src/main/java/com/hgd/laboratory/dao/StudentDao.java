package com.hgd.laboratory.dao;
import com.hgd.laboratory.po.Student;

public interface StudentDao extends BaseDao<Student> {
    //发送邮箱验证码
    boolean sendEmail(String email, String codes);
    //绑定邮箱
    boolean bandEmail(String email, String codes);
    //修改密码在BaseDao接口中


}
