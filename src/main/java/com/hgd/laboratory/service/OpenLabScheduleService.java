package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.OpenLabScheduleTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;

public interface OpenLabScheduleService extends  LabService{
    OpenLabScheduleTuple findFirstOpenLabSchedule(String term);

    public void mergeToLabOpenSchedule(String term,String labId) throws OpenPlanExpressinException, OpenPlanDataIncompleteException;

    OpenLabScheduleTuple findOpenLabSchedule(String term,String labId);
}
