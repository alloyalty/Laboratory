package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.util.Page;

import java.util.List;

public interface AnnounceService {
    //category 类别常数
    final  String ANNOUNCE="announce";
    final String RULES="rules";
    Long getMainArticleId();
    Notice findOne(long id);
    List<Notice> query(String category, int i);
    PaginationData query(String category,String keyword, Page page);

    Long queryTotalRows(String category, String keyword);
}
