package com.hgd.laboratory.service;

import com.hgd.laboratory.po.DutyStudent;
import com.hgd.laboratory.po.Student;

import java.util.List;

public interface DutyStudentService {
    boolean existInDuty(String code);

    boolean addDutyStudent(String code);

    void delDutyStudent(int code);

    List<DutyStudent> query(Integer delflag);

    List<Student> queryByNameOrId(String stuName);

    boolean firstStuIsDuty(List<Student> studentList, String semester);
}
