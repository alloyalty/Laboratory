package com.hgd.laboratory.service;

import com.hgd.laboratory.po.User;

import javax.servlet.http.HttpServletRequest;

public interface FindBackPwdService {
    User findUser(String name);

    void sendEmail(User user, HttpServletRequest req) throws Exception;

    boolean checkValidAndChangePwd(User user, String digest, String newPassword) throws Exception;

    boolean checkValid(User user, String sid) throws Exception;
}
