package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.util.Page;
import org.springframework.transaction.annotation.Transactional;

public interface RulesAndNoticeService {
    int SCRATCH =1;
    int WAITFOR_AUDIT=2;
    int WAITFOR_PUBLISH=4;
    int PUBLISH =8;
    int ALL =-1;

    PaginationData<Notice> query(String keyword, int status, String category, Page page);

    boolean statusTranIsRight(Notice notice);

    @Transactional
    void markDelete(Long specId);

    void saveOrUpdate(Notice notice, int status, int mode) throws Exception;

    Notice getNotice(Long specId);

    void delete(Long specId);

    Long query(String keyword, int status, String category);
}
