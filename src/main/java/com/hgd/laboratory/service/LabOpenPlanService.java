package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.LabOpenPlan;

import java.util.List;
import java.util.Map;

public interface LabOpenPlanService extends LabService{


    //取得指定学期内，所有实验室的开放计划
    List<LabOpenPlan> queryAllLabOpenPlans(String term) ;
    void saveUpdate(LabOpenPlan lop,Map<String,String> lopDetails,Integer status,int method) throws OpenPlanExpressinException;
    void delete(int id);
    //void deleteAllRec();
    void publish(LabOpenPlan model,Map<String, String> labOpenPlanDetailMap) throws OpenPlanExpressinException, OpenPlanDataIncompleteException;
    LabOpenPlanTuple createBlankLabOpenPlan(String term,String labId);
    LabOpenPlan findOneLabOpenPlan(int openPlanId);
    LabOpenPlanTuple findLabOpenPlan(int openPlanId)  throws OpenPlanDataIncompleteException;
    LabOpenPlanTuple findLabOpenPlan(String term,String labId)  throws OpenPlanDataIncompleteException ;

    List<String> queryAvailLabListForWorkout(String term);
}
