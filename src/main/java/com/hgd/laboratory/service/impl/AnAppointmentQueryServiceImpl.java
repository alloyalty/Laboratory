package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.ApplyForLabAudit;
import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.OpenRes;
import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.service.AnAppointmentQueryService;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.util.AuditQueryReqParameter;
import com.hgd.laboratory.util.DateUtil;
import com.hgd.laboratory.util.Page;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.*;

public class AnAppointmentQueryServiceImpl implements AnAppointmentQueryService {
    private SessionFactory sessionFactory;

    /**
     * 如果xdate> apply.date 则有 true
     * 如果xdate==apply.date 则有t1<tx，返回true
     *
     * @param currDateTime 当前日期，注意只要年月日
     * @param tx           查询时刻所在的节次
     * @param page         分页显示参数
     * @return
     */
    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PaginationData<ApplyForLab> queryHistory(Date currDateTime, int tx, Page page) {
        //只取日期，不要小、分秒
        java.sql.Timestamp currDate = onlyFetchDate(currDateTime);
        long totoalRecs = queryHistoryTotalRows(tx, currDate);
        page.setTotalRows(totoalRecs);

        Session session = sessionFactory.getCurrentSession();
        String hql = "from ApplyForLab where term=?1 and (applyTime< ?2  or (applyTime=?3 and timeslotFinish<?4))";
        int firstRecIndex = new Long(page.getFirstRecPos()).intValue();
        List<ApplyForLab> applyForLabList = (List<ApplyForLab>) session.createQuery(hql)
                .setParameter(1, TermUtil.getInstamce().getTermStr())
                .setParameter(2, currDate)
                .setParameter(3, currDate)
                .setParameter(4, tx)
                .setFirstResult(firstRecIndex)
                .setMaxResults(page.getPerPageRows())
                .list();
        return new PaginationData(page, applyForLabList, null);

    }

    private long queryHistoryTotalRows(int tx, Timestamp currDate) {
        Session session = sessionFactory.getCurrentSession();
        String hql0 = "select count(*) from ApplyForLab where term=?1 and (applyTime< ?2  or (applyTime=?3 and timeslotFinish<?4))";

        return (long) session.createQuery(hql0)
                .setParameter(1, TermUtil.getInstamce().getTermStr())
                .setParameter(2, currDate)
                .setParameter(3, currDate)
                .setParameter(4, tx)
                .uniqueResult();
    }

    //用户的当前预约=====已经成功完成预约的预约（未考虑审核通过已否）
    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PaginationData<ApplyForLab> queryCurrent(Date currDateTime, int tx, Page page) {
        //只取日期，不要小、分秒
        java.sql.Timestamp currDate = onlyFetchDate(currDateTime);
        long totoalRecs = queryCurrentTotalRows(tx, currDate);
        page.setTotalRows(totoalRecs);

        Session session = sessionFactory.getCurrentSession();
        String hql = "                  from ApplyForLab where term=?1 and (applyTime> ?2  or (applyTime=?3 and timeslotStart>=?4))";
        int firstRecIndex = new Long(page.getFirstRecPos()).intValue();
        List<ApplyForLab> applyForLabList = (List<ApplyForLab>) session.createQuery(hql)
                .setParameter(1, TermUtil.getInstamce().getTermStr())
                .setParameter(2, currDate)
                .setParameter(3, currDate)
                .setParameter(4, tx)
                .setFirstResult(firstRecIndex)
                .setMaxResults(page.getPerPageRows())
                .list();
        return new PaginationData<>(page, applyForLabList, null);

    }

    private long queryCurrentTotalRows(int tx, Timestamp currDate) {
        Session session = sessionFactory.getCurrentSession();
        String hql0 = "select count (*) from ApplyForLab where term=?1 and (applyTime> ?2  or (applyTime=?3 and timeslotStart>=?4))";
        long totoalRecs = (long) session.createQuery(hql0)
                .setParameter(1, TermUtil.getInstamce().getTermStr())
                .setParameter(2, currDate)
                .setParameter(3, currDate)
                .setParameter(4, tx)
                .uniqueResult();
        return totoalRecs;
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PaginationData<ApplyForLabAudit> queryWaitForAudit(AuditQueryReqParameter qPara, Page page, java.sql.Date today) {
        //查询本学期内的所有的申请记录，其申请日在今天及其以后
        Session session = sessionFactory.getCurrentSession();
        String hqlStat = "select state,count(*)  from ApplyForLab where term=:currTerm and applyTime>= :today";
        hqlStat = buildHql(qPara, hqlStat);
        hqlStat += " group by state";

        Query hquerStat = session.createQuery(hqlStat);
        setQueryParameters(qPara, today, hquerStat);

        List list = hquerStat.list();
        Map<Integer, Integer> statics = new HashMap<>();
        int sum = 0;
        for (Object o : list) {
            Object[] item = (Object[]) o;
            Integer key = (Integer) item[0];
            Long val = (Long) item[1];
            sum += val.intValue();
            statics.put(key, val.intValue());
        }
        statics.put(-1, sum);


        String hql = "                  from ApplyForLab where term=:currTerm and applyTime>= :today";
        hql = buildHql(qPara, hql);

        int firstRecIndex = page.getFirstRecPos().intValue();
        Query hquer = session.createQuery(hql);
        setQueryParameters(qPara, today, hquer);

        List<ApplyForLab> applyForLabList = (List<ApplyForLab>) hquer
                .setFirstResult(firstRecIndex)
                .setMaxResults(page.getPerPageRows())
                .list();

        List<ApplyForLabAudit> applyForLabAuditList = new LinkedList<>();
        for (ApplyForLab applyForLab : applyForLabList) {

            Student student = session.get(Student.class, applyForLab.getUserId());
            java.sql.Date theDate = new java.sql.Date(applyForLab.getApplyTime().getTime());
            List<OpenRes> openResList = new LinkedList<>();
            for (int timeslot = applyForLab.getTimeslotStart(); timeslot <= applyForLab.getTimeslotFinish(); timeslot++) {
                OpenRes openRes = (OpenRes) session.createQuery("from OpenRes  where labId=?1 and openDate=?2 and timeSlot=?3")
                        .setParameter(1, applyForLab.getLabId())
                        .setParameter(2, theDate)
                        .setParameter(3, timeslot)
                        .uniqueResult();
                openResList.add(openRes);
            }

            applyForLabAuditList.add(new ApplyForLabAudit(applyForLab, student, openResList));
        }
        return new PaginationData<>(page, applyForLabAuditList, statics);
    }

    private String buildHql(AuditQueryReqParameter qPara, String hql) {
        if (qPara.labId != null && !qPara.labId.isEmpty()) hql += " and labId=:labId";
        if (qPara.applyDate != null) {
            hql += " and applyTime=:applyDate";
        }
        if (!(qPara.timeslot == null || qPara.timeslot == -1))
            hql += " and timeslotStart=:timeslot";

        if (!(qPara.state == null || qPara.state == -1))
            hql += " and state=:state";
        return hql;
    }

    private void setQueryParameters(AuditQueryReqParameter qPara, java.sql.Date today, Query hquer) {
        hquer.setParameter("currTerm", TermUtil.getInstamce().getTermStr());
        hquer.setParameter("today", today);
        if (qPara.labId != null && !qPara.labId.isEmpty()) hquer.setParameter("labId", qPara.labId);
        if (qPara.applyDate != null) hquer.setParameter("applyDate", qPara.applyDate);
        if (!(qPara.timeslot == null || qPara.timeslot == -1)) {
            hquer.setParameter("timeslot", qPara.timeslot);
        }

        if (!(qPara.state == null || qPara.state == -1)) {
            hquer.setParameter("state", qPara.state);
        }
    }


    @Override
    @Transactional
    public boolean auditAll(List<Long> applyIdList, int mark) {
        //WAITFOR_AUDIT
        //对给定的全部的记录标记审核
        if (mark == AnAppointmentService.AUDIT_NOPASS || mark == AnAppointmentService.AUDITPASS_UNUSE) {
            Session session = sessionFactory.getCurrentSession();
            for (Long applyId : applyIdList) {
                ApplyForLab applyForLab = session.get(ApplyForLab.class, applyId);
                if (applyForLab == null) continue;
                applyForLab.setState(mark);
                session.update(applyForLab);

            }
            return true;
        }
        return false;
    }

    @Override
    @Transactional
    public List<ApplyForLab> queryApplyList(List<Long> applyIdList) {
        Session session = sessionFactory.getCurrentSession();
        List<ApplyForLab> list = new LinkedList<>();
        for (Long applyId : applyIdList) {
            list.add(session.get(ApplyForLab.class, applyId));
        }
        return list;
    }


    private java.sql.Timestamp onlyFetchDate(Date currDateTime) {
        return new java.sql.Timestamp(DateUtil.stdDate(currDateTime).getTime());
    }

    @Override
    @Transactional
    public boolean cancelAll(List<Long> applyIdList) {
        //WAITFOR_AUDIT
        //对给定的全部的记录,取消预约
        //思路应与预约相反
        //对于每一个applyId, 设置其状态为 取消预约状态
        //    对于此申请所涉及的每一个openRes,
        //        resConsumed--, resRest++
        // 检查是否影响到预约查询

        Session session = this.sessionFactory.getCurrentSession();
        synchronized (this) {
            for (Long applyId : applyIdList) {
                ApplyForLab applyForLab = session.get(ApplyForLab.class, applyId);
                if (applyForLab == null) continue;
                applyForLab.setState(AnAppointmentService.CANCEL_APPLY);//已经取消的预约
                session.update(applyForLab);
                String labId = applyForLab.getLabId();
                java.sql.Date applyDate = new java.sql.Date(applyForLab.getApplyTime().getTime());
                for (int timeslot = applyForLab.getTimeslotStart(); timeslot <= applyForLab.getTimeslotFinish(); timeslot++) {
                    String hql = "from OpenRes where openDate=?1 and timeSlot=?2 and labId=?3";
                    OpenRes openRes = (OpenRes) session.createQuery(hql).setParameter(1, applyDate)
                            .setParameter(2, timeslot)
                            .setParameter(3, labId)
                            .uniqueResult();

                    if (openRes != null) {
                        openRes.setResConsumed(openRes.getResConsumed() - 1);
                        openRes.setResRest(openRes.getResRest() + 1);
                        session.update(openRes);
                    }
                }


            }
        }
        return true;

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
