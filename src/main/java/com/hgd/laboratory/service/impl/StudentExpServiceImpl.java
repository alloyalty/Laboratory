package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.Facility;
import com.hgd.laboratory.service.StudentExpService;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;


import java.sql.Timestamp;
import java.util.List;

public class StudentExpServiceImpl implements StudentExpService {
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public ApplyForLab queryUniqueApply(String sId, Timestamp today, int timeslot, String labId, String term) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "                  from ApplyForLab where term=:currTerm ";
        hql += " and applyTime>= :today";
        hql += " and state=2 and labId=:labId and timeslotStart<=:timeslot1  and timeslotFinish>=:timeslot2";
        hql += " and (userId=:sId or applcatioCode=:code)";
        Query hQuery = session.createQuery(hql);
        List<ApplyForLab> applyForLabList=(List<ApplyForLab>) hQuery.setParameter("currTerm", term)
                .setParameter("today", today)
                .setParameter("labId", labId)
                .setParameter("timeslot1", timeslot)
                .setParameter("timeslot2", timeslot)
                .setParameter("sId", sId)
                .setParameter("code",sId)
                .list();
        if (applyForLabList==null  || applyForLabList.size()==0)
            return null;
        else return applyForLabList.get(0);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public String queryTheMachineId(String fId, Timestamp today, int timeslot, String labId, String term) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "                  from ApplyForLab where term=:currTerm and applyTime>= :today";
        hql += " and state=3 and labId=:labId and timeslotStart<=:timeslot1  and timeslotFinish>=:timeslot2";
        hql += " and faciId=:fId ";

        Query hQuery = session.createQuery(hql);
        List<ApplyForLab> applyForLabList=(List<ApplyForLab>) hQuery.setParameter("currTerm", term)
                .setParameter("today", today)
                .setParameter("labId", labId)
                .setParameter("timeslot1", timeslot)
                .setParameter("timeslot2", timeslot)
                .setParameter("fId",fId)
                .list();
        if (applyForLabList.size()>=1)
            return fId;
        else
            return null;
    }

    @Override
    @Transactional
    public void confirmApply(String fId, Long applyNumber) {
        Session session = sessionFactory.getCurrentSession();
        ApplyForLab applyForLab = session.get(ApplyForLab.class, applyNumber);
        applyForLab.setFaciId(fId);
        applyForLab.setState(3);
        session.update(applyForLab);
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PaginationData<ApplyForLab> queryConfirmedApply(String labId, Timestamp today, int timeslot, String term, Page page) {
        Session session = sessionFactory.getCurrentSession();
        String hql = "                  from ApplyForLab where term=:currTerm and applyTime>= :today";
        hql += " and state=3 and labId=:labId and timeslotStart<=:timeslot1  and timeslotFinish>=:timeslot2";

        Query hQuery = session.createQuery(hql);
        List<ApplyForLab> applyForLabList=(List<ApplyForLab>) hQuery.setParameter("currTerm", term)
                .setParameter("today", today)
                .setParameter("labId", labId)
                .setParameter("timeslot1", timeslot)
                .setParameter("timeslot2", timeslot)
                .setFirstResult(page.getFirstRecPos().intValue())
                .setMaxResults(page.getPerPageRows())
                .list();
        return new PaginationData<>(page,applyForLabList,null);
    }

    @Override
    public boolean queryExistFaciId(String labId, String fId) {
        Session session =sessionFactory.getCurrentSession();
        Facility facility = (Facility) session.createQuery("from Facility where fId =:fId and lId=:lId")
                .setParameter("fId",fId)
                .setParameter("lId",labId)
                .uniqueResult();

        return facility!=null;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
