package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.DutyStudent;
import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.service.DutyStudentService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class DutyStudentServiceImpl implements DutyStudentService {
    private static final Integer DELFLAG_ALL = -1 ;
    SessionFactory sessionFactory;


    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public boolean existInDuty(String code) {
        Session session =sessionFactory.getCurrentSession();
        List<DutyStudent> dutyStudentList =(List<DutyStudent>)session.createQuery("from DutyStudent where sId=:code and delflag=0")
                .setParameter("code",code)
                .list();
        return  dutyStudentList!=null && dutyStudentList.size()>0;
    }

    @Override
    public boolean addDutyStudent(String code) {
        if (existInDuty(code)) {
            return true;
        } else {
            Session session = sessionFactory.getCurrentSession();
            Student student = session.get(Student.class,code);
            DutyStudent dutyStudent = new DutyStudent(student);
            session.save(dutyStudent);
            return true;
        }
    }

    @Override
    @Transactional
    public void delDutyStudent(int id) {
            Session session =sessionFactory.getCurrentSession();
            DutyStudent dutyStudent =session.get(DutyStudent.class,id);
            if (dutyStudent!=null) {
                dutyStudent.setDelflag(1);
                session.update(dutyStudent);
            }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<DutyStudent> query(Integer delflag) {
        Session session =sessionFactory.getCurrentSession();

        String hql ="from DutyStudent  ";
        if (!delflag.equals(DELFLAG_ALL)) hql += " where delflag=:delflag";

        Query hquery =session.createQuery(hql);
        if (!delflag.equals(DELFLAG_ALL)) hquery.setParameter("delflag",delflag);

        return  hquery.list();
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Student> queryByNameOrId(String stuNameOrStuId) {
        Session session =sessionFactory.getCurrentSession();
        String hql ="from Student where (sName=:stuName or sId=:stuId) ";

        Query hquery = session.createQuery(hql).setParameter("stuName",stuNameOrStuId)
                .setParameter("stuId",stuNameOrStuId);
        return  hquery.list();
    }

    @Override
    @SuppressWarnings("uncheck")
    public boolean firstStuIsDuty(List<Student> studentList, String semester) {
        if (studentList ==null || studentList.size()==0) return false;
        Student student = (Student) studentList.get(0);
        Session session =sessionFactory.getCurrentSession();
        List<DutyStudent> dutyStudentList= (List<DutyStudent>) session.createQuery("from DutyStudent where sId=:sId and semester=:semester and delflag=0")
                .setParameter("sId",student.getsId())
                .setParameter("semester",semester)
                .list();
        return dutyStudentList!=null && dutyStudentList.size()>0;

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
