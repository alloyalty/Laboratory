package com.hgd.laboratory.service.impl;


import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.dto.TempOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.TempOpenPlan;
import com.hgd.laboratory.po.TempOpenPlanDetail;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.hgd.laboratory.service.TempOpenPlanService;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.hgd.laboratory.util.OpenPlanUtil.openPlan_OpenStatus;


public class TempOpenPlanServiceImpl implements TempOpenPlanService {
    private SessionFactory sessionFactory;
    private OpenLabScheduleService openLabScheduleService;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    @Transactional
    public void deleteAllRec() {
        String hql = "delete from  TempOpenPlan";
        Session session = this.getSessionFactory().getCurrentSession();
        session.createQuery( hql ).executeUpdate();
    }

    @Override
    public Session getCurrentSession() {
        return this.getSessionFactory().getCurrentSession();
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings("unchecked")
    public List<TempOpenPlan> queryAllTempOpenPlans(String term) {
        Session session = this.getSessionFactory().getCurrentSession();
        String hql = " from TempOpenPlan where term = ?1 ";
        return (List<TempOpenPlan>) session.createQuery( hql )
                .setParameter( 1,term )
                .list();
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        Session session = this.sessionFactory.getCurrentSession();
        TempOpenPlan temp = session.get( TempOpenPlan.class,id );
        if (temp!=null)  session.delete( temp );
    }


    @Override
    @Transactional(readOnly = true)
    public TempOpenPlanTuple findTempOpenPlan(int srch_tempPlanId) throws OpenPlanDataIncompleteException {
        Session session = this.sessionFactory.getCurrentSession();
        TempOpenPlan top = session.get( TempOpenPlan.class,srch_tempPlanId );

        if (top == null) return null;
        return new TempOpenPlanTuple( top,getMap( top,session ) );

    }

    @SuppressWarnings("unchecked")
    private Map<String, TempItem> getMap(TempOpenPlan top,Session session) throws OpenPlanDataIncompleteException {
        Map<String, TempItem> m = new HashMap<>();
        String hql = "from TempOpenPlanDetail where tempOpenPlanByTempPlanId = ?1 ";

        List<TempOpenPlanDetail> topDetailList = (List<TempOpenPlanDetail>) session.createQuery( hql )
                .setParameter( 1,top )
                .list();
        if (topDetailList != null && topDetailList.size()==(7*LabService.TIMESLOTNUM)) {
            for (TempOpenPlanDetail topdetail : topDetailList) {
                String key = "t" + topdetail.getTimeSlot() + "w" + topdetail.getDayOfWeek();
                m.put( key,new TempItem( topdetail.getOrign(),topdetail.getTemp() ) );

            }
            return m;
        } else {
            throw new OpenPlanDataIncompleteException( "tempOpenPlan数据不完整，请咨询系统管理员。" );

        }

    }


    @Override
    @Transactional(readOnly = true)
    public TempOpenPlanTuple findTempOpenPlan(String term,String srch_labId,int srch_weekOfTerm) throws OpenPlanDataIncompleteException {
        Session session = this.sessionFactory.getCurrentSession();
        String hql = "from TempOpenPlan where term=?1 and labId=?2 and weekOfTerm=?3";
        TempOpenPlan top = (TempOpenPlan) session.createQuery( hql ).setParameter( 1,term )
                .setParameter( 2,srch_labId )
                .setParameter( 3,srch_weekOfTerm )
                .uniqueResult();

        if (top == null) return null;
        return new TempOpenPlanTuple( top,getMap( top,session ) );
    }

    @Override
    @Transactional
    public TempOpenPlanTuple createBlankTempOpenPlan(String term,String labId,int weekOfTerm,LabOpenPlanTuple labOpenPlanTuple) throws OpenPlanExpressinException {
        int weeksOfTerm = TermUtil.getCurrentTerm().getWeeksOfTerm();
        Session session = this.sessionFactory.getCurrentSession();
        TempOpenPlan top = new TempOpenPlan( null,term,labId,weekOfTerm,0,"" );
        session.save( top );

        Map<String, TempItem> tempPlanMap = new HashMap<>();
        Map<String, String> labOpenPlanDetailMap = labOpenPlanTuple.getLabOpenPlanDetailMap();
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;

                //依据penPlan,构造分量orign
                int timeSlot = key.charAt( 1 ) - '0';
                int dayOfWeek = key.charAt( 3 ) - '0';

                String orgin = openPlan_OpenStatus( weekOfTerm,labOpenPlanDetailMap.get( key ), weeksOfTerm);
                String temp = "";

                TempOpenPlanDetail tempPlanDetail = new TempOpenPlanDetail(null,dayOfWeek,timeSlot,orgin,temp,top);

                TempItem it = new TempItem( orgin,temp );
                tempPlanMap.put( key,it );

                session.save( tempPlanDetail );
            }
        }
        return new TempOpenPlanTuple( top,tempPlanMap );
    }

    private void saveDetails(Session session,TempOpenPlan top,Map<String, TempItem> map) {
        for (String key : map.keySet()) {
            //对每一个key 新建一个LabOpenPlanDetail,再保存
            int timeSlot = key.charAt( 1 ) - '0';
            int dayOfWeek = key.charAt( 3 ) - '0';

            TempItem it = map.get( key );

            TempOpenPlanDetail tempPlanDetail = new TempOpenPlanDetail(null,dayOfWeek,timeSlot,it.getOrign(),it.getTemp(),top);
            session.save( tempPlanDetail );
        }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public void saveUpdate(TempOpenPlan top,Map<String, TempItem> topDetailMap,Integer status,int method) throws OpenPlanExpressinException {
        Session session = this.getSessionFactory().getCurrentSession();
        top.setStatus( status );

        if (method == LabService.SAVE) {
            session.save( top );
            saveDetails( session,top,topDetailMap );

        } else {
            session.update( top );
            String hql = "from TempOpenPlanDetail where tempOpenPlanByTempPlanId=?1";
            List<TempOpenPlanDetail> topDetailList = (List<TempOpenPlanDetail>) session.createQuery( hql )
                    .setParameter( 1,top )
                    .list();

            for (TempOpenPlanDetail tempOpenPlanDetail : topDetailList) {
                String key = "t" + tempOpenPlanDetail.getTimeSlot() + "w" + tempOpenPlanDetail.getDayOfWeek();
                tempOpenPlanDetail.setTemp( topDetailMap.get( key ).getTemp() );
                tempOpenPlanDetail.setOrign( topDetailMap.get( key ).getOrign() );
                session.update( tempOpenPlanDetail );

            }

        }
    }

    @Override
    public void publish(TempOpenPlan tempOpenPlan,Map<String, TempItem> tempOpenPlanDetailMap) throws OpenPlanExpressinException, OpenPlanDataIncompleteException {
                saveUpdate( tempOpenPlan,tempOpenPlanDetailMap,LabService.PUBLISH,LabService.UPDATE );
                openLabScheduleService.mergeToLabOpenSchedule( tempOpenPlan.getTerm(),tempOpenPlan.getLabId());

    }

    @Override
    @SuppressWarnings( "unchecked" )
    @Transactional(readOnly = true)
    public List<TempOpenPlan> findTempOpenPlanByLabId(String term,String labId) {
        Session session = this.sessionFactory.getCurrentSession();
        String hql = "from TempOpenPlan where term=?1 and labId=?2 ";
        return (List<TempOpenPlan>) session.createQuery( hql ).setParameter( 1,term )
                .setParameter( 2,labId )
                .list();
    }

    @Override
    @Transactional
    public TempOpenPlan findOneTempOpenPlan(int tempPlanId) {
        return this.sessionFactory.getCurrentSession().get( TempOpenPlan.class,tempPlanId);
    }

    public OpenLabScheduleService getOpenLabScheduleService() {
        return openLabScheduleService;
    }

    public void setOpenLabScheduleService(OpenLabScheduleService openLabScheduleService) {
        this.openLabScheduleService = openLabScheduleService;
    }
}

