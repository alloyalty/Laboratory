package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.FaultRepair;

import com.hgd.laboratory.po.Laboratory;
import com.hgd.laboratory.service.FaultRepairService;

import  com.hgd.laboratory.po.Facility;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public class FaultRepairServiceImpl implements FaultRepairService {//配置成单例?
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    @Transactional
    public boolean fillFault(FaultRepair fmrec) {
        //将DAO层简化---去掉
        /*
        假定设备的id号  equId;

        从数据库的角度来说，对于插入没有限制，因其主键是自增的。

        但对于某一个特定的设备equ.Id而言，对于Faultandmaintencetab对应的表而言，有三种情形
            1) 还在equ.Id设备的记录
                    FM<equ.Id,Fault> 加入到表Faultandmaintencetab中
                    equ.fSate 《 Fault
            2) 在表FM中存在equ.Id记录，这些记录表示此设备故障申报与维修的记录。
                    并且所有这些记录的状态都应是 RECOVERY
                    否则，表明数据非法----备注：未维修处理，多次申报
                    加入一条记录。
            3) 否则 加入一条记录进入


         */
        //填写故障申报
        fmrec.setFstate(FaultRepairService.FAULT);
        Session session = this.getSessionFactory().getCurrentSession();
        session.save(fmrec);

        //更新设备状态
        Facility facility = session.get(Facility.class,fmrec.getFid());
        if (facility!=null) {
            facility.setState(FaultRepairService.FAULT);
            session.update(facility);
        }
        return true;
    }

    @Transactional
    @Override
    public FaultRepair updateMaintance(int id,String maintenceRecs) {
        //填写维修记录
        Session session = this.getSessionFactory().getCurrentSession();
        FaultRepair fmrec = session.get(FaultRepair.class,id);

        fmrec.setFstate(FaultRepairService.RECOVERY);
        fmrec.setMaintenceRecs(maintenceRecs);
        session.update(fmrec);

        //更新设备状态
        Facility facility = session.get(Facility.class,fmrec.getFid());
        if (facility!=null) {
            facility.setState(FaultRepairService.RECOVERY);
            session.update(facility);
        }
        return fmrec;

    }



    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<FaultRepair> queryStatusFaultListByLid(String labId) {
          /*
        获得Hibernate Session
        写一个HQL 语句：取  限定某实验室全部待修的记录，即状态==FAULT
        执行返回对象集合
         */

        String hql ="from FaultRepair where lid = ?1 and fstate=?2";

        Session session = this.getSessionFactory().getCurrentSession();
        return (List<FaultRepair>) session.createQuery(hql)
                .setParameter(1,labId)
                .setParameter(2,FaultRepairService.FAULT)
                .list();

    }

    @Override
    @SuppressWarnings("unchecked")
    public PaginationData<FaultRepair> queryStatusFaultList(Page page) {
              /*
        获得Hibernate Session
        写一个HQL 语句：取  全部待修的记录，即状态==FAULT
        执行返回对象集合
         */
        String hql ="from FaultRepair where fstate=?1";

        List<FaultRepair> faultRepairList;
        faultRepairList = this.getSessionFactory().getCurrentSession().createQuery(hql)
                .setParameter(1, FaultRepairService.FAULT)
                .setFirstResult(page.getFirstRecPos().intValue())
                .setMaxResults(page.getPerPageRows())
                .list();
        return new PaginationData<>(page,faultRepairList,null);

    }

    @Override
    public Long queryStatusFaultListTotalRows() {
        String hql ="select count(*) from FaultRepair where fstate=?1";

        return (long) this.getSessionFactory().getCurrentSession().createQuery(hql)
                .setParameter(1, FaultRepairService.FAULT)
                .uniqueResult();
    }

    @Override
    public boolean isValid(String lid, String fid) {
        Session session =sessionFactory.getCurrentSession();
        if (session.get(Laboratory.class,lid)==null) return false;
        if (session.get(Facility.class,fid)==null) return  false;
        return true;
    }

    public Session getCurrentSession() {
        return this.getSessionFactory().getCurrentSession();
    }

}
