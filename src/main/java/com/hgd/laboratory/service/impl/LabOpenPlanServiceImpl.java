package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.LabOpenPlan;
import com.hgd.laboratory.po.LabOpenPlanDetail;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.hgd.laboratory.util.SpringUils;
import org.hibernate.Session;

import org.hibernate.SessionFactory;

import org.springframework.transaction.annotation.Transactional;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Transactional
public class LabOpenPlanServiceImpl  implements LabOpenPlanService {
    private SessionFactory sessionFactory;
    private OpenLabScheduleService openLabScheduleService;

    private SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    @Transactional(readOnly=true)
    @SuppressWarnings("unchecked")
    public List<LabOpenPlan> queryAllLabOpenPlans(String term) {
        Session session = this.getSessionFactory().getCurrentSession();
        String hql = " from LabOpenPlan where term = ?1 ";
        return (List<LabOpenPlan>)session.createQuery(hql)
                .setParameter(1,term)
                .list();
    }


    @Override
    @Transactional
    public void publish(LabOpenPlan model,Map<String, String> labOpenPlanDetailMap) throws OpenPlanExpressinException, OpenPlanDataIncompleteException {
        saveUpdate(model,labOpenPlanDetailMap,LabOpenPlanService.PUBLISH,LabService.UPDATE);
        openLabScheduleService.mergeToLabOpenSchedule( model.getTerm(),model.getLabId());
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public void saveUpdate(LabOpenPlan lop,Map<String, String> labOpenDetailMap,Integer status,int method) throws OpenPlanExpressinException {


        Session session = this.getSessionFactory().getCurrentSession();
        lop.setStatus(status);

        if (method== LabService.SAVE) {
            session.save(lop);
            saveDetails(session,lop,labOpenDetailMap);

        } else {
            session.update(lop);
            String hql ="from LabOpenPlanDetail where labOpenPlanByOpenPlanId=?1";
            List<LabOpenPlanDetail> lopDetailList = (List<LabOpenPlanDetail>)session.createQuery(hql)
                    .setParameter(1,lop)
                    .list();

            for (LabOpenPlanDetail lopDetail:lopDetailList) {
                int timeSlot = lopDetail.getTimeSlot();
                int dayOfWeek =lopDetail.getDayOfWeek();
                String key = "t"+timeSlot+"w"+dayOfWeek;
                lopDetail.setContent(labOpenDetailMap.get(key));
                session.update(lopDetail);
            }


        }

    }




    public Session getCurrentSession() {
        return this.getSessionFactory().getCurrentSession();
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional
    public LabOpenPlanTuple findLabOpenPlan(int openPlanId) throws OpenPlanDataIncompleteException {
        Session session = this.getSessionFactory().getCurrentSession();
        LabOpenPlan lop = session.get(LabOpenPlan.class,openPlanId);

        return getLabOpenPlanTuple(session,lop);
    }

    @SuppressWarnings("unchecked")
     private LabOpenPlanTuple getLabOpenPlanTuple(Session session,LabOpenPlan lop) throws OpenPlanDataIncompleteException {
        if (lop==null) {
            return null;  //未找到
        } else {
            Map<String, String> m = new HashMap<>();
            String hql ="from LabOpenPlanDetail where labOpenPlanByOpenPlanId = ?1 ";

            List<LabOpenPlanDetail> lopDetailList =(List<LabOpenPlanDetail>) session.createQuery(hql)
                    .setParameter(1,lop)
                    .list();
            if (lopDetailList!=null) {
                for (LabOpenPlanDetail lopd:lopDetailList) {
                    m.put("t"+lopd.getTimeSlot()+"w"+lopd.getDayOfWeek(),lopd.getContent());

                }
                return new LabOpenPlanTuple(lop,m);
            } else {
                throw new OpenPlanDataIncompleteException("openPlan数据不完整，请咨询系统管理员。");

            }

        }
    }

    @Override
    @Transactional(readOnly = true)
    public LabOpenPlanTuple findLabOpenPlan(String term,String labId)  throws OpenPlanDataIncompleteException  {
        String hql = "from LabOpenPlan where term = ?1 and labId=?2";

        Session session = this.getSessionFactory().getCurrentSession();
        LabOpenPlan lop= (LabOpenPlan)session.createQuery(hql)
                .setParameter(1,term)
                .setParameter(2,labId)
                .uniqueResult();
        return getLabOpenPlanTuple(session,lop);


    }

    @Override
    @SuppressWarnings("uncheck")
    public List<String> queryAvailLabListForWorkout(String term) {
        List<String> labList=new ArrayList<>(  );
        final Session currentSession = sessionFactory.getCurrentSession();

        List<String> schLabList =  currentSession.createQuery("select labId from OpenLabSchedule where term = ?1")
                .setParameter( 1,term )
                .list();

        List<String> allLabList =  currentSession.createQuery("select labId from Laboratory")
                .list();

        allLabList.removeAll(schLabList);
        return allLabList;
    }

    @Override
    @Transactional
    public LabOpenPlanTuple createBlankLabOpenPlan(String term,String labId) {
           /*
        新建一记录，save
        新建 一个空白明细，save
        返回结果
         */

        Session session = this.sessionFactory.getCurrentSession();
        LabOpenPlan lop = new LabOpenPlan(0,term,labId,0,"");
        session.save(lop);

        Map<String,String> map = LabOpenPlanDetail.mockLabOpenPlanBlankDetails();
        saveDetails(session,lop,map);

        return new LabOpenPlanTuple(lop,map);

    }

    private void saveDetails(Session session,LabOpenPlan lop, Map<String, String> map) {
        for (String key : map.keySet()) {
            //对每一个key 新建一个LabOpenPlanDetail,再保存
            LabOpenPlanDetail lopDetail = new LabOpenPlanDetail();
            int timeSlot = key.charAt(1) - '0';
            int dayOfWeek = key.charAt(3) - '0';

            lopDetail.setTimeSlot(timeSlot);
            lopDetail.setDayOfWeek(dayOfWeek);
            lopDetail.setLabOpenPlanByOpenPlanId(lop);
            lopDetail.setContent(map.get(key));

            session.save(lopDetail);
        }
    }

    @Override
    @Transactional
    public void delete(int id) {
        String hql = "delete from LabOpenPlan where openPlanId=?1";
        this.getSessionFactory().getCurrentSession().createQuery( hql )
                .setParameter( 1,id )
                .executeUpdate();
    }



    @Override
    @Transactional
    public LabOpenPlan findOneLabOpenPlan(int openPlanId) {
        return this.sessionFactory.getCurrentSession().get( LabOpenPlan.class,openPlanId );
    }

    public OpenLabScheduleService getOpenLabScheduleService() {
        return openLabScheduleService;
    }

    public void setOpenLabScheduleService(OpenLabScheduleService openLabScheduleService) {
        this.openLabScheduleService = openLabScheduleService;
    }
}
