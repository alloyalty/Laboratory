package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.FindBackPwdService;
import com.hgd.laboratory.util.SendEmailUtil;
import com.mchange.v2.c3p0.test.C3P0BenchmarkApp;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.openxmlformats.schemas.drawingml.x2006.main.CTLineEndProperties;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;

public class FindBackPwdServiceImpl implements FindBackPwdService {
    private SessionFactory sessionFactory = null;

    @Override
    public User findUser(String name) {
        return (User) sessionFactory.getCurrentSession().get(User.class, name);
    }

    @Override
    @Transactional
    public void sendEmail(User user, HttpServletRequest req) throws Exception {
        Session session = sessionFactory.openSession();
        //计算url
        //发送邮件
        String subject = "找回您的账户密码";
        String validateCode = UUID.randomUUID().toString(); // 本次密钥
        Timestamp outDate = new Timestamp(System.currentTimeMillis() + 30 * 60 * 1000);// 30分钟后过期
        outDate =ignoreMillSecond(outDate);

        // 存入DB
        user.setOutDate(outDate);
        user.setValidateCode(validateCode);
        session.update(user);
        // 注意：存入outDate 与取出的outDtaeTemp 不同
        final User uerTemp = session.get(User.class,user.getId());

        String userId = uerTemp.getId();
        final Timestamp outDateTemp = uerTemp.getOutDate();
        String digitalSignature = calDigest(validateCode, outDateTemp, userId);
        System.out.println("url摘要:"+digitalSignature);
        System.out.println("参数:validateCode"+validateCode+",outDateTemp"+outDateTemp+",userId"+userId);
        long date = outDateTemp.getTime() / 1000 * 1000;// 忽略毫秒数 mySql 取出时间是忽略毫秒数的
        String key = userId + "$" + date + "$" + validateCode;

        String path = req.getContextPath();
        String basePath = req.getScheme() + "://" + req.getServerName() + ":" + req.getServerPort() + path + "/";
        String resetPassHref = basePath + "findBackPwd_prepare?sid=" + digitalSignature + "&id=" + userId;
        String emailContent = "请勿回复本邮件.点击下面的链接,重设密码<br/><a href=" + resetPassHref + " target='_BLANK'>" + resetPassHref
                + "</a>  或者    <a href=" + resetPassHref + " target='_BLANK'>点击我重新设置密码</a>"
                + "<br/>tips:本邮件超过30分钟,链接将会失效，需要重新申请'找回密码'";

        SendEmailUtil.sendMail(user.getEmaill(), subject, emailContent);


    }

    public Timestamp ignoreMillSecond(Timestamp outDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(outDate.getTime());
        calendar.set(Calendar.MILLISECOND,0);
        return new Timestamp(calendar.getTimeInMillis());

    }

    @Override
    @Transactional
    public boolean checkValidAndChangePwd(User user, String digest, String newPassword) throws Exception {
        Session session = sessionFactory.getCurrentSession();

        if (checkValid(user,digest)) {
            System.out.println("验证通过");
            user.setPassword(newPassword);
            user.setValidateCode(UUID.randomUUID().toString());
            session.update(user);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public boolean checkValid(User user, String digest) throws Exception {
        final Timestamp outDate = user.getOutDate();
        final String validateCode = user.getValidateCode();
        final String userId = user.getId();

        String cDigest = calDigest(validateCode, outDate, userId);
        System.out.println("check摘要:"+cDigest);
        System.out.println("参数:validateCode"+validateCode+",outDate"+outDate+",userId"+userId);
        return cDigest.equals(digest);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public static String getMD5(String str) throws Exception {
        try {
            // 生成一个MD5加密计算摘要
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 计算md5函数
            md.update(str.getBytes());
            // digest()最后确定返回md5 hash值，返回值为8为字符串。因为md5 hash值是16位的hex值，实际上就是8位的字符
            // BigInteger函数则将8位的字符串转换成16位hex值，用字符串来表示；得到字符串形式的hash值
            return new BigInteger(1, md.digest()).toString(16);
        } catch (Exception e) {
            throw new Exception("MD5加密出现错误");
        }
    }

    public String calDigest(String validateCode, final Timestamp outDate, String userId) throws Exception {
        String digitalSignature;
        long date = outDate.getTime() / 1000 * 1000;// 忽略毫秒数 mySql 取出时间是忽略毫秒数的
        String key = userId + "$" + date + "$" + validateCode;
        System.out.println(" key>>>" + key);
        digitalSignature = getMD5(key);// 数字签名
        return digitalSignature;
    }

}
