package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.ImportResult;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.ImportDataService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ImportDataServiceImpl implements ImportDataService, com.hgd.laboratory.service.DataTranService {
    private SessionFactory sessionFactory;


    @Override
    @Transactional
    public <T> ImportResult importData(File file, Class<T> tClass, final String ext) {
        int rows = 0;
        final String fileName = file.getName();
        List<String> list = new ArrayList<>();
        try (InputStream is = new FileInputStream(file)) {
            Workbook wb;

            if (".xls".equals(ext)) {
                wb = new HSSFWorkbook(is);
            } else if ((".xlsx").equals(ext)) {
                wb = new XSSFWorkbook(is);
            } else {
                throw new Exception("文件类型不正确，仅支持.xls/.xlsx文件。");
            }

            Sheet sheet = wb.getSheetAt(0);//表
            rows = sheet.getPhysicalNumberOfRows();

            System.out.println(file.getAbsolutePath() + " 表的总行数:" + rows);
            for (int i = 0; i < 2; i++) {
                Row row = sheet.getRow(i);
                if (!isTitleRow(row, tClass)) {
                    throw new Exception(tClass.getSimpleName() + "文件头二行标题内容不符。");
                }
            }

            for (int i = 2; i < rows; i++) {
                Row row = sheet.getRow(i);
                if (row == null) continue;
                T t = tClass.newInstance();
                if (buildObjFromRow(t, row)) {
                    if (!importData(t)) list.add(getMessage(row));
                } else {
                    list.add(getMessage(row));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ImportResult importResult = new ImportResult();
        importResult.setUnImportDatas(list);
        importResult.setTotalRows(rows - 2);
        importResult.setImportedDataRows(rows - 2 - list.size());
        return importResult;

    }

    private String getMessage(Row row) {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 3; i++) {
            if (i != 0) s.append(",");
            s.append(getStringRowCell(row, i));
        }
        s.append("......");
        return s.toString();
    }

    private String getStringRowCell(Row row, int i) {
        CellType cellType = row.getCell(i).getCellType();
        switch (cellType) {
            case BLANK:
            case _NONE:
                return "";
            case STRING:
                return row.getCell(i).getStringCellValue();
            case NUMERIC:
                Long l = (long) row.getCell(i).getNumericCellValue();
                return l.toString();
        }
        return "";

    }

    @Override
    public <T> boolean buildObjFromRow(T t, Row row) {
        if (t instanceof Student) {
            Student student = (Student) t;
            student.setsId(row.getCell(0).getStringCellValue());
            student.setsName(row.getCell(1).getStringCellValue());
            student.setCollage(row.getCell(2).getStringCellValue());
            student.setSpecialty(row.getCell(3).getStringCellValue());

            Cell cell = row.getCell(4);
            if (cell.getCellType() == CellType.STRING) {
                student.setsClass("" + cell.getStringCellValue());
            } else if (cell.getCellType() == CellType.NUMERIC) {
                int iclass = (int) cell.getNumericCellValue();
                student.setsClass(String.format("%04d", iclass));
            }

            student.setsAllDefault(0);
            student.setsNewDefault(0);
            student.setSpunish(0);
            return true;
        } else if (t instanceof Teacher) {
            Teacher teacher = (Teacher) t;
            teacher.settId(row.getCell(0).getStringCellValue());
            teacher.settName(row.getCell(1).getStringCellValue());
            teacher.setCollage(row.getCell(2).getStringCellValue());
            return true;
        } else if (t instanceof Laboratory) {
            Laboratory laboratory = (Laboratory) t;
            laboratory.setLabId(row.getCell(0).getStringCellValue());
            laboratory.setLabName(row.getCell(1).getStringCellValue());
            laboratory.setAdress(row.getCell(2).getStringCellValue());
            laboratory.setState((int) row.getCell(3).getNumericCellValue());
            laboratory.setLabFor((int) row.getCell(4).getNumericCellValue());
            String configName =  row.getCell(5).getStringCellValue();
            laboratory.setInstallSoft(queryConfigId(configName));
            laboratory.setHardware(row.getCell(6).getStringCellValue());
            return true;
        } else if (t instanceof Facility) {
            Facility facility = (Facility) t;
            facility.setfId(row.getCell(0).getStringCellValue());
            facility.setfName(row.getCell(1).getStringCellValue());
            facility.setState((int) row.getCell(2).getNumericCellValue());
            facility.setSoftName(row.getCell(3).getStringCellValue());
            facility.setlId(row.getCell(4).getStringCellValue());
            return true;
        } else if (t instanceof SoftwareConfig) {
            SoftwareConfig softwareConfig = (SoftwareConfig) t;
            softwareConfig.setConfigName(row.getCell(0).getStringCellValue());
            softwareConfig.setDesp(row.getCell(1).getStringCellValue());
            return true;
        } else if (t instanceof SoftwareDetails) {
            SoftwareDetails details = (SoftwareDetails) t;

            final Cell cell0 = row.getCell(0);
            final String cellString = getCellString(cell0);
            details.setSofwareName(cellString);

            final Cell cell1 = row.getCell(1);
            details.setVerion(getCellString(cell1));

            final Cell cell2 = row.getCell(2);
            String configName = cell2.getStringCellValue();

            SoftwareConfig softwareConfig = (SoftwareConfig) sessionFactory.getCurrentSession()
                    .createQuery("from SoftwareConfig where configName=?1")
                    .setParameter(1, configName)
                    .uniqueResult();
            details.setSoftwareConfigByConfigid(softwareConfig);
            return true;
        } else {
        }
        return false;
    }

    public String getCellString(Cell cellx) {
        String s = "";
        if (cellx.getCellType() == CellType.STRING) {
            s = cellx.getStringCellValue();
        } else if (cellx.getCellType() == CellType.NUMERIC) {
            s = "" + cellx.getNumericCellValue();
        } else {
            s = cellx.getStringCellValue();
        }
        return s;
    }

    @Override
    public <T> boolean isTitleRow(Row row, Class<T> tClass) {
        String w0 = row.getCell(0).getStringCellValue();
        String w1 = row.getCell(1).getStringCellValue();
        Cell cell = row.getCell(2);
        String w2 = "";
        if (cell != null) w2 = cell.getStringCellValue();

        return isTitleWords(tClass, w0, w1, w2);


    }

    public <T> boolean isTitleWords(Class<T> tClass, String s0, String s1, String s2) {
        final String[] StuentHEAD1 = new String[]{"学号", "姓名", "学院"};
        final String[] StuentHEAD2 = new String[]{"s_id","s_name","collage"};

        final String[] TeacherHEAD1 = new String[]{"工号","教师姓名","学院"};
        final String[] TeacherHEAD2 = new String[]{"t_id","t_name","collage"};

        final String[] LaboratoryHEAD1 = new String[]{"实验室编号","实验室名称","实验室地址"};
        final String[] LaboratoryHEAD2 = new String[]{"l_id","l_name","address"};

        final String[] FacilityHEAD1 = new String[]{"设备编号","设备名称","设备状态"};
        final String[] FacilityHEAD2 = new String[]{"f_id","f_name","state"};

        final String[] SoftwareDetailsHEAD1 = new String[]{"软件名","软件版本号","软件配置名称"};
        final String[] SoftwareDetailsHEAD2 = new String[]{"softwareName","version","config_name"};




        switch (tClass.getSimpleName()) {
            case "Student":
                return isTitilep(s0, s1, s2, StuentHEAD1, StuentHEAD2);
            case "Teacher":
                return isTitilep(s0, s1, s2, TeacherHEAD1, TeacherHEAD2);
            case "Laboratory":
                return isTitilep(s0, s1, s2, LaboratoryHEAD1, LaboratoryHEAD2);
            case "Facility":
                return isTitilep(s0, s1, s2, FacilityHEAD1, FacilityHEAD2);
            case "SoftwareConfig":
                boolean titilep = "配置名称".equals(s0) && "描述".equals(s1);
                titilep = titilep || ("config_name".equals(s0) && "desp".equals(s1));
                return titilep;
            case "SoftwareDetails":
                return isTitilep(s0, s1, s2, SoftwareDetailsHEAD1, SoftwareDetailsHEAD2);
            default:
                return false;

        }
    }

    private boolean isTitilep(String s0, String s1, String s2, String[] teacherHEAD1, String[] teacherHEAD2) {
        boolean titilep;
        titilep = teacherHEAD1[0].equals(s0) && teacherHEAD1[1].equals(s1) && teacherHEAD1[2].equals(s2);
        titilep = titilep || (teacherHEAD2[0].equals(s0) && teacherHEAD2[1].equals(s1) && teacherHEAD2[2].equals(s2));
        return titilep;
    }




    @Override
    public <T> boolean importData(T t) {
        Session session = sessionFactory.getCurrentSession();
        switch (t.getClass().getSimpleName()) {
            case "Student":
                this.importStudent((Student) t);
                return true;
            case "Teacher":
                this.importTeacher((Teacher) t);
                return true;
            case "Laboratory":
                this.importLaboratory((Laboratory) t);
                return true;
            case "Facility":
                this.importFacility((Facility) t);
                return true;
            case "SoftwareConfig":
                importSoftwareConfig(t, session);
                return true;
            case "SoftwareDetails":
                return importSoftwareDetails((SoftwareDetails) t, session);
            default:
                return false;

        }

    }

    @Transactional
    public <T> boolean importSoftwareDetails(SoftwareDetails t, Session session) {
        if (t.getSoftwareConfigByConfigid() == null) return false;
        SoftwareDetails softwareDetails = (SoftwareDetails) session.createQuery("from SoftwareDetails where sofwareName=:sofwareName and softwareConfigByConfigid=:softwareConfigByConfigid")
                .setParameter("sofwareName", t.getSofwareName())
                .setParameter("softwareConfigByConfigid", t.getSoftwareConfigByConfigid())
                .uniqueResult();
        if (softwareDetails == null)
            session.save(t);
        else {
            t.setId(softwareDetails.getId());
            session.merge(t);
        }
        return true;

    }

    @Transactional
    public <T> void importSoftwareConfig(T t, Session session) {
        Query query = session.createQuery("from SoftwareConfig where configName=:configName");
        query.setParameter("configName", ((SoftwareConfig) t).getConfigName());
        if (query.list().size() == 0) {
            session.save(t);
        }
    }

    @Override
    @Transactional
    public void importFacility(Facility facility) {
        Session session = sessionFactory.getCurrentSession();
        Facility facilityInDb = session.get(Facility.class, facility.getfId());
        if (facilityInDb == null) {
            session.save(facility);
        } else {
            session.merge(facility);
        }
    }

    @Override
    @Transactional
    public void importStudent(Student student) {
        Session session = sessionFactory.getCurrentSession();
        Student studentInDb = session.get(Student.class, student.getsId());
        if (studentInDb == null) {
            session.save(student);
        } else {
            session.merge(student);
        }
    }

    @Override
    @Transactional
    public void importTeacher(Teacher teacher) {
        Session session = sessionFactory.getCurrentSession();
        Teacher teacherInDb = session.get(Teacher.class, teacher.gettId());
        if (teacherInDb == null) {
            session.save(teacher);
        } else {
            session.merge(teacher);
        }
    }

    @Override
    @Transactional
    public void importLaboratory(Laboratory laboratory) {
        Session session = sessionFactory.getCurrentSession();
        Laboratory laboratoryInDb = session.get(Laboratory.class, laboratory.getLabId());
        if (laboratoryInDb == null) {
            session.save(laboratory);
        } else {
            session.merge(laboratory);
        }
    }

    @Override
    public Integer queryConfigId(String configName) {
        Session session = sessionFactory.getCurrentSession();
        SoftwareConfig softwareConfig = (SoftwareConfig) session.createQuery("from SoftwareConfig where configName=:configName")
                .setParameter("configName",configName)
                .uniqueResult();
        if (softwareConfig==null) return 0;
        return softwareConfig.getConfigid();


    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}

