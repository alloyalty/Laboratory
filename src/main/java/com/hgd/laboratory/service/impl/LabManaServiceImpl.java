package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.LabTeacher;
import com.hgd.laboratory.po.Teacher;
import com.hgd.laboratory.service.LabManaService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class LabManaServiceImpl implements LabManaService
{
    private static final Integer DELFLAG_ALL = -1 ;
    SessionFactory sessionFactory;

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<LabTeacher> query(Integer delflag) {
        Session session =sessionFactory.getCurrentSession();

        String hql ="from LabTeacher  ";
        if (!delflag.equals(DELFLAG_ALL)) hql += " where delflag=:delflag";

        Query hquery =session.createQuery(hql);
        if (!delflag.equals(DELFLAG_ALL)) hquery.setParameter("delflag",delflag);

        return  hquery.list();


    }

    @Override
    @Transactional
    public boolean addLabManager(String code) {
        if (existInDuty(code)) {
            return true;
        } else {
            Session session = sessionFactory.getCurrentSession();
            Teacher teacher = session.get(Teacher.class,code);
            LabTeacher labTeacher = new LabTeacher(teacher);
            session.save(labTeacher);
            return true;
        }
    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public void delLabManager(int id) {
        Session session =sessionFactory.getCurrentSession();
        LabTeacher labTeacher =session.get(LabTeacher.class,id);
        if (labTeacher!=null) {
            labTeacher.setDelflag(1);
            session.update(labTeacher);
        }

    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public boolean existInDuty(String code) {
        Session session =sessionFactory.getCurrentSession();
        List<LabTeacher> labTeacherList =(List<LabTeacher>)session.createQuery("from LabTeacher where code=:code and delflag=0")
                .setParameter("code",code)
                .list();
        return  labTeacherList!=null && labTeacherList.size()>0;


    }

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public List<Teacher> queryByNameOrId(String teaNameOrId) {
        Session session =sessionFactory.getCurrentSession();
        Integer delflag =0;
        String hql ="from Teacher where (tName=:teaName or tId=:teaId) ";

        Query hquery = session.createQuery(hql).setParameter("teaName",teaNameOrId)
                .setParameter("teaId",teaNameOrId);
        return  hquery.list();
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
