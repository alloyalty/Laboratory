package com.hgd.laboratory.service.impl;
import com.hgd.laboratory.util.SpringUils;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.List;

public class TestService {
//	public List<String> getOpenLabList() {
//		String [] al ={"6312","6320","6345","6311","6334","6318"};
//
//		List<String> openLabList  =new ArrayList<>();
//		for (int i=0;i<al.length;i++) openLabList.add(al[i]);
//		return openLabList;
//
//	}
    @SuppressWarnings( "unchecked" )
	public List<String> getOpenLabList(String term) {
		List<String> labList=new ArrayList<>(  );
		SessionFactory sf = SpringUils.getBean( "sessionFactory" );

		if (sf==null)
			return labList;
		String hql ="select labId from OpenLabSchedule where term = ?1";
		return  sf.getCurrentSession().createQuery( hql )
				.setParameter( 1,term )
				.list();

	}


	@SuppressWarnings( "unchecked" )
	public List<String> getAllLabList() {
		List<String> labList=new ArrayList<>(  );
		SessionFactory sf = SpringUils.getBean( "sessionFactory" );
		if (sf==null)
			return labList;

		String hql ="select labId from Laboratory";
		return  sf.getCurrentSession().createQuery( hql )
				.list();


	}
}
