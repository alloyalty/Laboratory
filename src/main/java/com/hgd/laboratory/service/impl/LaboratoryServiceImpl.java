package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.Laboratory;
import com.hgd.laboratory.po.SoftwareConfig;
import com.hgd.laboratory.po.SoftwareDetails;
import com.hgd.laboratory.service.LaboratoryService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class LaboratoryServiceImpl implements LaboratoryService {
    private SessionFactory sessionFactory;


    @Override
    @SuppressWarnings( "unchecked" )
    public String outlineOfLab(String labId) {
        Session currentSession = this.sessionFactory.getCurrentSession();

        //可用于开放实验使用的机器数
        long pcnum = (long)currentSession.createQuery(" select count(*) from Facility where lId=?1 and state <> 2" )
                .setParameter( 1,labId ).list().get( 0 );

        //软件配置
        Laboratory labor = currentSession.get( Laboratory.class,labId );
        SoftwareConfig softwareConfig =(SoftwareConfig) currentSession.get(SoftwareConfig.class,labor.getInstallSoft());
        List<SoftwareDetails> softwareList = (List<SoftwareDetails>) currentSession.createQuery( "from SoftwareDetails where softwareConfigByConfigid=?1" )
                .setParameter( 1,softwareConfig )
                .list();
        StringBuffer buff = new StringBuffer(  );
        for (SoftwareDetails software:softwareList   ) {
            buff.append( software.getSofwareName() );
            buff.append( ";" );

        }
        return labId+"实验室，共有"+pcnum+"计算机。\n"+labor.getHardware()+"\n "+new String(buff);

        //mock  data
        //return "6312机房，共有40台计算机。安装C,C++,phthon,Java,JavaEE 学习与开发环境 ";

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
