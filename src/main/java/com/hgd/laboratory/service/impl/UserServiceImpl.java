package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dao.UserDao;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.po.UserRole;
import com.hgd.laboratory.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserServiceImpl implements UserService {
    //对象注入
    private SessionFactory sessionFactory;

    //登录验证
    @Override
    public User checkUser(String id, String password, String role){
        Session session =sessionFactory.getCurrentSession();
//        User user = (User) session.createQuery("from User where id = ?1 and password = ?2 and role = ?3")
//                .setParameter(1,id)
//                .setParameter(2,password)
//                .setParameter(3,role)
//                .uniqueResult();
//        return user;
        User user = (User) session.createQuery("from User where id = ?1 and password = ?2")
                .setParameter(1,id)
                .setParameter(2,password)
                .uniqueResult();
        if (user==null)  return null;

        List<UserRole> userRoleList = session.createQuery("from UserRole where userById=:userById")
                .setParameter("userById", user)
                .list();
        List<UserRole> match = userRoleList.stream().filter(ur -> ur.getRole().equalsIgnoreCase(role)).collect(Collectors.toList());
        if (match!=null && match.size()>=1) {
            user.setRole(role);
            return user;
        }
        return null;
    }
    //查找用户
    @Override
    public User findOne(String id) {
        Session session =sessionFactory.getCurrentSession();
        return session.get(User.class,id);
    }

    //更新用户信息（修改密码，绑定邮箱、修改邮箱）
    @Override
    @Transactional
    public void update(User user) {
        Session session =sessionFactory.getCurrentSession();
        session.update(user);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
