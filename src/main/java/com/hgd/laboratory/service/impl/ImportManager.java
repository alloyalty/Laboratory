package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.Facility;
import com.hgd.laboratory.service.ImportDataService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ImportManager {
    public static void main( String[] args ) {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext("applicationContext.xml");
        ImportDataService importDataService = (ImportDataService) appContext.getBean("importDataService");
        Facility facility =new Facility("FAULT1101","台式计算机A",0,"config6311","6311");
        importDataService.importFacility(facility);
        Facility facility1 =new Facility("FAULT1102","台式计算机B",0,"config6311","6311");
        importDataService.importFacility(facility1);

    }
}
