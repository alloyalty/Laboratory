package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.service.TermService;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;


public class TermServiceImpl implements TermService {
    SessionFactory sessionFactory;

    @Override
    @Transactional
    public boolean init(String year, Integer term, Integer weeksOfTerm, Date dayOfFirstWeekMonday) {
        Session session = sessionFactory.getCurrentSession();
        if (!termIsRunning()) {
            Term  newTerm = new Term(year,term,weeksOfTerm,dayOfFirstWeekMonday);
            session.save(newTerm);
            //TermUtil.reset();
            if (clearNewTermData(TermUtil.getTermStr())) {
                TermUtil.reset();
                return true;
            } else {
                System.out.println("数据清理失败");
                session.getTransaction().rollback();
            }
        }
        return false;
    }

    @SuppressWarnings("uncheck")
    @Override
    @Transactional
    public boolean   clearNewTermData(String termStr) {
        Session session =sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab where term=:term").setParameter("term",termStr).executeUpdate();
        session.createQuery("delete from DutySchedule where term=:term").setParameter("term",termStr).executeUpdate();
        session.createQuery("delete from DutyStudent where semester=:term").setParameter("term",termStr).executeUpdate();
        session.createQuery("delete from LabOpenPlan where term=:term").setParameter("term",termStr).executeUpdate();
        session.createQuery("delete from OpenLabSchedule where term=:term").setParameter("term",termStr).executeUpdate();
        session.createQuery("delete from OpenRes").executeUpdate();
        session.createQuery("delete from ResourceGenerationDate ").executeUpdate();
        session.createQuery("delete from ResourceSpecDate ").executeUpdate();
        session.createQuery("delete from TempOpenPlan  where term=:term").setParameter("term",termStr).executeUpdate();

        List list = new ArrayList();

        list.addAll(session.createQuery("from ApplyForLab where term=:term").setParameter("term",termStr).list());
        list.addAll(session.createQuery("from DutySchedule where term=:term").setParameter("term",termStr).list());
        list.addAll(session.createQuery("from DutyStudent where semester=:term").setParameter("term",termStr).list());
        list.addAll(session.createQuery("from LabOpenPlan where term=:term").setParameter("term",termStr).list());
        list.addAll(session.createQuery("from OpenLabSchedule where term=:term").setParameter("term",termStr).list());
        list.addAll(session.createQuery("from OpenRes").list());
        list.addAll(session.createQuery("from ResourceGenerationDate ").list());
        list.addAll(session.createQuery("from ResourceSpecDate ").list());
        list.addAll(session.createQuery("from TempOpenPlan  where term=:term").setParameter("term",termStr).list());
        //所有列表皆为零
        long c =list.stream().filter(b->((List)b).size()==0).count();
        return c==0L;



    }

    @Override
    @Transactional
    public boolean closure() {
        Session session = sessionFactory.getCurrentSession();
        if (termIsRunning()) {
            Term currentTerm = (Term) session.createQuery( "from Term where status=1" ).uniqueResult();
            currentTerm.setStatus(0);
            session.update(currentTerm);
            TermUtil.reset();
            return true;
        }
        return false;
    }
    @Override
    @SuppressWarnings("uncheck")
    @Transactional
    public boolean termIsRunning() {
        Session session =sessionFactory.getCurrentSession();
        List<Term> list =  session.createQuery( "from Term where status=1" ).list();
        return  (list!=null && list.size()==1);

    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
