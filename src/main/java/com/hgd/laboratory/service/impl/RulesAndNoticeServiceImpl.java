package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.RulesAndNoticeService;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public class RulesAndNoticeServiceImpl implements RulesAndNoticeService {

    private SessionFactory sessionFactory;


    private boolean whereIsAdd = false;

    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public PaginationData<Notice> query(String keyword, int status, String category, Page page) {
        Session session = sessionFactory.getCurrentSession();
        whereIsAdd = false;
        String hql = " from Notice ";
        hql = getHqlString(keyword, status, hql);

        Query hquery = getQuerySetParam(keyword, status, category, session, hql);
        List<Notice> list =(List<Notice>) hquery
                .setFirstResult(page.getFirstRecPos().intValue())
                .setMaxResults(page.getPerPageRows())
                .list();
        return new PaginationData<>(page,list,null);
    }


    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public Long query(String keyword, int status, String category) {
        Session session = sessionFactory.getCurrentSession();
        whereIsAdd = false;
        String hql = "select count(*) from Notice ";
        hql = getHqlString(keyword, status, hql);

        Query hquery = getQuerySetParam(keyword, status, category, session, hql);
        Long totalRows = (Long) hquery.list().get(0);
        return totalRows;
    }

    private String getHqlString(String keyword, int status, String hql) {
        hql += whereIf() + " category=:category ";
        if (!ignoreStatus(status)) hql += whereIf() + " status=:status";
        if (!ignoreKeyword(keyword)) hql += whereIf() + " title like :keyword";
        return hql;
    }

    private Query getQuerySetParam(String keyword, int status, String category, Session session, String hql) {
        Query hquery = session.createQuery(hql);
        hquery.setParameter("category", category);
        if (!ignoreStatus(status)) hquery.setParameter("status", status);
        if (!ignoreKeyword(keyword)) hquery.setParameter("keyword", "%" + keyword + "%");
        return hquery;
    }

    private boolean ignoreKeyword(String keyword) {
        if (keyword == null) return true;
        return keyword.trim().isEmpty();
    }

    private boolean ignoreStatus(int status) {
        return status == RulesAndNoticeService.ALL;
    }

    private String whereIf() {
        if (whereIsAdd)
            return " and  ";
        else {
            this.whereIsAdd = true;
            return " where ";
        }
    }

    @Override
    public boolean statusTranIsRight(Notice notice) {
        return true;
    }

    @Override
    @Transactional
    public void delete(Long specId) {
        Session session = sessionFactory.getCurrentSession();
        Notice notice = session.get(Notice.class, specId);
        session.delete(notice);
    }

    @Override
    @Transactional
    public void markDelete(Long specId) {
        Session session = sessionFactory.getCurrentSession();
        Notice notice = session.get(Notice.class, specId);
        notice.setDelflag(1);
        session.update(notice);
    }

    @Override
    @Transactional
    public void saveOrUpdate(Notice notice, int status, int mode) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        if (mode == LabService.SAVE) {
            notice.setStatus(status);
            session.save(notice);
        } else {
            if (status == RulesAndNoticeService.WAITFOR_AUDIT) {
                notice.setStatus(status);
                if (notice.getId() == null) {
                    session.save(notice);
                } else {
                    session.update(notice);
                }
            } else {
//                if (session.get(Notice.class,notice.getId()) ==null) throw new Exception("Error in Update mode in saveOrUpdate. ");
                notice.setStatus(status);
                session.update(notice);
            }
        }

    }

    @Override
    @Transactional
    public Notice getNotice(Long specId) {
        Session session = sessionFactory.getCurrentSession();
        return (Notice) session.get(Notice.class, specId);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
