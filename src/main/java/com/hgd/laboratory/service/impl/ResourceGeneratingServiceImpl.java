package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.OpenLabScheduleTuple;
import com.hgd.laboratory.exception.IncompleteDataException;
import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.hgd.laboratory.util.*;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.transaction.annotation.Transactional;

import java.text.SimpleDateFormat;
import java.util.*;

public class ResourceGeneratingServiceImpl implements com.hgd.laboratory.service.ResourceGeneratingService, ApplicationListener<ContextRefreshedEvent> {
    @Autowired @Getter @Setter
    private SessionFactory sessionFactory;

    @Autowired
    OpenLabScheduleService openLabScheduleService;

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent event) {

//        TermUtil termUtil =TermUtil.getInstamce();
//        String term = termUtil.getTermStr();
        //todo 在调试成功后，加上资源生成代码
        /*
        try {
            generatingResFor(new Date(  ),term);
        } catch (OpenPlanExpressinException e) {
            e.printStackTrace();
        } catch (IncompleteDataException e) {
            e.printStackTrace();
        } catch (InvalidDateInTerm invalidDateInTerm) {
            invalidDateInTerm.printStackTrace();
        }

         */


    }

    //系统在刚刚进入新的一天之时，调度执行资 源生成算法
    @Override
    public void resGeneratingPer24Hours(final String term) {
        Timer timer = new Timer();
        //延时到明天00：00：01秒
        long delay = millSecToTomorrow();
        long period = 24 * 60 * 60 * 1000;
        timer.schedule( new TimerTask() {
            public void run() {
                //生成资源
                try {
                    generatingResFor( new Date(),term );
                } catch (OpenPlanExpressinException e) {
                    e.printStackTrace();
                } catch (IncompleteDataException e) {
                    e.printStackTrace();
                } catch (InvalidDateInTerm invalidDateInTerm) {
                    invalidDateInTerm.printStackTrace();
                }
            }

        },delay,period );
    }

    private long millSecToTomorrow() {
        //取当时间
        //取明日0点0分0秒的时间
        //计算差值
        long currentTime = System.currentTimeMillis();
        Calendar today = Calendar.getInstance();
        today.setTimeInMillis( currentTime );

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.set( today.get( Calendar.YEAR ),today.get( Calendar.MONTH ),today.get( Calendar.DAY_OF_MONTH ),23,59,60 );
        tomorrow.set(Calendar.MILLISECOND,0);
        ;
        return tomorrow.getTimeInMillis() - currentTime;

    }

    //立即执行
    @Override
    public void resGeneratingImmediate(final String term) {
        Timer timer = new Timer();
        long delay = 100;
        timer.schedule( new TimerTask() {
            public void run() {
                //生成资源
                try {
                    generatingResFor( new Date(),term );
                } catch (OpenPlanExpressinException e) {
                    e.printStackTrace();
                } catch (IncompleteDataException e) {
                    e.printStackTrace();
                } catch (InvalidDateInTerm invalidDateInTerm) {
                    invalidDateInTerm.printStackTrace();
                }
            }
        },delay );

    }

    @Override
    @Transactional(readOnly = true )
    public boolean todayResGenTaskIsCompleted(Date today) {
        Session session = this.sessionFactory.getCurrentSession();
        String id = new SimpleDateFormat( "yyyyMMdd" ).format( today );
        ResourceGenerationDate resourceGenerationDate = session.get( ResourceGenerationDate.class,id );
        return resourceGenerationDate == null ? false : true;
    }

    @Override
    @Transactional
    public void generatingResFor(Date today,String term) throws OpenPlanExpressinException, IncompleteDataException, InvalidDateInTerm {
        if (todayResGenTaskIsCompleted( today ))
            return;
        Session session = sessionFactory.getCurrentSession();
        List<Date> dateList = new ReservationRules( 1,3).visibleResDates(today);
        List<String> laboratoryList = new TestService().getOpenLabList( term );

        synchronized (this) {
            for (Date date : dateList) {
                if (specDateResIsAleadyGenerated(date)) continue;
                Integer dayOfWeek = ApplyUtil.calDayOfWeek( date );
                Integer weekOfTerm = TermUtil.getInstamce().calWeekOfTerm( date, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday());

                for (String labId : laboratoryList) {
                    //生成指定的实验室指定某一天的预约资源的起始号
                    generateResForLab(term, session, date, dayOfWeek, weekOfTerm, labId, calResTotalNum(labId, session));
                }
                markSpecDateResIsReady( date );

            }
            String id = new SimpleDateFormat( "yyyyMMdd" ).format( today );
            ResourceGenerationDate resourceGenerationDate = new ResourceGenerationDate( id );
            session.save( resourceGenerationDate );

        }


    }

    public void generateResForLab(String term, Session session, Date date, Integer dayOfWeek, Integer weekOfTerm, String labId, final int resourceTotalNum) throws OpenPlanExpressinException {
        OpenLabScheduleTuple openScheduleTuple = openLabScheduleService.findOpenLabSchedule( term,labId );

        if (openScheduleTuple==null) return;
        if (openScheduleTuple.getOpenLabSchedule() == null) return;
        final Map<String, String> openLabScheduleDetailMap = openScheduleTuple.getOpenLabScheduleDetailMap();
        for (int timeslot = 1; timeslot <= LabService.TIMESLOTNUM; timeslot++) {
            /*
            计算指定日期在本学期的周次 weeksOfTerm、星期几 dayOfWeek
            查询此实验室日的开放情况列表
             */
            String key = "t"+ timeslot +"w"+dayOfWeek;
            String content = openLabScheduleDetailMap.get( key );

            if (isOpen(content, weekOfTerm)) {
                OpenRes openRes = new OpenRes(timeslot, new java.sql.Date(date.getTime()), labId);
                ApplyUtil.fillResMess(date, labId, timeslot, openRes, resourceTotalNum);
                session.save(openRes);
            }

        }
    }


    private boolean isOpen(String content,Integer weekOfTerm) throws OpenPlanExpressinException {
        if (content.trim().isEmpty()) return false;
        Set<Integer> set = OpenPlanUtil.transferToSet( content );
        if (set.contains( weekOfTerm )) return true;
        return false;
    }


    private void markSpecDateResIsReady(Date date) {
        Session session = sessionFactory.getCurrentSession();
        String id = new SimpleDateFormat( "yyyyMMdd" ).format( date );
        session.saveOrUpdate( new ResourceSpecDate(id) );
    }



    private boolean specDateResIsAleadyGenerated(Date date) {
        String id = new SimpleDateFormat( "yyyyMMdd" ).format( date );
        Session session = sessionFactory.getCurrentSession();
        ResourceSpecDate resourceSpecDate =session.get(ResourceSpecDate.class,id);
        return (resourceSpecDate==null)?false:true;
    }


    @Override
    public String initialAvailApplyCode(String labId,java.sql.Date applyTime,Integer timeslot) {
        return ApplyUtil.initialAvailApplyCode( labId,applyTime,timeslot );
    }

    @Override
    public int calResTotalNum(String labId,Session session) {

        return ApplyUtil.calResTotalNum( labId,session );
    }


}
