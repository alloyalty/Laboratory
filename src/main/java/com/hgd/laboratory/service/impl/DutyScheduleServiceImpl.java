package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.DutyDetails;
import com.hgd.laboratory.dto.DutyScheduleTuple;
import com.hgd.laboratory.po.DutySchedule;
import com.hgd.laboratory.po.DutyScheduleDetail;
import com.hgd.laboratory.po.DutyStudent;
import com.hgd.laboratory.service.DutyScheduleService;
import com.hgd.laboratory.service.DutyStudentService;
import com.hgd.laboratory.util.Consistencies;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

public class DutyScheduleServiceImpl implements DutyScheduleService {
    private SessionFactory sessionFactory;

    @Override
    public Map<String, DutyDetails> getCurrentDutyDatas(String term, String labId) {
        return null;
    }

    @Override
    public Consistencies checkConsistency(String term, String labId, Map<String, DutyDetails> dutyDatas) {
        return null;
    }

    @Override
    public void updateDutyDatas(String term, String labId, Map<String, DutyDetails> dutyDatas) {

    }

    @Override
    public DutyScheduleTuple findDutySchedule(String term, String srch_labId) {
        return null;
    }

    @Override
    @Transactional
    public DutyScheduleTuple createBlankDutySchedule(String term, String srch_labId) {
        Session session =sessionFactory.getCurrentSession();
        DutySchedule dutySchedule = new DutySchedule(term,srch_labId,0,"值班安排");
        session.save(dutySchedule);

        Map<String,String> map = DutyScheduleDetail.mockBlankDetail();
        saveDetails(session,dutySchedule,map);
        return new DutyScheduleTuple(dutySchedule,map);
    }

    private void saveDetails(Session session, DutySchedule dutySchedule, Map<String, String> map) {
        for (String key : map.keySet()) {
            DutyScheduleDetail dutyDetails = new DutyScheduleDetail();
            int timeSlot = key.charAt(1) - '0';
            int dayOfWeek = key.charAt(3) - '0';

            dutyDetails.setTimeSlot(timeSlot);
            dutyDetails.setDayOfWeek(dayOfWeek);
            dutyDetails.setDutyScheduleByDutyScheduleId(dutySchedule);
            dutyDetails.setContent(map.get(key));

            session.save(dutyDetails);
        }
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

}

