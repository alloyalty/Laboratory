package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.dto.OpenLabScheduleTuple;
import com.hgd.laboratory.dto.TempOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.hgd.laboratory.service.TempOpenPlanService;
import com.hgd.laboratory.util.OpenPlanUtil;
import com.hgd.laboratory.util.SpringUils;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

public class OpenLabScheduleServiceImpl implements OpenLabScheduleService {
    @Getter @Setter
    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    LabOpenPlanService labOpenPlanService;
    @Autowired
    TempOpenPlanService tempOpenPlanService;


    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public OpenLabScheduleTuple findFirstOpenLabSchedule(String term) {
        /*
         * 查找本学期内第一个可用的开放计划，返回结果
         */
        Session session = sessionFactory.getCurrentSession();
        String hql = " from OpenLabSchedule where term = ?1 order by labId";
        List<OpenLabSchedule> opsList = (List<OpenLabSchedule>) session.createQuery( hql )
                .setParameter( 1,term )
                .list();
        if (opsList == null || opsList.size() == 0) return null;

        OpenLabSchedule ols = opsList.get( 0 );
        return new OpenLabScheduleTuple( ols,getScheduleMap( session,ols ) );
    }


    @Override
    @Transactional
    @SuppressWarnings("unchecked")
    public OpenLabScheduleTuple findOpenLabSchedule(String term,String labId) {

        Session session = sessionFactory.getCurrentSession();
        String hql = " from OpenLabSchedule where term = ?1 and labId=?2";
        List<OpenLabSchedule> opsList = (List<OpenLabSchedule>) session.createQuery( hql )
                .setParameter( 1,term )
                .setParameter( 2,labId )
                .list();
        if (opsList == null || opsList.size() == 0) return null;


        OpenLabSchedule ols = opsList.get( 0 );
        return new OpenLabScheduleTuple( ols,getScheduleMap( session,ols ) );
    }


    @SuppressWarnings("unchecked")
    private Map<String, String> getScheduleMap(Session session,OpenLabSchedule ols) {
        Map<String, String> map = new HashMap<>();
        String hql1 = "from OpenLabScheduleDetail where openLabScheduleByOpenScheId=?1";
        List<OpenLabScheduleDetail> olsDetailList = session.createQuery( hql1 ).setParameter( 1,ols ).list();

        if (olsDetailList != null) {
            for (OpenLabScheduleDetail olsd : olsDetailList) {
                map.put( "t" + olsd.getTimeSlot() + "w" + olsd.getDayOfWeek(),olsd.getContent() );
            }
        }
        return map;
    }



    @Transactional
    @SuppressWarnings( "unchecked" )
    public void mergeToLabOpenScheduleDeprecated(String term,String labId) throws OpenPlanExpressinException {

        /*
        发布计划的自动生成算法：
        对于实验室labid任一时段[i=1..timeslot,j=星期1..星期日] 有
        {
            1.求得学期计划的开放周次集合 W={w1,w2,..., wk}
            2.openSet ={},  closeSet ={}
            foreach  (临时计划:实验室labid的全部临时计划)
            {
                x =临时计划.周次；
                如果没有临时计划，则什么也不做；
                如果临时开放，则openSet.add(wx);
                如果临时关闭，则closeSet.add(wx);
            }

           3.发布开发计划[i,j]={w1,w2,...,wk) +openSet -closeSet;
        }
         */
        Session session = this.sessionFactory.getCurrentSession();
        LabOpenPlan labOpenPlan = (LabOpenPlan) session.createQuery( "from LabOpenPlan where status=1 and term = ?1 and labId = ?2" )
                .setParameter( 1,term )
                .setParameter( 2,labId )
                .uniqueResult();

        String lpdhql = "from LabOpenPlanDetail where  labOpenPlanByOpenPlanId=?1 order by timeSlot, dayOfWeek";
        List<LabOpenPlanDetail> labOpenPlanDetailList = (List<LabOpenPlanDetail>) session.createQuery( lpdhql )
                .setParameter( 1,labOpenPlan )
                .list();



        OpenLabSchedule openLabSchedule = (OpenLabSchedule) session.createQuery( "from OpenLabSchedule where status=1 and term = ?1 and labId= ?2" )
                .setParameter( 1,term )
                .setParameter( 2,labId )
                .uniqueResult();

        final Integer openPlanId = labOpenPlan.getOpenPlanId();
        if (openLabSchedule==null) {
            //创建一个空白的OpenLabSchedule
            openLabSchedule = createEmptyOpenLabSchedule( term,labId,session,openPlanId );
        }

        String schHql ="from OpenLabScheduleDetail where  openLabScheduleByOpenScheId= ?1  order by timeSlot, dayOfWeek";
        List<OpenLabScheduleDetail> openLabScheduleDetails =(List<OpenLabScheduleDetail>)session.createQuery( schHql )
                .setParameter( 1,openLabSchedule )
                .list();




        List<TempOpenPlan> tempOpenPlanList = (List<TempOpenPlan>) session.createQuery( "from TempOpenPlan where status=1 and term = ?1 and labId = ?2" )
                .setParameter( 1,term )
                .setParameter( 2,labId )
                .list();
        List<List<TempOpenPlanDetail>> tempOpenPlanDetail_listList = new ArrayList<>();
        for (TempOpenPlan tempOpenPlan : tempOpenPlanList) {
            String tempHql = "from TempOpenPlanDetail where tempOpenPlanByTempPlanId = ?1  order by timeSlot, dayOfWeek";
            List<TempOpenPlanDetail> tempOpenPlanDetailList = session.createQuery( tempHql )
                    .setParameter( 1,tempOpenPlan )
                    .list();
            tempOpenPlanDetail_listList.add( tempOpenPlanDetailList );
        }

        for (int i = 0; i < labOpenPlanDetailList.size(); i++) {
            LabOpenPlanDetail labOpenPlanDetail = labOpenPlanDetailList.get( i );
            Integer timeslot = labOpenPlanDetail.getTimeSlot();
            Integer dayOfWeek = labOpenPlanDetail.getDayOfWeek();

            Set<Integer> openWeekSet = OpenPlanUtil.transferToSet( labOpenPlanDetail.getContent() );
            Set<Integer> openSet = new HashSet<>(  );
            Set<Integer> closeSet = new HashSet<>(  );

            for (int j = 0; j < tempOpenPlanDetail_listList.size(); j++) {
                List<TempOpenPlanDetail> tempOpenPlanDetailList = tempOpenPlanDetail_listList.get( j );

                TempOpenPlanDetail tempOpenPlanDetail = tempOpenPlanDetailList.get( i );
                if (timeslot != tempOpenPlanDetail.getTimeSlot() || (dayOfWeek != tempOpenPlanDetail.getDayOfWeek())) {
                    session.getTransaction().rollback();
                    return;
                }

                if (!OpenPlanUtil.valid_tempPlan( openWeekSet,tempOpenPlanDetail.getTempOpenPlanByTempPlanId().getWeekOfTerm(),
                        tempOpenPlanDetail.getOrign(),tempOpenPlanDetail.getTemp() )) {
                    session.getTransaction().rollback();
                    return;
                }

                Integer weekOfTerm = tempOpenPlanDetail.getTempOpenPlanByTempPlanId().getWeekOfTerm();

                final String tempplan = tempOpenPlanDetail.getTemp().trim();
                if (tempplan.equals( "" )) {
                    ;
                } else if (tempplan.equals( "1" )) {
                    openSet.add( weekOfTerm );
                } else if (tempplan.equals( "0" )) {
                    closeSet.add( weekOfTerm );
                } else {
                    ;
                }

            }
            openWeekSet.addAll( openSet );
            openWeekSet.removeAll( closeSet );
            String s = OpenPlanUtil.reverseToString(openWeekSet);


            //更新公开计划---前件，此计划已经存在---先前代码保证
            OpenLabScheduleDetail openLabScheduleDetail = openLabScheduleDetails.get( i );
            openLabScheduleDetail.setContent( s );
            session.update( openLabScheduleDetail );


        }


        ;
    }

    public OpenLabSchedule createEmptyOpenLabSchedule(String term,String labId,Session session,Integer openPlanId) {
        OpenLabSchedule openLabSchedule;
        openLabSchedule = new OpenLabSchedule( openPlanId,term,labId,1,"实验室 开放计划" );
        session.save( openLabSchedule );
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                OpenLabScheduleDetail openLabScheduleDetail = new OpenLabScheduleDetail( null,i,j,"",openLabSchedule );
                session.save( openLabScheduleDetail );
            }
        }
        return openLabSchedule;
    }



    @Override
    @Transactional
    @SuppressWarnings( "unchecked" )
    public void mergeToLabOpenSchedule(String term,String labId) throws OpenPlanDataIncompleteException, OpenPlanExpressinException {
        Session session = this.sessionFactory.getCurrentSession();

        //LabOpenPlanService labOpenPlanService =(LabOpenPlanService)SpringUils.getBean( "labOpenPlanService" );
        //TempOpenPlanService tempOpenPlanService =(TempOpenPlanService) SpringUils.getBean( "tempOpenPlanService" );

        //1.找出labOpenPlan
        LabOpenPlanTuple labOpenPlanTuple =labOpenPlanService.findLabOpenPlan( term,labId );
        if (labOpenPlanTuple==null) return ;  //先有olabOpenPlan,才有 tempOpenPlan

        List<TempOpenPlan> tempOpenPlanList = (List<TempOpenPlan>)tempOpenPlanService.findTempOpenPlanByLabId(term,labId );

        //2.找出相应的TempOpenPlan
        List<TempOpenPlanTuple> tempOpenPlanTupleList = new ArrayList<>(  );
        for (int i = 0; i <tempOpenPlanList.size() ; i++) {
            TempOpenPlanTuple x = tempOpenPlanService.findTempOpenPlan( tempOpenPlanList.get( i ).getTempPlanId() );
            tempOpenPlanTupleList.add(x);
        }


        //3.找出相对应的SchedulePlan
        final Integer openPlanId = labOpenPlanTuple.getLabOpenPlan().getOpenPlanId();
        OpenLabScheduleTuple opSchduleTuple = this.findOpenLabSchedule( term,labId );
        if (opSchduleTuple==null) {
            //创建一个空白的OpenLabSchedule
            OpenLabSchedule openLabSchedule = createEmptyOpenLabSchedule( term,labId,session,openPlanId );
            opSchduleTuple = this.findOpenLabSchedule( term,labId );
        }


        //开始合并，将LabOpenPlan与TempOpenPlan二者合并成SchedulePlan
        String schHql ="from OpenLabScheduleDetail where  openLabScheduleByOpenScheId= ?1  order by timeSlot, dayOfWeek";
        List<OpenLabScheduleDetail> openLabScheDetailList =(List<OpenLabScheduleDetail>)session.createQuery( schHql )
                .setParameter( 1,opSchduleTuple.getOpenLabSchedule() )
                .list();

        for (OpenLabScheduleDetail olsd:openLabScheDetailList   ) {
            int timeslot = olsd.getTimeSlot();
            int dayOfWeek =olsd.getDayOfWeek();
            String key = "t"+timeslot+"w"+dayOfWeek;

            Set<Integer> openWeekSet = OpenPlanUtil.transferToSet( labOpenPlanTuple.getLabOpenPlanDetailMap().get( key ) );
            Set<Integer> openSet = new HashSet<>(  );
            Set<Integer> closeSet = new HashSet<>(  );

            for (int i = 0; i <tempOpenPlanTupleList.size() ; i++) {
                TempOpenPlanTuple tempOpenPlanTuple = tempOpenPlanTupleList.get( i );

                Integer weekOfTerm = tempOpenPlanTuple.getTempOpenPlan().getWeekOfTerm();
                final String tempPlan = tempOpenPlanTuple.getTempOpenPlanDetailMap().get( key ).getTemp();
                if (tempPlan.equals( "" )) {
                    ;
                } else if (tempPlan.equals( "1" )) {
                    openSet.add( weekOfTerm );
                } else if (tempPlan.equals( "0" )) {
                    closeSet.add( weekOfTerm );
                } else {
                    ;
                }

            }

            openWeekSet.addAll( openSet );
            openWeekSet.removeAll( closeSet );
            String content = OpenPlanUtil.reverseToString(openWeekSet);
            olsd.setContent( content );
            session.update( olsd );
        }

    }

}
