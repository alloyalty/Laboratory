package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.AnnounceService;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class AnnounceServiceImpl implements AnnounceService {
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public Long
    getMainArticleId() {
        Session session= sessionFactory.getCurrentSession();
        //取最后一个
        String hql = "from Notice  order by id desc";
        List<Notice> noticeList = session.createQuery(hql).setFirstResult(0).setMaxResults(1).list();
        if (noticeList==null || noticeList.size()==0)
            return null;
        else
            return  noticeList.get(0).getId();
    }

    @Override
    public Notice findOne(long id) {
        Session session= sessionFactory.getCurrentSession();
        return session.get(Notice.class,id);
    }

    @Override
    @SuppressWarnings("uncheck")
    public List<Notice> query(String type, int num) {
        Session session= sessionFactory.getCurrentSession();
        //取最后一个
        String hql = "from Notice where category=:category order by id desc";
        List<Notice> noticeList = (List<Notice> )session.createQuery(hql)
                .setParameter("category", type)
                .setFirstResult(0).setMaxResults(num).list();
        return noticeList;
    }

    @Override
    public Long queryTotalRows(String category, String keyword) {
        Session session= sessionFactory.getCurrentSession();
        //取最后一个
        String hql = "select count(*) from Notice where category=:category ";
        if (keyword!=null && !keyword.trim().isEmpty()) {
            hql += " and title like '%"+keyword.trim()+"%' ";
        }


        long totoalRecs = (long) session.createQuery(hql)
                .setParameter("category", category)
                .uniqueResult();
        return totoalRecs;
    }

    @Override
    @SuppressWarnings("uncheck")
    public PaginationData query(String announce,String keyword, Page page) {
        Session session= sessionFactory.getCurrentSession();
        //取最后一个
        String hql = "from Notice where category=:category ";
        if (keyword!=null && !keyword.trim().isEmpty()) {
            hql += " and title like '%"+keyword.trim()+"%' ";
        }
        hql += "  order by id desc ";

        List<Notice> noticeList = (List<Notice>)session.createQuery(hql)
                .setParameter("category", announce)
                .setFirstResult(page.getFirstRecPos().intValue()).setMaxResults(page.getPerPageRows()).list();
        return new PaginationData(page,noticeList,null);
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
