package com.hgd.laboratory.service.impl;


import com.hgd.laboratory.dto.AppCase;
import com.hgd.laboratory.exception.IncompleteDataException;
import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.ReservationRulesService;
import com.hgd.laboratory.service.ResourceGeneratingService;
import com.hgd.laboratory.util.SpringUils;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AnAppointmentServiceImpl implements AnAppointmentService {
    private SessionFactory sessionFactory;
    @Autowired
    private ReservationRulesService reservationRulesService;


    @Override
    @Transactional
    @SuppressWarnings( "unchecked" )
    public List<AppCase> query(String term,Date appDate,String labId) {
        List<AppCase> appCaseList = null;
        ResourceGeneratingService resourceGeneratingService =(ResourceGeneratingService)
                SpringUils.getBean( "resourceGeneratingService" );
        try {

            Date today = new Date();
            assert resourceGeneratingService != null;
            if (!resourceGeneratingService.todayResGenTaskIsCompleted(today)) {
                resourceGeneratingService.generatingResFor( today,term );
            }
        } catch (OpenPlanExpressinException e) {
            e.printStackTrace();
        } catch (IncompleteDataException e) {
            e.printStackTrace();
        } catch (InvalidDateInTerm invalidDateInTerm) {
            invalidDateInTerm.printStackTrace();
        }
        final List<OpenRes> openResList = getOpenResList(appDate, labId);

        appCaseList =mockDefaultAppCases( labId );
        for (OpenRes openRes:openResList ) {
            int index = openRes.getTimeSlot() -1;
            AppCase app  = appCaseList.get( index );
            app.setOpen( true );
            app.setRest( openRes.getResRest() );
        }
        if (!reservationRulesService.obeyRule(appDate,new Date())) {
            for (int i=0;i<appCaseList.size();i++) appCaseList.get(i).setIgnored(true);
        }
        return appCaseList;
    }

    @Override
    public String queryDateInfo(Date appDate) throws InvalidDateInTerm {
        //获取第几周，星期几的信息
        int weekNo = TermUtil.calWeekOfTerm(appDate, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday());
        int dayOfWeek = TermUtil.calWeekDay(appDate);

        return String.format("第%s周，星期%s",weekNo,dayOfWeek);
    }

    @Override
    @Transactional
    @SuppressWarnings( "unchecked" )
    public List<OpenRes> getOpenResList(Date appDate, String labId) {
        String hql ="from OpenRes where openDate =?1 and labId=?2";
        Session session = sessionFactory.getCurrentSession();
        return (List<OpenRes>) session.createQuery( hql )
                .setParameter( 1,appDate )
                .setParameter( 2,labId )
                .list();
    }

    private List<AppCase> mockDefaultAppCases(String labId) {
        List<AppCase> appCaseList = new ArrayList<>();
        for (int timeslot = 1; timeslot <= LabService.TIMESLOTNUM; timeslot++) {
            appCaseList.add( new AppCase( labId,timeslot,false,false,0 ));
        }
        return appCaseList;
    }

    private List<AppCase> mockAppCases() {
        List<AppCase> appCaseList = new ArrayList<>();
        String labId = "6312";
        appCaseList.add( new AppCase( labId,1,true,false ,34) );
        appCaseList.add( new AppCase( labId,2,true,false ,0) );
        appCaseList.add( new AppCase( labId,3,true,false ,34) );
        appCaseList.add( new AppCase( labId,4,false,false ,45) );
        appCaseList.add( new AppCase( labId,5,true,false ,45) );
        appCaseList.add( new AppCase( labId,6,true,false ,34) );
        appCaseList.add( new AppCase( labId,7,true,false ,23) );
        return appCaseList;
    }

    @Override
    @Transactional
    public boolean applySubmit(ApplyForLab applyForLab) throws Exception {
        Session session = this.sessionFactory.getCurrentSession();
        synchronized (this) {

            String labId = applyForLab.getLabId();
            java.sql.Date applyDate = new java.sql.Date( applyForLab.getApplyTime().getTime() );

            for (int timeslot =applyForLab.getTimeslotStart();timeslot<=applyForLab.getTimeslotFinish();timeslot++) {
                String hql = "from OpenRes where openDate=?1 and timeSlot=?2 and labId=?3";
                OpenRes openRes = (OpenRes) session.createQuery( hql ).setParameter( 1,applyDate )
                        .setParameter( 2,timeslot )
                        .setParameter( 3,labId )
                        .uniqueResult();

                if (openRes == null) {
                    //初始化过程-----此过程不应出现。因为没有资源，用户如何请求此资源
                    throw new Exception( "不合法申请。" );
                }
                if (openRes.getResRest() > 0) {
                    String appNewCode = openRes.getCurrentAvailApplyCode();
                    applyForLab.setApplcatioCode( appNewCode );
                    session.save( applyForLab );

                    updateRescouce( openRes );
                    session.save( openRes );

                } else {
                    return false;
                }

            }


        }
        return true;
    }

    private void updateRescouce(OpenRes openRes) {
        openRes.setResConsumed( openRes.getResConsumed() + 1 );
        openRes.setResRest( openRes.getResRest() - 1 );
        openRes.setCurrentAvailApplyCode( addOne( openRes.getCurrentAvailApplyCode() ) );
    }

    public String addOne(String currentAvailApplyCode) {
        //YYYYMMDD#<labId>#<节次=1..9>001
        //"20180830#6312#3002"
        String[] parts = currentAvailApplyCode.split( "#" );
        Integer code = Integer.parseInt( parts[2] );
        code++;

        return parts[0]+"#"+parts[1]+  String.format( "#%04d",code );

    }

    @Override
    @Transactional(readOnly = true)
    public boolean isRepeatedApplication(ApplyForLab applyForLab) {
        //如果是重复申请，返回真
        Session session =sessionFactory.getCurrentSession();
        int t0 =applyForLab.getTimeslotStart();
        int t1 =applyForLab.getTimeslotFinish();
        for (int tt = t0;tt<=t1;tt++) {
//            final String hql = "from ApplyForLab where userId=?1 and applyTime=?2 and labId=?3 and ?4>=timeslotStart and ?5<=timeslotFinish and (state=0 or state=1 or state=2)";
            final String hql = "from ApplyForLab where userId=?1 and applyTime=?2 and labId=?3 and ?4>=timeslotStart and ?5<=timeslotFinish and (state=0 or state=2)";
            List list = session.createQuery( hql )
                    .setParameter(1, applyForLab.getUserId() )
                    .setParameter( 2,applyForLab.getApplyTime() )
                    .setParameter( 3,applyForLab.getLabId() )
                    .setParameter( 4,tt )
                    .setParameter( 5,tt )
                    .list();
            if (list!=null && list.size()>=1)
                return true;
        }
        return false;
    }

    @Override
    @Transactional(readOnly = true)
    @SuppressWarnings( "unchecked" )
    public List<String> getSoftwareList(String labId) {
        Session session = sessionFactory.getCurrentSession();

        Laboratory labor = session.get( Laboratory.class,labId );
        SoftwareConfig softwareConfig =(SoftwareConfig) session.get(SoftwareConfig.class,labor.getInstallSoft());
        List<SoftwareDetails> softwareList = (List<SoftwareDetails>) session.createQuery( "from SoftwareDetails where softwareConfigByConfigid=?1" )
                .setParameter( 1,softwareConfig )
                .list();
        List<String> softList = new ArrayList<>(  );
        for (SoftwareDetails details:softwareList ) {
            softList.add( details.getSofwareName() );
        }
        return softList;
    }

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
