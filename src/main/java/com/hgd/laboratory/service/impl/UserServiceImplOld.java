package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dao.UserDao;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.UserService;
import org.springframework.transaction.annotation.Transactional;

public class UserServiceImplOld implements UserService {
    //对象注入
    private UserDao userDao;
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    //检索用户
    @Override
    public User checkUser(String id, String password, String role){
        return userDao.checkLogin(id,password,role);
    }
    //查找用户
    @Override
    public User findOne(String id) {
        return userDao.findOne(id);
    }

    //更新用户信息（修改密码）
    @Override
    public void update(User user) {
        userDao.update(user);
    }
}
