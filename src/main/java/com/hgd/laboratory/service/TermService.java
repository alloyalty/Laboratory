package com.hgd.laboratory.service;

import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;

import java.sql.Date;
import java.util.Map;

public interface TermService {
    boolean init(String year, Integer term, Integer weeksOfTerm, Date dayOfFirstWeekMonday);

    boolean termIsRunning();

    boolean closure();

    boolean clearNewTermData(String termStr);

    public static void regLoginStatus(Map<String, Object> sessionMap, User user) {
        Term currentTermObj = TermUtil.getCurrentTerm();
        String termStr = "";
        if (currentTermObj != null)
            termStr = currentTermObj.getYear() + "-" + currentTermObj.getTerm();

        sessionMap.put("user", user);
        sessionMap.put("currentTermObj", currentTermObj);
        sessionMap.put("currentTerm", termStr);
    }
}

