package com.hgd.laboratory.service;
import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.FaultRepair;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;

import java.util.List;

public interface FaultRepairService {
    /*
    “可用/正常、故障”
     */
       int RECOVERY = 0;   //可预约
       int SUBSCRIBED = 1;  //已预约
       int FAULT = 2;       //故障

    /*
       对于任一申报维修记录而言，有三个状态：
           故障状态FAULT--------申报时填入的状态
           已经修复RECOVERY-----维修完成，且设备已经可以正常工作。
        */
    boolean fillFault(FaultRepair fmrec);                       //填写报修
    FaultRepair updateMaintance(int id,String maintenceRecs);      //更新维修记录,id 为key
     PaginationData<FaultRepair> queryStatusFaultList(Page pae);                  //查找全部未修设备
    public List<FaultRepair> queryStatusFaultListByLid(String lid);
    public Session getCurrentSession();
    public boolean isValid(String lid,String fid);


    Long queryStatusFaultListTotalRows();
}
