package com.hgd.laboratory.service;

import com.hgd.laboratory.exception.IncompleteDataException;
import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import org.hibernate.Session;

import java.util.Date;

public interface ResourceGeneratingService {
    //系统在刚刚进入新的一天之时，调度执行资 源生成算法
    void resGeneratingPer24Hours(String term);

    //立即执行
    void resGeneratingImmediate(String term);
    //检查指定日期是否已经运行过资源生成
    public boolean todayResGenTaskIsCompleted(Date today);
    //计算初始可用的申请码号
    String initialAvailApplyCode(String labId,java.sql.Date applyTime,Integer timeslot);

    int calResTotalNum(String labId,Session session);
    //生成指定日期的申请资源
    public void generatingResFor(Date today,String term) throws OpenPlanExpressinException, IncompleteDataException, InvalidDateInTerm;

    public void generateResForLab(String term, Session session, Date date, Integer dayOfWeek, Integer weekOfTerm, String labId, final int resourceTotalNum) throws OpenPlanExpressinException;

}
