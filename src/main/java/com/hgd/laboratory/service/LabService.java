package com.hgd.laboratory.service;

import org.hibernate.Session;

public interface LabService {
    int TIMESLOTNUM =7;

    //SAVE--Insert, UPDATE--Update
    public static  final int SAVE = 0;
    public static  final int UPDATE = 1;


    //草稿， 发布
    int SCRATCH =0;
    int PUBLISH =1;
}
