package com.hgd.laboratory.service;

import java.util.Date;
import java.util.List;

public interface ReservationRulesService {
    /*
            未来哪些天资源可见
            currentDate是 20180801
            可见天是20180803,20180804,20180805

         */
    List<Date> visibleResDates(Date currDate);

    boolean obeyRule(Date specDate, Date currDate);
    int getDaysInAdvance();
    int getForUseDays();
}
