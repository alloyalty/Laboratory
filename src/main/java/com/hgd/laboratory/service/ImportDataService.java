package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.ImportResult;
import com.hgd.laboratory.po.Facility;
import com.hgd.laboratory.po.Laboratory;
import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.po.Teacher;

import java.io.File;
import java.io.IOException;

public interface ImportDataService {
    <T> ImportResult importData(File file, Class<T> tClass, final String ext) throws IOException;
    void importFacility(Facility facility);
    void importStudent(Student student);
    void importTeacher(Teacher teacher);
    void importLaboratory(Laboratory laboratory);
    Integer queryConfigId(String configName);

}
