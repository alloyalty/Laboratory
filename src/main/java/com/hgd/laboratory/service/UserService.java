package com.hgd.laboratory.service;

import com.hgd.laboratory.po.User;

public interface UserService {
    //检索用户
    User checkUser(String id, String password, String role);

    //查找用户
    User findOne(String id);

    //更新用户信息（修改密码）
    void update(User user);

}
