package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.AppCase;
import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.OpenRes;

import java.util.Date;
import java.util.List;

public interface AnAppointmentService {
    //申请状态=ENUM(''待审核'', ''未通过'', ''通过未使用'', ''通过已使用'')

    int WAITFOR_AUDIT=0;
    int AUDIT_NOPASS =1;
    int AUDITPASS_UNUSE=2;
    int AUDITPASS_USED=3;
    int CANCEL_APPLY =4;

    List<AppCase> query(String term,Date appDate,String labId);

    boolean applySubmit(ApplyForLab applyForLab) throws Exception;

    List<String> getSoftwareList(String labId);

    boolean isRepeatedApplication(ApplyForLab applyForLab);
    public List<OpenRes> getOpenResList(Date appDate, String labId);

    String queryDateInfo(Date appDate) throws InvalidDateInTerm;
}
