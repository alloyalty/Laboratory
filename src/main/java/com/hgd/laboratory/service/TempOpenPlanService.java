package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.dto.TempOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.TempOpenPlan;
import org.hibernate.Session;

import java.util.List;
import java.util.Map;

public interface TempOpenPlanService {
    int SCARTCH =0;
    int PUBLISH =1;

    //取得指定学期内，所有实验室的临时开放计划
    List<TempOpenPlan> queryAllTempOpenPlans(String term);
    Session getCurrentSession();
    TempOpenPlanTuple findTempOpenPlan(int srch_tempPlanId)  throws OpenPlanDataIncompleteException;
    TempOpenPlanTuple findTempOpenPlan(String term,String srch_labId,int srch_weekOfTerm) throws  OpenPlanDataIncompleteException ;

    TempOpenPlanTuple createBlankTempOpenPlan(String term,String labId,int weekOfTerm,LabOpenPlanTuple labOpenPlanTuple)
            throws OpenPlanExpressinException;

    void saveUpdate(TempOpenPlan top,Map<String,TempItem> topDetailMap,Integer status,int method) throws OpenPlanExpressinException;
    public void delete(Integer id);
    public void deleteAllRec();

    TempOpenPlan findOneTempOpenPlan(int tempPlanId);
    void publish(TempOpenPlan tempOpenPlan,Map<String, TempItem> tempOpenPlanDetailMap) throws OpenPlanExpressinException, OpenPlanDataIncompleteException;
    List<TempOpenPlan> findTempOpenPlanByLabId(String term,String labId);

}
