package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.util.Page;

import java.sql.Timestamp;


public interface StudentExpService {
    ApplyForLab queryUniqueApply(String sId, Timestamp today, int timeslot, String labId, String term);

    String queryTheMachineId(String fId, Timestamp today, int timeslot, String labId,  String term);

    void confirmApply(String fId, Long applyNumber);

    PaginationData<ApplyForLab> queryConfirmedApply(String labId, Timestamp today, int timeslot, String term, Page page);

    boolean queryExistFaciId(String labId, String fId);
}
