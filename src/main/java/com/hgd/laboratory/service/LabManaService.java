package com.hgd.laboratory.service;

import com.hgd.laboratory.po.LabTeacher;
import com.hgd.laboratory.po.Teacher;

import java.util.List;

public interface LabManaService {
    List<LabTeacher> query(Integer delflag);

    boolean addLabManager(String code);

    void delLabManager(int code);

    boolean existInDuty(String code);

    List<Teacher> queryByNameOrId(String teaNameOrId);
}
