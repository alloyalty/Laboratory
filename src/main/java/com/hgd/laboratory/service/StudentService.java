package com.hgd.laboratory.service;


import com.hgd.laboratory.dao.StudentDao;
import com.hgd.laboratory.po.Student;

import javax.transaction.Transactional;

@Transactional
public class StudentService {
    //对象注入
    private StudentDao studentDao;

    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }
    //查找用户
    public Student findOne(String id) {
        return studentDao.findOne(id);
    }
    //发送邮箱验证码
    public boolean sendEmail(String email,String codes){
         return studentDao.sendEmail(email,codes);
    }
    //绑定邮箱
    public boolean bindEmail(String email,String codes){
        return studentDao.bandEmail(email,codes);
    }

    //更新用户数据（修改邮箱）
    public void update(Student student) {
        studentDao.update(student);
    }


}
