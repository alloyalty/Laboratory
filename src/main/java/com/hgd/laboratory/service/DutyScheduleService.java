package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.DutyDetails;
import com.hgd.laboratory.dto.DutyScheduleTuple;
import com.hgd.laboratory.util.Consistencies;

import java.util.Map;

public interface DutyScheduleService {
    Map<String, DutyDetails> getCurrentDutyDatas(String term, String labId);

    Consistencies checkConsistency(String term, String labId, Map<String, DutyDetails> dutyDatas);

    void updateDutyDatas(String term, String labId, Map<String, DutyDetails> dutyDatas);

    DutyScheduleTuple findDutySchedule(String term, String srch_labId);

    DutyScheduleTuple createBlankDutySchedule(String term, String srch_labId);
}
