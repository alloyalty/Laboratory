package com.hgd.laboratory.service;

import org.apache.poi.ss.usermodel.Row;

public interface DataTranService {
    <T> boolean buildObjFromRow(T t, Row row);
    <T> boolean importData(T t);
    <T> boolean isTitleRow(Row row, Class<T> tClass);
}
