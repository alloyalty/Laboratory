package com.hgd.laboratory.service;

import com.hgd.laboratory.dto.ApplyForLabAudit;
import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.util.AuditQueryReqParameter;
import com.hgd.laboratory.util.Page;

import java.util.Date;
import java.util.List;

public interface AnAppointmentQueryService {

    //以分页方式查询历史预约
    PaginationData<ApplyForLab> queryHistory(Date currDateTime, int tx, Page page);
    //以分页方式查询当前预约
    PaginationData<ApplyForLab> queryCurrent(Date currDateTime, int tx, Page page);
    //以分页方式查询所有待审定的预约
    PaginationData<ApplyForLabAudit> queryWaitForAudit(AuditQueryReqParameter auditQueryReqParameter, Page page, java.sql.Date today);
    //查询指定的IdList的预约集合
    List<ApplyForLab> queryApplyList(List<Long> applyIdList);

    //取消指定的IdList的集合预约
    boolean cancelAll(List<Long> applyIdList);
    //审核通过指定的预约列表
    boolean auditAll(List<Long> applyIdList, int mark);
}
