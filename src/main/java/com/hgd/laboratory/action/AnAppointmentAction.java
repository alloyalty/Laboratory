package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.AppCase;
import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.service.LaboratoryService;
import com.hgd.laboratory.service.ReservationRulesService;
import com.hgd.laboratory.util.ReservationRules;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

public class AnAppointmentAction extends ActionSupport {
    //自动插入
    private AnAppointmentService anAppointmentService;
    private LaboratoryService laboratoryService;
    private ReservationRulesService reservationRulesService;


    //action query input/output
    private Date appDate;
    private String labId;

    //action query output
    private String outlineOfLaboratory;
    private List<AppCase> appCaseList;


    //query method
    public String query() {
        String term = (String) ActionContext.getContext().getSession().get( "currentTerm" );

        //实验室概况
        this.outlineOfLaboratory = this.laboratoryService.outlineOfLab( this.labId );
        //输入日期的信息：第几周，星期几
        String dateInfo = null;
        String appDuration ="";
        try {
            appDuration = transString(this.reservationRulesService.visibleResDates(new Date()),reservationRulesService.getForUseDays());
            dateInfo = anAppointmentService.queryDateInfo(appDate);
            ActionContext.getContext().put( "dateInfo",dateInfo+appDuration );
        } catch (InvalidDateInTerm invalidDateInTerm) {
            invalidDateInTerm.printStackTrace();
        }
        //开放情况
        this.appCaseList = anAppointmentService.query( term,appDate,labId );
        //可用软件列表
        this.chkboxDataList = anAppointmentService.getSoftwareList( labId );
        if (this.appCaseList == null || this.appCaseList.size() == 0) {
            ActionContext.getContext().put( "tip","未找到预约机位，" );
            return "mess";
        } else
            return INPUT;
    }

    private String transString(List<Date> visibleResDates,int d) {
        StringBuffer sb  = new StringBuffer();
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
        sb.append("只能预约");
        sb.append(d);
        sb.append("天时间: ");
        for (int i=0;i<visibleResDates.size();i++) {
            String s = sdf.format(visibleResDates.get(i));
            if (i!=0) s= "、"+s;
            sb.append(s);
        }
        return new String (sb);
    }


    //预约申请提交action--命名为 applySubmit
    //输入输出 appDate,labId         上面已经定义

    //输入: timeslotStart,timeslotFinish
    //输出：applyList
    //    用户预约请求参数  《----》applyList
    //系统依据applyList,生成表单的请求参数
    //过程parseApply,由request获取并生成 applyList

    //action_applySubmit:input
    private int timeslotStart, timeslotFinish;
    //    private Date appDate;
//    private String labId;
    private String courseName;
    private String expName;
    private String hardwares;
    private List<String> chkboxDataList = null;
    private String[] softwares;

    private String softwaresStr;

    public String applySubmit() throws Exception {
        String term = (String) ActionContext.getContext().getSession().get( "currentTerm" );
        if (this.labId == null || this.appDate == null) return ERROR;
        String ret = parseApplyRequest();
        if (prepareDataForLogicalView( term,ret )) return "input";

        java.sql.Date applyTime = new java.sql.Date( this.appDate.getTime() );
        User user = ((User) ActionContext.getContext().getSession().get( "user" ));
        String userId = user.getId();
        String userName = user.getName();

        ApplyForLab applyForLab = new ApplyForLab( applyTime,userId,userName,labId,"",courseName,expName,hardwares,softwaresStr,
                timeslotStart,timeslotFinish,term );
        if (anAppointmentService.isRepeatedApplication( applyForLab )) {
            this.addActionError( "----重复申请无效。" );
            prepareDataForLogicalView( term,"input" );
            return "input";
        }
        if (this.anAppointmentService.applySubmit( applyForLab )) {
            ActionContext.getContext().put( "tip", getMess(applyForLab));
            return SUCCESS;
        } else {
            if (prepareDataForLogicalView( term,"input" )) return "input";
        }
        ;

        return "input";

    }

    private String getMess(ApplyForLab applyForLab) {
        StringBuffer buff =new StringBuffer();
        buff.append("<p>预约申请成功:</p>");

        String str = "申请日期:"+new SimpleDateFormat("yyyy-MM-dd").format(applyForLab.getApplyTime());
        appendParap(buff, str);
        appendParap(buff,"实验室:"+applyForLab.getLabId());
        if (applyForLab.getTimeslotStart() == applyForLab.getTimeslotFinish())
            str = ""+applyForLab.getTimeslotStart();
        else
            str = ""+applyForLab.getTimeslotStart()+"-"+applyForLab.getTimeslotFinish();
        appendParap(buff,"申请时段:"+str);
        appendParap(buff,"申请人："+applyForLab.getUserName());
        return new String(buff);

    }

    private void appendParap(StringBuffer buff, String str) {
        buff.append("<p>");
        buff.append(str);
        buff.append("</p>");
    }

    private boolean prepareDataForLogicalView(String term,String logicalView) {
        if (logicalView.equals( "input" )) {
            //数据准备
            this.outlineOfLaboratory = this.laboratoryService.outlineOfLab( this.labId );
            this.appCaseList = anAppointmentService.query( term,appDate,labId );
            this.chkboxDataList = anAppointmentService.getSoftwareList( this.labId );
            //数据回填
            backFill( this.appCaseList );
            //todo 所有出错有待处理，在页面上未显示出来
            return true;
        }
        return false;
    }

    private void backFill(List<AppCase> appCaseList) {
        HttpServletRequest request = ServletActionContext.getRequest();

        String paraName = "lab" + this.labId;
        String[] paraValues = (String[]) request.getParameterValues( paraName );

        if (paraValues==null ) return;
        List<Integer> intValues = new ArrayList<>();
        for (int i = 0; i < paraValues.length; i++) {
            intValues.add( Integer.parseInt( paraValues[i] ) );
        }
        Collections.sort( intValues );

        for (int i = 0; i < appCaseList.size(); i++) {
            AppCase appCase = appCaseList.get( i );
            if (intValues.contains( (Integer) appCase.getTimeSlot() )) {
                appCase.setSubscribed( true );

            }

        }

    }

    private boolean checkNotValid() {
        //todo
        return false;
    }

    private String parseApplyRequest() {
        /*
        参数如果表示的问题
        使用命名空间表示 lab<实验室id><时段id><开放否>   <预约否>
        lab<实验室id><时段id><开放否> 用来表示参数的名字空间，后者表示参数的值
        中间用分隔符分开，分隔符使用@@@表示.
        比如说 （6312，1，1，1）表示成
        <input name = "lab6312", value="1" type=checkbox />
         */
        HttpServletRequest request = ServletActionContext.getRequest();
        String softArr[] = request.getParameterValues( "softwares" );
        this.softwaresStr = acqSoftwaresStr( softArr );

        String paraName = "lab" + this.labId;
        String[] paraValues = (String[]) request.getParameterValues( paraName );
        if (paraValues==null){
            this.addActionError("未填写时段。");
            return  "input";
        }
        List<Integer> intValues = new ArrayList<>();
        for (int i = 0; i < paraValues.length; i++) {
            intValues.add( Integer.parseInt( paraValues[i] ) );
        }
        Collections.sort( intValues );


        int len = paraValues.length;

        String returnValue = "";
        if (len == 0) {
            returnValue = "input";
        } else if (len == 1) {
            this.timeslotStart = intValues.get( 0 );
            this.timeslotFinish = this.timeslotStart;
            returnValue = "message";

        } else {
            boolean isContinueSerial = true;
            for (int i = 0; i < intValues.size() - 1; i++) {
                if (intValues.get( i ) != intValues.get( i + 1 ) - 1) {
                    isContinueSerial = false;
                    break;
                }
            }
            if (isContinueSerial) {
                this.timeslotStart = intValues.get( 0 );
                this.timeslotFinish = intValues.get( intValues.size() - 1 );
                returnValue = "message";
            } else {
                this.addActionError("所选时段不连续。");
                returnValue = "input";
            }
        }
        return returnValue;


    }

    private String acqSoftwaresStr(String[] softArr) {
        if (softArr==null) return "";
        StringBuffer sbuff = new StringBuffer();
        for (int i1 = 0; i1 < softArr.length; i1++) {
            if (i1 != 0) {
                sbuff.append( "###" );
            }
            sbuff.append( softArr[i1] );
        }
        return new String( sbuff );
    }


    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getHardwares() {
        return hardwares;
    }

    public void setHardwares(String hardwares) {
        this.hardwares = hardwares;
    }


    public String getExpName() {
        return expName;
    }

    public void setExpName(String expName) {
        this.expName = expName;
    }

    public int getTimeslotStart() {
        return timeslotStart;
    }

    public void setTimeslotStart(int timeslotStart) {
        this.timeslotStart = timeslotStart;
    }

    public int getTimeslotFinish() {
        return timeslotFinish;
    }

    public void setTimeslotFinish(int timeslotFinish) {
        this.timeslotFinish = timeslotFinish;
    }

    public LaboratoryService getLaboratoryService() {
        return laboratoryService;
    }

    public void setLaboratoryService(LaboratoryService laboratoryService) {
        this.laboratoryService = laboratoryService;
    }

    public String getOutlineOfLaboratory() {
        return outlineOfLaboratory;
    }

    public void setOutlineOfLaboratory(String outlineOfLaboratory) {
        this.outlineOfLaboratory = outlineOfLaboratory;
    }

    public List<AppCase> getAppCaseList() {
        return appCaseList;
    }

    public void setAppCaseList(List<AppCase> appCaseList) {
        this.appCaseList = appCaseList;
    }

    public AnAppointmentService getAnAppointmentService() {
        return anAppointmentService;
    }

    public void setAnAppointmentService(AnAppointmentService anAppointmentService) {
        this.anAppointmentService = anAppointmentService;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String[] getSoftwares() {
        return softwares;
    }

    public void setSoftwares(String[] softwares) {
        this.softwares = softwares;
    }

    public List<String> getChkboxDataList() {
        return chkboxDataList;
    }

    public void setChkboxDataList(List<String> chkboxDataList) {
        this.chkboxDataList = chkboxDataList;
    }

    public String getSoftwaresStr() {
        return softwaresStr;
    }

    public void setSoftwaresStr(String softwaresStr) {
        this.softwaresStr = softwaresStr;
    }

    public ReservationRulesService getReservationRulesService() {
        return reservationRulesService;
    }

    public void setReservationRulesService(ReservationRulesService reservationRulesService) {
        this.reservationRulesService = reservationRulesService;
    }
}
