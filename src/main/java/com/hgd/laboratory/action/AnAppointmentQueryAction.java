package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.ApplyForLabAudit;
import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.AnAppointmentQueryService;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.util.ApplyUtil;
import com.hgd.laboratory.util.AuditQueryReqParameter;
import com.hgd.laboratory.util.Page;
import com.hgd.laboratory.util.TimeSlot;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import java.util.Date;
import java.util.List;

public class AnAppointmentQueryAction extends ActionSupport {
    private AnAppointmentQueryService anAppointmentQueryService;

    //请求查询并显示历史预约的数据，默认为一页2条数据，显示第1页
    public String queryHistory() {
        String pageNoStr = ServletActionContext.getRequest().getParameter( "pageNo" );
        String perPageRowsStr = ServletActionContext.getRequest().getParameter( "perPageRows" );
        Page page = acqQueryHistoryPage( pageNoStr,perPageRowsStr );

        Date currDateTime = new Date();
        int tx = calTimeslot( currDateTime );
        PaginationData<ApplyForLab> paginationData = anAppointmentQueryService.queryHistory( currDateTime,tx,page );
        ActionContext.getContext().put( "paginationData",paginationData );
        return INPUT;

    }


    //请求查询并显示当前预约的数据，默认为一页2条数据，显示第1页
    public String queryCurrent() {
        String pageNoStr = ServletActionContext.getRequest().getParameter( "pageNo" );
        String perPageRowsStr = ServletActionContext.getRequest().getParameter( "perPageRows" );
        Page page = acqQueryCurrentPage( pageNoStr,perPageRowsStr );


        final Date currDateTime = new Date();
        int tx = calTimeslot( currDateTime );
        PaginationData<ApplyForLab> paginationData = anAppointmentQueryService.queryCurrent( currDateTime,tx,page );
        ActionContext.getContext().put( "paginationData",paginationData );
        return INPUT;

    }


    private Page acqQueryHistoryPage(String pageNoStr,String perPageRowsStr) {
        int pageNo = 1;
        int perPageRows = 4;
        return getPage( pageNoStr,perPageRowsStr,pageNo,perPageRows );
    }

    private Page acqQueryCurrentPage(String pageNoStr,String perPageRowsStr) {
        int pageNo = 1;
        int perPageRows = 3;
        return getPage( pageNoStr,perPageRowsStr,pageNo,perPageRows );
    }

    private Page acqAuditPage(String pageNoStr,String perPageRowsStr) {
        int pageNo = 1;
        int perPageRows = 10;
        return getPage( pageNoStr,perPageRowsStr,pageNo,perPageRows );
    }

    private Page getPage(String pageNoStr,String perPageRowsStr,long  pageNo,int perPageRows) {
        try {
            pageNo = Long.parseLong( pageNoStr );
        } catch (NumberFormatException ignored) {
        }
        try {
            perPageRows = Integer.parseInt( perPageRowsStr );
        } catch (NumberFormatException ignored) {
        }
        Page page = new Page( perPageRows );
        page.setCurrentPage( pageNo );
        return page;
    }

    //取消当前预约
    public String cancel() {
        //使用页面异步请的方式：why.如果不成功，则直接显示信息，不必要去准备整个页面显示bean
        String[] applyIds_str = ServletActionContext.getRequest().getParameterValues( "applyNumber" );
        List<Long> applyIdList = ApplyUtil.parseApplyIds( applyIds_str );
        boolean succ = anAppointmentQueryService.cancelAll( applyIdList );

        //与查询当前预约相同
       return  queryCurrent();
//        List<ApplyForLab> applyForLabList = anAppointmentQueryService.queryApplyList( applyIdList );
//        ActionContext.getContext().put( "applyList",applyForLabList );
//        return succ ? "cancel_succ" : "cancel_fail";


    }

    //查询待审的预约申请
    //输入参数: labId, applyDate， timeslot
    //如果输入为空，或者为null，则表示不考虑此项条件的限制
    private String labId=null;
    private java.util.Date applyDate=null;
    private Integer timeslot=null;
    private Integer state =null;

    private Integer pageNo;

    public String queryWaitForAudit() {
        String pageNoStr = ServletActionContext.getRequest().getParameter( "pageNo" );
        String perPageRowsStr = ServletActionContext.getRequest().getParameter( "perPageRows" );
        Page page = acqAuditPage( pageNoStr,perPageRowsStr );
        AuditQueryReqParameter reqParameter = new AuditQueryReqParameter(labId, applyDate, timeslot, state);

        java.sql.Date today =  new java.sql.Date(new Date().getTime());
        PaginationData<ApplyForLabAudit> paginationData = anAppointmentQueryService.queryWaitForAudit(reqParameter, page,today);
        ActionContext.getContext().put("paginationData",paginationData);
        return INPUT;
    }

    public String auditPass() {
        return audit(AnAppointmentService.AUDITPASS_UNUSE);
    }

    public String auditNotPass() {
        return audit(AnAppointmentService.AUDIT_NOPASS);
    }

    public String audit(int mode) {
        String[] applyIds_str = ServletActionContext.getRequest().getParameterValues( "applyNumber" );
        List<Long> applyIdList = ApplyUtil.parseApplyIds( applyIds_str );
        //AnAppointmentService.AUDITPASS_UNUSE
        boolean succ = anAppointmentQueryService.auditAll( applyIdList,mode );

        //重显第一页，审核时需求每页显示10条记录，应定义为常数
        queryWaitForAudit();
        return INPUT;
    }


    private int calTimeslot(Date currDateTime) {
        return  TimeSlot.getInstance().calTimeslot( currDateTime );
    }

    public AnAppointmentQueryService getAnAppointmentQueryService() {
        return anAppointmentQueryService;
    }

    public void setAnAppointmentQueryService(AnAppointmentQueryService anAppointmentQueryService) {
        this.anAppointmentQueryService = anAppointmentQueryService;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public Date getApplyDate() {
        return applyDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    public Integer getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(Integer timeslot) {
        this.timeslot = timeslot;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }
}
