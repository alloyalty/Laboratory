package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.AnnounceService;
import com.hgd.laboratory.util.Page;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;



import java.util.List;

public class AnnounceAction extends ActionSupport {
    private AnnounceService announceService;

    public String execute() {
        Long id = announceService.getMainArticleId();
        Notice item=null;
        if (id!=null)
            item = announceService.findOne(id);
        ActionContext.getContext().put("item",item);

        List<Notice> announceList = announceService.query("announce",7);
        List<Notice> rulesList = announceService.query("rules",7);
        ActionContext.getContext().put("announceList",announceList);
        ActionContext.getContext().put("rulesList",rulesList);

        return SUCCESS;
    }

    private Long id=null;
    public String view() {
        if (id==null)
            return SUCCESS;
        Notice item = announceService.findOne(id);
        ActionContext.getContext().put("item",item);
        return  SUCCESS;
    }

    private Long pageNo=null;
    private String keyword=null;
    public String moreAnnounces() {
        Page page = new Page(20);
        if (pageNo==null) pageNo=1L;
        page.setCurrentPage(pageNo);
        Long totalRows = announceService.queryTotalRows("announce",keyword);
        page.setTotalRows(totalRows);
        PaginationData paginationData=  announceService.query("announce",keyword,page);
        ActionContext.getContext().put("paginationData",paginationData);
        return SUCCESS;
    }

    private String category="announce";
    public String pageQuery() {
        if ("announce".equals(category))
            moreAnnounces();
        else
            moreRules();
        return SUCCESS;
    }
    public String moreRules() {
        Page page = new Page(20);
        if (pageNo==null) pageNo=1L;
        page.setCurrentPage(pageNo);
        Long totalRows = announceService.queryTotalRows("rules",keyword);
        page.setTotalRows(totalRows);
        PaginationData paginationData= announceService.query("rules",keyword,page);
        ActionContext.getContext().put("paginationData",paginationData);
        return SUCCESS;
    }


    public AnnounceService getAnnounceService() {
        return announceService;
    }

    public void setAnnounceService(AnnounceService announceService) {
        this.announceService = announceService;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
