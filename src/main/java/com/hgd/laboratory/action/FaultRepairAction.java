package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.FaultRepair;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.FaultRepairService;
import com.hgd.laboratory.util.Debug;
import com.hgd.laboratory.util.Page;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

public class FaultRepairAction  extends ActionSupport implements ModelDriven<FaultRepair> {

    //故障填报 lid,fid,faultDFesp
    //维修查询 lid
    //维修填报 id,maintenceRecs

    public FaultRepairAction() {
        this.faultRepair = new FaultRepair(  );
    }

    private FaultRepair faultRepair ;
    private List<FaultRepair> faultFillList;

    private FaultRepairService faultRepairService;


    public List<FaultRepair> getFaultFillList() {
        return faultFillList;
    }
    public void setFaultFillList(List<FaultRepair> faultFillList) {
        this.faultFillList = faultFillList;
    }

    public FaultRepair getFaultRepair() {
        return faultRepair;
    }
    public void setFaultRepair(FaultRepair faultRepair) {
        this.faultRepair = faultRepair;
    }

    public FaultRepairService getFaultRepairService() {
        return faultRepairService;
    }
    public void setFaultRepairService(FaultRepairService faultRepairService) {
        this.faultRepairService = faultRepairService;
    }


    @Override
    public FaultRepair getModel() {
        return faultRepair;
    }


    //Action的方法

    /**
     * input:pageNo
     * output:faultFillList，空白FaultRepair
     * @return
     */

    private Long pageNo;

    public String queryPaginationData() {
        Page page = new Page(10);
        if (pageNo==null) pageNo=1L;
        page.setCurrentPage(pageNo);
        Long totalRows = this.faultRepairService.queryStatusFaultListTotalRows();
        page.setTotalRows(totalRows);

        PaginationData<FaultRepair> paginationData=  this.faultRepairService.queryStatusFaultList(page);
        ActionContext.getContext().put("paginationData",paginationData);
        this.faultRepair = new FaultRepair(  );
        return INPUT;
    }

    /**
     * 输入: lid,fid,faultDFesp ,且写入faultRepair 之中
     * 输出：faultRepair,faultFillList
     * @return
     */
    public String fillFaultSubmit() {
        if (validateFillFault()){
            //故障填报form含： lid,fid,faultDFesp
            User user = (User)ActionContext.getContext().getSession().get("user");
            faultRepair.setFillingId(user.getId());
            this.faultRepair.setFstate( FaultRepairService.FAULT );
            Date fillDate = new Date();
            this.faultRepair.setFillingDate( new Timestamp( fillDate.getTime() ) );
            this.faultRepairService.fillFault( this.faultRepair );
            queryPaginationData();
            return INPUT;

        } else {
            this.addActionError("实验室ID或者设备ID不存在。");
            queryPaginationData();
            return INPUT;

        }



    }

    private boolean validateFillFault() {
        return this.faultRepairService.isValid(faultRepair.getLid(), faultRepair.getFid());
    }

    /**
     * 输入:lid
     * 输出:lid, faultFillList
     * @return
     */
    public String queryFaultByLid() {
        //维修查询 lid
        String lid = this.faultRepair.getLid();
        if (lid!=null) {
            Debug.consoleOut(false,lid);
            this.faultFillList = this.faultRepairService.queryStatusFaultListByLid(lid);
            Debug.consoleOut(true,"逻辑视图："+SUCCESS);

        }
        return SUCCESS;

    }


    public String updateMaintance() throws Exception {
        //维修填报 id,maintenceRecs
        /*
        如果有数据提交，则填写维修访记录
         */
        int id = this.faultRepair.getId();
        String maintenceRecs = this.faultRepair.getMaintenceRecs();
        String lid = this.faultRepair.getLid();

        if ("正常".equals(this.faultRepair.getFstate())) {
            Debug.consoleOut(true,this.faultRepair.getMaintenceRecs());


            FaultRepair frrec = this.faultRepairService.updateMaintance(id,maintenceRecs);
            Debug.consoleOut(false,"逻辑视图："+SUCCESS);
            lid = frrec.getLid();
            this.faultFillList = this.faultRepairService.queryStatusFaultListByLid(lid);
        }


        return SUCCESS;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }
}
