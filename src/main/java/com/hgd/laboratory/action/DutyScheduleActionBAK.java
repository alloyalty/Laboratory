package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.DutyDetails;
import com.hgd.laboratory.service.DutyScheduleService;
import com.hgd.laboratory.util.Consistencies;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

public class DutyScheduleActionBAK extends ActionSupport {
    private String labId;
    private DutyScheduleService dutyScheduleService;
    private Map<String, DutyDetails> dutyDatas;

    public String manage() {
        dutyDatas = dutyScheduleService.getCurrentDutyDatas(getCurrentTerm(), labId);
        return "success";

    }

    public String workoutByLabId() {
        return "success";

    }
    public String getCurrentTerm() {
        return (String) ActionContext.getContext().getSession().get("currentTerm");
    }

    private Consistencies consistencies;
    public String checkConsistency() {
        consistencies = dutyScheduleService.checkConsistency(getCurrentTerm(), labId, dutyDatas);
        return "success";

    }
    public String arrangeSave() {
        dutyScheduleService.updateDutyDatas(getCurrentTerm(), labId, dutyDatas);
        return "pauseSave";

    }

    public String view() {
        return "success";

    }

    public String workoutById () {
        return "success";

    }

    public String delete() {
        return "success";
    }
    public Consistencies getConsistencies() {
        return consistencies;
    }

    public void setConsistencies(Consistencies consistencies) {
        this.consistencies = consistencies;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public Map<String, DutyDetails> getDutyDatas() {
        return dutyDatas;
    }

    public void setDutyDatas(Map<String, DutyDetails> dutyDatas) {
        this.dutyDatas = dutyDatas;
    }

    public DutyScheduleService getDutyScheduleService() {
        return dutyScheduleService;
    }

    public void setDutyScheduleService(DutyScheduleService dutyScheduleService) {
        this.dutyScheduleService = dutyScheduleService;
    }
}
