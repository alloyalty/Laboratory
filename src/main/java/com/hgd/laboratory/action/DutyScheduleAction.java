package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.DutyScheduleTuple;
import com.hgd.laboratory.service.DutyScheduleService;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionContext;

public class DutyScheduleAction {
    private DutyScheduleService dutyScheduleService;


    private DutyScheduleTuple dutyScheduleTuple;


    public String manage() {
        return Action.SUCCESS;
    }

    private String srch_labId;

    public String workoutByLabId() {
        //如果通过srch_labID找到 DutyScheduleTuple,则返回
        //否则新建一个空白的DutyScheduleTuple,并且返回
        // 设置 属性dutyScheduleTuple
        try {
            String term = (String) ActionContext.getContext().getSession().get("currentTerm");
            DutyScheduleTuple dutyScheduleTupleTemp = this.dutyScheduleService.findDutySchedule(term, srch_labId);
            if (dutyScheduleTupleTemp == null) {//不存在此计划，需要新建
                dutyScheduleTupleTemp = this.dutyScheduleService.createBlankDutySchedule(term, srch_labId);
            }
            this.setDutyScheduleTuple(dutyScheduleTupleTemp);
        } catch (Exception e) {
            e.printStackTrace();
            return Action.ERROR;
        }
        return Action.SUCCESS;
    }



    public DutyScheduleService getDutyScheduleService() {
        return dutyScheduleService;
    }

    public void setDutyScheduleService(DutyScheduleService dutyScheduleService) {
        this.dutyScheduleService = dutyScheduleService;
    }

    public DutyScheduleTuple getDutyScheduleTuple() {
        return dutyScheduleTuple;
    }

    public void setDutyScheduleTuple(DutyScheduleTuple dutyScheduleTuple) {
        this.dutyScheduleTuple = dutyScheduleTuple;
    }

    public String getSrch_labId() {
        return srch_labId;
    }

    public void setSrch_labId(String srch_labId) {
        this.srch_labId = srch_labId;
    }
}
