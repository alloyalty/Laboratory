package com.hgd.laboratory.action;

import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.FindBackPwdService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

public class FindBackPwdAction extends ActionSupport {
    private FindBackPwdService findBackPwdService;
    //used for sendEmail,prepare
    private String id;

    private String sid;
    private String newPassword;

    public String sendEmail() throws Exception {
        if (id ==null || id.trim().equals("")) {
            this.addFieldError("id", "用户登录名不能为空。 ");
            return "input";
        }

        User user = findBackPwdService.findUser(id);
        if (user==null) {
            this.addFieldError("id", "用户登录名未找到。 ");
            return "input";
        }

        String email = user.getEmaill();
        if (email==null || email.trim().equals("")) {
            this.addFieldError("id", "此用户未绑定邮箱，不能使用邮箱找回密码功能。请向实验室管理员资询。");
            return "input";
        }
        //获取当前用户对象
        //获取url 字符串
        //获取用户email地址
        HttpServletRequest req = ServletActionContext.getRequest();
        findBackPwdService.sendEmail(user,req);
        ActionContext.getContext().put("tip", "找回密码链接已经发送到指定信箱，链接30分钟有效。");
        return "success";
    }


    public String prepare() throws Exception {
        User user = findBackPwdService.findUser(id);
        if (user == null) {
            ActionContext.getContext().put("tip", "链接不正确,是否已经过期了?重新申请吧.");
            return "message";        }
        boolean succ = this.findBackPwdService.checkValid(user,sid);
        if (!succ) {
            ActionContext.getContext().put("tip", "链接不正确,是否已经过期了?重新申请吧.");
            return "message";
        }
        return "success";

    }
    public String validAndchangePwd() throws Exception {
        //提取摘要参数digest
        HttpServletRequest request = ServletActionContext.getRequest();
        String digest = request.getParameter("sid");
        String newPassword = request.getParameter("newPassword");

        if (id ==null || id.trim().equals("")) {
            this.addFieldError("id", "链接不完整,请重新生成。 ");
            return "message";
        }

        User user = findBackPwdService.findUser(id);
        if (user==null) {
            this.addFieldError("id", "链接不完整,请重新生成。 ");
            return "message";
        }

        Timestamp outDate = (Timestamp) user.getOutDate();

        if (outDate.getTime() <= System.currentTimeMillis()) { // 表示已经过期
            ActionContext.getContext().put("tip", "链接已经过期,请重新申请找回密码.");
            return "message";
        }
        //提取用户的id，
        //获取用户的id的对象
        //计算其摘要
        //如果摘要相同，修改密码。更新密钥，使此次url无效。
        boolean succ = findBackPwdService.checkValidAndChangePwd(user,digest,newPassword);
        if (!succ) {
            ActionContext.getContext().put("tip", "链接不正确,是否已经过期了?重新申请吧.");
            return "message";
        }
        return "success";
    }

    public FindBackPwdService getFindBackPwdService() {
        return findBackPwdService;
    }

    public void setFindBackPwdService(FindBackPwdService findBackPwdService) {
        this.findBackPwdService = findBackPwdService;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}

