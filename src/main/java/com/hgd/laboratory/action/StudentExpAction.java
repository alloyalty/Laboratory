package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.StudentExpService;
import com.hgd.laboratory.util.DateUtil;
import com.hgd.laboratory.util.Page;
import com.hgd.laboratory.util.TermUtil;
import com.hgd.laboratory.util.TimeSlot;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import java.sql.Timestamp;


public class StudentExpAction extends ActionSupport {
    private String sId;
    private String fId;
    private String labId;

    private String sfind="1";
    private StudentExpService studentExpService;
    private ApplyForLab applyForLab;

    public String applyExp() {
        java.util.Date date = DateUtil.stdDate(new java.util.Date());
        java.sql.Timestamp today= new java.sql.Timestamp(date.getTime());
        int timeslot= TimeSlot.getInstance().calTimeslot(new java.util.Date());
        String term= TermUtil.getInstamce().getTermStr();

        String pageNoStr = ServletActionContext.getRequest().getParameter( "page" );
        String perPageRowsStr = ServletActionContext.getRequest().getParameter( "perPageRows" );
        Page page = acqPage( pageNoStr,perPageRowsStr );

        if (checkValid(today, timeslot, term)) {
            studentExpService.confirmApply(fId,applyForLab.getApplyNumber());
            PaginationData<ApplyForLab> paginationData =  studentExpService.queryConfirmedApply(labId,today,timeslot,term,page);
            ActionContext.getContext().put("paginationData",paginationData);
            return SUCCESS;
        }
        PaginationData<ApplyForLab> paginationData =  studentExpService.queryConfirmedApply(labId,today,timeslot,term,page);
        ActionContext.getContext().put("paginationData",paginationData);
        return INPUT;

    }
    public String checkStudent() {
        java.util.Date date = DateUtil.stdDate(new java.util.Date());
        java.sql.Timestamp today= new java.sql.Timestamp(date.getTime());
        int timeslot= TimeSlot.getInstance().calTimeslot(new java.util.Date());
        String term= TermUtil.getInstamce().getTermStr();

        applyForLab = studentExpService.queryUniqueApply(sId,today,timeslot,labId,term);
        if (applyForLab==null){
//            this.addFieldError("sId","未找到该学生相对应申请单号。");
            sfind="0";
            return "success";
        }
        return "success";
    }
    private boolean checkValid(Timestamp today, int timeslot, String term) {

        applyForLab = studentExpService.queryUniqueApply(sId,today,timeslot,labId,term);
        if (applyForLab==null){
            this.addFieldError("sId","未找到该学生相对应申请单号。");
            return false;
        }
        ActionContext.getContext().put("applyForLab",applyForLab);
        if (fId.isEmpty()) {
            this.addFieldError("fId","机器编号非空。");
            return  false;
        } else {
            String machineId = studentExpService.queryTheMachineId(fId,today,timeslot,labId,term);
            if (machineId!=null && machineId.equals(fId)) {
                this.addFieldError("fId","机器编号已被占用。");
                return false;
            }
        }
        return true;
    }

    private Page acqPage(String pageNoStr, String perPageRowsStr) {
        int pageNo = 1;
        int perPageRows = 10;
        Page page = getPage( pageNoStr,perPageRowsStr,pageNo,perPageRows );
        return page;
    }


    private Page getPage(String pageNoStr,String perPageRowsStr,long  pageNo,int perPageRows) {
        try {
            pageNo = Long.parseLong( pageNoStr );
        } catch (NumberFormatException e) {
            ;
        }
        try {
            perPageRows = Integer.parseInt( perPageRowsStr );
        } catch (NumberFormatException e) {
            ;
        }
        Page page = new Page( perPageRows );
        page.setCurrentPage( pageNo );
        return page;
    }

    @JSON(serialize=false)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @JSON(serialize=false)
    public StudentExpService getStudentExpService() {
        return studentExpService;
    }

    public void setStudentExpService(StudentExpService studentExpService) {
        this.studentExpService = studentExpService;
    }

    public ApplyForLab getApplyForLab() {
        return applyForLab;
    }

    public void setApplyForLab(ApplyForLab applyForLab) {
        this.applyForLab = applyForLab;
    }

    @JSON(serialize=false)
    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    @JSON(serialize=false)
    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public String getSfind() {
        return sfind;
    }

    public void setSfind(String sfind) {
        this.sfind = sfind;
    }
}
