package com.hgd.laboratory.action;


import com.hgd.laboratory.dto.OpenLabScheduleTuple;
import com.hgd.laboratory.po.OpenLabSchedule;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;

public class OpenLabScheduleAction extends ActionSupport {
    @Getter     @Setter
    private String srch_openLabId;
    @Getter     @Setter
    private String srch_term;

    //output attribute
    @Getter     @Setter
    private OpenLabSchedule openLabSchedule;
    @Getter     @Setter
    private Map<String,String> openLabScheduleDetailMap;

    //service
    @Getter     @Setter
    private OpenLabScheduleService openLabScheduleService;


    //action方法
    @Override
    public String execute() {
        OpenLabScheduleTuple openLabScheduleTuple;
       if (srch_openLabId==null) {
            //无参数请求
            String term =(String) ActionContext.getContext().getSession().get("currentTerm");
            openLabScheduleTuple =this.openLabScheduleService.findFirstOpenLabSchedule(term);
        } else {
           String srch_term =(String) ActionContext.getContext().getSession().get("currentTerm");
            openLabScheduleTuple =this.openLabScheduleService.findOpenLabSchedule( srch_term,srch_openLabId );
        }
        if (openLabScheduleTuple==null) {
            openLabSchedule =null;
            openLabScheduleDetailMap =null;
        } else {
            openLabSchedule =openLabScheduleTuple.getOpenLabSchedule();
            openLabScheduleDetailMap =openLabScheduleTuple.getOpenLabScheduleDetailMap();
        }
        return "view";

    }

}
