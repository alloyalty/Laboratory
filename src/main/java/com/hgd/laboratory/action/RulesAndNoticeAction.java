package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.RulesAndNoticeService;
import com.hgd.laboratory.util.Page;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.poi.ss.formula.functions.T;

import java.util.List;

public class RulesAndNoticeAction extends ActionSupport {
    RulesAndNoticeService rulesAndNoticeService;


    private String category = "announce";
    private int status = RulesAndNoticeService.SCRATCH;
    private String keyword = "";
    private Integer pageNo=1;

    public String query() {
        if (category==null || category.isEmpty()) category="announce";
        Page page = new Page(10);
        if (pageNo==null) pageNo=1;
        page.setCurrentPage(pageNo.longValue());
        page.setTotalRows(rulesAndNoticeService.query(keyword, status, category));

        PaginationData<Notice> paginationData = rulesAndNoticeService.query(keyword, status, category, page);
        ActionContext.getContext().put("paginationData", paginationData);
        return INPUT;
    }

    public Integer getPageNo() {
        return pageNo;
    }

    public void setPageNo(Integer pageNo) {
        this.pageNo = pageNo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public RulesAndNoticeService getRulesAndNoticeService() {
        return rulesAndNoticeService;
    }

    public void setRulesAndNoticeService(RulesAndNoticeService rulesAndNoticeService) {
        this.rulesAndNoticeService = rulesAndNoticeService;
    }
}
