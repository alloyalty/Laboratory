package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.ImportResult;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.ImportDataService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ImportDataAction extends ActionSupport {
    private ImportDataService importDataService;

    private String title;
    private File upload;
    private String uploadContentType;
    private String uploadFileName;
    private String savePath;

    private File teacher;
    private String teacherContentType;
    private String teacherFileName;

    private File laboratory;
    private String laboratoryContentType;
    private String laboratoryFileName;

    private File facility;
    private String facilityContentType;
    private String facilityFileName;

    private File softwareConfig;
    private String softwareConfigContentType;
    private String softwareConfigFileName;

    private File softwareDetails;
    private String softwareDetailsContentType;
    private String softwareDetailsFileName;



    private List<ImportResult>  importResultList = new ArrayList<>();



    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public String getSavePath() {
        return ServletActionContext.getServletContext().getRealPath(savePath);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public File getUpload() {
        return upload;
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    public String getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }

    public String importXLSX() throws Exception {
        String[] fileNameArray = new String[] {getUploadFileName(),getTeacherFileName(),getLaboratoryFileName(),
                getFacilityFileName(),getSoftwareConfigFileName(),getSoftwareDetailsFileName()};
        File[] fileArray = new File[] {getUpload(),getTeacher(),getLaboratory(),
                getFacility(),getSoftwareConfig(),getSoftwareDetails()};
        Class[] classArray = new Class[] {Student.class, Teacher.class, Laboratory.class,
                Facility.class, SoftwareConfig.class,SoftwareDetails.class};
        for (int i=0;i<fileNameArray.length;i++) {
            if (fileArray[i]==null) continue;
                imortAnXLSXFile(fileNameArray[i], fileArray[i], classArray[i]);
        }
        ActionContext.getContext().put("tip", getMessage(importResultList));
        return INPUT;

    }

    private String imortAnXLSXFile(String fileName, File file, Class tClass) throws Exception {
        String ext = checkFileExt(fileName);
        ImportResult importResult = importDataService.importData(file, tClass, ext);
        importResult.setImportFileName(fileName);
        importResultList.add(importResult);
        if (importResult.getUnImportDatas().size()==0) {
            importResult.setResult("导入成功。");
        } else {
            importResult.setResult("导入未完全成功。");
        }
        return INPUT;
    }

    private String checkFileExt(String s) throws Exception {
        String ext = s.substring(s.lastIndexOf('.'));
        if (".xls".equals(ext)  || ".xlsx".equals(ext)) {

        } else {
            throw new Exception(s + "文件类型不正确，仅支持.xls/.xlsx文件。");
        }
        return ext;
    }

    private String getMessage(List<ImportResult> importResultList) {
        StringBuilder mess= new StringBuilder();
        for (ImportResult ir: importResultList  ) {
            mess.append(ir.toString());
        }
        return mess.toString();
    }

    public File getTeacher() {
        return teacher;
    }

    public void setTeacher(File teacher) {
        this.teacher = teacher;
    }

    public String getTeacherContentType() {
        return teacherContentType;
    }

    public void setTeacherContentType(String teacherContentType) {
        this.teacherContentType = teacherContentType;
    }

    public String getTeacherFileName() {
        return teacherFileName;
    }

    public void setTeacherFileName(String teacherFileName) {
        this.teacherFileName = teacherFileName;
    }

    public File getLaboratory() {
        return laboratory;
    }

    public void setLaboratory(File laboratory) {
        this.laboratory = laboratory;
    }

    public String getLaboratoryContentType() {
        return laboratoryContentType;
    }

    public void setLaboratoryContentType(String laboratoryContentType) {
        this.laboratoryContentType = laboratoryContentType;
    }

    public String getLaboratoryFileName() {
        return laboratoryFileName;
    }

    public void setLaboratoryFileName(String laboratoryFileName) {
        this.laboratoryFileName = laboratoryFileName;
    }

    public File getFacility() {
        return facility;
    }

    public void setFacility(File facility) {
        this.facility = facility;
    }

    public String getFacilityContentType() {
        return facilityContentType;
    }

    public void setFacilityContentType(String facilityContentType) {
        this.facilityContentType = facilityContentType;
    }

    public String getFacilityFileName() {
        return facilityFileName;
    }

    public void setFacilityFileName(String facilityFileName) {
        this.facilityFileName = facilityFileName;
    }

    public File getSoftwareConfig() {
        return softwareConfig;
    }

    public void setSoftwareConfig(File softwareConfig) {
        this.softwareConfig = softwareConfig;
    }

    public String getSoftwareConfigContentType() {
        return softwareConfigContentType;
    }

    public void setSoftwareConfigContentType(String softwareConfigContentType) {
        this.softwareConfigContentType = softwareConfigContentType;
    }

    public String getSoftwareConfigFileName() {
        return softwareConfigFileName;
    }

    public void setSoftwareConfigFileName(String softwareConfigFileName) {
        this.softwareConfigFileName = softwareConfigFileName;
    }

    public File getSoftwareDetails() {
        return softwareDetails;
    }

    public void setSoftwareDetails(File softwareDetails) {
        this.softwareDetails = softwareDetails;
    }

    public String getSoftwareDetailsContentType() {
        return softwareDetailsContentType;
    }

    public void setSoftwareDetailsContentType(String softwareDetailsContentType) {
        this.softwareDetailsContentType = softwareDetailsContentType;
    }

    public String getSoftwareDetailsFileName() {
        return softwareDetailsFileName;
    }

    public void setSoftwareDetailsFileName(String softwareDetailsFileName) {
        this.softwareDetailsFileName = softwareDetailsFileName;
    }

    public ImportDataService getImportDataService() {
        return importDataService;
    }

    public void setImportDataService(ImportDataService importDataService) {
        this.importDataService = importDataService;
    }
}
