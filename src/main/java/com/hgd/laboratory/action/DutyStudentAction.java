package com.hgd.laboratory.action;

import com.hgd.laboratory.po.DutyStudent;
import com.hgd.laboratory.service.DutyStudentService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.List;

public class DutyStudentAction extends ActionSupport {
   private DutyStudentService dutyStudentService;


    private String code;
    public String addDutyStudent(){
        if (!dutyStudentService.existInDuty(code)) {
            dutyStudentService.addDutyStudent(code);
        } else {
            this.addActionError(code+"不存在。");
        }

        this.delflag =0;
        return query();
    }

    private int id;  //DutyStudent 主键
    public String delDutyStudent() {
        //页面部分确保，输入code的正确性.即使不正确，页面也不需要反馈信息。
        dutyStudentService.delDutyStudent(id);
        this.delflag =0;
        return query();
    }

    private Integer delflag;
    public String query() {
        //查询全部值班人员，在此基础上，可以执行状态{0在值，1已经删除}过滤,默认0【在值】
        List<DutyStudent>   dutyStudentList =dutyStudentService.query(delflag);
        ActionContext.getContext().put("dutyStudentList",dutyStudentList);
        return SUCCESS;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DutyStudentService getDutyStudentService() {
        return dutyStudentService;
    }

    public void setDutyStudentService(DutyStudentService dutyStudentService) {
        this.dutyStudentService = dutyStudentService;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }
}
