package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.StudentExpService;
import com.hgd.laboratory.util.DateUtil;
import com.hgd.laboratory.util.Page;
import com.hgd.laboratory.util.TermUtil;
import com.hgd.laboratory.util.TimeSlot;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;


public class StudentExpJsonAction extends ActionSupport {
    //输入参数
    private String sId;
    private String labId;
    private String fId;
    //输出参数
    private String name=null;
    private String timeslot=null;
    private String applyNo=null;
    private String sfind="1";
    private String appCodeError=null;
    private String facilityError=null;
    private PaginationData<ApplyForLab> paginationData=null;

    //服务
    private StudentExpService studentExpService;

    /*
    以预约号或者学生学号为参数，检查是否符合登记上机条件。
    如果符合条件给出学生姓名，时段，申请号等信息，否则就给出申请号相关的错误信息。
    返回数据通过Jsom返回。
     */
    public String checkStudent() {
        final Date currDate = new Date();

        Timestamp today= new Timestamp(DateUtil.stdDate(currDate).getTime());
        int timeslot= TimeSlot.getInstance().calTimeslot(currDate);
        String term= TermUtil.getInstamce().getTermStr();

        ApplyForLab applyForLab = studentExpService.queryUniqueApply(sId,today,timeslot,labId,term);
        if (applyForLab==null){
            sfind="0";
            appCodeError="未找到相关预约或与学号相关的可用的预约。";
        } else {
            sfind="1";
            calApplyMsg(applyForLab);
        }
        return "success";
    }


    public void calApplyMsg(ApplyForLab applyForLab) {
        name = applyForLab.getUserName() + " ";
        if (applyForLab.getTimeslotStart().equals(applyForLab.getTimeslotFinish())) {
            this.timeslot ="第" + applyForLab.getTimeslotStart() + "大节";
        } else {
            this.timeslot = "第" + applyForLab.getTimeslotStart() + "-" + applyForLab.getTimeslotFinish() + "大节";
        }
        applyNo = applyForLab.getApplcatioCode();
    }


    public String applyExp() {
        final Date currDate = new Date();

        Timestamp today= new Timestamp(DateUtil.stdDate(currDate).getTime());
        int timeslot= TimeSlot.getInstance().calTimeslot(currDate);
        String term= TermUtil.getInstamce().getTermStr();

        String pageNoStr = ServletActionContext.getRequest().getParameter( "page" );
        String perPageRowsStr = ServletActionContext.getRequest().getParameter( "perPageRows" );
        Page page = acqPage( pageNoStr,perPageRowsStr );

        if (checkValid(today, timeslot, term)) {
            studentExpService.confirmApply(fId,applyForLab.getApplyNumber());
            paginationData =  studentExpService.queryConfirmedApply(labId,today,timeslot,term,page);
        }
        return SUCCESS;

    }
    //中间变量
    private ApplyForLab applyForLab;
    private boolean checkValid(Timestamp today, int timeslot, String term) {

        applyForLab = studentExpService.queryUniqueApply(sId, today, timeslot, labId, term);
        if (applyForLab==null){
            appCodeError ="未找到申请单号或者该学生相关的申请单号。";
            return false;
        } else {
            calApplyMsg(applyForLab);
        }
        if (fId.isEmpty()) {
            facilityError ="机器号必填！";
            return  false;
        } else if (!studentExpService.queryExistFaciId(labId,fId)) {
            facilityError ="机器编号未找到！";
            return  false;
        } else {
            //检查机器编号是否已经被其它上机者占用
            String machineId = studentExpService.queryTheMachineId(fId,today,timeslot,labId,term);
            if (machineId!=null && machineId.equals(fId)) {
                facilityError ="机器编号已被占用。";
                return false;
            }
        }
        return true;
    }

    private Page acqPage(String pageNoStr, String perPageRowsStr) {
        int pageNo = 1;
        int perPageRows = 10;
        Page page = getPage( pageNoStr,perPageRowsStr,pageNo,perPageRows );
        return page;
    }


    private Page getPage(String pageNoStr,String perPageRowsStr,long  pageNo,int perPageRows) {
        try {
            pageNo = Long.parseLong( pageNoStr );
        } catch (NumberFormatException e) {
            ;
        }
        try {
            perPageRows = Integer.parseInt( perPageRowsStr );
        } catch (NumberFormatException e) {
            ;
        }
        Page page = new Page( perPageRows );
        page.setCurrentPage( pageNo );
        return page;
    }

    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getSfind() {
        return sfind;
    }

    public void setSfind(String sfind) {
        this.sfind = sfind;
    }
    @JSON(serialize=false)
    public StudentExpService getStudentExpService() {
        return studentExpService;
    }


    public void setStudentExpService(StudentExpService studentExpService) {
        this.studentExpService = studentExpService;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getApplyNo() {
        return applyNo;
    }

    public void setApplyNo(String applyNo) {
        this.applyNo = applyNo;
    }
    public String getTimeslot() {
        return timeslot;
    }

    public void setTimeslot(String timeslot) {
        this.timeslot = timeslot;
    }

    public String getAppCodeError() {
        return appCodeError;
    }

    public void setAppCodeError(String appCodeError) {
        this.appCodeError = appCodeError;
    }

    public String getFacilityError() {
        return facilityError;
    }

    public void setFacilityError(String facilityError) {
        this.facilityError = facilityError;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public PaginationData<ApplyForLab> getPaginationData() {
        return paginationData;
    }

    public void setPaginationData(PaginationData<ApplyForLab> paginationData) {
        this.paginationData = paginationData;
    }

    @JSON(serialize = false)
    public ApplyForLab getApplyForLab() {
        return applyForLab;
    }

    public void setApplyForLab(ApplyForLab applyForLab) {
        this.applyForLab = applyForLab;
    }
}
