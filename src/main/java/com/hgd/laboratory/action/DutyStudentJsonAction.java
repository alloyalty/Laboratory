package com.hgd.laboratory.action;

import com.hgd.laboratory.po.DutyStudent;
import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.service.DutyStudentService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.json.annotations.JSON;

import java.util.List;

public class DutyStudentJsonAction extends ActionSupport {

    private DutyStudentService dutyStudentService;

    //输入参数
    private String stuNameOrId;
    //输出参数
    private List<Student> studentList;
    private boolean isDuty= false;

    //依据学生学号或者姓名从学生表中查找学生
    public String queryByNameOrId() {
        String semester =(String) ActionContext.getContext().getSession().get("currentTerm");

       studentList = dutyStudentService.queryByNameOrId(stuNameOrId);
       isDuty = dutyStudentService.firstStuIsDuty(studentList,semester);
       return SUCCESS;


    }



    @JSON(serialize=false)
    public DutyStudentService getDutyStudentService() {
        return dutyStudentService;
    }

    public void setDutyStudentService(DutyStudentService dutyStudentService) {
        this.dutyStudentService = dutyStudentService;
    }


    public String getStuNameOrId() {
        return stuNameOrId;
    }

    public void setStuNameOrId(String stuNameOrId) {
        this.stuNameOrId = stuNameOrId;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    public boolean isDuty() {
        return isDuty;
    }

    public void setDuty(boolean duty) {
        isDuty = duty;
    }
}
