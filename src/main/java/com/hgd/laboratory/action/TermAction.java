package com.hgd.laboratory.action;

import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.TermService;
import com.hgd.laboratory.util.TermUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.sql.Date;
import java.util.Map;

public class TermAction extends ActionSupport {
    private TermService termService;

    private String year;
    private Integer term;
    private Integer weeksOfTerm;
    private Date dayOfFirstWeekMonday;

    public String init() {
        try {
            if (termService.init(year,term,weeksOfTerm,dayOfFirstWeekMonday)) {
                ActionContext.getContext().put("tip", TermUtil.getTermStr() + "新学期初始化完成。建议重启web服务。");

                //重新设定会话。
                TermUtil.reset();
                Map<String, Object> sessionMap = ActionContext.getContext().getSession();
                User user =(User)sessionMap.get("user");
                TermService.regLoginStatus(sessionMap,user);
                return SUCCESS;
            }
            else {
                ActionContext.getContext().put("tip", TermUtil.getTermStr() + "当前学期正在运行。\n应先终止当前学期的运行，再初始化新学期。");
                return INPUT;
            }
        } catch (Exception ex) {
            ActionContext.getContext().put("tip", year+"-"+term+"已经终止，不可初始化为新学期。");
            return INPUT;
        }


    }



    public String closure() {
        String oldTerm = TermUtil.getTermStr();
        if (termService.closure()) {
            ActionContext.getContext().put("tip", oldTerm + "学期运行终止。");
            //重新设定会话。
            TermUtil.reset();
            Map<String, Object> sessionMap = ActionContext.getContext().getSession();
            User user =(User)sessionMap.get("user");
            TermService.regLoginStatus(sessionMap,user);

            return SUCCESS;
        } else {
            ActionContext.getContext().put("tip", oldTerm + "学期已经运行终止。");
            return INPUT;
        }
    }

    public TermService getTermService() {
        return termService;
    }

    public void setTermService(TermService termService) {
        this.termService = termService;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Integer getWeeksOfTerm() {
        return weeksOfTerm;
    }

    public void setWeeksOfTerm(Integer weeksOfTerm) {
        this.weeksOfTerm = weeksOfTerm;
    }

    public Date getDayOfFirstWeekMonday() {
        return dayOfFirstWeekMonday;
    }

    public void setDayOfFirstWeekMonday(Date dayOfFirstWeekMonday) {
        this.dayOfFirstWeekMonday = dayOfFirstWeekMonday;
    }
}
