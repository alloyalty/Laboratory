package com.hgd.laboratory.action;

import com.hgd.laboratory.po.Student;
import com.hgd.laboratory.service.StudentService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.Setter;

public class StudentAction extends ActionSupport implements ModelDriven<Student> {
    //模型驱动封装
    private Student model = new Student();
    @Override
    public Student getModel() {
        return model;
    }
    //对象注入
    private StudentService studentService;

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }


    //转到绑定邮箱
    public String toBandEmail(){
        return "toBandEmail";
    }

    //发送验证码
    public String sendEmail(){
        //StudentDao dao = new StudentDaoImpl();
        String codes = "";
        if (studentService.sendEmail(model.getEmail(),codes)){
            return "sendEmail";
        }
        return ERROR;
    }

    //绑定邮箱
    @Setter @Getter
    private String codes;

    public String bandEmail(){
        String codes1 = (String) ActionContext.getContext().getSession().get("codes");
        if (codes.equals(codes1)){
            Student student = studentService.findOne(model.getsId());
            //重新根据表单中的值设置修改的数据
            student.setEmail(model.getEmail());
            //提交修改
            studentService.update(student);
            return "bandEmail";
        }else {
            return ERROR;
        }
    }

    //转到修改邮箱
    public String toAlterEmail(){
        return "toAlterEmail";
    }
    //修改邮箱
    public String alterEmail(){
        Student student = studentService.findOne(model.getsId());
        //重新根据表单中的值设置修改的数据
        student.setEmail(model.getEmail());
        //提交修改
        studentService.update(student);
        return "changePwd";
    }



}
