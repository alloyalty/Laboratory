package com.hgd.laboratory.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class DutyAction extends ActionSupport {

    private String target="studentExp.jsp";
    public String checkLabSetting() {
        String tmp =(String) ActionContext.getContext().getSession().get("labId");
        if (tmp==null)
            return "labSetting";
        else
           return SUCCESS;
    }

    private String labId;
    public String labSetting() {
        if (labId.isEmpty()) {
            return INPUT;
        } else {
            ActionContext.getContext().getSession().put("labId",labId);
            return SUCCESS;
        }
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }
}
