package com.hgd.laboratory.action;


import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.TermService;
import com.hgd.laboratory.service.UserService;
import com.hgd.laboratory.util.TermUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.Map;

public class UserAction extends ActionSupport implements ModelDriven<User> {
    //模型驱动封装
    private User model = new User();
    //专用测试接缝
    private Map<String, Object> sessionMap=null;

    @Override
    public User getModel() {
        return model;
    }

    //对象注入
    private UserService userService;
    @Autowired
    private TermService termService;

    public TermService getTermService() {
        return termService;
    }

    public void setTermService(TermService termService) {
        this.termService = termService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Setter @Getter
    private String oldPassword;


    //转到登录页面
    public String toLogin(){
        return "toLogin";
    }
    public String blank(){
        return "blank";
    }
    //登录方法
    public String login(){
        final Map<String, Object> sessionMap = getSession();
        sessionMap.clear();

        User user = userService.checkUser(this.model.getId(),this.model.getPassword(),this.model.getRole());
        System.out.println("用户界面输入："+this.model);
        if (user == null){
            //登录失败
            this.addActionError("用户名、密码或角色不正确。");
            System.out.println("测试后台登录："+"失败.");
            return INPUT;
        }else {
            String role= user.getRole().toString().toLowerCase();
            String[] roleList={"student","teacher","duty","labmanager","admin"};
            if (!Arrays.asList(roleList).contains(role)) {
                System.out.println("用户数据完整性错误.");
                return ERROR;
            }
            //TermUtil.getCurrentTerm().getStatus()==0;
            final boolean termIsEnded = !this.termService.termIsRunning();//学期结束
            if (termIsEnded && !user.getRole().equalsIgnoreCase("labmanager")) {
                this.addActionError("原学期结束，新学期还未开放。");
                System.out.println("原学期结束，新学期还未开放。");
                return INPUT;
            }
            ActionContext.getContext().put("tip","登录成功。请继续......");
            user.setPassword(null);
            TermService.regLoginStatus(sessionMap, user);

            //测试
            System.out.println("后台测试登录："+user.toString());
            return SUCCESS;

        }
    }

//    public static void regLoginStatus(Map<String, Object> sessionMap, User user) {
//        Term currentTermObj = TermUtil.getCurrentTerm();
//        String termStr = currentTermObj.getYear() + "-" + currentTermObj.getTerm();
//
//        sessionMap.put("user",user);
//        sessionMap.put("currentTermObj",currentTermObj);
//        sessionMap.put("currentTerm",termStr);
//    }

    public Map<String, Object> getSession() {
        if (this.sessionMap!=null) {
            return sessionMap;
        }
        return ActionContext.getContext().getSession();
    }

    //注销方法
    public String logout(){
        ServletActionContext.getRequest().getSession().invalidate();
        return "logout";
    }

    public String bindEmail() {
        HttpServletRequest request = ServletActionContext.getRequest();
        String email = request.getParameter("email");

        User currentUser = (User) getSession().get("user");
        User user = userService.findOne(currentUser.getId());
        user.setEmaill(email);
        //提交修改
        userService.update(user);
        getSession().put("user",user);
        ActionContext.getContext().put("tip","邮箱绑定成功。");
        return SUCCESS;
    }

    public String modifyEmail() {
        HttpServletRequest request = ServletActionContext.getRequest();
        String newEmail = request.getParameter("newEmail");

        User currentUser = (User) getSession().get("user");
        User user = userService.findOne(currentUser.getId());
        user.setEmaill(newEmail);
        //提交修改
        userService.update(user);
        getSession().put("user",user);
        ActionContext.getContext().put("tip","邮箱修改成功。");
        return SUCCESS;
    }

    //修改密码的方法
    public String changePwd(){
        HttpServletRequest request = ServletActionContext.getRequest();
        String newPassword = request.getParameter("newPassword");
        String reNewPassword = request.getParameter("reNewPassword");


        User currentUser = (User) getSession().get("user");
        User user = userService.findOne(currentUser.getId());
        //验证旧密码
        if (!user.getPassword().equals(oldPassword)){
            this.addActionError("原密码错误！");
            return "input";
        }
        if (!newPassword.equals(reNewPassword)) {
            this.addActionError("二次输入的新密码不同！");
            return "input";
        }
        //重新根据表单中的值设置修改的数据
        user.setPassword(newPassword);
        //提交修改
        userService.update(user);
        ActionContext.getContext().put("tip","密码更新成功。");
        return SUCCESS;
    }




    public void setSessionMap(Map<String, Object> sessionMap) {
        this.sessionMap = sessionMap;
    }
}
