package com.hgd.laboratory.action;

import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.FindBackPwdService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.json.annotations.JSON;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;

public class FindBackPwdActionAsyn extends ActionSupport {
    private FindBackPwdService findBackPwdService;
    private String id;
    private String sid;
    private String newPassword;

    private String backMsg;
    private String succ="fail";


    public String sendEmail() throws Exception {
        if (id == null || id.trim().equals("")) {
            //前端校验
            this.backMsg = "用户登录名不能为空。 ";
            return "success";

        }

        User user = findBackPwdService.findUser(id);
        if (user == null) {
            this.backMsg = "用户登录名未找到。 ";
            return "success";
        }

        String email = user.getEmaill();
        if (email == null || email.trim().equals("")) {
            this.backMsg = "此用户未绑定任务邮箱，不能使用邮箱找回密码功能。";
            return "success";
        }
        //获取当前用户对象
        //获取url 字符串
        //获取用户email地址
        HttpServletRequest req = ServletActionContext.getRequest();
        findBackPwdService.sendEmail(user, req);
        this.backMsg = "找回密码链接已经发送到指定信箱，链接30分钟有效。";
        this.succ="succ";
        return "success";
    }


    public String validAndchangePwd() throws Exception {
        //提取摘要参数digest
        HttpServletRequest request = ServletActionContext.getRequest();
        String digest = request.getParameter("sid");
        String newPassword = request.getParameter("newPassword");

        if (id == null || id.trim().equals("")) {
            this.backMsg = "链接不完整,请重新生成。 ";
            return "success";
        }

        User user = findBackPwdService.findUser(id);
        if (user == null) {
            this.backMsg = "链接不完整,请重新生成。 ";
            return "success";
        }

        Timestamp outDate = user.getOutDate();

        if (outDate.getTime() <= System.currentTimeMillis()) { // 表示已经过期
            this.backMsg = "链接已经过期,请重新申请找回密码. ";
            return "success";
        }
        //提取用户的id，
        //获取用户的id的对象
        //计算其摘要
        //如果摘要相同，修改密码。更新密钥，使此次url无效。
        boolean succ = findBackPwdService.checkValidAndChangePwd(user, digest, newPassword);
        if (!succ) {
            this.backMsg = "链接不正确,是否已经过期了?重新申请吧.";
            return "success";
        }
        this.backMsg = "密码修改成功。";
        this.succ="succ";
        return "success";
    }

    @JSON(serialize = false)
    public FindBackPwdService getFindBackPwdService() {
        return findBackPwdService;
    }

    public void setFindBackPwdService(FindBackPwdService findBackPwdService) {
        this.findBackPwdService = findBackPwdService;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JSON(serialize = false)
    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    @JSON(serialize = false)
    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getBackMsg() {
        return backMsg;
    }

    public void setBackMsg(String backMsg) {
        this.backMsg = backMsg;
    }

    public String getSucc() {
        return succ;
    }

    public void setSucc(String succ) {
        this.succ = succ;
    }
}

