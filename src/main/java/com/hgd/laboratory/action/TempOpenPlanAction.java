package com.hgd.laboratory.action;


import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.dto.TempOpenPlanTuple;
import com.hgd.laboratory.exception.FormParameterException;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.exception.WorkProgramException;
import com.hgd.laboratory.po.TempOpenPlan;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.TempOpenPlanService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

public class TempOpenPlanAction<view> extends ActionSupport implements ModelDriven<TempOpenPlan> {
    //对象注入
    TempOpenPlanService tempOpenPlanService;

    //对象注入
    LabOpenPlanService labOpenPlanService;



    int srch_tempPlanId = 0;
    String srch_labId;
    int srch_weekOfTerm;

    TempOpenPlan tempOpenPlan;

    //保存表单页面数据,关键字为"w"+dayOfWeek+"t"+timeSlot，与界面上的参数名相同
    Map<String, TempItem> tempOpenPlanDetailMap;

    //向页面输出结果
    //向manageTempOpenPlans.jsp页面输出
    List<TempOpenPlan> tempOpenPlanList;

    @Override
    public TempOpenPlan getModel() {
        if (this.tempOpenPlan == null)
            this.tempOpenPlan = new TempOpenPlan();
        return tempOpenPlan;
    }

    public TempOpenPlanAction() {
        tempOpenPlanDetailMap = new HashMap<>();
    }

    //向页面workoutTempOpenPlans.jsp,viewTempOpenPlan.jsp输出
    //共用 tempOpenPlan

    /////////////////////////////////////////////
    //getter/setter



    public TempOpenPlan getTempOpenPlan() {
        return tempOpenPlan;
    }

    public void setTempOpenPlan(TempOpenPlan tempOpenPlan) {
        this.tempOpenPlan = tempOpenPlan;
    }

    public TempOpenPlanService getTempOpenPlanService() {
        return tempOpenPlanService;
    }

    public void setTempOpenPlanService(TempOpenPlanService tempOpenPlanService) {
        this.tempOpenPlanService = tempOpenPlanService;
    }


    public List<TempOpenPlan> getTempOpenPlanList() {
        return tempOpenPlanList;
    }

    public void setTempOpenPlanList(List<TempOpenPlan> tempOpenPlanList) {
        this.tempOpenPlanList = tempOpenPlanList;
    }

    public Map<String, TempItem> getTempOpenPlanDetailMap() {
        return tempOpenPlanDetailMap;
    }

    public void setTempOpenPlanDetailMap(Map<String, TempItem> tempOpenPlanDetailMap) {
        this.tempOpenPlanDetailMap = tempOpenPlanDetailMap;
    }

    public int getSrch_tempPlanId() {
        return srch_tempPlanId;
    }

    public void setSrch_tempPlanId(int srch_tempPlanId) {
        this.srch_tempPlanId = srch_tempPlanId;
    }

    public String getSrch_labId() {
        return srch_labId;
    }

    public void setSrch_labId(String srch_labId) {
        this.srch_labId = srch_labId;
    }

    public int getSrch_weekOfTerm() {
        return srch_weekOfTerm;
    }

    public void setSrch_weekOfTerm(int srch_weekOfTerm) {
        this.srch_weekOfTerm = srch_weekOfTerm;
    }


    public LabOpenPlanService getLabOpenPlanService() {
        return labOpenPlanService;
    }

    public void setLabOpenPlanService(LabOpenPlanService labOpenPlanService) {
        this.labOpenPlanService = labOpenPlanService;
    }


    //action 的方法

    /**
     * 输入：无
     * 输出：tempOpenPlanList
     *
     * @return
     */
    public String managetp() {
        String currentTerm = (String) ActionContext.getContext().getSession().get( "currentTerm" );
        this.tempOpenPlanList = tempOpenPlanService.queryAllTempOpenPlans( currentTerm );
        return "manage";
    }

    /**
     * 表单或者url输入: tempPlanId
     * 输出：tempOpenPlan属性, tempOpenPlanDetailMap是一个Map
     *
     * @return view
     */
    public String view() throws OpenPlanDataIncompleteException {
        TempOpenPlanTuple tempOpenPlanTuple = this.tempOpenPlanService.findTempOpenPlan( srch_tempPlanId );
        resolved( tempOpenPlanTuple );
        return "view";
    }

    private void resolved(TempOpenPlanTuple tempOpenPlanTuple) {
        if (tempOpenPlanTuple == null) {
            this.tempOpenPlan = null;
            this.tempOpenPlanDetailMap = null;
        } else {
            this.tempOpenPlan = tempOpenPlanTuple.getTempOpenPlan();
            this.tempOpenPlanDetailMap = tempOpenPlanTuple.getTempOpenPlanDetailMap();

        }
    }


    /**
     * 表单或url输入：
     * 方式一： tempPlanId
     * 方式二：term,weekOfTerm,labId
     * 输出：给workoutTempOpenPlans.jsp使用
     * tempOpenPlan的六个属性, tempOpenPlanDetailMap是一个Map
     * --与view 相同
     *
     * @return
     */
    public String workout() throws WorkProgramException, Exception {
        //一种情形，只有tempPlanId 参数数，没有 term,weekOfTerm,labId参数  url=tempOpenPlan_workout?tempPlanId=
        HttpServletRequest request = ServletActionContext.getRequest();
        String s_tempPlanId = request.getParameter( "srch_tempPlanId" );

        if (s_tempPlanId != null) {
            TempOpenPlanTuple tempOpenPlanTuple = this.tempOpenPlanService.findTempOpenPlan( srch_tempPlanId );
            resolved( tempOpenPlanTuple );
            return "workout";
        } else {
            String term = (String) ActionContext.getContext().getSession().get( "currentTerm" );
            LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan( term,srch_labId );
            TempOpenPlanTuple tempOpenPlanTuple = this.tempOpenPlanService.findTempOpenPlan( term,srch_labId,srch_weekOfTerm );

            if (labOpenPlanTuple == null || labOpenPlanTuple.getLabOpenPlan() == null) {
                throw new WorkProgramException( String.format( "实验室%s开放未拟定。请先拟定实验室开放计划，再拟定临时计划",srch_labId ) );
            }
            if (tempOpenPlanTuple == null) {
                tempOpenPlanTuple = this.tempOpenPlanService.createBlankTempOpenPlan( term,srch_labId,srch_weekOfTerm,labOpenPlanTuple );
            }
            resolved( tempOpenPlanTuple );
            return "workout";

        }

    }

    public String publish() throws FormParameterException, NumberFormatException, OpenPlanExpressinException, OpenPlanDataIncompleteException {
        this.parseTempPlan();
        this.tempOpenPlanService.publish( tempOpenPlan,tempOpenPlanDetailMap);
        ActionContext.getContext().put( "tip","临时计划发布成功" );
        return "publish_succ";
    }


    /**
     * 输入：表单参数--
     * private long tempPlanId;
     * private String term;
     * private String labId;
     * private int weekOfTerm;
     * private Integer status;
     * private String desp;
     * 细节数据
     * 输出: 表单参数, tempOpenPlanDetailMap
     *
     * @return
     * @throws FormParameterException
     * @throws NumberFormatException
     */
    public String save() throws FormParameterException, NumberFormatException, OpenPlanExpressinException {
        this.parseTempPlan();
        this.tempOpenPlanService.saveUpdate( tempOpenPlan,tempOpenPlanDetailMap,LabService.SCRATCH,LabService.UPDATE );
        return "save_succ";
    }


    private void parseTempPlan() throws FormParameterException, NumberFormatException {
        /*
        获取request对象
        获取term,lid
        获取周一到周日的每一个时段的开放计划,
           存放到labOpenPlan
        返回真
         */

        HttpServletRequest request = ServletActionContext.getRequest();

        int tempPlanId = this.tempOpenPlan.getTempPlanId();
        tempOpenPlan = this.tempOpenPlanService.findOneTempOpenPlan(tempPlanId);

        Map<String, TempItem> tempOpenPlanDetailMap = new HashMap<>();
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                String orgin = request.getParameter( key + "_orgin" );
                String temp = request.getParameter( key + "_temp" );
                if (orgin == null)
                    throw new FormParameterException( "所需的表单参数不存在;" + key + "_orgin" );
                if (temp == null)
                    throw new FormParameterException( "所需的表单参数不存在;" + key + "_temp" );

                TempItem it = new TempItem( orgin,temp );

                this.tempOpenPlanDetailMap.put( key,it );

            }
        }


    }

    /**
     *   @param  tempOpenId 表单或url中请求参数
     */
    public  String delete() {
        HttpServletRequest request = ServletActionContext.getRequest();
        String term = (String)ActionContext.getContext().getSession().get("currentTerm");
        int srch_tempPlanId = Integer.parseInt(request.getParameter("srch_tempPlanId"));

        this.tempOpenPlanService.delete(srch_tempPlanId);
        this.tempOpenPlanList = this.tempOpenPlanService.queryAllTempOpenPlans( term );
        return "manage";
    }


}
