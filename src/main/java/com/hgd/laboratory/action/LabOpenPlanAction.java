package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.LabOpenPlan;

import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.util.TermUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import lombok.Getter;
import lombok.Setter;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LabOpenPlanAction extends ActionSupport implements ModelDriven<LabOpenPlan> {
    @Getter @Setter
    private int srch_openPlanId;
    @Getter @Setter
    private String srch_labId;

    //对象注入

    public LabOpenPlanService getLabOpenPlanService() {
        return labOpenPlanService;
    }

    public void setLabOpenPlanService(LabOpenPlanService labOpenPlanService) {
        this.labOpenPlanService = labOpenPlanService;
    }

    private LabOpenPlanService labOpenPlanService;

    //保存表单页面数据, 有二部分。
    // 其一是LabOpenPlan
    // 其二 是与LabOpenPlan 关联细节数据部分用一个MAP表示更为合适, 关键字为"w"+dayOfWeek+"t"+timeSlot，与界面上的参数名相同
    private LabOpenPlan model;
    @Getter @Setter
    private Map<String,String> labOpenPlanDetailMap = new HashMap<>();

    //向页面输出结果
    //向manageOpenPlans.jsp页面输出
    @Getter @Setter
    private List<LabOpenPlan> labOpenPlanList;

    //向页面workoutOpenPlanStage.jsp,viewOpenPlan.jsp输出
    //共用 labOpenPlan,labOpenPlanDetailMap
    @Override
    public LabOpenPlan getModel() {
        return model;
    }

    public void setModel(LabOpenPlan model) {
        this.model = model;
    }
    private int maxWeekNo;

    //Action 方法的实现

    /**
     * @Parameter   srch_openPlanId
     * @return
     */
    public String view() {
        HttpServletRequest request = ServletActionContext.getRequest();

        try {
            String s_openPlanId =  request.getParameter("srch_openPlanId");
            int openPlanId = Integer.parseInt(s_openPlanId);
            LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan(openPlanId);
            resolved(labOpenPlanTuple);
            if (this.model==null) throw new Exception("指定id的开放计划不存在 。");
            return "view";

        } catch (Exception e) {
            e.printStackTrace();
            this.addActionError(e.getMessage());
            return ERROR;
        }

    }

    private void resolved(LabOpenPlanTuple labOpenPlanTuple) {
        if (labOpenPlanTuple==null) {
            this.model=null;
            this.labOpenPlanDetailMap=null;
        } else {
            this.model=labOpenPlanTuple.getLabOpenPlan();
            this.labOpenPlanDetailMap = labOpenPlanTuple.getLabOpenPlanDetailMap();

        }
    }


    /**
     * @param 无
     * 以登录后的当前学期为参数
     * 输出: labOpenPlanList
     */
    private List<String> availLabList;

    public List<String> getAvailLabList() {
        return availLabList;
    }

    public void setAvailLabList(List<String> availLabList) {
        this.availLabList = availLabList;
    }

    public String manage() {
        String term = (String)ServletActionContext.getRequest().getSession().getAttribute("currentTerm");
        this.labOpenPlanList = this.labOpenPlanService.queryAllLabOpenPlans(term);
        this.availLabList =this.labOpenPlanService.queryAvailLabListForWorkout(term);
        return "manage";
    }

    /** 从表单form 或者 url请求来的输入参数
     * input:
     * @param srch_openPlanId   Case1 已知 openPlanID
     *
     * @param  srch_labId   Case 2 openPlanID未知，但term,labID 已知
     * output:
     *  model, labOpenPlanDetailMap
     *
     * @return
     */
    public String workout() {

        HttpServletRequest request = ServletActionContext.getRequest();

        String s_openPlanId =  request.getParameter("srch_openPlanId");
       if (s_openPlanId!=null) {
            try {
                int openPlanId = Integer.parseInt(s_openPlanId);
                LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan(openPlanId);
                this.resolved(labOpenPlanTuple);
                return "workout";
            } catch (NumberFormatException e) {
                e.printStackTrace();
                System.out.println();
                this.addActionError("ACTION请求参数openPlanId值类型不正确或者值不对。"+s_openPlanId);
                return ERROR;
            } catch (Exception e) {
                e.printStackTrace();
                this.addActionError(e.getMessage());
                return ERROR;
            }
        } else {
           //有二种情形：(1) 没有，需要新建；(2) 有，查询返回即可
           try {
               String tempLabID = request.getParameter("srch_labId");
               String term = (String)request.getSession().getAttribute("currentTerm");
               LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan(term,tempLabID);
               if (labOpenPlanTuple==null ){//不存在此计划，需要新建
                   labOpenPlanTuple = this.labOpenPlanService.createBlankLabOpenPlan(term,tempLabID);
               }
               this.resolved(labOpenPlanTuple);
           } catch (OpenPlanDataIncompleteException e) {
               e.printStackTrace();
               return ERROR;
           }
           return "workout";

       }

    }


    public String publish() throws OpenPlanExpressinException, OpenPlanDataIncompleteException {
        parseOpenPlan();

        this.labOpenPlanService.publish(model,labOpenPlanDetailMap);

        ActionContext ctx = ActionContext.getContext();
        ctx.put("tip",model.getTerm()+" 实验室"+model.getLabId()+"开发计划发布成功！");
        return "publish_succ";
    }

    public String save() throws OpenPlanExpressinException {
        parseOpenPlan();
        this.labOpenPlanService.saveUpdate(model,labOpenPlanDetailMap,LabOpenPlanService.SCRATCH,LabService.UPDATE);
        return "save_succ";

    }


    /**
     * @param openPlanId  ---请求表单中的参数
     * @param labId   ---请求表单中的参数
     * @param status  ---请求表单中的参数
     * @param desp    ---请求表单中的参数
     * @param tiwj[]   ---请求表单中的参数
     * @ 处理后输出参数 model,labOpenPlanDetailMap
     */
    private void parseOpenPlan() {
        /*
        获取request对象
        获取term,lid
        获取周一到周日的每一个时段的开放计划,
           存放到labOpenPlan
           //合法性检查，放到前台处理
        返回真
         */
        HttpServletRequest request = ServletActionContext.getRequest();

        int openPlanId = this.model.getOpenPlanId();
        model =this.labOpenPlanService.findOneLabOpenPlan(openPlanId);
        labOpenPlanDetailMap = new HashMap<>();
        for (int i = 1; i <LabService.TIMESLOTNUM+1; i++) {
            for (int j = 1;j <=7; j++) {
                String key ="t"+i+"w"+j;
                this.labOpenPlanDetailMap.put(key,request.getParameter(key));
            }
        }


    }

    /**
     *   @param  labOpenId 表单或url中请求参数
     */
    public  String delete() {
        HttpServletRequest request = ServletActionContext.getRequest();
        String term = (String)request.getSession().getAttribute("currentTerm");
        int srch_openPlanId = Integer.parseInt(request.getParameter("srch_openPlanId"));

        this.labOpenPlanService.delete(srch_openPlanId);
        this.labOpenPlanList = this.labOpenPlanService.queryAllLabOpenPlans(term);
        return "manage";
    }

    public int getMaxWeekNo() {
        return TermUtil.getCurrentTerm().getWeeksOfTerm();
    }

    public void setMaxWeekNo(int maxWeekNo) {
        this.maxWeekNo = maxWeekNo;
    }
}
