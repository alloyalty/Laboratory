package com.hgd.laboratory.action;

import com.hgd.laboratory.po.LabTeacher;
import com.hgd.laboratory.po.Teacher;
import com.hgd.laboratory.service.LabManaService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.json.annotations.JSON;

import java.util.List;

public class LabManaAction extends ActionSupport {
    private LabManaService labManaService;

    private String teaNameOrId;
    private List<Teacher> teacherList;
    public String queryByNameOrId() {
        teacherList = labManaService.queryByNameOrId(teaNameOrId);
        return SUCCESS;
    }


    private String code;  //依据教师的ID，加入管理员
    public String addLabManager(){
        if (!labManaService.existInDuty(code)) {
            labManaService.addLabManager(code);
        } else {
            this.addActionError(code +"不存在。");
        }

        this.delflag =0;
        return query();
    }
    private int id;  //依照主键,在LabManager标记已经删除
    public String delLabManager() {
        //页面部分确保，输入code的正确性.即使不正确，页面也不需要反馈信息。
        labManaService.delLabManager(id);
        this.delflag =0;
        return query();
    }

    private Integer delflag=0;
    public String query() {
        //查询全部实验室管理人员，在此基础上，可以执行状态{0在值，1已经删除}过滤,默认0【在值】
        List<LabTeacher>   labTeacherList =labManaService.query(delflag);
        ActionContext.getContext().put("labTeacherList",labTeacherList);
        return SUCCESS;
    }


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Teacher> getTeacherList() {
        return teacherList;
    }

    public void setTeacherList(List<Teacher> teacherList) {
        this.teacherList = teacherList;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTeaNameOrId() {
        return teaNameOrId;
    }

    public void setTeaNameOrId(String teaNameOrId) {
        this.teaNameOrId = teaNameOrId;
    }

    @JSON(serialize=false)
    public LabManaService getLabManaService() {
        return labManaService;
    }

    public void setLabManaService(LabManaService labManaService) {
        this.labManaService = labManaService;
    }

    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }
}
