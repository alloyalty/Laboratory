package com.hgd.laboratory.action;

import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.RulesAndNoticeService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import org.hibernate.Session;

public class RulesAndNoticeMakeAction extends ActionSupport implements ModelDriven<Notice> {
    RulesAndNoticeService rulesAndNoticeService;

    private Notice notice = new Notice();

    @Override
    public Notice getModel() {
        return this.notice;
    }


    public String saveScratch() throws Exception {
        if (rulesAndNoticeService.statusTranIsRight(notice)) {
            notice.setDrawupDate(new java.sql.Date(new java.util.Date().getTime()));
            if (notice.getId() == null) {
                rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.SCRATCH, LabService.SAVE);
            } else {
                rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.SCRATCH, LabService.UPDATE);
            }

            String message = getMessage(notice);
            ActionContext.getContext().put("tip", message);
            return INPUT;
        } else
            return INPUT;
    }

    public String saveWaitForAudit() throws Exception {
        if (rulesAndNoticeService.statusTranIsRight(notice)) {
            rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.WAITFOR_AUDIT, LabService.UPDATE);
            String message = getMessage(notice);
            ActionContext.getContext().put("tip", message);
            return SUCCESS;
        } else
            return INPUT;
    }


    public String saveWaitForPublish() throws Exception {
        if (rulesAndNoticeService.statusTranIsRight(notice)) {
            rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.WAITFOR_PUBLISH, LabService.UPDATE);
            String message = getMessage(notice);
            ActionContext.getContext().put("tip", message);
            return SUCCESS;
        } else
            return INPUT;
    }

    public String publish() throws Exception {
        if (rulesAndNoticeService.statusTranIsRight(notice)) {
            rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.PUBLISH, LabService.UPDATE);
            String message = getMessage(notice);
            ActionContext.getContext().put("tip", message);
            return SUCCESS;
        } else
            return INPUT;
    }

    public String recallPublish() throws Exception {
        if (specId != -1L) {
            Notice notice1 = rulesAndNoticeService.getNotice(specId);
            notice1.setComment("发布已经撤回");
            rulesAndNoticeService.saveOrUpdate(notice1, RulesAndNoticeService.WAITFOR_PUBLISH, LabService.UPDATE);
            String message = getMessage(notice1);
            ActionContext.getContext().put("tip", message);
            return SUCCESS;
        } else
            return INPUT;
    }

    //saveAuditNoPass
    public String saveAuditNoPass() throws Exception {
        if (rulesAndNoticeService.statusTranIsRight(notice)) {
            notice.setComment("上次审核未通过");
            rulesAndNoticeService.saveOrUpdate(notice, RulesAndNoticeService.SCRATCH, LabService.UPDATE);
            String message = getMessage(notice);
            ActionContext.getContext().put("tip", message);
            return SUCCESS;
        } else
            return INPUT;
    }

    private Long specId = -1L;

    public String toPrepareData() {
        if (specId != -1L) {
            Notice notice1 = rulesAndNoticeService.getNotice(specId);
            this.notice.setNotice(notice1);
        }
        return SUCCESS;
    }

    public String delete() {
        if (specId != -1L) {
            rulesAndNoticeService.delete(specId);
        } else if (notice.getId()!=null) {
            rulesAndNoticeService.delete(notice.getId());
        }
        return SUCCESS;
    }


    private String getMessage(Notice notice) {
        String message = "";
        message += notice.getTitle() + "<br/>";
        message += notice.getSubTitle() + "<br/>";
        switch (notice.getStatus()) {
            case RulesAndNoticeService.SCRATCH:
                message = "草稿保存成功。";
                break;
            case RulesAndNoticeService.WAITFOR_AUDIT:
                message = "待审稿保存成功。";
                break;

            case RulesAndNoticeService.WAITFOR_PUBLISH:
                message = "待发布稿保存成功。";
                break;

            case RulesAndNoticeService.PUBLISH:
                message = "成功发布。";
                break;
        }
        return message;
    }

    public RulesAndNoticeService getRulesAndNoticeService() {
        return rulesAndNoticeService;
    }

    public void setRulesAndNoticeService(RulesAndNoticeService rulesAndNoticeService) {
        this.rulesAndNoticeService = rulesAndNoticeService;
    }

    public Notice getNotice() {
        return notice;
    }

    public void setNotice(Notice notice) {
        this.notice = notice;
    }

    public Long getSpecId() {
        return specId;
    }

    public void setSpecId(Long specId) {
        this.specId = specId;
    }
}
