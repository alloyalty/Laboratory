package com.hgd.laboratory.util;

//需要用户名密码邮件发送实例
//文件名 SendEmail2.java
//本实例以QQ邮箱为例，你需要在qq后台设置

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailUtil {
	public enum EmailType {

	    SimpleEmail, HtmlEmail
	    }
	
	public static void main(String[] args) {
		// 收件人电子邮箱
		String to = "wangcq2171@163.com";

		// Set Subject: 头部头字段
		String subject = "This is the Subject Line!";
		// 设置消息体
		String txt = "This is test message by wcq";

		sendMail(to, subject, txt);
	}

	public static void sendMail(String to, String subject, String txt) {
		// 发件人电子邮箱
		String from = "1582495447@qq.com";
		String authorizationCode = "muftgblcntwbhcef";

		// 指定发送邮件的主机为 smtp.qq.com
		String host = "smtp.qq.com"; // QQ 邮件服务器

		_sendAEmail(new MailParam(to, from, authorizationCode, host, subject, txt),EmailType.HtmlEmail);
	}

	private static void _sendAEmail(MailParam parameterObject,EmailType emailType) {
		// 获取系统属性
		Properties properties = System.getProperties();

		// 设置邮件服务器
		properties.setProperty("mail.smtp.host", parameterObject.host);

		properties.put("mail.smtp.auth", "true");
		// 获取默认session对象
		Session session = Session.getDefaultInstance(properties, new Authenticator() {
			public PasswordAuthentication getPasswordAuthentication() {

				return new PasswordAuthentication(parameterObject.from, parameterObject.authorizationCode); // 发件人邮件用户名、授权码
			}
		});

		try {
			// 创建默认的 MimeMessage 对象
			MimeMessage message = new MimeMessage(session);

			// Set From: 头部头字段
			message.setFrom(new InternetAddress(parameterObject.from));

			// Set To: 头部头字段
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(parameterObject.to));
			message.setSubject(parameterObject.subject);

			if (emailType == EmailType.SimpleEmail)
			   message.setText(parameterObject.txt);
			else 
			   message.setContent(parameterObject.txt,"text/html;charset=utf-8");

			// 发送消息
			Transport.send(message);
			System.out.println("Sent message successfully....");

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
	
	
	
}
