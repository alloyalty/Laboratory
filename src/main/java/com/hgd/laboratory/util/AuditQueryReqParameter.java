package com.hgd.laboratory.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AuditQueryReqParameter {
    public String labId=null;
    public java.sql.Date applyDate=null;
    public Integer timeslot=null;
    public Integer state =null;

    public AuditQueryReqParameter(String labId, Date applyDate, Integer timeslot, Integer state)  {
        this.labId = labId;
        if (applyDate==null) {
            this.applyDate=null;
        } else {
            this.applyDate = new java.sql.Date(applyDate.getTime());
        }

        this.timeslot = timeslot;
        this.state = state;
    }
    public AuditQueryReqParameter(String labId, String applyDate, Integer timeslot, Integer state) throws ParseException {
        this.labId = labId;
        if (applyDate==null) {
            this.applyDate=null;
        } else {
            Date temp = new SimpleDateFormat( "yyyy-MM-dd" ).parse( applyDate );
            this.applyDate = new java.sql.Date(temp.getTime());
        }
        this.timeslot = timeslot;
        this.state = state;
    }
}
