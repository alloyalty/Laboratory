package com.hgd.laboratory.util;

import com.hgd.laboratory.po.OpenRes;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class ApplyUtil {

    public static String  initialAvailApplyCode(String labId,java.sql.Date applyTime,Integer timeslot) {
//已经测试
        java.util.Date date = new java.util.Date( applyTime.getTime());
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );

        int year = calendar.get( Calendar.YEAR );
        int month = calendar.get(Calendar.MONTH)+1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);

        return String.format( "%04d%02d%02d",year,month,day )+"#"+labId+"#"+ timeslot +"001";

    }
    //已经测试
    public static int calDayOfWeek(java.sql.Date applyDate) {
        // 星期1，返回1，....,星期日 返回7
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( applyDate);
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        int dfw = calendar.get(Calendar.DAY_OF_WEEK)-1;
        return dfw==0?7:dfw;
    }

    public static int calDayOfWeek(java.util.Date applyDate) {
        // 星期1，返回1，....,星期日 返回7
       return calDayOfWeek(new java.sql.Date(applyDate.getTime()));
    };

    public static  int calResTotalNum(String labId,Session session) {
        //可用于开放实验使用的机器数
        long pcnum = (long)session.createQuery(" select count(*) from Facility where lId=?1 and state <> 2" )
                .setParameter( 1,labId ).list().get( 0 );
        return (int)pcnum;
    }


    public static void fillResMess(Date date,String labId,int timeslot,OpenRes openRes,int resourceTotalNum) {

        openRes.setResTotalNum( resourceTotalNum );
        openRes.setResRest( resourceTotalNum );
        openRes.setResConsumed( 0 );

        java.sql.Date applyTime = new java.sql.Date( date.getTime() );
        openRes.setCurrentAvailApplyCode( initialAvailApplyCode( labId,applyTime,timeslot ) );
        openRes.setDayOfWeek( ApplyUtil.calDayOfWeek( applyTime ));
    }

    public static List<Long> parseApplyIds(String[] applyIds_str) {
        List<Long> applyIdList = new ArrayList<>();
        for (int i = 0; i < applyIds_str.length; i++) {
            try {
                long x = Long.parseLong( applyIds_str[i] );
                applyIdList.add( x );
            } catch (Exception e) {
            }

        }
        return applyIdList;
    }
}
