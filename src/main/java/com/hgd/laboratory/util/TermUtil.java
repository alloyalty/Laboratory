package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.Term;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.Calendar;
import java.util.Date;

public class TermUtil {
    private static TermUtil termUtil = null;
    private static Term term = null;   //当前学期
    private static Term customTerm =null;  //用户查桩
    //此处 term可以setter注入，用于测试

//    public static void setTerm(Term term) {
//        TermUtil.term = term;
//    }

    public static Integer calWeekOfTerm(Date date, java.sql.Date dayOfFirstWeekMonday) throws InvalidDateInTerm {
        //周次：
        //合法性： date >= term.firstMondyDate
        //计算方法： =（(date - term.firstMondayDate) % 7） +1

//        if (term.getDayOfFirstWeekMonday().compareTo(date) > 0)
//            throw new InvalidDateInTerm("非本学期日期");
        if (dayOfFirstWeekMonday.compareTo(date) > 0)
            throw new InvalidDateInTerm("非本学期日期");

        Calendar calendar = acqCalendarYearMonthDate(date);
        Calendar firstCalendar = acqCalendarYearMonthDate(dayOfFirstWeekMonday);
        long days = (long) ((calendar.getTimeInMillis() - firstCalendar.getTimeInMillis()) / (1000 * 60 * 60 * 24.00f));


        return (int) (days / 7) + 1;
    }

    public static int calWeekDay(Date date) {
        //星期一，1   ； 星期二,2 ; ...... ,星期日 7
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        //一周第一天是否为星期天
        boolean isFirstSunday = (calendar.getFirstDayOfWeek() == Calendar.SUNDAY);
         //获取周几
        int weekDay = calendar.get(Calendar.DAY_OF_WEEK);
        //若一周第一天为星期天，则-1
        if (isFirstSunday) {
            weekDay = weekDay - 1;
            if (weekDay == 0) {
                weekDay = 7;
            }
        }
        return weekDay;
    }

    public static Calendar acqCalendarYearMonthDate(Date theDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(theDate);

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int date = calendar.get(Calendar.DATE);

        Calendar calendarNew = Calendar.getInstance();
        calendarNew.set(year, month, date, 0, 0, 0);
        return calendarNew;
    }

    public static TermUtil getInstamce() {
        if (termUtil == null) {
            termUtil = new TermUtil();
        }
        return termUtil;

    }

    private static Term readFromDB() {
        Session session = ((SessionFactory) SpringUils.getBean("sessionFactory")).getCurrentSession();
        return (Term) session.createQuery("from Term where status=1").uniqueResult();

    }

    private TermUtil() {
        initTerm();
    }

    private static void initTerm() {
        //定制优先
        if (customTerm!=null)
            term = customTerm;
        if (term == null) {
            term = readFromDB();
        }
    }

    public static String getTermStr() {
        getInstamce();
        initTerm();
        if (term==null) return "";
        if (term.getStatus()==0) return "";

        return term.getYear() + "-" + term.getTerm();

    }

    public static void reset() {
        termUtil = null;
        term = null;
        customTerm =null;
    }

    public static Term getCurrentTerm() {
        getInstamce();
        return term;
    }

    public static void setCustomTerm(Term customTerm) {
        TermUtil.customTerm = customTerm;
    }
}
