package com.hgd.laboratory.util;

public class MailParam {
	
	public String from;
	public String authorizationCode;
	public String host;
	
	public String to;
	
	public String subject;
	public String txt;

	
	
	public MailParam(String to, String from, String authorizationCode, String host, String subject, String txt) {
		this.to = to;
		this.from = from;
		this.authorizationCode = authorizationCode;
		this.host = host;
		this.subject = subject;
		this.txt = txt;
	}



	
	
}