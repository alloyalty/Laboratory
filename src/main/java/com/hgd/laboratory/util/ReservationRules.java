package com.hgd.laboratory.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ReservationRules implements com.hgd.laboratory.service.ReservationRulesService {
    private int daysInAdvance=1;
    private int forUseDays=3;


    /*
        未来哪些天资源可见
        currentDate是 20180801
        可见天是20180803,20180804,20180805

     */
    @Override
    public List<Date> visibleResDates(Date currDate) {
        List<Date> dateList = new ArrayList<>(  );

        Calendar calendarNew = DateUtil.acqCalendarYearMonthDate(currDate);

        //取得当前日期，当前日期+2,+3,...
        calendarNew.add( Calendar.DATE,daysInAdvance );
        for (int i = 1; i <=forUseDays ; i++) {
            dateList.add(addDay(calendarNew,1));
        }
        return dateList;

    }

    private Date addDay(Calendar calendar,int days) {
        calendar.add(Calendar.DAY_OF_MONTH,days);
        return calendar.getTime();
    }

    @Override
    public boolean obeyRule(Date specDate, Date currDate) {
        /*
        currentDate是 20180801
        specDate 是20180803,20180804,20180805是符合规定的

        依据20180801 计算出合理的区间startdate.enddDate，如果在此区间之内，则合规；否则不规。
         */
       //取出年月日，取出合均由列表，比较
        Calendar date = DateUtil.acqCalendarYearMonthDate(specDate);
        List<Date> dateList = visibleResDates(currDate);

        Calendar startDate = DateUtil.acqCalendarYearMonthDate( dateList.get(0) );
        Calendar endDate =acqCalendarYearMonthDateMax(dateList.get( dateList.size()-1 ));
        return (date.compareTo( startDate)>=0  && date.compareTo( endDate)<=0 );
    }

    private Calendar acqCalendarYearMonthDateMax(Date theDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( theDate );

        int year = calendar.get( Calendar.YEAR );
        int month =calendar.get( Calendar.MONTH );
        int date = calendar.get(Calendar.DATE);

        Calendar calendarNew = Calendar.getInstance();
        calendarNew.set( year,month,date ,23,59,59);
        return calendarNew;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReservationRules that = (ReservationRules) o;
        return daysInAdvance == that.daysInAdvance &&
                forUseDays == that.forUseDays;
    }
    public static ReservationRules buildRR(String s) {

        InputStream in = ReservationRules.class.getResourceAsStream(s);
        if (in==null) System.out.println("未找到文件"+s);
        Properties p = new Properties();
        try {
            p.load(in);
            String daysInAdvance=p.getProperty("daysInAdvance");
            String forUseDays=p.getProperty("forUseDays");
            int idaysInAdvance = Integer.parseInt(daysInAdvance);
            int iforUseDays =Integer.parseInt(forUseDays);
            System.out.println(daysInAdvance+"，"+forUseDays);
            return  new ReservationRules(idaysInAdvance,iforUseDays);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public int hashCode() {
        return Objects.hash(daysInAdvance, forUseDays);
    }

    public ReservationRules() {
    }

    public ReservationRules(int daysInAdvance,int forUseDays) {
        this.daysInAdvance = daysInAdvance;
        this.forUseDays = forUseDays;
    }

    public int getDaysInAdvance() {
        return daysInAdvance;
    }

    public void setDaysInAdvance(int daysInAdvance) {
        this.daysInAdvance = daysInAdvance;
    }

    public int getForUseDays() {
        return forUseDays;
    }

    public void setForUseDays(int forUseDays) {
        this.forUseDays = forUseDays;
    }

}
