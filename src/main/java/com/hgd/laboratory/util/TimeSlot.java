package com.hgd.laboratory.util;

import java.util.Calendar;
import java.util.Date;

public class TimeSlot {
    static TimeSlot instance = null;

    private TimeSlot() {

    }

    public static TimeSlot getInstance() {
        if (instance == null)
            instance = new TimeSlot();
        return instance;
    }


    public static void setCustomTimeTable(String[][] customTimeTable) {
        TimeSlot.customTimeTable = customTimeTable;
    }

    static String[][] customTimeTable = null;

    public static void reset() {
        customTimeTable =null;
        instance =null;
    }
    //计算一个时段===

    private String[][] getTimeTable() {
        if (customTimeTable != null)
            return customTimeTable;
        else
            return getDefaultTimeTable();
    }

    private String[][] getDefaultTimeTable() {
        return new String[][]{{"8:30", "10:05"}, {"10:25", "12:00"},
                {"12:10", "14:20"}, {"14:30", "16:05"}, {"16:25", "18:00"},
                {"19:30", "21:10"}};

    }


    public long[][] trans(String[][] timeTableDef) {
        long[][] intTimeTable = new long[timeTableDef.length][2];
        for (int i = 0; i < timeTableDef.length; i++) {
            for (int j = 0; j < 2; j++) {
                final String str = timeTableDef[i][j];
                final int miliec = calMiliec(str);
                intTimeTable[i][j] = miliec;
            }
        }
        return intTimeTable;
    }

    public int calMiliec(String str) {
        String[] expr = str.split(":");
        int hour = Integer.parseInt(expr[0]);
        int minute = Integer.parseInt(expr[1]);
        return (hour * 60 + minute) * 60 * 1000;
    }

    public int calTimeslot(Date currDateTime) {
        //取currDateTime表示的hh,mmm，ss成一个long curr
        // 对于第一时段[start,end]之前的时间，如果  curr < start, 则返回timeslot=0
        // 对于最后一个时段之后的的时间，则返回 max（timeslot）+1

        // 对于一个时段 {start:end}, 如果   start=< curr< end 则返回 返回此时段的timeslot
        //对于一个时隙，表示[s1,t1],如果   s1=< curr <t1 ,则返回下一时段的timeslot

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(currDateTime);

        long curr = (calendar.get(Calendar.HOUR_OF_DAY) * 60 * 60 + calendar.get(Calendar.MINUTE) * 60 + calendar.get(Calendar.SECOND)) * 1000;

        long[][] intTimeTable = trans(getTimeTable());
        if (curr < intTimeTable[0][0]) return 0;
        if (curr > intTimeTable[intTimeTable.length - 1][1]) return intTimeTable.length + 1;

        for (int i = 0; i < intTimeTable.length; i++) {
            int timeslot = i + 1;
            if (curr >= intTimeTable[i][0] && curr <= intTimeTable[i][1])
                return timeslot;
            if (i != intTimeTable.length - 1) {//间隙
                if (curr > intTimeTable[i][1] && curr < intTimeTable[i + 1][0])
                    return timeslot + 1;
            }
        }

        throw new AssertionError();

    }


}
