package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.ActionOutAttriException;
import com.hgd.laboratory.exception.FormParameterException;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.ServletActionContext;

import javax.servlet.http.HttpServletRequest;
import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

public  class Debug {
    private static final Boolean  DEBUG =true;

    //用于action执行前、后输出action的输入、输出属性

    /**
     * 常量DEBUG 用于仅在调试下有效
     * @param before  指action 执行的前后
     * @param objs    输出action的属性
     */

    public static void consoleOut(boolean before,Object ...objs) {
        if (DEBUG) {
            if (before)
                System.out.print("Action 执行前：");
            else
                System.out.print("Action 执行后：");

            for (int i=0;i<objs.length;i++){
                System.out.print(objs[i].getClass().getSimpleName()+" ");
                System.out.println(objs[i]);
            }


        }
    }


    //用于检查表单必须提交的参数，只检查参数的存在性

    /**
     *
     * @param paraNames 表单中必须存在的参数名的集合
     * @throws FormParameterException 如果缺少某个参数，抛出此异常.此异常由struts.xml全局Exceptng-mapping 捕获，并转到错误页面显示
     */
    public static void paramInCheck(String[] paraNames) throws FormParameterException {
        Set<String> set = ServletActionContext.getRequest().getParameterMap().keySet();
        List<String> paramList = Arrays.asList(paraNames);
        List<String> unOfferParaList = new ArrayList<>();

        for (String name:set) {
            if (!paramList.contains(name))
                 unOfferParaList.add(name);
        }

        if (unOfferParaList.size()!=0) {
            StringBuffer bf = new StringBuffer();
            for (int i=0;i<unOfferParaList.size();i++) bf.append(" "+unOfferParaList.get(i));
            throw new FormParameterException("页面或者url未提供参数："+new String(bf));
        }
    }

    //用于获取一个java bean属性名集合，这是一个工具方法
    public static List<String> getEntityPropNames(Object bean){

        List<String> propNameList = new ArrayList<>();
        try{
            //throws IntrospectionException
            BeanInfo beanInfo= Introspector.getBeanInfo(bean.getClass(), Object.class);
            if(beanInfo!=null){
                for(PropertyDescriptor p: beanInfo.getPropertyDescriptors()){
                    //获得属性名称
                    propNameList.add(p.getName());

                }
            }

        }catch (Exception e){

        }
        return propNameList;


    }

    //action输出属性的检查

    /**
     *
     * @param paraNames action 必须输出的属性名集合
     * @throws ActionOutAttriException 缺少必须属性时抛出异常
     */
    public static void paramOutCheck(String[] paraNames) throws ActionOutAttriException {
        List<String> paramList = Arrays.asList(paraNames);
        paramOutCheck(paramList);

    }

    public static void paramOutCheck(List<String> paraNames) throws ActionOutAttriException {

        Set<String> set = ActionContext.getContext().getContextMap().keySet();
        List<String> paramList = paraNames ;

        List<String> unOfferParaList = new ArrayList<>();
        for (String name:paramList) {
            if (!set.contains(name))
                unOfferParaList.add(name);

        }
        if (unOfferParaList.size()!=0) {
            StringBuffer bf = new StringBuffer();
            for (int i=0;i<unOfferParaList.size();i++) bf.append(" "+unOfferParaList.get(i));
            throw new ActionOutAttriException("action未提供输出参数："+new String(bf));
        }
    }
}
