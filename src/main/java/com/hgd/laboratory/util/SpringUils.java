package com.hgd.laboratory.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;

@SuppressWarnings( "unchecked" )
public class SpringUils implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("\n\n\nSpringUtil 装配！！！");
        SpringUils.applicationContext =applicationContext;
    }

    public static <T> T getBean(String beanName) {
        if(applicationContext.containsBean( beanName )) {
            return (T) applicationContext.getBean( beanName );
        } else {
            return null;
        }
    }

    public static <T> Map<String, T> getBeansOfType(Class<T> baseType) {
        return applicationContext.getBeansOfType( baseType );

    }
}
