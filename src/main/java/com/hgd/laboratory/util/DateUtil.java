package com.hgd.laboratory.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtil {

    /**
     *
     * <p>Description: 本地时间转化为UTC时间</p>
     * @param localTime
     * @return
     * @author
     * @date
     *
     */
    public static Date localToUTC(String localTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date localDate= null;
        try {
            localDate = sdf.parse(localTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long localTimeInMillis=localDate.getTime();
        /** long时间转换成Calendar */
        Calendar calendar= Calendar.getInstance();
        calendar.setTimeInMillis(localTimeInMillis);
        /** 取得时间偏移量 */
        int zoneOffset = calendar.get(java.util.Calendar.ZONE_OFFSET);
        /** 取得夏令时差 */
        int dstOffset = calendar.get(java.util.Calendar.DST_OFFSET);
        /** 从本地时间里扣除这些差量，即可以取得UTC时间*/
        calendar.add(java.util.Calendar.MILLISECOND, -(zoneOffset + dstOffset));
        /** 取得的时间就是UTC标准时间 */
        Date utcDate=new Date(calendar.getTimeInMillis());
        return utcDate;
    }

    /**
     *
     * <p>Description:UTC时间转化为本地时间 </p>
     * @param utcTime
     * @return
     * @author wgs
     * @date  2018年10月19日 下午2:23:24
     *
     */
    public static Date utcToLocal(String utcTime){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date utcDate = null;
        try {
            utcDate = sdf.parse(utcTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        sdf.setTimeZone(TimeZone.getDefault());
        Date locatlDate = null;
        String localTime = sdf.format(utcDate.getTime());
        try {
            locatlDate = sdf.parse(localTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return locatlDate;
    }

    public static void main(String[] args) {
        String time = "2018-10-19 04:23:34";
        System.out.println(DateUtil.utcToLocal(time));
    }

    //标准的日期，只有yyyymmdd,没有hhmmss,或者说hh:mm:ss为00:00:00
    public static Date stdDate(Date date) {
        //换成calendar,求出yyyy,mm,dd重新构造一个即可
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis( date.getTime() );
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        calendar.set(year,month,day,00,00,00);
        calendar.set(Calendar.MILLISECOND,0);

        return new Date(calendar.getTimeInMillis());

    }

    public static  Calendar acqCalendarYearMonthDate(Date theDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime( theDate );

        int year = calendar.get( Calendar.YEAR );
        int month =calendar.get( Calendar.MONTH );
        int date = calendar.get(Calendar.DATE);

        Calendar calendarNew = Calendar.getInstance();
        calendarNew.set( year,month,date ,0,0,0);
        calendarNew.set(Calendar.MILLISECOND,0);
        return calendarNew;
    }
}
