package com.hgd.laboratory.util;

public class Page {
    private Long currentPage;//当前页 从1 开始
    private Integer perPageRows;//每页的记录数
    private Long totalRows;//总记录数：
    private int perPanelPages = 10; //每个panel 上的页码数


    public int getPerPanelPages() {
        return perPanelPages;
    }

    public void setPerPanelPages(int perPanelPages) {
        this.perPanelPages = perPanelPages;
    }

    public int getMaxPanelNo() {
        if (this.getTotalPages()!=0) {
            return (int) ((this.getTotalPages()-1 )/ perPanelPages  +1);
        }
        return 0;
    }

    public int getCurrentPanelNo() { //当前的pannel 号，从1开始,可依据currentPage计算出来
        return (int) ((currentPage-1) / perPanelPages  +1);
    }

    //计算当前panel中页码的延续一个页码和最后一个页码 [一个panel中，放5个页码]
    //例如当前 PanelNo=1, 应有 1,2,3,4,5
    //例如当前 PanelNo=2,应有  6,7,8,9,10
    //例如当前 PanelNo=
    public int getFirstPageNoInCurrentPanel() {
        return (this.getCurrentPanelNo()-1)*this.perPanelPages+1;
    }

    public int getLastPageNoInCurrentPanel() {
        //如果当前的PanelNo 不等于最大的 PanelMax， 则
        if (this.getCurrentPanelNo()!=this.getMaxPanelNo()) {
            return   this.getCurrentPanelNo()*this.perPanelPages;
        } else {
            return getTotalPages().intValue();
        }
    }

    public Long getFirstRecPos() {
        //用于计算分页的第一记录
        //query.setFirstResult(startRow);起始数据编号，从零开始编号。与数据库中的数据的id没有关系。
        //query.setMaxResults(maxResult);所要读取的数据条数
        return (currentPage - 1) * perPageRows;
    }
    public Long getLastRecPos() {
        if (this.getCurrentPage()!=this.getTotalPages()) {
            return currentPage  * perPageRows;
        }  else {
            return this.getTotalRows();
        }

    }

    public Long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Long currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getPerPageRows() {
        return perPageRows;
    }

    public void setPerPageRows(Integer perPageRows) {
        this.perPageRows = perPageRows;
    }

    public Long getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(Long totalRows) {
        this.totalRows = totalRows;
    }

    public Long getTotalPages() {
        //证明正确
        if (this.getPerPageRows()!=0) {
            return (this.getTotalRows()-1) / this.getPerPageRows() +1;
        }
        return 0L;
    }

    public Page(Long currentPage, Integer perPageRows, Long totalRows) {
        this.currentPage = currentPage;
        this.perPageRows = perPageRows;
        this.totalRows = totalRows;
        this.perPanelPages = 5;
    }

    public Page(Integer perPageRows) {
        this.perPageRows = perPageRows;
        this.currentPage = 1L;
        this.totalRows=0L;
    }

    public Page() {
    }

    public int getPrevPage() {
        if (this.currentPage-1<1) {
            return 1;
        }
        return ((Long)(currentPage-1L)).intValue();

    }

    public int getNextPage() {
        if (this.currentPage+1>this.getTotalPages()) {
            return (this.getTotalPages()).intValue();
        }
        return ((Long)(currentPage+1L)).intValue();
    }
}
