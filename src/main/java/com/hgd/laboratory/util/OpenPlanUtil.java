package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.po.TempOpenPlan;
import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.service.LabService;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import static java.time.temporal.ChronoUnit.DAYS;

public class OpenPlanUtil {
    //用于输出以周方式给出的开放计划表

    /**
     *
     * @param planDeatils  开放计划的明细
     */

    public static void printOpenPlanDetails(Map<String,String> planDeatils) {
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <=7; j++) {
                String key ="t"+i+"w"+j;
                System.out.printf("%-10s",planDeatils.get(key));
            }
            System.out.println();

        }
    }

    //在指定周次时，生成特定周的空白的临时计划（内含特定周的原计划）

    /**
     *
     * @param wOfTerm 指定的周次
     * @param optDetailMap 以周为周期的开放计划
     * @param weeksOfTerm
     * @return 空白的特定周次有临时计划，用于临时计划的填写
     * @throws Exception 如果原开放计划不合规，则抛出异常----一般情况下不应发生。
     */
    public static Map<String, TempItem>  construct_empty(int wOfTerm, Map<String, String> optDetailMap, final int weeksOfTerm) throws Exception {
        Map<String,TempItem> topDetailMap = new HashMap<>();

        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <=7 ; j++) {
                String key="t"+i+"w"+j;
                //在指定学期，实验室，指定周次，原OpenPlan是否开放{"0","1"}

                String orignOpen = openPlan_OpenStatus(wOfTerm,optDetailMap.get(key), weeksOfTerm);
                topDetailMap.put(key,new TempItem(orignOpen,""));

            }

        }
        return  topDetailMap;
    }

    /**
     *
     * @param wOfTerm 指定的周次
     * @param content 在开放计划中，特定时间段的内的开放计划的内容
     * @param weeksOfTerm
     * @return 指定的周次的开放计划状态{"1"，"0"} 开放、关闭
     * @throws OpenPlanExpressinException
     */
    public static String openPlan_OpenStatus(int wOfTerm, String content, final int weeksOfTerm) throws OpenPlanExpressinException{
        List<Integer> weekList = new ArrayList<>();
        String[] parts = content.split(",");

        for (int i = 0; i < parts.length ; i++) {
            String [] ele = parts[i].split("-");

            switch (ele.length) {
                case 0:
                    break;
                case 1:
                    if (!ele[0].trim().isEmpty()) {
                        int e = Integer.parseInt(ele[0]);
                        validWeek(e, weeksOfTerm);
                        weekList.add(e);
                    }
                    break;
                case 2:
                    int start = Integer.parseInt(ele[0]);
                    int end =   Integer.parseInt(ele[1]);
                    validWeek(start, weeksOfTerm);
                    validWeek(end, weeksOfTerm);
                    if (start>end) {
                        int tmp = start;
                        start = end;
                        end =tmp;
                    }

                    for (int j = start; j <=end ; j++) {
                        if (!weekList.contains(j))  weekList.add(j);
                    }
                    break;
                default:
                    throw new OpenPlanExpressinException("周次表达式错误:"+ parts[i]);
            }

        }
        Collections.sort(weekList);
        return  weekList.contains(wOfTerm)?"1":"0";


    }

    public static boolean validWeek(int e, final int weeksOfTerm) throws OpenPlanExpressinException {

        if (e<1  || e> weeksOfTerm) throw new OpenPlanExpressinException("周次非法:"+e);
        return true;
    }



    public static Map<String, TempItem> mockTempOpenPlanDetails( ) {
        Map<String,TempItem> topDetailMap = new HashMap<>();
        for (int i = 1; i < LabService.TIMESLOTNUM +1; i++) {
            for (int week=1; week<=7;week++) {
                String key ="t"+i+"w"+week;
                String orgin=i%2==1?"1":"0";
                String temp=week%2==1?"1":"0";
                topDetailMap.put(key,new TempItem(orgin,temp));
            }

        }
        return topDetailMap;

    }

    public static Map<String,TempItem> mockTempOpenPlanBlankDetails( ) {
        Map<String,TempItem> topDetailMap = new HashMap<>();
        for (int i = 1; i < LabService.TIMESLOTNUM +1; i++) {
            for (int week=1; week<=7;week++) {
                String key ="t"+i+"w"+week;
                String orgin=i%2==1?"1":"0";
                String temp="";
                topDetailMap.put(key,new TempItem(orgin,temp));
            }

        }
        return topDetailMap;

    }

    public static int getCurrentWeek(Term term,Date theDate ) {
        //给定所在学期、日期，求得所在周次
        return calWeekNo(theDate, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday());
    }

    public static int calWeekNo(Date theDate, java.sql.Date dayOfFirstWeekMonday) {
        LocalDate dateBefore=UDateToLocalDate(theDate);
        LocalDate dateAfter =UDateToLocalDate(dayOfFirstWeekMonday);
        Long daysBetween = DAYS.between(dateBefore, dateAfter);
        return (daysBetween.intValue() / 7)+1;
    }

    public static LocalDate UDateToLocalDate(Date date) {
        Instant instant = date.toInstant();
        ZoneId zone = ZoneId.systemDefault();
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, zone);
        return  localDateTime.toLocalDate();
    }
    public static String reverseToString(Set<Integer> openWeekSet) {
        if (openWeekSet==null) return "";
        List<Integer> list = new ArrayList<>(  );
        for (Integer x:openWeekSet  ) {
            list.add(x);
        }

        Collections.sort( list );
        StringBuffer sbuff = new StringBuffer(  );

        //扫描一段
        /*
        如果只有一个元素，直接返回此元素;
        如果有二个或者以上元素，则
            一直移动指针，直到失败为此
            while (list.get(start)+(index-start)== list.get(index)) do
              index++;
            start = index;

         */

        int start=0;
        int index =0;

        while (start<= list.size()-1)  {
            index =start;

            while (index<list.size() && (list.get( start )+(index-start)== list.get( index )) )
                index++;
            if (sbuff.length()!=0) sbuff.append( "," );
            if (index-1==start) {
                sbuff.append( list.get( start ) );
            } else {
                sbuff.append(""+ list.get( start )+"-"+list.get( index-1 ) );
            }
            start = index;

        };

        return new String(sbuff);
    }

    public static boolean valid_tempPlan(Set<Integer> openWeekSet,Integer weekOfTem,String orign,String temp) {
        //如果 temp=="" OK
        //否则  temp=="0" ==>weekOfTerm in openWeekSet
       //       temp="1"  ==>weekOfTerm not in openWeekSet

        if (orign.trim().equals( "0" ) && openWeekSet.contains( weekOfTem ))
            return false;

        if (orign.trim().equals( "1" ) && (!openWeekSet.contains( weekOfTem )))
            return false;

        if (temp.trim().isEmpty())
            return true;

        if (temp.trim().equals( orign.trim() ))
            return false;
        return true;
    }

    public static Set<Integer> transferToSet(String content) throws OpenPlanExpressinException {
        String[] parts = content.split(",");
        Set<Integer> weekList = new HashSet<>(  );
        int weeksOfTerm = TermUtil.getCurrentTerm().getWeeksOfTerm();

        for (int i = 0; i < parts.length ; i++) {
            String [] ele = parts[i].split("-");

            switch (ele.length) {
                case 0:
                    break;
                case 1:
                    if (!ele[0].trim().isEmpty()) {
                        int e = Integer.parseInt(ele[0].trim());
                        validWeek(e, weeksOfTerm);
                        weekList.add(e);
                    }
                    break;
                case 2:
                    int start = Integer.parseInt(ele[0].trim());
                    int end =   Integer.parseInt(ele[1].trim());
                    validWeek(start, weeksOfTerm);
                    validWeek(end, weeksOfTerm);
                    if (start>end) {
                        int tmp = start;
                        start = end;
                        end =tmp;
                    }

                    for (int j = start; j <=end ; j++) {
                        if (!weekList.contains(j))  weekList.add(j);
                    }
                    break;
                default:
                    throw new OpenPlanExpressinException("周次表达式错误:"+ parts[i]);
            }

        }
        return weekList;
    }
}
