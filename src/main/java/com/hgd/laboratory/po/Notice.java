package com.hgd.laboratory.po;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Notice {
    private Long id;
    private String category;
    private String title;
    private String subTitle;
    private String text;
    private String drawupId;
    private Date drawupDate;
    private String auditId;
    private String publishId;
    private Date publishDate;
    private Integer status;
    private String author;
    private Integer delflag;
    private String comment;

    public Notice(String category, String title, String subTitle, String text,  Integer status, String author, Integer delflag, String comment) {
        this.id = null;
        this.category = category;
        this.title = title;
        this.subTitle = subTitle;
        this.text = text;
        this.status = status;
        this.author = author;
        this.delflag = delflag;
        this.comment = comment;
    }

    public Notice() {
    }

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "category", nullable = false, length = 20)
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Basic
    @Column(name = "title", nullable = true, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "sub_title", nullable = true, length = 100)
    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    @Basic
    @Column(name = "text", nullable = true, length = 3000)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Basic
    @Column(name = "drawup_id", nullable = true, length = 12)
    public String getDrawupId() {
        return drawupId;
    }

    public void setDrawupId(String drawupId) {
        this.drawupId = drawupId;
    }

    @Basic
    @Column(name = "drawup_date", nullable = true)
    public Date getDrawupDate() {
        return drawupDate;
    }

    public void setDrawupDate(Date drawupDate) {
        this.drawupDate = drawupDate;
    }

    @Basic
    @Column(name = "audit_id", nullable = true, length = 12)
    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    @Basic
    @Column(name = "publish_id", nullable = true, length = 12)
    public String getPublishId() {
        return publishId;
    }

    public void setPublishId(String publishId) {
        this.publishId = publishId;
    }

    @Basic
    @Column(name = "publish_date", nullable = true)
    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "author", nullable = true, length = 20)
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "delflag", nullable = true)
    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }

    @Basic
    @Column(name = "comment", nullable = true, length = 50)
    public String getComment() {
        return comment;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Notice notice = (Notice) o;
        return Objects.equals(id, notice.id) &&
                Objects.equals(category, notice.category) &&
                Objects.equals(title, notice.title) &&
                Objects.equals(subTitle, notice.subTitle) &&
                Objects.equals(text, notice.text) &&
                Objects.equals(drawupId, notice.drawupId) &&
                Objects.equals(drawupDate, notice.drawupDate) &&
                Objects.equals(auditId, notice.auditId) &&
                Objects.equals(publishId, notice.publishId) &&
                Objects.equals(publishDate, notice.publishDate) &&
                Objects.equals(status, notice.status) &&
                Objects.equals(author, notice.author) &&
                Objects.equals(delflag, notice.delflag) &&
                Objects.equals(comment, notice.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, category, title, subTitle, text, drawupId, drawupDate, auditId, publishId, publishDate, status, author, delflag, comment);
    }

    public void setNotice(Notice n) {
        this.id = n.getId();
        this.category = n.getCategory();
        this.title = n.getTitle();
        this.subTitle = n.getSubTitle();
        this.text = n.getText();
        this.drawupId =n.getDrawupId();
        this.drawupDate = n.getDrawupDate();
        this.auditId = n.getAuditId();
        this.publishId = n.getPublishId();
        this.publishDate = n.getPublishDate();
        this.status = n.getStatus();
        this.author = n.getAuthor();
        this.comment = n.getComment();
    }



    public void setComment(String comment) {
        this.comment = comment;
    }
}
