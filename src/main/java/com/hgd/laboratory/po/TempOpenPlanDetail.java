package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "temp_open_plan_detail", schema = "reservation", catalog = "")
public class TempOpenPlanDetail {
    private Long id;
    private Integer dayOfWeek;
    private Integer timeSlot;
    private String orign;
    private String temp;
    private TempOpenPlan tempOpenPlanByTempPlanId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "dayOfWeek", nullable = true)
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "timeSlot", nullable = true)
    public Integer getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Integer timeSlot) {
        this.timeSlot = timeSlot;
    }

    @Basic
    @Column(name = "orign", nullable = true, length = 255)
    public String getOrign() {
        return orign;
    }

    public void setOrign(String orign) {
        this.orign = orign;
    }

    @Basic
    @Column(name = "temp", nullable = true, length = 255)
    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TempOpenPlanDetail that = (TempOpenPlanDetail) o;
        return Objects.equals( id,that.id ) &&
                Objects.equals( dayOfWeek,that.dayOfWeek ) &&
                Objects.equals( timeSlot,that.timeSlot ) &&
                Objects.equals( orign,that.orign ) &&
                Objects.equals( temp,that.temp );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id,dayOfWeek,timeSlot,orign,temp );
    }

    @ManyToOne
    @JoinColumn(name = "temp_plan_id", referencedColumnName = "temp_plan_id", nullable = false)
    public TempOpenPlan getTempOpenPlanByTempPlanId() {
        return tempOpenPlanByTempPlanId;
    }

    public void setTempOpenPlanByTempPlanId(TempOpenPlan tempOpenPlanByTempPlanId) {
        this.tempOpenPlanByTempPlanId = tempOpenPlanByTempPlanId;
    }

    public TempOpenPlanDetail(Long id,Integer dayOfWeek,Integer timeSlot,String orign,String temp,TempOpenPlan tempOpenPlanByTempPlanId) {
        this.id = id;
        this.dayOfWeek = dayOfWeek;
        this.timeSlot = timeSlot;
        this.orign = orign;
        this.temp = temp;
        this.tempOpenPlanByTempPlanId = tempOpenPlanByTempPlanId;
    }

    public TempOpenPlanDetail() {
    }
}
