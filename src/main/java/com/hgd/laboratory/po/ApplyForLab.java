package com.hgd.laboratory.po;

import com.hgd.laboratory.service.AnAppointmentService;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

@Entity
@Table(name = "apply_for_lab", schema = "reservation", catalog = "")
public class ApplyForLab {
    private Long applyNumber;
    private Date applyTime;
    private String userId;
    private String userName;
    private String labId;
    private String faciId;
    private String courseName;
    private String expName;
    private String hardware;
    private String software;
    private Integer timeslotStart;
    private Integer timeslotFinish;
    private Integer state;
    private String auditor;
    private Timestamp auditTime;
    private String term;
    private String applcatioCode;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "apply_number", nullable = false)
    public Long getApplyNumber() {
        return applyNumber;
    }

    public void setApplyNumber(Long applyNumber) {
        this.applyNumber = applyNumber;
    }

    @Basic
    @Column(name = "apply_time", nullable = false)
    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    @Basic
    @Column(name = "user_id", nullable = false, length = 12)
    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_name", nullable = false, length = 20)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "lab_id", nullable = true, length = 20)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "faci_id", nullable = true, length = 20)
    public String getFaciId() {
        return faciId;
    }

    public void setFaciId(String faciId) {
        this.faciId = faciId;
    }

    @Basic
    @Column(name = "course_name", nullable = true, length = 255)
    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Basic
    @Column(name = "exp_name", nullable = false, length = 30)
    public String getExpName() {
        return expName;
    }

    public void setExpName(String expName) {
        this.expName = expName;
    }

    @Basic
    @Column(name = "hardware", nullable = true, length = 255)
    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    @Basic
    @Column(name = "software", nullable = false, length = 500)
    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    @Basic
    @Column(name = "timeslot_start", nullable = false)
    public Integer getTimeslotStart() {
        return timeslotStart;
    }

    public void setTimeslotStart(Integer timeslotStart) {
        this.timeslotStart = timeslotStart;
    }

    @Basic
    @Column(name = "timeslot_finish", nullable = false)
    public Integer getTimeslotFinish() {
        return timeslotFinish;
    }

    public void setTimeslotFinish(Integer timeslotFinish) {
        this.timeslotFinish = timeslotFinish;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "auditor", nullable = true, length = 20)
    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    @Basic
    @Column(name = "audit_time", nullable = true)
    public Timestamp getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Timestamp auditTime) {
        this.auditTime = auditTime;
    }

    @Basic
    @Column(name = "term", nullable = false, length = 20)
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Basic
    @Column(name = "applcatio_code", nullable = true, length = 20)
    public String getApplcatioCode() {
        return applcatioCode;
    }

    public void setApplcatioCode(String applcatioCode) {
        this.applcatioCode = applcatioCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplyForLab that = (ApplyForLab) o;
        return Objects.equals(applyNumber, that.applyNumber) &&
                Objects.equals(applyTime, that.applyTime) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(labId, that.labId) &&
                Objects.equals(faciId, that.faciId) &&
                Objects.equals(courseName, that.courseName) &&
                Objects.equals(expName, that.expName) &&
                Objects.equals(hardware, that.hardware) &&
                Objects.equals(software, that.software) &&
                Objects.equals(timeslotStart, that.timeslotStart) &&
                Objects.equals(timeslotFinish, that.timeslotFinish) &&
                Objects.equals(state, that.state) &&
                Objects.equals(auditor, that.auditor) &&
                Objects.equals(auditTime, that.auditTime) &&
                Objects.equals(term, that.term) &&
                Objects.equals(applcatioCode, that.applcatioCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applyNumber, applyTime, userId, userName, labId, faciId, courseName, expName, hardware, software, timeslotStart, timeslotFinish, state, auditor, auditTime, term, applcatioCode);
    }

    public ApplyForLab(Date applyTime, String userId, String userName, String labId, String faciId, String courseName, String expName, String hardware, String software, Integer timeslotStart, Integer timeslotFinish, String term) {
        this.applyTime = applyTime;
        this.userId = userId;
        this.userName = userName;
        this.labId = labId;
        this.faciId = faciId;
        this.courseName = courseName;
        this.expName = expName;
        this.hardware = hardware;
        this.software = software;
        this.timeslotStart = timeslotStart;
        this.timeslotFinish = timeslotFinish;
        this.term = term;
        this.state = AnAppointmentService.WAITFOR_AUDIT;
    }
    private String nulltoEmpty(String x) {
        return x==null?"":x.toString();
    }
    public  String genConstructCode() {
       return
        String.format("ApplyForLab.genApplyForLab(%s,'%s','%s','%s','%s','%s','%s','%s','%s','%s',%s,%s,%s,'%s','%s','%s','%s')",
        applyNumber,
        applyTime,
                nulltoEmpty(userId),
                nulltoEmpty(userName),
                nulltoEmpty(labId),
                nulltoEmpty(faciId),
                nulltoEmpty(courseName),
                nulltoEmpty(expName),
                nulltoEmpty(hardware),
                nulltoEmpty(software),
        timeslotStart,
        timeslotFinish,
        state,
                nulltoEmpty(auditor),
        auditTime,
                nulltoEmpty(term),
                nulltoEmpty(applcatioCode)
                );

    }

    public  ApplyForLab genApplyForLab(Long applyNumber, String applyTimeStr, String userId, String userName, String labId, String faciId, String courseName, String expName, String hardware, String software, Integer timeslotStart, Integer timeslotFinish, Integer state, String auditor, String auditTimeStr, String term, String applcatioCode)   {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date applyTime = new Date(sdf.parse(applyTimeStr).getTime());

            Timestamp auditTime = null;
            if (!auditTimeStr.equalsIgnoreCase("null")) {
                auditTime = new Timestamp(sdf.parse(auditTimeStr).getTime());
            }
            return new ApplyForLab(applyNumber,applyTime,userId,userName,labId,faciId, courseName, expName, hardware, software, timeslotStart, timeslotFinish, state, auditor, auditTime,  term,  applcatioCode);

        } catch (Exception e) {
            return null;
        }

    }

    public ApplyForLab(Long applyNumber, Date applyTime, String userId, String userName, String labId, String faciId, String courseName, String expName, String hardware, String software, Integer timeslotStart, Integer timeslotFinish, Integer state, String auditor, Timestamp auditTime, String term, String applcatioCode) {
        this.applyNumber = applyNumber;
        this.applyTime = applyTime;
        this.userId = userId;
        this.userName = userName;
        this.labId = labId;
        this.faciId = faciId;
        this.courseName = courseName;
        this.expName = expName;
        this.hardware = hardware;
        this.software = software;
        this.timeslotStart = timeslotStart;
        this.timeslotFinish = timeslotFinish;
        this.state = state;
        this.auditor = auditor;
        this.auditTime = auditTime;
        this.term = term;
        this.applcatioCode = applcatioCode;
    }

    public ApplyForLab() {
    }


//    int WAITFOR_AUDIT=0;
//    int AUDIT_NOPASS =1;
//    int AUDITPASS_UNUSE=2;
//    int AUDITPASS_USED=3;
//    int CANCEL_APPLY =4;
    public static String stateDisplay(int status) {
        String [] display = new String[] {"待审核","审核未通过","审核通过但未使用","审核通过且已使用","被取消预约"};
        if (status>=0 && status<=4) {
            return display[status];
        }
        return "未定义";
    }
}
