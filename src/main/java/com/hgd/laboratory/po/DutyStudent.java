package com.hgd.laboratory.po;

import com.hgd.laboratory.util.TermUtil;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "duty_student", schema = "reservation", catalog = "")
public class DutyStudent {
    private Integer id;
    private String semester;
    private String sId;
    private String sName;
    private String email;
    private Integer delflag;
    private String specialty;
    private String sClass;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "semester", nullable = false, length = 20)
    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    @Basic
    @Column(name = "s_id", nullable = false, length = 12)
    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    @Basic
    @Column(name = "s_name", nullable = false, length = 20)
    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 25)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "delflag", nullable = true)
    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DutyStudent that = (DutyStudent) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(semester, that.semester) &&
                Objects.equals(sId, that.sId) &&
                Objects.equals(sName, that.sName) &&
                Objects.equals(email, that.email) &&
                Objects.equals(delflag, that.delflag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, semester, sId, sName, email, delflag);
    }

    @Basic
    @Column(name = "specialty", nullable = false, length = 50)
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Basic
    @Column(name = "s_class", nullable = false, length = 4)
    public String getsClass() {
        return sClass;
    }

    public void setsClass(String sClass) {
        this.sClass = sClass;
    }

    public DutyStudent(Integer id, String semester, String sId, String sName, String email, Integer delflag, String specialty, String sClass) {
        this.id = id;
        this.semester = semester;
        this.sId = sId;
        this.sName = sName;
        this.email = email;
        this.delflag = delflag;
        this.specialty = specialty;
        this.sClass = sClass;
    }

    public DutyStudent() {
    }

    public DutyStudent(Student student) {
        this.id = null;
        this.semester = TermUtil.getTermStr();
        this.sId = student.getsId();
        this.sName = student.getsName();
        this.email = student.getEmail();
        this.delflag = 0;
        this.specialty = student.getSpecialty();
        this.sClass = student.getsClass();
    }

}
