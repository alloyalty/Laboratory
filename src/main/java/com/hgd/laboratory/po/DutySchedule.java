package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "duty_schedule", schema = "reservation", catalog = "")
public class DutySchedule {
    private Integer dutyScheduleId;
    private String term;
    private String labId;
    private Integer status;
    private String desp;

    @Id
    @Column(name = "duty_schedule_id", nullable = false)
    public Integer getDutyScheduleId() {
        return dutyScheduleId;
    }

    public void setDutyScheduleId(Integer dutyScheduleId) {
        this.dutyScheduleId = dutyScheduleId;
    }

    @Basic
    @Column(name = "term", nullable = false, length = 11)
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Basic
    @Column(name = "labID", nullable = false, length = 10)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "desp", nullable = true, length = 255)
    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DutySchedule that = (DutySchedule) o;
        return Objects.equals(dutyScheduleId, that.dutyScheduleId) &&
                Objects.equals(term, that.term) &&
                Objects.equals(labId, that.labId) &&
                Objects.equals(status, that.status) &&
                Objects.equals(desp, that.desp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dutyScheduleId, term, labId, status, desp);
    }

    public DutySchedule(String term, String labId, Integer status, String desp) {
        this.term = term;
        this.labId = labId;
        this.status = status;
        this.desp = desp;
    }
}
