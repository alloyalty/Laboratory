package com.hgd.laboratory.po;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "open_res", schema = "reservation", catalog = "")
public class OpenRes {
    private Long id;
    private Integer timeSlot;
    private Date openDate;
    private Integer dayOfWeek;
    private String labId;
    private Integer resTotalNum;
    private Integer resConsumed;
    private Integer resRest;
    private String currentAvailApplyCode;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "time_slot", nullable = false)
    public Integer getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Integer timeSlot) {
        this.timeSlot = timeSlot;
    }

    @Basic
    @Column(name = "open_date", nullable = false)
    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    @Basic
    @Column(name = "day_of_week", nullable = false)
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "lab_id", nullable = false, length = 20)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "res_total_num", nullable = true)
    public Integer getResTotalNum() {
        return resTotalNum;
    }

    public void setResTotalNum(Integer resTotalNum) {
        this.resTotalNum = resTotalNum;
    }

    @Basic
    @Column(name = "res_consumed", nullable = true)
    public Integer getResConsumed() {
        return resConsumed;
    }

    public void setResConsumed(Integer resConsumed) {
        this.resConsumed = resConsumed;
    }

    @Basic
    @Column(name = "res_rest", nullable = true)
    public Integer getResRest() {
        return resRest;
    }

    public void setResRest(Integer resRest) {
        this.resRest = resRest;
    }

    @Basic
    @Column(name = "current_avail_apply_code", nullable = true, length = 20)
    public String getCurrentAvailApplyCode() {
        return currentAvailApplyCode;
    }

    public void setCurrentAvailApplyCode(String currentAvailApplyCode) {
        this.currentAvailApplyCode = currentAvailApplyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenRes openRes = (OpenRes) o;
        return Objects.equals( id,openRes.id ) &&
                Objects.equals( timeSlot,openRes.timeSlot ) &&
                Objects.equals( openDate,openRes.openDate ) &&
                Objects.equals( dayOfWeek,openRes.dayOfWeek ) &&
                Objects.equals( labId,openRes.labId ) &&
                Objects.equals( resTotalNum,openRes.resTotalNum ) &&
                Objects.equals( resConsumed,openRes.resConsumed ) &&
                Objects.equals( resRest,openRes.resRest ) &&
                Objects.equals( currentAvailApplyCode,openRes.currentAvailApplyCode );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id,timeSlot,openDate,dayOfWeek,labId,resTotalNum,resConsumed,resRest,currentAvailApplyCode );
    }

    public OpenRes(Integer timeSlot,Date openDate,String labId) {
        this.timeSlot = timeSlot;
        this.openDate = openDate;
        this.labId = labId;
    }

    public OpenRes() {
    }

    public OpenRes(Integer timeSlot, Date openDate, Integer dayOfWeek, String labId, Integer resTotalNum, Integer resConsumed, Integer resRest, String currentAvailApplyCode) {
        this.id = null;
        this.timeSlot = timeSlot;
        this.openDate = openDate;
        this.dayOfWeek = dayOfWeek;
        this.labId = labId;
        this.resTotalNum = resTotalNum;
        this.resConsumed = resConsumed;
        this.resRest = resRest;
        this.currentAvailApplyCode = currentAvailApplyCode;
    }

    public boolean contentEquals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenRes openRes = (OpenRes) o;
        return  Objects.equals( timeSlot,openRes.timeSlot ) &&
                Objects.equals( openDate,openRes.openDate ) &&
                Objects.equals( dayOfWeek,openRes.dayOfWeek ) &&
                Objects.equals( labId,openRes.labId ) &&
                Objects.equals( resTotalNum,openRes.resTotalNum ) &&
                Objects.equals( resConsumed,openRes.resConsumed ) &&
                Objects.equals( resRest,openRes.resRest ) &&
                Objects.equals( currentAvailApplyCode,openRes.currentAvailApplyCode );
    }

    @Override
    public String toString() {
        return "OpenRes{" +
                "id=" + id +
                ", timeSlot=" + timeSlot +
                ", openDate=" + openDate +
                ", dayOfWeek=" + dayOfWeek +
                ", labId='" + labId + '\'' +
                ", resTotalNum=" + resTotalNum +
                ", resConsumed=" + resConsumed +
                ", resRest=" + resRest +
                ", currentAvailApplyCode='" + currentAvailApplyCode + '\'' +
                '}';
    }
}
