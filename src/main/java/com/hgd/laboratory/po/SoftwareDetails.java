package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "software_details", schema = "reservation", catalog = "")
public class SoftwareDetails {
    private Integer id;
    private String sofwareName;
    private String verion;
    private SoftwareConfig softwareConfigByConfigid;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "sofware_name", nullable = false, length = 80)
    public String getSofwareName() {
        return sofwareName;
    }

    public void setSofwareName(String sofwareName) {
        this.sofwareName = sofwareName;
    }

    @Basic
    @Column(name = "verion", nullable = true, length = 80)
    public String getVerion() {
        return verion;
    }

    public void setVerion(String verion) {
        this.verion = verion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SoftwareDetails that = (SoftwareDetails) o;
        return Objects.equals( id,that.id ) &&
                Objects.equals( sofwareName,that.sofwareName ) &&
                Objects.equals( verion,that.verion );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id,sofwareName,verion );
    }

    @ManyToOne
    @JoinColumn(name = "configid", referencedColumnName = "configid")
    public SoftwareConfig getSoftwareConfigByConfigid() {
        return softwareConfigByConfigid;
    }

    public void setSoftwareConfigByConfigid(SoftwareConfig softwareConfigByConfigid) {
        this.softwareConfigByConfigid = softwareConfigByConfigid;
    }

    @Override
    public String toString() {
        return "SoftwareDetails{" +
                "id=" + id +
                ", sofwareName='" + sofwareName + '\'' +
                ", verion='" + verion + '\'' +
                ", softwareConfigByConfigid=" + softwareConfigByConfigid +
                '}';
    }


}
