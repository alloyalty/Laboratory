package com.hgd.laboratory.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class User {
    private String id;
    private String name;
    private String password;
    private String role;
    private String emaill;
    private Timestamp outDate;
    private String validateCode;

    @Id
    @Column(name = "id", nullable = false, length = 12)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 20)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "role", nullable = false, length = 20)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Basic
    @Column(name = "emaill", nullable = true, length = 50)
    public String getEmaill() {
        return emaill;
    }

    public void setEmaill(String emaill) {
        this.emaill = emaill;
    }

    @Basic
    @Column(name = "out_date", nullable = true)
    public Timestamp getOutDate() {
        return outDate;
    }

    public void setOutDate(Timestamp outDate) {
        this.outDate = outDate;
    }

    @Basic
    @Column(name = "validate_code", nullable = true, length = 50)
    public String getValidateCode() {
        return validateCode;
    }

    public void setValidateCode(String validateCode) {
        this.validateCode = validateCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(name, user.name) &&
                Objects.equals(password, user.password) &&
                role.equals(user.role) &&
                Objects.equals(emaill, user.emaill);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, password, role, emaill);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", role='" + role + '\'' +
                ", emaill='" + emaill + '\'' +
                '}';
    }

    public String acqSystemRole() {
        if (!role.equalsIgnoreCase("labManager"))
            return role;
        if ("admin".equalsIgnoreCase(id)) {
            return "admin";
        } else {
            return "labManager";
        }


    }

    public User(String id, String password, String role, String emaill) {
        this.id = id;
        this.password = password;
        this.role = role;
        this.emaill = emaill;
        this.name ="unknown";
    }

    public User() {
    }
}

