package com.hgd.laboratory.po;

import javax.persistence.*;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Objects;

@Entity
public class Term {
    private Long id;
    private String year;
    private Integer term;
    private Integer weeksOfTerm;
    private Date dayOfFirstWeekMonday;
    private Integer status;



    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "year", nullable = false, length = 20)
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "term", nullable = false)
    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    @Basic
    @Column(name = "weeks_of_term", nullable = false)
    public Integer getWeeksOfTerm() {
        return weeksOfTerm;
    }

    public void setWeeksOfTerm(Integer weeksOfTerm) {
        this.weeksOfTerm = weeksOfTerm;
    }

    @Basic
    @Column(name = "day_of_first_week_monday", nullable = false)
    public Date getDayOfFirstWeekMonday() {
        return dayOfFirstWeekMonday;
    }

    public void setDayOfFirstWeekMonday(Date dayOfFirstWeekMonday) {
        this.dayOfFirstWeekMonday = dayOfFirstWeekMonday;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Term term1 = (Term) o;
        return Objects.equals( id,term1.id ) &&
                Objects.equals( year,term1.year ) &&
                Objects.equals( term,term1.term ) &&
                Objects.equals( weeksOfTerm,term1.weeksOfTerm ) &&
                Objects.equals( dayOfFirstWeekMonday,term1.dayOfFirstWeekMonday ) &&
                Objects.equals( status,term1.status );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id,year,term,weeksOfTerm,dayOfFirstWeekMonday,status );
    }

    public Term(String year, Integer term, Integer weeksOfTerm, Date dayOfFirstWeekMonday) {
        this.year = year;
        this.term = term;
        this.weeksOfTerm = weeksOfTerm;
        this.dayOfFirstWeekMonday = dayOfFirstWeekMonday;
        this.status=1;
    }

    public Term(String year, int term, int weeksOfTerm, String s) throws ParseException {
        java.util.Date sDate = new SimpleDateFormat("yyyy-MM-dd").parse(s);
        Date firstDay = new java.sql.Date(sDate.getTime());
        this.year = year;
        this.term = term;
        this.weeksOfTerm = weeksOfTerm;
        this.dayOfFirstWeekMonday = firstDay;
        this.status=1;


    }
    public static  Term termBuilder(String year, Integer term, Integer weeksOfTerm, String dayOfFirstWeekMonday) throws ParseException {
        Term t = new Term();
        t.setYear(year);
        t.setTerm(term);
        t.setWeeksOfTerm(weeksOfTerm);
        java.util.Date day = new SimpleDateFormat("yyyy-MM-dd").parse(dayOfFirstWeekMonday);
        t.setDayOfFirstWeekMonday(new  java.sql.Date(day.getTime()));
        t.setStatus(1);
        return t;
    }

    public Term() {
    }
}
