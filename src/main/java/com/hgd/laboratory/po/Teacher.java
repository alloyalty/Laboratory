package com.hgd.laboratory.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Teacher {
    private String tId;
    private String tName;
    private String collage;
    private String email;

    @Id
    @Column(name = "t_id", nullable = false, length = 20)
    public String gettId() {
        return tId;
    }

    public void settId(String tId) {
        this.tId = tId;
    }

    @Basic
    @Column(name = "t_name", nullable = false, length = 20)
    public String gettName() {
        return tName;
    }

    public void settName(String tName) {
        this.tName = tName;
    }

    @Basic
    @Column(name = "collage", nullable = false, length = 50)
    public String getCollage() {
        return collage;
    }

    public void setCollage(String collage) {
        this.collage = collage;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 25)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return Objects.equals(tId, teacher.tId) &&
                Objects.equals(tName, teacher.tName) &&
                Objects.equals(collage, teacher.collage) &&
                Objects.equals(email, teacher.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tId, tName, collage, email);
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "tId='" + tId + '\'' +
                ", tName='" + tName + '\'' +
                ", collage='" + collage + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public Teacher(String tId, String tName, String collage) {
        this.tId = tId;
        this.tName = tName;
        this.collage = collage;
        this.email="";

    }

    public Teacher() {
    }
}
