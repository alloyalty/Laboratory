package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "resource_detail", schema = "reservation", catalog = "")
public class ResourceDetail {
    private Long id;
    private String faciId;
    private String applcationCode;
    private OpenRes openResByOpenResId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "faci_id", nullable = true, length = 20)
    public String getFaciId() {
        return faciId;
    }

    public void setFaciId(String faciId) {
        this.faciId = faciId;
    }

    @Basic
    @Column(name = "applcation_code", nullable = true, length = 20)
    public String getApplcationCode() {
        return applcationCode;
    }

    public void setApplcationCode(String applcationCode) {
        this.applcationCode = applcationCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceDetail that = (ResourceDetail) o;
        return Objects.equals( id,that.id ) &&
                Objects.equals( faciId,that.faciId ) &&
                Objects.equals( applcationCode,that.applcationCode );
    }

    @Override
    public int hashCode() {
        return Objects.hash( id,faciId,applcationCode );
    }

    @ManyToOne
    @JoinColumn(name = "open_res_id", referencedColumnName = "id")
    public OpenRes getOpenResByOpenResId() {
        return openResByOpenResId;
    }

    public void setOpenResByOpenResId(OpenRes openResByOpenResId) {
        this.openResByOpenResId = openResByOpenResId;
    }
}
