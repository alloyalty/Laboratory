package com.hgd.laboratory.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Facility {
    private String fId;
    private String fName;
    private int state;
    private String softName;
    private String lId;

    @Id
    @Column(name = "f_id", nullable = false, length = 20)
    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    @Basic
    @Column(name = "f_name", nullable = false, length = 20)
    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Basic
    @Column(name = "softName", nullable = false, length = 20)
    public String getSoftName() {
        return softName;
    }

    public void setSoftName(String softName) {
        this.softName = softName;
    }

    @Basic
    @Column(name = "l_id", nullable = false, length = 20)
    public String getlId() {
        return lId;
    }

    public void setlId(String lId) {
        this.lId = lId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Facility facility = (Facility) o;
        return state == facility.state &&
                Objects.equals(fId,facility.fId) &&
                Objects.equals(fName,facility.fName) &&
                Objects.equals(softName,facility.softName) &&
                Objects.equals(lId,facility.lId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fId,fName,state,softName,lId);
    }

    @Override
    public String toString() {
        return "Facility{" +
                "fId='" + fId + '\'' +
                ", fName='" + fName + '\'' +
                ", state=" + state +
                ", softName='" + softName + '\'' +
                ", lId='" + lId + '\'' +
                '}';
    }

    public Facility() {
    }

    public Facility(String fId, String fName, int state, String softName, String lId) {
        this.fId = fId;
        this.fName = fName;
        this.state = state;
        this.softName = softName;
        this.lId = lId;
    }
}
