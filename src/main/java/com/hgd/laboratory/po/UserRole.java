package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "user_role", schema = "reservation", catalog = "")
public class UserRole {
    private Integer iduserRole;
    private String role;
    private User userById;

    @Id
    @Column(name = "iduser_role", nullable = false)
    public Integer getIduserRole() {
        return iduserRole;
    }

    public void setIduserRole(Integer iduserRole) {
        this.iduserRole = iduserRole;
    }

    @Basic
    @Column(name = "role", nullable = true, length = 20)
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRole userRole = (UserRole) o;
        return Objects.equals(iduserRole, userRole.iduserRole) &&
                Objects.equals(role, userRole.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(iduserRole, role);
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false)
    public User getUserById() {
        return userById;
    }

    public void setUserById(User userById) {
        this.userById = userById;
    }
}
