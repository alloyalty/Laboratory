package com.hgd.laboratory.po;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "resource_spec_date", schema = "reservation", catalog = "")
public class ResourceSpecDate {
    private String generationDate;



    @Id
    @Column(name = "generation_date", nullable = false, length = 8)
    public String getGenerationDate() {
        return generationDate;
    }

    public void setGenerationDate(String generationDate) {
        this.generationDate = generationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ResourceSpecDate that = (ResourceSpecDate) o;
        return Objects.equals( generationDate,that.generationDate );
    }

    @Override
    public int hashCode() {
        return Objects.hash( generationDate );
    }

    public ResourceSpecDate(String id) {
        this.generationDate =id;
    }

    public ResourceSpecDate() {
    }
}
