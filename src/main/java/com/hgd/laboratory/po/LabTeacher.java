package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "lab_teacher", schema = "reservation", catalog = "")
public class LabTeacher {
    private Integer id;
    private String code;
    private String name;
    private String collage;
    private String email;
    private Integer delflag;

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "code", nullable = false, length = 20)
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 20)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "collage", nullable = false, length = 50)
    public String getCollage() {
        return collage;
    }

    public void setCollage(String collage) {
        this.collage = collage;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 25)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "delflag", nullable = false)
    public Integer getDelflag() {
        return delflag;
    }

    public void setDelflag(Integer delflag) {
        this.delflag = delflag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabTeacher that = (LabTeacher) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(code, that.code) &&
                Objects.equals(name, that.name) &&
                Objects.equals(collage, that.collage) &&
                Objects.equals(email, that.email) &&
                Objects.equals(delflag, that.delflag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code, name, collage, email, delflag);
    }

    public LabTeacher() {
    }

    public LabTeacher(Integer id, String code, String name, String collage, String email, Integer delflag) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.collage = collage;
        this.email = email;
        this.delflag = delflag;
    }
    public LabTeacher(Teacher teacher) {
        this.id = null;
        this.code = teacher.gettId();
        this.name = teacher.gettName();
        this.collage = teacher.getCollage();
        this.email = teacher.getEmail();
        this.delflag = 0;
    }
}
