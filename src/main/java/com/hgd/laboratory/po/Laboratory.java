package com.hgd.laboratory.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Laboratory {
    private String labId;
    private String labName;
    private String adress;
    private Integer state;
    private Integer labFor;
    private Integer installSoft;
    private String hardware;

    @Id
    @Column(name = "lab_id", nullable = false, length = 20)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "lab_name", nullable = false, length = 20)
    public String getLabName() {
        return labName;
    }

    public void setLabName(String labName) {
        this.labName = labName;
    }

    @Basic
    @Column(name = "adress", nullable = false, length = 20)
    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    @Basic
    @Column(name = "state", nullable = false)
    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    @Basic
    @Column(name = "lab_for", nullable = false)
    public Integer getLabFor() {
        return labFor;
    }

    public void setLabFor(Integer labFor) {
        this.labFor = labFor;
    }

    @Basic
    @Column(name = "installSoft", nullable = true)
    public Integer getInstallSoft() {
        return installSoft;
    }

    public void setInstallSoft(Integer installSoft) {
        this.installSoft = installSoft;
    }

    @Basic
    @Column(name = "hardware", nullable = true, length = 300)
    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Laboratory that = (Laboratory) o;
        return Objects.equals( labId,that.labId ) &&
                Objects.equals( labName,that.labName ) &&
                Objects.equals( adress,that.adress ) &&
                Objects.equals( state,that.state ) &&
                Objects.equals( labFor,that.labFor ) &&
                Objects.equals( installSoft,that.installSoft ) &&
                Objects.equals( hardware,that.hardware );
    }

    @Override
    public int hashCode() {
        return Objects.hash( labId,labName,adress,state,labFor,installSoft,hardware );
    }

    public Laboratory(String labId, String labName, String adress, Integer state, Integer labFor, Integer installSoft, String hardware) {
        this.labId = labId;
        this.labName = labName;
        this.adress = adress;
        this.state = state;
        this.labFor = labFor;
        this.installSoft = installSoft;
        this.hardware = hardware;
    }

    public Laboratory() {
    }
}
