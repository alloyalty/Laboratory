package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "lab_open_plan", schema = "reservation", catalog = "")
public class LabOpenPlan {
    private Integer openPlanId;
    private String term;
    private String labId;
    private Integer status;
    private String desp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "open_plan_id", nullable = false)
    public Integer getOpenPlanId() {
        return openPlanId;
    }

    public void setOpenPlanId(Integer openPlanId) {
        this.openPlanId = openPlanId;
    }

    @Basic
    @Column(name = "term", nullable = false, length = 11)
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Basic
    @Column(name = "labID", nullable = false, length = 10)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "desp", nullable = true, length = 255)
    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabOpenPlan that = (LabOpenPlan) o;
        return Objects.equals(openPlanId,that.openPlanId) &&
                Objects.equals(term,that.term) &&
                Objects.equals(labId,that.labId) &&
                Objects.equals(status,that.status) &&
                Objects.equals(desp,that.desp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(openPlanId,term,labId,status,desp);
    }


    public LabOpenPlan(Integer openPlanId,String term,String labId,Integer status,String desp) {
        this.openPlanId = openPlanId;
        this.term = term;
        this.labId = labId;
        this.status = status;
        this.desp = desp;
    }

    public LabOpenPlan() {
    }
}
