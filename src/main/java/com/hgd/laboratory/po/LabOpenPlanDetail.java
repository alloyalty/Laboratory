package com.hgd.laboratory.po;

import com.hgd.laboratory.service.LabService;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "lab_open_plan_detail", schema = "reservation", catalog = "")
public class LabOpenPlanDetail {
    private long detailId;
    private Integer dayOfWeek;
    private Integer timeSlot;
    private String content;
    private LabOpenPlan labOpenPlanByOpenPlanId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "detailID", nullable = false)
    public long getDetailId() {
        return detailId;
    }

    public void setDetailId(long detailId) {
        this.detailId = detailId;
    }

    @Basic
    @Column(name = "dayOfWeek", nullable = true)
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "timeSlot", nullable = true)
    public Integer getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Integer timeSlot) {
        this.timeSlot = timeSlot;
    }

    @Basic
    @Column(name = "content", nullable = true, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LabOpenPlanDetail that = (LabOpenPlanDetail) o;
        return detailId == that.detailId &&
                Objects.equals(dayOfWeek,that.dayOfWeek) &&
                Objects.equals(timeSlot,that.timeSlot) &&
                Objects.equals(content,that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(detailId,dayOfWeek,timeSlot,content);
    }

    @ManyToOne
    @JoinColumn(name = "open_plan_id", referencedColumnName = "open_plan_id", nullable = false)
    public LabOpenPlan getLabOpenPlanByOpenPlanId() {
        return labOpenPlanByOpenPlanId;
    }

    public void setLabOpenPlanByOpenPlanId(LabOpenPlan labOpenPlanByOpenPlanId) {
        this.labOpenPlanByOpenPlanId = labOpenPlanByOpenPlanId;
    }



    public static Map<String,String> mockLabOpenPlanBlankDetails( ) {
        Map<String,String> lopDetailMap = new HashMap<>();
        for (int i = 1; i < LabService.TIMESLOTNUM +1; i++) {
            for (int week=1; week<=7;week++) {
                String key ="t"+i+"w"+week;
                lopDetailMap.put(key,"");
            }

        }
        return lopDetailMap;
    }
}
