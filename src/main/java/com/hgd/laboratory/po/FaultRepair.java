package com.hgd.laboratory.po;

import com.hgd.laboratory.service.FaultRepairService;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "fault_repair", schema = "reservation", catalog = "")
public class FaultRepair {
    private Integer id;
    private String lid;
    private String fid;
    private String faultDFesp;
    private Timestamp fillingDate;
    private String fillingId;
    private Integer fstate;
    private String maintenceRecs;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "Lid", nullable = false, length = 10)
    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    @Basic
    @Column(name = "Fid", nullable = false, length = 20)
    public String getFid() {
        return fid;
    }

    public void setFid(String fid) {
        this.fid = fid;
    }

    @Basic
    @Column(name = "FaultDFesp", nullable = true, length = 300)
    public String getFaultDFesp() {
        return faultDFesp;
    }

    public void setFaultDFesp(String faultDFesp) {
        this.faultDFesp = faultDFesp;
    }

    @Basic
    @Column(name = "FillingDate", nullable = false)
    public Timestamp getFillingDate() {
        return fillingDate;
    }

    public void setFillingDate(Timestamp fillingDate) {
        this.fillingDate = fillingDate;
    }

    @Basic
    @Column(name = "FillingId", nullable = true, length = 12)
    public String getFillingId() {
        return fillingId;
    }

    public void setFillingId(String fillingId) {
        this.fillingId = fillingId;
    }

    @Basic
    @Column(name = "Fstate", nullable = true)
    public Integer getFstate() {
        return fstate;
    }

    public void setFstate(Integer fstate) {
        this.fstate = fstate;
    }

    @Basic
    @Column(name = "MaintenceRecs", nullable = true, length = 300)
    public String getMaintenceRecs() {
        return maintenceRecs;
    }

    public void setMaintenceRecs(String maintenceRecs) {
        this.maintenceRecs = maintenceRecs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FaultRepair that = (FaultRepair) o;
        return id.equals(that.id) &&
                Objects.equals(lid, that.lid) &&
                Objects.equals(fid, that.fid) &&
                Objects.equals(faultDFesp, that.faultDFesp) &&
                Objects.equals(fillingDate, that.fillingDate) &&
                Objects.equals(fillingId, that.fillingId) &&
                Objects.equals(fstate, that.fstate) &&
                Objects.equals(maintenceRecs, that.maintenceRecs);
    }

    public boolean equalsInfo(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FaultRepair that = (FaultRepair) o;
        return id == that.id &&
                Objects.equals(lid,that.lid) &&
                Objects.equals(fid,that.fid) &&
                Objects.equals(faultDFesp,that.faultDFesp) &&
                Objects.equals(fstate,that.fstate) &&
                Objects.equals(maintenceRecs,that.maintenceRecs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, lid, fid, faultDFesp, fillingDate, fillingId, fstate, maintenceRecs);
    }


    public FaultRepair(String lid, String fid, String faultDFesp) {
        //你以为填报故障为依据，生成构造函数
        this.lid = lid;
        this.fid = fid;
        this.faultDFesp = faultDFesp;


        //Timestamp fillingDate
        this.fillingDate = new Timestamp(new Date().getTime());
        this.fstate = FaultRepairService.FAULT;
    }

    public FaultRepair() {
    }

    @Override
    public String toString() {
        return "FaultRepair{" +
                "id=" + id +
                ", lid='" + lid + '\'' +
                ", fid='" + fid + '\'' +
                ", faultDFesp='" + faultDFesp + '\'' +
                ", fillingDate=" + fillingDate +
                ", fillingId=" + fillingId +
                ", fstate=" + fstate +
                ", maintenceRecs='" + maintenceRecs + '\'' +
                '}';
    }
}
