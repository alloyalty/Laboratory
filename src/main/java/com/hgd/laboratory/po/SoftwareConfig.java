package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "software_config", schema = "reservation", catalog = "")
public class SoftwareConfig {
    private Integer configid;
    private String configName;
    private String desp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "configid", nullable = false)
    public Integer getConfigid() {
        return configid;
    }

    public void setConfigid(Integer configid) {
        this.configid = configid;
    }

    @Basic
    @Column(name = "config_name", nullable = false, length = 20)
    public String getConfigName() {
        return configName;
    }

    public void setConfigName(String configName) {
        this.configName = configName;
    }

    @Basic
    @Column(name = "desp", nullable = true, length = 500)
    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SoftwareConfig that = (SoftwareConfig) o;
        return Objects.equals( configid,that.configid ) &&
                Objects.equals( configName,that.configName ) &&
                Objects.equals( desp,that.desp );
    }

    @Override
    public int hashCode() {
        return Objects.hash( configid,configName,desp );
    }

    @Override
    public String toString() {
        return "SoftwareConfig{" +
                "configid=" + configid +
                ", configName='" + configName + '\'' +
                ", desp='" + desp + '\'' +
                '}';
    }

    public SoftwareConfig(String configName, String desp) {
        this.configName = configName;
        this.desp = desp;
    }

    public SoftwareConfig() {
    }
}
