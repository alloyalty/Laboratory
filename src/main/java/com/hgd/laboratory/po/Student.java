package com.hgd.laboratory.po;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Student {
    private String sId;
    private String sName;
    private String collage;
    private String specialty;
    private String sClass;
    private String email;
    private Integer sAllDefault;
    private Integer sNewDefault;
    private Integer spunish;

    @Id
    @Column(name = "s_id", nullable = false, length = 12)
    public String getsId() {
        return sId;
    }

    public void setsId(String sId) {
        this.sId = sId;
    }

    @Basic
    @Column(name = "s_name", nullable = false, length = 20)
    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    @Basic
    @Column(name = "collage", nullable = false, length = 50)
    public String getCollage() {
        return collage;
    }

    public void setCollage(String collage) {
        this.collage = collage;
    }

    @Basic
    @Column(name = "specialty", nullable = false, length = 50)
    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    @Basic
    @Column(name = "s_class", nullable = false, length = 4)
    public String getsClass() {
        return sClass;
    }

    public void setsClass(String sClass) {
        this.sClass = sClass;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 25)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "s_AllDefault", nullable = false)
    public Integer getsAllDefault() {
        return sAllDefault;
    }

    public void setsAllDefault(Integer sAllDefault) {
        this.sAllDefault = sAllDefault;
    }

    @Basic
    @Column(name = "s_NewDefault", nullable = false)
    public Integer getsNewDefault() {
        return sNewDefault;
    }

    public void setsNewDefault(Integer sNewDefault) {
        this.sNewDefault = sNewDefault;
    }

    @Basic
    @Column(name = "spunish", nullable = false)
    public Integer getSpunish() {
        return spunish;
    }

    public void setSpunish(Integer spunish) {
        this.spunish = spunish;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return sId.equals(student.sId) &&
                sName.equals(student.sName) &&
                collage.equals(student.collage) &&
                specialty.equals(student.specialty) &&
                sClass.equals(student.sClass) &&
                Objects.equals(email, student.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sId, sName, collage, specialty, sClass, email);
    }

    @Override
    public String toString() {
        return "Student{" +
                "sId='" + sId + '\'' +
                ", sName='" + sName + '\'' +
                ", collage='" + collage + '\'' +
                ", specialty='" + specialty + '\'' +
                ", sClass='" + sClass + '\'' +
                ", email='" + email + '\'' +
                '}';
    }

    public Student(String sId, String sName, String collage, String specialty, String sClass, Integer sAllDefault, Integer sNewDefault, Integer spunish) {
        this.sId = sId;
        this.sName = sName;
        this.collage = collage;
        this.specialty = specialty;
        this.sClass = sClass;
        this.sAllDefault = sAllDefault;
        this.sNewDefault = sNewDefault;
        this.spunish = spunish;
        this.email="";
    }

    public Student() {
    }
}
