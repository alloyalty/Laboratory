package com.hgd.laboratory.po;

import com.hgd.laboratory.service.LabService;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "open_lab_schedule_detail", schema = "reservation", catalog = "")
public class OpenLabScheduleDetail {
    private Long detailId;
    private Integer timeSlot;
    private Integer dayOfWeek;
    private String content;
    private OpenLabSchedule openLabScheduleByOpenScheId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "detailID", nullable = false)
    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    @Basic
    @Column(name = "timeSlot", nullable = true)
    public Integer getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Integer timeSlot) {
        this.timeSlot = timeSlot;
    }

    @Basic
    @Column(name = "dayOfWeek", nullable = true)
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "content", nullable = true, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenLabScheduleDetail that = (OpenLabScheduleDetail) o;
        return Objects.equals( detailId,that.detailId ) &&
                Objects.equals( timeSlot,that.timeSlot ) &&
                Objects.equals( dayOfWeek,that.dayOfWeek ) &&
                Objects.equals( content,that.content );
    }

    @Override
    public int hashCode() {
        return Objects.hash( detailId,timeSlot,dayOfWeek,content );
    }

    @ManyToOne
    @JoinColumn(name = "open_sche_id", referencedColumnName = "open_sche_id", nullable = false)
    public OpenLabSchedule getOpenLabScheduleByOpenScheId() {
        return openLabScheduleByOpenScheId;
    }

    public void setOpenLabScheduleByOpenScheId(OpenLabSchedule openLabScheduleByOpenScheId) {
        this.openLabScheduleByOpenScheId = openLabScheduleByOpenScheId;
    }


    public static Map<String,String> mockScheduleDetailMap() {
        Map<String,String> map = new HashMap<>();

        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <=7 ; j++) {
                String key ="t"+i+"w"+j;
                String value =""+i+"-"+j;
                map.put(key,value);
            }

        }
        return map;
    }
    public static Map<String,String> mockScheduleDetailMap1() {
        Map<String,String> map = new HashMap<>();

        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <=7 ; j++) {
                String key ="t"+i+"w"+j;
                String value =""+i+"-"+j+","+(j+3);
                map.put(key,value);
            }

        }
        return map;
    }

    public OpenLabScheduleDetail() {
    }

    public OpenLabScheduleDetail(Long detailId,Integer timeSlot,Integer dayOfWeek,String content,OpenLabSchedule openLabScheduleByOpenScheId) {
        this.detailId = detailId;
        this.timeSlot = timeSlot;
        this.dayOfWeek = dayOfWeek;
        this.content = content;
        this.openLabScheduleByOpenScheId = openLabScheduleByOpenScheId;
    }
}
