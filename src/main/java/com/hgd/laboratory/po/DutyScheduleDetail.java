package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Map;
import java.util.Objects;

@Entity
@Table(name = "duty_schedule_detail", schema = "reservation", catalog = "")
public class DutyScheduleDetail {
    private Long detailId;
    private Integer timeSlot;
    private Integer dayOfWeek;
    private String content;
    private DutySchedule dutyScheduleByDutyScheduleId;

    public static Map<String, String> mockBlankDetail() {
        //todo
        return null;
    }

    @Id
    @Column(name = "detailID", nullable = false)
    public Long getDetailId() {
        return detailId;
    }

    public void setDetailId(Long detailId) {
        this.detailId = detailId;
    }

    @Basic
    @Column(name = "timeSlot", nullable = false)
    public Integer getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(Integer timeSlot) {
        this.timeSlot = timeSlot;
    }

    @Basic
    @Column(name = "dayOfWeek", nullable = false)
    public Integer getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    @Basic
    @Column(name = "content", nullable = true, length = 255)
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DutyScheduleDetail that = (DutyScheduleDetail) o;
        return Objects.equals(detailId, that.detailId) &&
                Objects.equals(timeSlot, that.timeSlot) &&
                Objects.equals(dayOfWeek, that.dayOfWeek) &&
                Objects.equals(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(detailId, timeSlot, dayOfWeek, content);
    }

    @ManyToOne
    @JoinColumn(name = "duty_schedule_id", referencedColumnName = "duty_schedule_id", nullable = false)
    public DutySchedule getDutyScheduleByDutyScheduleId() {
        return dutyScheduleByDutyScheduleId;
    }

    public void setDutyScheduleByDutyScheduleId(DutySchedule dutyScheduleByDutyScheduleId) {
        this.dutyScheduleByDutyScheduleId = dutyScheduleByDutyScheduleId;
    }
}
