package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "temp_open_plan", schema = "reservation", catalog = "")
public class TempOpenPlan {
    private Integer tempPlanId;
    private String term;
    private String labId;
    private Integer weekOfTerm;
    private Integer status;
    private String desp;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "temp_plan_id", nullable = false)
    public Integer getTempPlanId() {
        return tempPlanId;
    }

    public void setTempPlanId(Integer tempPlanId) {
        this.tempPlanId = tempPlanId;
    }

    @Basic
    @Column(name = "term", nullable = false, length = 11)
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Basic
    @Column(name = "labID", nullable = false, length = 10)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "weekOfTerm", nullable = false)
    public Integer getWeekOfTerm() {
        return weekOfTerm;
    }

    public void setWeekOfTerm(Integer weekOfTerm) {
        this.weekOfTerm = weekOfTerm;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "desp", nullable = true, length = 255)
    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TempOpenPlan that = (TempOpenPlan) o;
        return Objects.equals( tempPlanId,that.tempPlanId ) &&
                Objects.equals( term,that.term ) &&
                Objects.equals( labId,that.labId ) &&
                Objects.equals( weekOfTerm,that.weekOfTerm ) &&
                Objects.equals( status,that.status ) &&
                Objects.equals( desp,that.desp );
    }

    @Override
    public int hashCode() {
        return Objects.hash( tempPlanId,term,labId,weekOfTerm,status,desp );
    }

    public TempOpenPlan(Integer tempPlanId,String term,String labId,Integer weekOfTerm,Integer status,String desp) {
        this.tempPlanId = tempPlanId;
        this.term = term;
        this.labId = labId;
        this.weekOfTerm = weekOfTerm;
        this.status = status;
        this.desp = desp;
    }

    public TempOpenPlan() {
    }
}
