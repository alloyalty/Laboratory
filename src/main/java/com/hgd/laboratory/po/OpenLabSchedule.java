package com.hgd.laboratory.po;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "open_lab_schedule", schema = "reservation", catalog = "")
public class OpenLabSchedule {
    private Integer openScheId;
    private String term;
    private String labId;
    private Integer status;
    private String desp;

    @Id
    @Column(name = "open_sche_id", nullable = false)
    public Integer getOpenScheId() {
        return openScheId;
    }

    public void setOpenScheId(Integer openScheId) {
        this.openScheId = openScheId;
    }

    @Basic
    @Column(name = "term", nullable = false, length = 11)
    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    @Basic
    @Column(name = "labID", nullable = false, length = 10)
    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    @Basic
    @Column(name = "status", nullable = true)
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Basic
    @Column(name = "desp", nullable = true, length = 255)
    public String getDesp() {
        return desp;
    }

    public void setDesp(String desp) {
        this.desp = desp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OpenLabSchedule that = (OpenLabSchedule) o;
        return Objects.equals( openScheId,that.openScheId ) &&
                Objects.equals( term,that.term ) &&
                Objects.equals( labId,that.labId ) &&
                Objects.equals( status,that.status ) &&
                Objects.equals( desp,that.desp );
    }

    @Override
    public int hashCode() {
        return Objects.hash( openScheId,term,labId,status,desp );
    }


    public OpenLabSchedule() {
    }

    public OpenLabSchedule(Integer openScheId,String term,String labId,Integer status,String desp) {
        this.openScheId = openScheId;
        this.term = term;
        this.labId = labId;
        this.status = status;
        this.desp = desp;
    }

    public static OpenLabSchedule mockInstance() {
        return new OpenLabSchedule(102,"2018-2019-1","6312",1,"TEST02");

    }
    public static OpenLabSchedule mockInstance1() {
        return new OpenLabSchedule(101,"2018-2019-1","6310",1,"TEST01");

    }
}
