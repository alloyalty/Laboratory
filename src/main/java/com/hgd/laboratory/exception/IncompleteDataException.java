package com.hgd.laboratory.exception;

public class IncompleteDataException extends Exception {
    public IncompleteDataException(String message) {
        super( message );
    }

    public IncompleteDataException() {
        super();
    }
}
