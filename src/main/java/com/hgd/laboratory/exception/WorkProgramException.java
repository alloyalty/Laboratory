package com.hgd.laboratory.exception;

public class WorkProgramException extends Exception {
    public WorkProgramException() {
    }

    public WorkProgramException(String message) {
        super( message );
    }
}
