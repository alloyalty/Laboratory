package com.hgd.laboratory.exception;

public class FormParameterException extends Exception {
    public FormParameterException() {
    }

    public FormParameterException(String message) {
        super(message);
    }
}
