package com.hgd.laboratory.exception;

public class ActionOutAttriException extends Exception {
    public ActionOutAttriException() {

    }

    public ActionOutAttriException(String message) {
        super(message);
    }
}
