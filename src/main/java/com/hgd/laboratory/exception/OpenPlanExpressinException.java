package com.hgd.laboratory.exception;

public class OpenPlanExpressinException extends Exception {
    public OpenPlanExpressinException() {
    }

    public OpenPlanExpressinException(String message) {
        super(message);
    }
}
