package com.hgd.laboratory.exception;

public class InvalidDateInTerm extends Exception {
    public InvalidDateInTerm() {
        super();
    }

    public InvalidDateInTerm(String message) {
        super( message );
    }
}
