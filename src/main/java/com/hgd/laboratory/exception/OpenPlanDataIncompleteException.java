package com.hgd.laboratory.exception;

public class OpenPlanDataIncompleteException extends Exception {
    public OpenPlanDataIncompleteException() {
    }

    public OpenPlanDataIncompleteException(String message) {
        super(message);
    }
}
