package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.OpenLabSchedule;

import java.util.Map;

public class OpenLabScheduleTuple {
    private OpenLabSchedule openLabSchedule;
    private Map<String,String> openLabScheduleDetailMap;

    public OpenLabScheduleTuple(OpenLabSchedule openLabSchedule,Map<String, String> openLabScheduleDetailMap) {
        this.openLabSchedule = openLabSchedule;
        this.openLabScheduleDetailMap = openLabScheduleDetailMap;
    }

    public OpenLabSchedule getOpenLabSchedule() {
        return openLabSchedule;
    }

    public void setOpenLabSchedule(OpenLabSchedule openLabSchedule) {
        this.openLabSchedule = openLabSchedule;
    }

    public Map<String, String> getOpenLabScheduleDetailMap() {
        return openLabScheduleDetailMap;
    }

    public void setOpenLabScheduleDetailMap(Map<String, String> openLabScheduleDetailMap) {
        this.openLabScheduleDetailMap = openLabScheduleDetailMap;
    }
}
