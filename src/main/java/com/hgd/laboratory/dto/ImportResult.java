package com.hgd.laboratory.dto;

import java.util.List;

public class ImportResult {
    String importFileName;
    List<String> unImportDatas;  //未成功导入的数据
    int importedDataRows;  //已经成功导入数据的行数;
    int totalRows;
    String result=null;

    public String toString() {
        StringBuffer buff= new StringBuffer();
        buff.append("<b>").append(this.importFileName).append("</b>");
        buff.append("   数据总行数:");
        buff.append(this.getTotalRows());

        buff.append("<br/>成功导入行数:").append(this.getImportedDataRows());
        if (this.getTotalRows()>this.getImportedDataRows()) {
            buff.append(",未导入行数:").append(this.getTotalRows()-this.getImportedDataRows());
        }
        buff.append("<br/>").append(this.getResult());
        buff.append("<br/><br/>");
        return new String(buff);
    }
    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getImportFileName() {
        return importFileName;
    }

    public void setImportFileName(String importFileName) {
        this.importFileName = importFileName;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public List<String> getUnImportDatas() {
        return unImportDatas;
    }

    public void setUnImportDatas(List<String> unImportDatas) {
        this.unImportDatas = unImportDatas;
    }

    public int getImportedDataRows() {
        return importedDataRows;
    }

    public void setImportedDataRows(int importedDataRows) {
        this.importedDataRows = importedDataRows;
    }
}
