package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.LabOpenPlan;

import java.util.Map;

public class LabOpenPlanTuple {
    private LabOpenPlan labOpenPlan;
    private Map<String,String> labOpenPlanDetailMap;

    public LabOpenPlan getLabOpenPlan() {
        return labOpenPlan;
    }

    public void setLabOpenPlan(LabOpenPlan labOpenPlan) {
        this.labOpenPlan = labOpenPlan;
    }

    public Map<String, String> getLabOpenPlanDetailMap() {
        return labOpenPlanDetailMap;
    }

    public void setLabOpenPlanDetailMap(Map<String, String> labOpenPlanDetailMap) {
        this.labOpenPlanDetailMap = labOpenPlanDetailMap;
    }

    public LabOpenPlanTuple(LabOpenPlan labOpenPlan,Map<String, String> labOpenPlanDetailMap) {
        this.labOpenPlan = labOpenPlan;
        this.labOpenPlanDetailMap = labOpenPlanDetailMap;
    }

    public LabOpenPlanTuple() {
    }
}
