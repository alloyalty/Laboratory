package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.OpenRes;
import com.hgd.laboratory.po.Student;

import java.util.List;

public class ApplyForLabAudit {
    private ApplyForLab applyForLab;//申请信息
    private Student student;       //学生信息
    private List<OpenRes> openResList;//申请的实验室及其时段资源信息

    public ApplyForLabAudit() {
    }

    public ApplyForLabAudit(ApplyForLab applyForLab, Student student, List<OpenRes> openResList) {
        this.applyForLab = applyForLab;
        this.student = student;
        this.openResList = openResList;
    }

    public ApplyForLab getApplyForLab() {
        return applyForLab;
    }

    public void setApplyForLab(ApplyForLab applyForLab) {
        this.applyForLab = applyForLab;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<OpenRes> getOpenResList() {
        return openResList;
    }

    public void setOpenResList(List<OpenRes> openResList) {
        this.openResList = openResList;
    }
}
