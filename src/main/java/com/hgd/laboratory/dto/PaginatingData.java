package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.util.Page;

import java.util.List;

public class PaginatingData<T> {
    private Page page;
    private List<ApplyForLab> applyForLabList;


    public PaginatingData() {
    }

    public PaginatingData(Page page,List<ApplyForLab> applyForLabList) {
        this.page = page;
        this.applyForLabList = applyForLabList;
    }

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<ApplyForLab> getApplyForLabList() {
        return applyForLabList;
    }

    public void setApplyForLabList(List<ApplyForLab> applyForLabList) {
        this.applyForLabList = applyForLabList;
    }
}
