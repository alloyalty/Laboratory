package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.TempOpenPlan;

import java.util.Map;

public class TempOpenPlanTuple {
    private TempOpenPlan tempOpenPlan;
    private Map<String, TempItem> tempOpenPlanDetailMap;

    public TempOpenPlanTuple(TempOpenPlan tempOpenPlan,Map<String, TempItem> tempOpenPlanDetailMap) {

        this.tempOpenPlan = tempOpenPlan;
        this.tempOpenPlanDetailMap = tempOpenPlanDetailMap;
    }

    public TempOpenPlan getTempOpenPlan() {
        return tempOpenPlan;
    }

    public void setTempOpenPlan(TempOpenPlan tempOpenPlan) {
        this.tempOpenPlan = tempOpenPlan;
    }

    public Map<String, TempItem> getTempOpenPlanDetailMap() {
        return tempOpenPlanDetailMap;
    }

    public void setTempOpenPlanDetailMap(Map<String, TempItem> tempOpenPlanDetailMap) {
        this.tempOpenPlanDetailMap = tempOpenPlanDetailMap;
    }
}
