package com.hgd.laboratory.dto;

import java.util.Objects;

//临时计划有二项数据，便于处理，定义一个类
public class TempItem {
    private String orign;
    private String temp;

    public String getOrign() {
        return orign;
    }

    public void setOrign(String orign) {
        this.orign = orign;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public TempItem(String orign,String temp) {
        this.orign = orign;
        this.temp = temp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TempItem tempItem = (TempItem) o;
        return Objects.equals(getOrign(),tempItem.getOrign()) &&
                Objects.equals(getTemp(),tempItem.getTemp());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOrign(),getTemp());
    }
}
