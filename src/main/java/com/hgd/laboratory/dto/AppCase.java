package com.hgd.laboratory.dto;

import java.util.Objects;

public class AppCase {
    private String labId;
    private int timeSlot;
    private int rest;  //可用资源数量
    private boolean isOpen;
    private boolean isSubscribed;
    private boolean ignored;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AppCase appCase = (AppCase) o;
        return getTimeSlot() == appCase.getTimeSlot() &&
                isOpen() == appCase.isOpen() &&
                isSubscribed() == appCase.isSubscribed() &&
                getLabId().equals( appCase.getLabId() );
    }

    @Override
    public int hashCode() {
        return Objects.hash( getLabId(),getTimeSlot(),isOpen(),isSubscribed() );
    }

    public AppCase(String labId,int timeSlot,boolean isOpen,boolean isSubscribed,int rest) {
        this.labId = labId;
        this.timeSlot = timeSlot;
        this.isOpen = isOpen;
        this.isSubscribed = isSubscribed;
        this.rest = rest;
        this.ignored = false;
    }

    public AppCase() {
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public int getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(int timeSlot) {
        this.timeSlot = timeSlot;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public boolean isSubscribed() {
        return isSubscribed;
    }

    public void setSubscribed(boolean subscribed) {
        isSubscribed = subscribed;
    }

    public int getRest() {
        return rest;
    }

    public void setRest(int rest) {
        this.rest = rest;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }
}
