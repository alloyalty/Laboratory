package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.AnAppointmentService;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

public class ApplyForLabDTO {
    private Long applyNumber;
    private java.sql.Date applyTime;
    private String userId;
    private String userName;
    private String labId;
    private String faciId;
    private String courseName;
    private String expName;
    private String hardware;
    private String software;
    private Integer timeslotStart;
    private Integer timeslotFinish;
    private Integer state;
    private String auditor;
    private Timestamp auditTime;
    private String term;
    private String applcatioCode;

    public Long getApplyNumber() {
        return applyNumber;
    }

    public void setApplyNumber(Long applyNumber) {
        this.applyNumber = applyNumber;
    }


    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLabId() {
        return labId;
    }

    public void setLabId(String labId) {
        this.labId = labId;
    }

    public String getFaciId() {
        return faciId;
    }

    public void setFaciId(String faciId) {
        this.faciId = faciId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getExpName() {
        return expName;
    }

    public void setExpName(String expName) {
        this.expName = expName;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public String getSoftware() {
        return software;
    }

    public void setSoftware(String software) {
        this.software = software;
    }

    public Integer getTimeslotStart() {
        return timeslotStart;
    }

    public void setTimeslotStart(Integer timeslotStart) {
        this.timeslotStart = timeslotStart;
    }

    public Integer getTimeslotFinish() {
        return timeslotFinish;
    }

    public void setTimeslotFinish(Integer timeslotFinish) {
        this.timeslotFinish = timeslotFinish;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public String getAuditor() {
        return auditor;
    }

    public void setAuditor(String auditor) {
        this.auditor = auditor;
    }

    public Timestamp getAuditTime() {
        return auditTime;
    }

    public void setAuditTime(Timestamp auditTime) {
        this.auditTime = auditTime;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getApplcatioCode() {
        return applcatioCode;
    }

    public void setApplcatioCode(String applcatioCode) {
        this.applcatioCode = applcatioCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplyForLabDTO that = (ApplyForLabDTO) o;
        return sAllDefault == that.sAllDefault &&
                sNewDefault == that.sNewDefault &&
                spunish == that.spunish &&
                Objects.equals(applyNumber, that.applyNumber) &&
                Objects.equals(applyTime, that.applyTime) &&
                Objects.equals(userId, that.userId) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(labId, that.labId) &&
                Objects.equals(faciId, that.faciId) &&
                Objects.equals(courseName, that.courseName) &&
                Objects.equals(expName, that.expName) &&
                Objects.equals(hardware, that.hardware) &&
                Objects.equals(software, that.software) &&
                Objects.equals(timeslotStart, that.timeslotStart) &&
                Objects.equals(timeslotFinish, that.timeslotFinish) &&
                Objects.equals(state, that.state) &&
                Objects.equals(auditor, that.auditor) &&
                Objects.equals(auditTime, that.auditTime) &&
                Objects.equals(term, that.term) &&
                Objects.equals(applcatioCode, that.applcatioCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applyNumber, applyTime, userId, userName, labId, faciId, courseName, expName, hardware, software, timeslotStart, timeslotFinish, state, auditor, auditTime, term, applcatioCode, sAllDefault, sNewDefault, spunish);
    }

    public ApplyForLabDTO(java.sql.Date applyTime, String userId, String userName, String labId, String faciId, String courseName, String expName, String hardware, String software, Integer timeslotStart, Integer timeslotFinish, String term) {
        this.applyTime = applyTime;
        this.userId = userId;
        this.userName = userName;
        this.labId = labId;

        this.faciId = faciId;
        this.courseName = courseName;
        this.expName = expName;
        this.hardware = hardware;

        this.software = software;
        this.timeslotStart = timeslotStart;
        this.timeslotFinish = timeslotFinish;
        this.term = term;
        this.state = AnAppointmentService.WAITFOR_AUDIT;
    }

    public ApplyForLabDTO(ApplyForLab alf,int sAllDefault,int sNewDefault,int spunish) {
        this.applyTime =alf.getApplyTime();
        this.userId =alf.getUserId();
        this.userName =alf.getUserName();
        this.labId =alf.getLabId();

        this.faciId =alf.getFaciId();
        this.courseName =alf.getCourseName();
        this.expName =alf.getExpName();
        this.hardware =alf.getHardware();

        this.software =alf.getSoftware();
        this.timeslotStart =alf.getTimeslotStart();
        this.timeslotFinish=alf.getTimeslotFinish();
        this.term = alf.getTerm();

        this.state =alf.getState();
        this.sAllDefault =sAllDefault;
        this.sNewDefault =sNewDefault;
        this.spunish =spunish;

    }


    private int sAllDefault;
    private int sNewDefault;
    private int spunish;

    public int getsAllDefault() {
        return sAllDefault;
    }

    public void setsAllDefault(int sAllDefault) {
        this.sAllDefault = sAllDefault;
    }

    public int getsNewDefault() {
        return sNewDefault;
    }

    public void setsNewDefault(int sNewDefault) {
        this.sNewDefault = sNewDefault;
    }

    public int getSpunish() {
        return spunish;
    }

    public void setSpunish(int spunish) {
        this.spunish = spunish;
    }
}
