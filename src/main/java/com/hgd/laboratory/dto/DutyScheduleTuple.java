package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.DutySchedule;

import java.util.Map;

public class DutyScheduleTuple {
    private DutySchedule dutySchedule;
    private Map<String,String> dutyScheduleDetailMap;

    public DutyScheduleTuple() {
    }

    public DutyScheduleTuple(DutySchedule dutySchedule, Map<String, String> dutyScheduleDetailMap) {
        this.dutySchedule = dutySchedule;
        this.dutyScheduleDetailMap = dutyScheduleDetailMap;
    }

    public DutySchedule getDutySchedule() {
        return dutySchedule;
    }

    public void setDutySchedule(DutySchedule dutySchedule) {
        this.dutySchedule = dutySchedule;
    }

    public Map<String, String> getDutyScheduleDetailMap() {
        return dutyScheduleDetailMap;
    }

    public void setDutyScheduleDetailMap(Map<String, String> dutyScheduleDetailMap) {
        this.dutyScheduleDetailMap = dutyScheduleDetailMap;
    }
}
