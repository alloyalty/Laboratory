package com.hgd.laboratory.dto;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.util.Page;

import java.util.List;
import java.util.Map;

public class PaginationData<T> {
    private Page page;
    private List<T> dataList;
    private Map<Integer,Integer> statics;

    public Page getPage() {
        return page;
    }

    public void setPage(Page page) {
        this.page = page;
    }

    public List<T> getDataList() {
        return dataList;
    }

    public void setDataList(List<T> dataList) {
        this.dataList = dataList;
    }



    public PaginationData(Page page, List<T> dataList, Map<Integer, Integer> statics) {
        this.page = page;
        this.dataList = dataList;
        this.statics = statics;
    }

    public Map<Integer, Integer> getStatics() {
        return statics;
    }

    public void setStatics(Map<Integer, Integer> statics) {
        this.statics = statics;
    }

    public PaginationData() {
    }
}
