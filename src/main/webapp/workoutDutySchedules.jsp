<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.hgd.laboratory.po.Term" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt"%>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">开放实验室值班安排</a></div>
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="defaultNavbar1">
                        <ul class="nav navbar-nav navbar-left">
                            <li class="active"><a href="#">拟订计划<span class="sr-only">(current)</span></a></li>
                        </ul>

                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <h3>实验室<strong><s:property value="model.labId"/>值班计划</strong></h3>
            <form role="form" action="dutySchedule_save.action" method="post" id="editForm">
                <table class="table table-bordered">
                    <input name="model.dutyScheduleId" value="<s:property value='model.dutyScheduleId'/>" hidden/>
                    <input name="model.labId"  value="<s:property value='model.labId'/>" hidden/>
                    <input name="model.desp"  value="<s:property value='model.desp'/>" hidden/>
                    <% Term currentTermObj = (Term)session.getAttribute("currentTermObj"); %>
                    <input id="maxWeekNo" name="maxWeekNo" value="<%= currentTermObj.getWeeksOfTerm() %>" hidden readonly/>

                    <thead>
                    <tr>
                        <th>#</th>
                        <th>星期一</th>
                        <th>星期二</th>
                        <th>星期三</th>
                        <th>星期四</th>
                        <th>星期五</th>
                        <th>星期六</th>
                        <th>星期日</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%
                        Map<String,String> lopMap =(Map<String,String>) request.getAttribute("dutyScheduleDetailMap");

                        if (lopMap!=null  && lopMap.size()!=0) {
                            for (int i = 1;
                                 i <= LabService.TIMESLOTNUM; i++) {
                                out.println("<tr>");
                                out.println("<th>第" + i + "大节</th>");
                                for (int j = 1; j <= 7; j++) {
                                    String key = "t" + i + "w" + j;
                                    String value = lopMap.get(key);
                                    String strtd = String.format("<td ><textarea id='%s'   name='%s' rows='3' cols='10'>%s</textarea></td>",key,key,value);
                                    out.println(strtd);

                                }
                                out.println("</tr>");
                            }
                        }
                    %>

                    </tbody>
                </table>
                <div class="btn-group btn-group-justified" role="group" aria-label="btn-group"  >
                    <div class="btn-group">
                        <button type="button" class="btn btn-info" onclick="saveXyz()" id="save">暂存</button>
                    </div>
                    <div class="btn-group">
                    <button type="button" class="btn btn-primary " onclick="publishXyz();" id="publish">确认并发布</button>
                    </div>
                </div>


                <p class="sj-div"><b>注：</b></p>
                <p class="sj-div">请填写【周次,姓名】：指定某一周次某人值班，例第1周李涛值班写成：1,李涛 </p>
                <p class="sj-div">请填写【周次-周次,姓名】：指定第几周到第几周某人值班，例第3周到6周李涛值班写成：3-6,李涛</p>
                <p class="sj-div">上面两种情况的复合，例第1周，第3周到6周李涛、张妙值班写成：1,3-6,李涛,张妙 </p>
                <p class="sj-div">多项值班可以写在一起，但使用“;”分隔，1,李涛;3-4，张妙 </p>
                <p></p>
            </form>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<%--<s:debug />--%>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function saveXyz() {
        //取表单，设置action属性,提交
        var form = document.getElementById('editForm');
        form.action = "labOpenPlan_save.action";
        let maxWeekNo = $("#maxWeekNo").val();
        if (exprsIsValid(maxWeekNo)) {
            form.submit();}
    }
    function publishXyz() {
        //取表单，设置action属性,提交
        var form = document.getElementById('editForm');
        form.action = "labOpenPlan_publish.action";
        let maxWeekNo = $("#maxWeekNo").val();
        if (exprsIsValid(maxWeekNo)) form.submit();
    }
</script>
<script>
    function checkValid(item,maxWeekNo) {
        if (item===null  || item===undefined) return true;
        if (item.trim()==='') return true;
        let arr = item.split(',');

        for (let i=0;i<arr.length;i++) {
            let weeks = arr[i].split('-');
            for (let j=0;j<weeks.length;j++) {
                let x = parseInt(weeks[j],10);
                if (isNaN(x)) return false;
                if (x<=0 || x>maxWeekNo) return false;

            }
        }
        return true;
    }

    function exprsIsValid(maxWeekNo) {
        let isValid = true;
        for (let i=1;i<=7;i++) {
            for (let j=1;j<=7;j++) {
                let keyv = "#t"+i+"w"+j;
                let item = $(keyv).val();
                if (checkValid(item,maxWeekNo)) {
                    $(keyv).css("color","#000000");//正常色
                    continue;
                } else {
                    $(keyv).css("color","#FF0000");//非正常色
                    isValid =false;
                    continue;
                }
            }
        }
        if (!isValid) {
            alert("存在非法周次表达。");
        }
        return isValid;
    }

</script>
</body>
<!-- InstanceEnd -->
</html>