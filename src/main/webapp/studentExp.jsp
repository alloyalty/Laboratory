<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .appExp {
            border-style: dotted;
            border-width: 1px;
            padding-top: 15px;
            padding-bottom: 15px;
            font-size: 14px;
        }

        .errorMessage {
            list-style: none;
            text-align: left;
            vertical-align: middle;
            padding: 0px 10px 0 20px;
            color: #ee0000;
        }

    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <div style="margin:0 auto;padding: 10px 10px 10px;min-width:400px">
            <h3 style="text-align: center">预约上机/实验登记</h3>
            <hr/>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3"></div>
                    <div class="col-lg-6 appExp">
                        <form class="form-horizontal" role="form" action="studentExpJson_applyExp.action" id="expSubmit">
                            <div class="form-group row">
                                <label for="sId" class="col-lg-4 control-label">预约号/学号</label>
                                <div class="col-lg-5">
                                    <input name="sId" type="text" class="form-control" id="sId" autofocus
                                           placeholder="请输入预约号或者学生学号" required onkeypress="searchByEnter()">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-lg-7 col-lg-offset-4" id="stuNo_ErrMessage">
                                    <p class="form-control-static"><s:fielderror fieldName="sId"
                                                                                 style="padding-left:20px"/></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-lg-4 control-label">姓名</label>
                                <div class="col-lg-5">
                                    <p class="form-control-static" id="name">
                                    </p>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">预约时段</label>
                                <div class="col-lg-5">
                                    <p class="form-control-static" id="timesolt"> &nbsp;
                                    </p>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">实验室</label>
                                <input name="labId" type="text" hidden value="<%= session.getAttribute("labId")%>"
                                       id="labId">
                                <div class="col-lg-5">
                                    <p class="form-control-static"><%= session.getAttribute("labId")%>
                                    </p>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">申请编号</label>
                                <div class="col-lg-5">
                                    <p class="form-control-static" id="applyNo">
                                        <%--                                        <s:property value="#applyForLab.applcatioCode"/>--%>
                                    </p>
                                </div>
                                <div class="col-lg-4">
                                    <s:fielderror fieldName=""/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="machNo" class="col-lg-4 control-label">分配机器编号</label>
                                <div class="col-lg-5">
                                    <input name="fId" type="text" class="form-control" id="machNo" required>
                                </div>

                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label">核对相关信息无误</label>
                                <div class="col-lg-5">
                                    <button type="button" class="btn btn-primary btn-sm btn-block" onclick="aysncSubmit();">确认</button>
                                </div>
                                <div class="form-group row">
                                    <div class="col-lg-7 col-lg-offset-4" id="submit_ErrMessage">
                                        <p class="form-control-static"> </p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
            <br/>
        </div>
        <s:debug/>
        <table class="table table-hover">
            <caption style="font-weight:600">
                上机情况
            </caption>
            <thead style="font-weight: 600">
            <tr>
                <th>学号</th>
                <th>姓名</th>
                <th>申请时段</th>
                <th>状态</th>
                <th>提醒</th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="#paginationData.dataList">
                <tr>
                    <td><s:property value="applyNumber"/></td>
                    <td><s:property value="userName"/></td>
                    <s:if test="timeslotStart==timeslotStart">
                        <td>第<s:property value="timeslotStart"/>时段</td>
                    </s:if>
                    <s:else>
                        <td>第<s:property value="timeslotStart"/>~<s:property value="timeslotFinish"/>时段</td>
                    </s:else>
                    <td><s:property value="applyNumber"/></td>
                    <td></td>
                </tr>
            </s:iterator>
            </tbody>
        </table>

        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function searchByEnter()
    {
        if(event.keyCode == 13 || event.keyCode == 9)
        {
            searchApply();
        }
    }

    function aysncSubmit() {

        let temp = $("#sId").val();
        if (temp === undefined || temp === null || temp.trim() === "") return;

        temp = $("#fId").val();
        if (temp === undefined || temp === null || temp.trim() === "") return;
        alert("开始工作.");
        $.post("studentExpJson_applyExp", $("#sId,#fId,#labId").serializeArray(),
            function (data, status) {
                // alert("数据: \n" + JSON.stringify(data) + "\n状态: " + status);
                alert("数据: \n" + data + "\n状态: " + status);

                let find = data["sfind"];
                if (find == "1") {
                    displayPagnationData(data["paginationData"]);
                    document.getElementById("sId").focus();
                    alert("上机已登记！");
                } else {
                    if (data["appCodeError"]!=null) {
                        $("#stuNo_ErrMessage").html("未找到申请单号或者该学生相关的申请单号。");
                        $("#stuNo_ErrMessage").addClass("errorMessage");
                        blankApply();
                    }
                    if (data["facilityError"]!=null) {
                        $("#submit_ErrMessage").html(data["facilityError"]);
                        $("#stuNo_ErrMessage").addClass("errorMessage");

                    }
                }

            }, "json"
        );
    }
    function searchApply() {
        let temp = $("#sId").val();
        if (temp === undefined || temp === null || temp.trim() === "") return;
        $.post("studentExpJson_checkStudent", $("#sId,#labId").serializeArray(),
            function (data, status) {
                //alert("数据: \n" + data + "\n状态: " + status);

                let find = data["sfind"];
                if (find == "0") {
                    $("#stuNo_ErrMessage").html("未找到申请单号或者该学生相关的申请单号。");
                    $("#stuNo_ErrMessage").addClass("errorMessage");
                    blankApply();
                } else {
                    // alert(JSON.stringify(data));
                    displayApply(data);
                }

            }, "json"
        );
    }

    function displayApply(data) {
        $("#stuNo_ErrMessage").html("");
        $("#stuNo_ErrMessage").addClass("errorMessage");
        $("#timesolt").text(data["timeslot"]);
        $("#name").text(data["name"]);
        $("#applyNo").text(data["applyNo"]);
        var myElement = document.getElementById("machNo");
        myElement.focus();
    }

    function blankApply() {
        $("#timesolt").text("");
        $("#name").text("");
        $("#applyNo").text("");
    }
    function displayPagnationData(applyList) {
        ;
    }
    // function submit() {
    //     var form = document.getElementById("expSubmit");
    //     aysncSubmit();
    // }

</script>
</body>
<!-- InstanceEnd -->
</html>