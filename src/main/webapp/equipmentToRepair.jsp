<%@ page import="java.util.List" %>
<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.FaultRepairService" %>
<%@ page import="java.util.Enumeration" %>
<%@ page import="com.hgd.laboratory.po.FaultRepair" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ page import="com.hgd.laboratory.util.Page" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        #adderr ul li {
            list-style: none;
            text-align: right;
            vertical-align: bottom;
            padding: 16px 10px 0 10px;
            color: #dd0000;
        }

        .errorMessage li {
            list-style: none;
            text-align: right;
            vertical-align: bottom;
            padding: 16px 10px 0 10px;
            color: #ee0000;
        }
    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->


        <div class="panel panel-default center-block" style="width: 500px">
            <div class="panel-heading">
                <h3 style="text-align: center">设备报修</h3>
            </div>
            <div class="panel-body">
                <%--    使用同步的方式，检查数据的合法性--%>
                <form role="form" method="post" enctype="multipart/form-data" action="faultRepair_fillFaultSubmit">
                    <div>
                        <div class="input-group input-group-lg"><span id="addon1"
                                                                      class="input-group-addon mylable">实验室：</span>
                            <input name="lid" type="text" class="form-control" placeholder="一个输入实验室编号"
                                   aria-describedby="addon1"
                                   autofocus required>
                        </div>
                        <br/>

                        <div class="input-group input-group-lg"><span id="addon2" class="input-group-addon mylable">设备标识号：</span>
                            <input name="fid" type="text" required class="form-control" placeholder="请输入计算机的ID"
                                   aria-describedby="addon2">
                        </div>
                        <br/>

                        <div class="input-group input-group-lg"><span id="addon3" class="input-group-addon mylable">故障描述：</span>
                            <textarea name="faultDFesp" required class="form-control" rows="6"
                                      placeholder="请输入具体故障现象"></textarea>
                        </div>
                        <div class="input-group input-group-lg"><span id="adderr" class="input-group-addon mylable">
                            <s:actionerror/>
                        </span>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-lg-2"></div>
                            <div class="col-lg-2">
                                <button type="submit" class="btn btn-primary btn-lg">报修</button>
                            </div>
                            <div class="col-lg-2"></div>
                            <div class="col-lg-2">
                                <button type="button" class="btn btn-primary btn-lg">取消</button>
                            </div>

                        </div>


                    </div>
                </form>
            </div>
            <div class="panel-footer"></div>
        </div>
        <h3>设备故障报修记录</h3>
        <table class="table table-bordered">
            <thead>
            <tr>
                <th>实验室</th>
                <th>设备编号</th>
                <th>故障</th>
                <th>报修人姓名</th>
                <th>设备最终状态</th>
            </tr>
            </thead>
            <tbody>
            <s:iterator value="#paginationData.dataList">
                <tr>
                    <td><s:property value="lid"/></td>
                    <td><s:property value="fid"/></td>
                    <td><s:property value="faultDFesp"/></td>
                        <%--          todo 要将fillingId 改为对应姓名,dependent srudent-duty 关联--%>
                    <td><s:property value="fillingId"/></td>
                    <%
                        ValueStack vs = (ValueStack) request.getAttribute("struts.valueStack");
                        int fstate_value = 0;
                        try {
                            fstate_value = (int) vs.findValue("fstate");
                        } catch (Exception e) {
                            ;
                        }
//          int fstate_value = (int)request.getAttribute("fstate");
//          Enumeration<String> attributeNames = request.getAttributeNames();
//          System.out.println("request 对象的所有属性key列表如下:........................................");
//          while (attributeNames.hasMoreElements()) {
//            String attrName =attributeNames.nextElement() ;
//            System.out.println(attrName);
//          }
//         经测试页面中此java代码，一次更新，执行了五次。
                    %>
                    <td><%= statusToString(fstate_value)     %>
                    </td>
                </tr>
            </s:iterator>
            <%!
                String statusToString(int status) {
                    if (status == FaultRepairService.FAULT)
                        return "故障";
                    else
                        return "正常";
                }
            %>

            </tbody>
        </table>
        <form method="post" id="searchForm">
            <input name="pageNo" hidden type="text" id="pageNo" value="<s:property value='pageNo' />">
        </form>
        <nav>
            <!-- Add class .pagination-lg for larger blocks or .pagination-sm for smaller blocks-->
            <%--            入口参数： form=searchForm, 类型Class<T> PaginationData<Notice>,statusFilter --%>
            <s:if test="#paginationData!=null and #paginationData.page.totalPages>=2">
                <ul class="pagination pagination-lg">
                    <% PaginationData<FaultRepair> paginationData = (PaginationData<FaultRepair>) request.getAttribute("paginationData");
                        Page pageNav = paginationData.getPage();
                        String prevDisabled = "";
                        if (pageNav.getCurrentPage() == 1) prevDisabled = "disabled";
                        String nextDisabled = "";
                        if (pageNav.getCurrentPage() == pageNav.getTotalPages()) nextDisabled = "disabled";
                    %>

                    <li>
                        <button type="button" class="btn btn-default"  <%= prevDisabled%>
                                onclick="statusFilter(<%=  pageNav.getPrevPage()%>)"
                                form="searchForm">
                            &laquo;
                        </button>
                    </li>

                    <s:bean name="org.apache.struts2.util.Counter" var="counter">
                        <s:param name="first" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                        <s:param name="last" value="#paginationData.page.lastPageNoInCurrentPanel"/>
                        <s:iterator>
                            <s:set var="current"/>
                            <s:set var="firstPageNo" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                            <s:if test="#current==#paginationData.page.currentPage">
                                <s:set var="active" value="'active'"/>
                            </s:if>
                            <s:else>
                                <s:set var="active" value="''"/>
                            </s:else>

                            <li>
                                <button type="button" class="btn btn-default <s:property value='#active' />"
                                        onclick="statusFilter(
                                            <s:property/> )"
                                        form="searchForm">
                                    <s:property/>
                                </button>
                            </li>
                        </s:iterator>
                    </s:bean>

                    <li>
                        <button type="button" class="btn btn-default"  <%= nextDisabled %>
                                onclick="statusFilter(<%=  pageNav.getNextPage()%>)"
                                form="searchForm">
                            &raquo;
                        </button>
                    </li>
                </ul>
            </s:if>
        </nav>
        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function statusFilter(pageNo) {
        if (pageNo != null) {
            $("#pageNo").val(pageNo);
        }
        $("#searchForm").attr("action", "faultRepair_reqfill");
        $("#searchForm").submit();
    }
</script>
</body>
</html>