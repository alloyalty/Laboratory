<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        h3 {
            text-align: center;
        }

        #contents {
            margin: 0 auto;
        }

        .clearfloat {
            clear: both;
            height: 0;
            font-size: 1px;
            line-height: 0px;
        }

        #leftpart {
            width: 100%;
            font-family: "宋体";
            font-size: 20px;
            float: left;
        }

        #addStu {
            width: 300px;
            font-family: "宋体";
            font-size: 16px;
            float: right;
            border: rgba(52, 31, 235, 1.00) thin solid;
            margin-left: 50px;
            margin-right: 50px;
            padding-left: 8px;
            padding-right: 8px;
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<%!
    String activeItem(int state, int activeState) {
        if (state == activeState)
            return " active ";
        else
            return "";
    }
%>

<div id="middle-part">
    <div id="contents">

        <div>
            <h3>值班人员管理</h3>
            <hr style="size: 2px; width: 99%; line-height: 10px">
            <div id="leftpart">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="collapse navbar-collapse" id="defaultNavbar1">
                            <ul class="nav navbar-nav">
                                <li><a data-toggle="modal" href="#addStuModal">新增值班人员</a></li>
                            </ul>
                            <form class="navbar-form navbar-right" role="search" action="dutyStudentAction_query"
                                  method="post"
                                  id="searchForm">
                                <input type="text" name="delflag" value="0" hidden id="delflag"/>
                            </form>
                            <%
                                int state = (Integer) request.getAttribute("delflag");
                            %>
                            <ul class="nav navbar-nav navbar-right">
                                <li>
                                    <div class="btn-group navbar-btn">
                                        <button type="button" class="btn btn-default <%= activeItem(state,0) %>"
                                                onclick="delflagFilter(0)" form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>在值
                                        </button>
                                        <button type="button" class="btn btn-default <%= activeItem(state,1) %>"
                                                onclick="delflagFilter(1)" form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>曾经
                                        </button>
                                        <button type="button" class="btn btn-default <%= activeItem(state,-1) %>"
                                                onclick="delflagFilter(-1)" form="searchForm">
                                            <span class="glyphicon glyphicon-filter"></span>全部
                                        </button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.navbar-collapse -->

                    </div>
                    <!-- /.container-fluid -->
                </nav>
            </div>
            <div class="modal fade" id="addStuModal" tabindex="-1" role="dialog"
                 aria-labelledby="addStuModalLable" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content container" style="width: 45em">
                        <form class="form-horizontal" role="form" action="dutyStudentAction_addDutyStudent"
                              id="addStudentForm">
                            <div class="modal-header">
                                <h3 class="modal-title" id="addStuModalLable">增加 实验室值班人员</h3>
                                <button type="button" class="close" data-dismiss="modal"
                                        aria-hidden="true">&times;
                                </button>
                            </div>
                            <div class="modal-body">
                                <fieldset>
                                    <div class="form-group">
                                        <label for="stuNameOrId" class="col-sm-4 control-label ">学号或姓名：</label>
                                        <div class="col-sm-4">
                                            <input name="stuNameOrId" type="text" class="form-control" autofocus="autofocus"
                                                   id="stuName" placeholder="请输入学生姓名或学号" onkeydown="queryByEnter();"
                                                   onblur = "queryStudentInfor();"
                                            >
                                        </div>
                                        <span class="col-sm-3">
                                        <%--     在学生表中查找指定的学生信息  --%>
                                            <button type="button" class="btn btn-primary" id="queryByNameOrId"
                                                    onclick="queryStudentInfor();">查询
                                            </button><span id="msg"></span>
                                    </div>
                            </div>
                            <div class="form-group">
                                <label for="sId" class="col-sm-4 control-label">学号：</label>
                                <div class="col-sm-8">
                                    <p id="sId" class="form-control-static"></p>
                                    <input name="code" hidden id="stuId">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sName" class="col-sm-4 control-label">姓名：</label>
                                <div class="col-sm-8">
                                    <p id="sName" class="form-control-static"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="sClass" class="col-sm-4 control-label">班级：</label>
                                <div class="col-sm-8">
                                    <p id="sClass" class="form-control-static"></p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="specialty" class="col-sm-4 control-label">专业：</label>
                                <div class="col-sm-8">
                                    <p id="specialty" class="form-control-static"></p>
                                </div>
                            </div>
                            </fieldset>
                            <br/>

                            <div class="modal-footer">
                                <div class="form-group">
                                    <div class="col-sm-4">
                                        <button type="button" class="btn btn-default close"
                                                data-dismiss="modal">关闭
                                        </button>
                                    </div>
                                    <div class="col-sm-offset-4 col-sm-4">
                                        <button type="button" class="btn btn-primary" id="addStudentSubmit" hidden>
                                            指定为值班人员
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfloat">
            <h3 style="text-align: left;">值班人员列表</h3>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th>学号</th>
                    <th>姓名</th>
                    <th>专业</th>
                    <th>班级</th>
                    <th>状态</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="#dutyStudentList">
                    <tr>
                        <td><s:property value="sId"/></td>
                        <td><s:property value="sName"/></td>
                        <td><s:property value="specialty"/></td>
                        <td><s:property value="sClass"/></td>
                        <td>
                            <s:if test="delflag==0">在值</s:if>
                            <s:else>不在值</s:else>
                        </td>
                        <td><a href="dutyStudentAction_delDutyStudent?id=<s:property value='id'/>">
                            <s:if test="delflag==0">删除</s:if>
                        </a></td>
                    </tr>
                </s:iterator>
                </tbody>
            </table>
        </div>
        <div id="debug"></div>
        <!-- InstanceEndEditable -->
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    let isExecQuery = false;

    function clearStuDisp() {
        $("#sId").text("");
        $("#sName").text("");
        $("#sClass").text("");
        $("#specialty").text("");
        $("#stuId").val("");
        $("#msg").html("");
    }

    function dispStu(stu) {
        $("#sId").text(stu["sId"]);
        $("#sName").text(stu["sName"]);
        $("#sClass").text(stu["sClass"]);
        $("#specialty").text(stu["specialty"]);
        $("#stuId").val(stu["sId"]);

    }

    function queryStudentInfor() {
        $.post("dutyStudentJsonAction_queryByNameOrId", $("#stuName").serializeArray(),
            function (data, status) {
                alert("数据: \n" + data + "\n状态: " + status);
                let arr = data.studentList;
                if (arr.length == 1) {
                    let stu = arr[0];
                    dispStu(stu);
                    $("#msg").html("");
                    alert(JSON.stringify(data));
                    if (data.duty) {
                        $("#msg").html(" 此学生已是值班员。");
                        $("#addStudentSubmit").hide();
                    } else {
                        $("#addStudentSubmit").show();
                    }

                } else {
                    clearStuDisp();
                    $("#msg").html(" 未找到!");
                    $("#addStudentSubmit").hide();
                }

            }, "json"
        );
    }

    function queryByEnter()
    {
        if(event.keyCode == 13)
        {
            queryStudentInfor();
        }
    }
    $(document).ready(function () {
        $("#addStudentSubmit").click(function () {
            //如果学生id为空，给出信息" 未找到。"
            // 否则，提交信息
            let v = $("#stuId").val();
            if (v === null || v === undefined || v.trim() == "") {
                $("#msg").html(" 未找到!");
            } else {
                $("#addStudentForm").attr("action", "dutyStudentAction_addDutyStudent");
                // $("#addStudentForm").action = "dutyStudentAction_addDutyStudent";
                $("#msg").html("");
                $("#addStudentSubmit").hide();
                $("#addStudentForm").submit();

            }
        });
    });

    function delflagFilter(delflag) {
        $("#delflag").val(delflag);
        $("#searchForm").attr("action", "dutyStudentAction_query");
        $("#searchForm").submit();
    }
</script>
</body>
</html>