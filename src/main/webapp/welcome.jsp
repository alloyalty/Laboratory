<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>

<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <br/>
        <%--			登录成功。请继续......--%>
        <h2 class="text-center"><s:property value="#tip"/></h2>

        <!-- InstanceEndEditable -->
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>