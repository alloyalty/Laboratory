<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }
    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <div id="login" class="container">
            <form class="form-signin" method="post" action="user_modifyEmail">
                <h3 class="form-signin-heading text-center">修改邮箱地址</h3>
                <br/>
                <div class="input-group"> <span class="input-group-addon"> <span
                        class="glyphicon glyphicon-user"></span> </span>
                    <label for="inputUsername" class="sr-only">用户名</label>
                    <input readonly="true"
                           name="logName" type="text" id="inputUsername" class="form-control"
                           value="<%= ((User)session.getAttribute("user")).getName()%>"
                           placeholder="用户名" style="width: 300px">
                </div>
                <%
                    User currentUser = (User) session.getAttribute("user");
                    String email = "";
                    if (currentUser != null)
                        email = currentUser.getEmaill();
                    if (email == null) email = "";
                %>
                <label class="sr-only" for="inputEmail">邮箱</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></div>
                    <input readonly="true" class="form-control" id="inputEmail"
                           name="email"
                           value="<%= email %>"
                           type="email" placeholder="邮箱" required style="width: 300px">
                </div>


                <div>
                    <div class="input-group"><span id="addon1" class="input-group-addon">新邮箱</span>
                        <input class="form-control" id="inputNewEmail" name="newEmail"
                               type="email" placeholder="新邮箱" required style="width: 272px" required autofocus>
                    </div>
                    <br/>

                    <button class="btn btn-lg btn-primary btn-block" type="submit"
                            style="width: 345px"> 修改
                    </button>
                </div>
            </form>
        </div>


        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>