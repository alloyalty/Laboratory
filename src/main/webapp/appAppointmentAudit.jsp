<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="java.io.*" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="com.hgd.laboratory.dto.ApplyForLabAudit" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ page import="java.util.Calendar" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>实验室开放管理系统</title>

    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .summary {
            line-height: 1.2;
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<%!
    String activeItem(int state, int activeState) {
        if (state == activeState)
            return " active ";
        else
            return "";
    }
%>

<div id="middle-part">
    <div id="contents">
        <h3>预约审核</h3>
        <div class="container">
            <div class="row" clas="summary">
                <div class="col-lg-2" style="padding-left: 20px">预约小计<s:property value="#paginationData.statics[-1]"/></div>
                <div class="col-lg-2">取消的预约<s:property value="#paginationData.statics[4]"/></div>
                <div class="col-lg-2">待审核预约<s:property value="#paginationData.statics[0]"/></div>
                <div class="col-lg-2">通过审核预约<s:property value="#paginationData.statics[2]"/></div>
                <div class="col-lg-2">未通过审核预约<s:property value="#paginationData.statics[1]"/></div>
               <hr/>
            </div>
            <form class="navbar-form" role="form" id="auditForm"  style="padding-left: 0px"
                  action="anAppointmentQuery_queryWaitForAudit">
                <div class="input-group" >
                    <div class="input-group-addon">
                        <span>开放实验室</span>
                    </div>
                    <select class="form-control" id="openLab" name="labId"
                            required>
                        <%
                            String term = (String) session.getAttribute("currentTerm");
                            List<String> openLabList = new TestService().getOpenLabList(term);
                            String lastSelected = (String) request.getParameter("labId");
                            if (lastSelected == null) lastSelected = "";

                            for (int i = 0; i < openLabList.size(); i++) {
                                String selected = openLabList.get(i).equalsIgnoreCase(lastSelected) ? "selected" : "";
                                out.print(String.format("<option %s>", selected));
                                out.print(openLabList.get(i));
                                out.print("</option>");
                            }
                            String selected = lastSelected.isEmpty() ? "selected" : "";
                        %>

                        <option value=""  <%=selected%>>不限实验室</option>
                    </select>

                </div>
                <%
                    Date theDate = (Date) request.getAttribute("applyDate");
                    String dateStr = calYuyueDefaultDate(theDate);
                %>
                <div class="input-group">
                    <span class="input-group-addon">预约日期：</span> <input type="date" name="applyDate"
                                                                        value="<%= dateStr %>"
                                                                        class="form-control"
                                                                        placeholder="输入预约日期">
                </div>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>开放时段</span>
                    </div>
                    <div class="input-group">
                        <select class="form-control" id="timescope" name="timeslot" required>
                            <%
                                {
                                    int defaultValue = -1;
                                    Integer ts = (Integer) request.getAttribute("timeslot");
                                    if (ts != null) defaultValue = (int) ts;
                                    for (int i = 0; i < LabService.TIMESLOTNUM; i++) {
                                        String seled = (i + 1) == defaultValue ? "selected" : "";
                            %>

                            <option value=<%=i+1%>  <%= seled%>>第<%=i + 1%>时段</option>
                            <%
                                }
                                String seled = (-1) == defaultValue ? "selected" : ""; %>
                            <option value=-1  <%= seled%>>不限时段</option>
                            <%
                                }
                            %>

                        </select>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>状态</span>
                    </div>
                    <%
                        Integer temp = (Integer) request.getAttribute("state");
                        int state = temp == null ? 0 : temp.intValue();
                    %>
                    <div class="input-group">
                        <select class="form-control" id="state" name="state" required>
                            <option <%= selectItem(state, 4)%> value="4">已取消</option>
                            <option <%= selectItem(state, 0)%> value="0">未审核</option>
                            <option <%= selectItem(state, 2)%> value="2">审核已通过</option>
                            <option <%= selectItem(state, 1)%> value="1">审核未通过</option>
                            <option <%= selectItem(state, -1)%> value="-1">全部</option>
                        </select>
                    </div>
                </div>

                <button type="submit" class="btn btn-default"
                        formaction="anAppointmentQuery_queryWaitForAudit">查询
                </button>
            </form>
            <form>
                <table class="table table-bordered">
                    <caption class="subtitle" style="text-align: left;padding-left: 5px">预约明细</caption>
                    <thead>
                    <tr>
                        <th>申请上机日期</th>
                        <th>班级</th>
                        <th>专业</th>
                        <th>姓名</th>
                        <th>学号</th>
                        <th>时段</th>
                        <th>状态</th>
                        <th>选中审定</th>
                    </tr>
                    </thead>
                    <tbody>
                    <s:iterator value="#paginationData.dataList">
                        <tr>
                            <td><s:property value="applyForLab.applyTime"/></td>
                            <td><s:property value="applyForLab.labId"/></td>
                            <td><s:property value="student.specialty"/></td>
                            <td><s:property value="applyForLab.userName"/></td>
                            <td><s:property value="student.sId"/></td>
                            <td>
                                <s:if test="applyForLab.timeslotStart==applyForLab.timeslotFinish">
                                    第<s:property value="applyForLab.timeslotStart"/>时段
                                </s:if>
                                <s:else>
                                    第<s:property value="applyForLab.timeslotStart"/> ~ <s:property
                                        value="applyForLab.timeslotFinish"/>时段
                                </s:else>
                            </td>
                            <td><s:property
                                    value="@com.hgd.laboratory.po.ApplyForLab@stateDisplay(applyForLab.state)"/></td>
                            <td>
                                <s:if test="#this.state==0">
                                    <input type="checkbox" name="applyNumber"
                                           value="<s:property value='applyForLab.applyNumber'/>"
                                           form="auditForm">审核&nbsp;&nbsp;
                                </s:if>
                            </td>

                        </tr>
                    </s:iterator>

                    </tbody>
                </table>
                <s:if test="#this.state==0">
                    <div class="btn-group btn-group-justified" role="group" aria-label="btn-group">
                        <div class="btn-group">
                            <button type="submit" class="btn btn-danger" form="auditForm"
                                    formaction="anAppointmentQuery_auditPass">批量审核不通过
                            </button>
                        </div>
                        <div class="btn-group">
                            <button type="submit" class="btn btn-primary" form="auditForm"
                                    formaction="anAppointmentQuery_auditNotPass">批量审核通过
                            </button>
                        </div>
                    </div>
                </s:if>
            </form>
        </div>

        <s:debug/>
    </div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script src="js/mydefFilter.js"></script>
</body>
</html>
<%!
    private String calYuyueDefaultDate(Date theDate) {
        if (theDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, 1);
            theDate = calendar.getTime();
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(theDate);
    }
%>
<%!
    String selectItem(int val, int selectVal) {
        if (val == selectVal)
            return " selected ";
        else
            return "";
    }
%>