<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Calendar" %>
<%@ page import="java.util.Date" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.io.*" %>
<%@ page import="com.hgd.laboratory.service.impl.TestService" %>
<%@ page import="com.hgd.laboratory.dto.AppCase" %>
<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="java.text.SimpleDateFormat" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


        div .emph {
            position: relative;
            border: 1px solid #73AD21;
            top: -5px;
            background-color: #ffff00;
        }

        mypadding {
            padding-right: 5px;
            padding-left: 5px;
        }

        .errorMessage li {
            list-style-type: none;
            text-align: right;
            background-color: #ffff00;
        }

        .errorMessage {
            padding: 0;
        }

        .box {
            border: rgb(48, 0, 0) solid thin;
            padding: 5px;
        }

        .my-control {
            border-top-left-radius: 0;
            border-bottom-left-radius: 0;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
        }
    </style>
</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<%

    Calendar calendar = new GregorianCalendar();
    calendar.setTime(new java.util.Date());
    calendar.add(calendar.DATE, 1);//把日期往后增加一天.整数往后推,负数往前移动
    Date date = calendar.getTime();   //这个时间就是日期往后推一天的结果

    java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd");
    String tomorrow = sdf.format(date);

%>
<div id="middle-part">
    <div id="contents">
        <!-- InstanceBeginEditable name="EditRegion_MainFunc" -->
        <%
            Date theDate = (Date) request.getAttribute("appDate");
            if (theDate == null) {
                theDate = new Date();
                Calendar temp = Calendar.getInstance();
                temp.setTime(theDate);
                temp.add(Calendar.DATE, 2);
                theDate = new Date(temp.getTimeInMillis());
            }
            String dateStr = new SimpleDateFormat("yyyy-MM-dd").format(theDate);
        %>
        <div id="anAppointment" class="container" style="width: 900px">
            <form class="form-signin" action="anAppointment_query.action" method="post">
                <h3 class="form-signin-heading">开放实验室预约申请
                    <s:if test="actionErrors!=null">
                        <span class="glyphicon glyphicon-exclamation-sign"></span>
                    </s:if>
                    <s:actionerror/>
                </h3>
                <div style="width:400px">
                    <div class="input-group">
                        <span class="input-group-addon"> <span>选择预约日期</span></span>
                        <label for="appDate" class="sr-only">预约时间</label> <input
                            name="appDate" type="date" id="appDate"
                            class="form-control" required autofocus value="<%= dateStr%>">
                    </div>
                    <label class="sr-only" for="openLab">开放实验室</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span>开放实验室</span>
                        </div>
                        <select class="form-control" id="labId" name="labId" required>
                            <%
                                String labId = (String) request.getAttribute("labId");
                                String term = (String) session.getAttribute("currentTerm");
                                List<String> openLabList = new TestService().getOpenLabList(term);
                                if (openLabList != null) {
                                    for (int i = 0; i < openLabList.size(); i++) {
                                        String selected = "";
                                        if (openLabList.get(i).equals(labId))
                                            selected = "selected";

                                        out.print(String.format("<option %s>", selected));
                                        out.print(openLabList.get(i));
                                        out.print("</option>");
                                    }
                                }

                            %>
                        </select>
                    </div>
                    <div class="input-group">
                        <button type="submit" class="btn btn-primary form-control">查询</button>
                        <div class="input-group-addon">
                            <span><s:property value="#dateInfo"/> </span>
                        </div>
                    </div>
                </div>
            </form>

            <form id="applyForm" action="anAppointment_applySubmit.action" method="post">
                <input type="text" name="appDate" value="<s:property value='appDate'/>" hidden/>
                <input type="text" name="labId" value="<s:property value='labId'/>" hidden/>

                <div class="input-group">
                    <span class="input-group-addon"> <span>设备特性</span></span>
                    <div class="my-control">
                        <p class="form-control-static ml-3 "><s:property value="outlineOfLaboratory"/></p>
                    </div>
                </div>
                <h3>选择时段</h3>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>实验室\节</th>
                        <%
                            String[] serno = new String[]{"一", "二", "三", "四", "五", "六", "七", "八", "九", "十"};
                            for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
                                out.print("<th>第");
                                out.print(serno[i - 1]);
                                out.println("大节</th>");
                            }
                        %>

                    </tr>
                    </thead>
                    <tbody id="queryResult">
                    <%
                        boolean submitHide =false;
                        List<AppCase> appCaseList = (List<AppCase>) request.getAttribute("appCaseList");
                        if (appCaseList == null || appCaseList.size() == 0) {
                            submitHide =true;
                        } else {
                            out.println(" <tr class='active'>");
                            if (appCaseList.get(0).isIgnored()) submitHide=true;
                            out.println(String.format(" <td class='warning'>%s</td>", appCaseList.get(0).getLabId()));
                            for (int i = 0; i < appCaseList.size(); i++) {
                                AppCase appCase = appCaseList.get(i);

                                String classname = appCase.isOpen() ? "success" : "danger";
                                String openClose="";
                                if (!appCase.isIgnored())
                                    openClose = appCase.isOpen() ? "开放" : "不开放";
                                if (appCase.isOpen()) {
                                    openClose = String.format("<div>余量<span class='emph'>%s</span><div>", appCase.getRest());
                                }

                                out.print(String.format("<td class='%s'>", classname));
                                out.print(openClose);
                                if (appCase.isOpen()) {
                                    String paraName = "lab" + appCase.getLabId();
                                    String checked = appCase.isSubscribed() ? " checked " : "";
                                    out.print(String.format("<input name='%s' value='%s' type='checkbox' %s> 预约",
                                            paraName, appCase.getTimeSlot(), checked));
                                }
                            }

                            out.println("</tr>");
                        }
                    %>

                    </tbody>
                </table>
                <h3 class="form-signin-heading">填写相关信息</h3>
                <div class="input-group">
                    <span class="input-group-addon"> <span>实验项目</span></span> <label
                        for="expItem" class="sr-only">实验项目</label>
                    <input name="expName" value="<s:property value='expName' /> "
                           type="text" id="expItem" class="form-control"
                           required autofocus>
                </div>
                <label class="sr-only" for="courseName">课程名称</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>课程名称</span>
                    </div>
                    <input class="form-control" id="courseName" name="courseName"
                           value="<s:property value='courseName' />"
                           type="text" required>
                </div>

                <%--                todo 实验室所关联的软件配置，没有功能设置 --%>
                <%--                UI合法性检验 --%>
                <div class="checkbox my-control">
                    <label>软件： &nbsp; </label>
                    <s:iterator value="chkboxDataList">
                        <s:set var="currentItem"></s:set>
                        <input type="checkbox" name="softwares"
                               value="<s:property />"
                               <s:if test="#currentItem in softwares">checked</s:if> >
                        <s:property/> &nbsp; &nbsp; &nbsp;
                    </s:iterator>

                </div>

                <label class="sr-only" for="hardwares">硬件</label>
                <div class="input-group">
                    <div class="input-group-addon">
                        <span>硬件</span>
                    </div>
                    <input class="form-control" id="hardwares" name="hardwares"
                           value="<s:property value='hardwares' /> "
                           type="text" required>
                </div>

                <br/>
                <div class="row">
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                    <% if (!submitHide) {
                        out.println(" <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">提交</button>");
                    }
                    %>
                    </div>
                    <div class="col-lg-2"></div>
                    <div class="col-lg-3">
                        <button class="btn btn-lg btn-primary btn-block" type="button"
                                onclick="window.location.href='blank.jsp';" on>取消
                        </button>

                    </div>
                    <div class="col-lg-2"></div>
                </div>


            </form>
        </div>

        <!-- InstanceEndEditable -->
    </div>
</div>
<s:debug/>

<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>