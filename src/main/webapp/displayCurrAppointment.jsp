<%@ page import="com.hgd.laboratory.dto.PaginatingData" %>
<%@ page import="com.hgd.laboratory.util.Page" %>
<%@ page import="com.hgd.laboratory.po.ApplyForLab" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }


    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents">
        <form method="post" action="anAppointmentQuery_cancel">
            <table class="table table-bordered">
                <caption>
                    <h3>个人当前预约信息</h3>
                </caption>
                <thead>
                <tr>
                    <th style="font-weight: 600">当前预约</th>
                    <th style="font-weight: 600">选择<code> </code>
                        <button type="submit" class="btn btn-primary">取消选中预约</button>
                        (只有未审核的预约可以取消)
                    </th>
                </tr>
                </thead>
                <tbody>
                <s:iterator value="#paginationData.dataList">
                    <tr>
                        <td>

                            <ul class="list-group">
                                <li class="list-group-item">申请号：<s:property value="applcatioCode"/></li>
                                <li class="list-group-item" style="font-weight: 600">学号/工号：<s:property
                                        value="userId"/></li>
                                <li class="list-group-item" style="font-weight: 600">姓名：<s:property
                                        value="userName"/></li>
                                <li class="list-group-item">申请日期：<s:date name="applyTime" format="yyyy-MM-dd"/></li>
                                <li class="list-group-item">申请时段：
                                    <s:if test="timeslotStart=timeslotFinish">
                                        <s:property value="timeslotStart" />
                                    </s:if>
                                    <s:else>
                                        <s:property value="timeslotStart"/>-<s:property
                                            value="timeslotFinish"/>
                                    </s:else>
                                </li>
                                <li class="list-group-item">实验室:<s:property value="labId"/></li>
                                <li class="list-group-item">状态：<s:property
                                        value="@com.hgd.laboratory.po.ApplyForLab@stateDisplay(#this.state)"/></li>
                            </ul>
                        </td>
                        <td>
                            <div class="checkbox">
                                <label>
                                    <s:if test="#this.state==0">
                                        <input type="checkbox" name="applyNumber"
                                               value="<s:property value='applyNumber'/>">打勾可取消
                                    </s:if>
                                </label>
                            </div>
                        </td>
                    </tr>
                </s:iterator>

                </tbody>

                <nav>

                    <%--            入口参数： form=searchForm, 类型Class<T> PaginationData<Notice>,statusFilter --%>
                    <s:if test="#paginationData!=null and #paginationData.page.totalPages>=2">
                        <ul class="pagination pagination-lg">
                            <% PaginationData<ApplyForLab> paginationData = (PaginationData<ApplyForLab>) request.getAttribute("paginationData");
                                Page pageNav = paginationData.getPage();
                                String prevDisabled = "";
                                if (pageNav.getCurrentPage() == 1) prevDisabled = "disabled";
                                String nextDisabled = "";
                                if (pageNav.getCurrentPage() == pageNav.getTotalPages()) nextDisabled = "disabled";
                            %>

                            <li>
                                <button type="button" class="btn btn-default"  <%= prevDisabled%>
                                        onclick="statusFilter(<%=  pageNav.getPrevPage()%>)"
                                        form="searchForm">
                                    &laquo;
                                </button>
                            </li>

                            <s:bean name="org.apache.struts2.util.Counter" var="counter">
                                <s:param name="first" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                                <s:param name="last" value="#paginationData.page.lastPageNoInCurrentPanel"/>
                                <s:iterator>
                                    <s:set var="current"/>
                                    <s:set var="firstPageNo" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                                    <s:if test="#current==#paginationData.page.currentPage">
                                        <s:set var="active" value="'active'"/>
                                    </s:if>
                                    <s:else>
                                        <s:set var="active" value="''"/>
                                    </s:else>

                                    <li>
                                        <button type="button" class="btn btn-default <s:property value='#active' />"
                                                onclick="statusFilter(<s:property/> )"
                                                form="searchForm">
                                            <s:property/>
                                        </button>
                                    </li>
                                </s:iterator>
                            </s:bean>

                            <li>
                                <button type="button" class="btn btn-default"  <%= nextDisabled %>
                                        onclick="statusFilter(<%=  pageNav.getNextPage()%>)"
                                        form="searchForm">
                                    &raquo;
                                </button>
                            </li>
                        </ul>
                    </s:if>
                </nav>

            </table>
        </form>
        <form action="anAppointmentQuery_queryCurrent" method="post" id="searchForm">
            <input type="text" name="pageNo"  hidden id="pageNo">
        </form>
    </div>
</div>
<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    function statusFilter(pageNo) {
        // !null 参数，表示此参数代替表单参数。
        // null 表示不更改表单参数，使用表单参数
        if (pageNo != null) {
            $("#pageNo").val(pageNo);
        }
        $("#searchForm").attr("action", "anAppointmentQuery_queryCurrent");
        $("#searchForm").submit();
    }
</script>
</body>
</html>