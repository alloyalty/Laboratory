<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><s:property value="#item.title"/></title>

    <%@include file="WEB-INF/include/share.txt" %>
    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .h-title {
            margin-top: 30px;
            padding: 0 20px;
            text-align: center;
            font-size: 36px;
            color: #404040;
            font-weight: 600;
        }

        #p-detail {
            margin: 0 0 22px 0;
            font-size: 18px;
            color: #404040;
            line-height: 28px;
        }

        .lb {
            position: relative;
            width: 100%;
            overflow: hidden;
            margin-bottom: 22px;
        }

        footer {
            line-height: 1.8em;
            text-indent: 2em;
            font-size: 18px;
            padding: 10px 0 10px 0;
            font-weight: normal;
            text-align: right;
            color: #333;
            font-family: "宋体";
        }
    </style>
</head>

<body>
<div id="p-detail">
    <div class="row">
        <s:push value="#item">
            <div id="contents" class="col-lg-12">
                <h3 id="title" class="text-center"><s:property value="title"/></h3>
                <h4 id="subTitle" class="text-center"><s:property value="subTitle"/></h4>
                <s:generator separator="\r\n" val="text" var="plist"/>
                <s:iterator value="#plist">
                    <p><s:property/></p>
                </s:iterator>
                <footer><s:property value="author"/></footer>
                <footer><s:property value="publishDate"/></footer>
            </div>
        </s:push>
    </div>
</div>
</div>
</body>
</html>
