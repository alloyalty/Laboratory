<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>

    <%@include file="WEB-INF/include/share.txt"%>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>

</head>
<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container">
            <h3>基础数据导入</h3>
            <div class="row">
                <div class="col-md-7">
                    <form role="form" method="post" enctype="multipart/form-data" action="importData_importXLSX">
                        <div style="width: 650px">
                            <p style="font-weight: 600">学生信息文件(*.xlsx)的格式应与studentTemplate.xlsx的格式相符合。</p>
                            <div class="input-group">
                                <span id="addon1" class="input-group-addon">学生信息文件：</span>
                                <input name="upload" type="file" class="form-control" id="f1"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>

                            <p style="font-weight: 600">教师信息文件(*.xlsx)的格式应与teacherTemplate.xlsx的格式相符合。</p>
                            <div class="input-group"><span id="addon2" class="input-group-addon">教师信息文件：</span>
                                <input name="teacher" type="file" class="form-control"  id="f2"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>

                            <p style="font-weight: 600">实验室信息文件(*.xlsx)的格式应与laboratoryTemplate.xlsx的格式相符合。</p>
                            <div class="input-group">
                                <span id="addon3" class="input-group-addon">实验室信息文件：</span>
                                <input name="laboratory" type="file" class="form-control"  id="f3"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>

                            <p style="font-weight: 600">设备信息文件(*.xlsx)的格式应与facilityTemplate.xlsx的格式相符合。</p>
                            <div class="input-group">
                                <span id="addon4" class="input-group-addon">设备信息文件：</span>
                                <input name="facility" type="file" class="form-control"  id="f4"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>

                            <p style="font-weight: 600">软件配置主信息文件(*.xlsx)的格式应与softwareConfigTemplate.xlsx的格式相符合。</p>
                            <div class="input-group">
                                <span id="addon5" class="input-group-addon">软件配置主信息文件：</span>
                                <input name="softwareConfig" type="file" class="form-control"  id="f5"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>

                            <p style="font-weight: 600">软件配置详细信息文件(*.xlsx)的格式应与softwareDeailsTemplate.xlsx的格式相符合。</p>
                            <div class="input-group">
                                <span id="addon6" class="input-group-addon">软件配置详细信息文件：</span>
                                <input name="softwareDetails" type="file" class="form-control"  id="f6"
                                       placeholder="normal sized input group" aria-describedby="addon1">
                            </div>
                            <br/>
                            <button type="submit" class="btn btn-primary btn-block" id="btnSubmit">导入数据</button>
                        </div>

                    </form>
                    <br/><br/><br/><br/>
                </div>
                <div class="col-md-5">
                        <h4>数据导入情况：</h4><br/>
                        <s:property value="tip" escapeHtml="false"/>
                </div>
            </div>

        </div>

        <!-- InstanceEndEditable --></div>
</div>
<%@ include file="WEB-INF/include/footer.jsp" %>
<script>
    $(document).ready(function(){
        $("#btnSubmit").click(function(){
            var p1 = $("#f1").val();
            var p2 = $("#f2").val();
            var p3 = $("#f3").val();
            var p4 = $("#f4").val();
            var p5 = $("#f5").val();
            var p6 = $("#f6").val();
            var plist =[p1,p2,p3,p4,p5,p6];
            var present=false;
            for (i=0;i<plist.length;i++) {
                if (plist[i]!== "") {
                    present =true;
                    break;
                }
            }

            if (present==false) {
                alert("没有文件可以导入，请选择文件。");
                return false;
            }
            return true;
        });

    });

</script>
</body>
<!-- InstanceEnd -->
</html>