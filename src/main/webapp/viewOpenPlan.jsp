<%@ page import="com.hgd.laboratory.service.LabService" %>
<%@ page import="com.opensymphony.xwork2.ActionContext" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.opensymphony.xwork2.util.ValueStack" %>
<%@ page import="com.hgd.laboratory.service.LabOpenPlanService" %>
<%@ page import="com.hgd.laboratory.po.LabOpenPlan" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<%@taglib prefix="s" uri="/struts-tags" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

    </style>
</head>
<%!
    String labOpenPlanStatusToString(int status) {
        if (status == LabOpenPlanService.SCRATCH)
            return "草稿";
        else
            return "已发布";
    }
%>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part">
    <div id="contents"><!-- InstanceBeginEditable name="EditRegion_MainFunc" -->

        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#defaultNavbar1" aria-expanded="false"><span class="sr-only">Toggle navigation</span><span
                                class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">开放计划</a></div>
                    <div>
                        <p class="navbar-text">学期：${sessionScope.currentTerm}</p>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->

                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
            <%
                ValueStack vs = (ValueStack) request.getAttribute("struts.valueStack");
                int status_value = 0;
                try {
                    LabOpenPlan labplan = (LabOpenPlan) vs.findValue("model");
                    status_value = labplan.getStatus();
                } catch (Exception e) {
                    ;
                }
            %>
            <h3>实验室<strong><s:property value="model.labId"/>
                开放计划</strong><strong><%= "--" + labOpenPlanStatusToString(status_value)%>
            </strong></h3>
            <%--  <form role="form" >--%>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>星期一</th>
                    <th>星期二</th>
                    <th>星期三</th>
                    <th>星期四</th>
                    <th>星期五</th>
                    <th>星期六</th>
                    <th>星期日</th>
                </tr>
                </thead>
                <tbody>
                <%
                    Map<String, String> lopMap = (Map<String, String>) request.getAttribute("labOpenPlanDetailMap");
                    for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
                        out.println("<tr>");
                        out.println("<th>第" + i + "大节</th>");
                        for (int j = 1; j <= 7; j++) {
                            String key = "t" + i + "w" + j;
                            String value = lopMap.get(key);
                            String strtd = String.format("<td id='%s'><textarea   name='%s' rows='3' cols='10' readonly>%s</textarea></td>", key, key, value);
                            out.println(strtd);

                        }
                        out.println("</tr>");

                    }
                %>
                </tbody>
            </table>
            <div class="btn-group btn-group-justified" role="group"
                 aria-label="Justified button group with nested dropdown"><a href="#" class="btn btn-primary btn-sm"
                                                                             role="button"
                                                                             onclick="window.history.back();location.reload();">关闭</a>
            </div>
            <p class="sj-div"><b>注：</b></p>
            <p class="sj-div">请填写开放周次：指定某一周次，例第1周写成：1 </p>
            <p class="sj-div">请填写开放周次：指定第几周到第几周，例第3周到6周写成：3-6</p>
            <p class="sj-div">请填写开放周次：上面两种情况的复合，例第1周，第3周到6周写成：1，3-6 </p>
            <p></p>
            <%--  </form>--%>
        </div>

        <!-- InstanceEndEditable --></div>
</div>
<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
<!-- InstanceEnd -->
</html>