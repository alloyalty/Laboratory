<%@ page import="com.hgd.laboratory.po.Notice" %>
<%@ page import="com.hgd.laboratory.dto.PaginationData" %>
<%@ page import="com.hgd.laboratory.util.Page" %>

<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>通知/规章</title>
    <%@include file="WEB-INF/include/share.txt"%>

    <style>
        section ul li {
            font-size: 16px;
            color: #424242;
            list-style: none;
            line-height: 28px;
            display: inline-block;
            white-space: nowrap;
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>

</head>

<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"
                    aria-expanded="false"><span class="sr-only">Toggle navigation</span><span
                    class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="defaultNavbar1">
            <form class="navbar-form navbar-right" role="search" id="searchForm">
                <div class="form-group">
                    <input type="text" class="form-control" placeholder="输入关键字"
                           name="keyword" id="keyword" value="<s:property value='keyword' />">
                </div>
                <button type="submit" class="btn btn-default" onclick="queryFilter(null,null,1)">搜索</button>
            </form>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container-fluid -->
</nav>
<div class="row">
    <div class="col-lg-2"><br/>
        <div class="btn-group-vertical" role="group" aria-label="Vertical button group">
            <button type="button" class="btn btn-default" style="width: 100%;text-align: right" onClick="queryFilter('','announce',1)" form="searchForm">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;通知
            </button>
            <button type="button" class="btn btn-default" style="width: 100%;text-align: right" onClick="queryFilter('','rules',1)"  form="searchForm">
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;规章
            </button>
        </div>
    </div>
    <div class="col-lg-10">
        <section>
            <ul class="dataList01">
                <s:iterator value="#paginationData.dataList">
                    <li><a href="announce_view?id=<s:property value='id' />" target="_blank"><s:property value="title" /> </a></li>
                </s:iterator>
            </ul>
        </section>
        <input id="pageNo" type="text" name="pageNo" hidden value="<s:property value='pageNo' />" form="searchForm"/>
        <input id="category" type="text" name="category" hidden value="<s:property value='category' />" form="searchForm"/>



<%--        分页--%>
        <nav>
            <!-- Add class .pagination-lg for larger blocks or .pagination-sm for smaller blocks-->
            <%--            入口参数： form=searchForm, 类型Class<T> PaginationData<Notice>,queryFilter --%>
            <s:if test="#paginationData!=null and #paginationData.page.totalPages>=2">
                <ul class="pagination pagination-lg">
                    <% PaginationData<Notice> paginationData = (PaginationData<Notice>) request.getAttribute("paginationData");
                        Page pageNav = paginationData.getPage();
                        String prevDisabled = "";
                        if (pageNav.getCurrentPage() == 1) prevDisabled = "disabled";
                        String nextDisabled = "";
                        if (pageNav.getCurrentPage() == pageNav.getTotalPages()) nextDisabled = "disabled";
                    %>

                    <li>
                        <button type="button" class="btn btn-default"  <%= prevDisabled%>
                                onclick="queryFilter('',null,<%=  pageNav.getPrevPage()%>)"
                                form="searchForm">
                            &laquo;
                        </button>
                    </li>

                    <s:bean name="org.apache.struts2.util.Counter" var="counter">
                        <s:param name="first" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                        <s:param name="last" value="#paginationData.page.lastPageNoInCurrentPanel"/>
                        <s:iterator>
                            <s:set var="current"/>
                            <s:set var="firstPageNo" value="#paginationData.page.firstPageNoInCurrentPanel"/>
                            <s:if test="#current==#paginationData.page.currentPage">
                                <s:set var="active" value="'active'"/>
                            </s:if>
                            <s:else>
                                <s:set var="active" value="''"/>
                            </s:else>

                            <li>
                                <button type="button" class="btn btn-default <s:property value='#active' />"
                                        onclick="queryFilter('',null,<s:property/> )"
                                        form="searchForm">
                                    <s:property/>
                                </button>
                            </li>
                        </s:iterator>
                    </s:bean>

                    <li>
                        <button type="button" class="btn btn-default"  <%= nextDisabled %>
                                onclick="queryFilter('',null,<%=  pageNav.getNextPage()%>)"
                                form="searchForm">
                            &raquo;
                        </button>
                    </li>
                </ul>
            </s:if>
        </nav>
    </div>
</div>

<%@include file="WEB-INF/include/jquery-bootstrap-ref.txt"%>
<script>
    function queryFilter(keyword,category, pageNo) {
        // !null 参数，表示此参数代替表单参数。
        // null 表示不更改表单参数，使用表单参数
        if (keyword != null) {
            $("#keyword").val(keyword);
        }
        if (category != null) {
            $("#category").val(category);
        }
        if (pageNo != null) {
            $("#pageNo").val(pageNo);
        }
        $("#searchForm").attr("action", "announce_pageQuery");
        $("#searchForm").submit();
    }
</script>

<s:debug/>
</body>
</html>
