<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>实验室开放管理系统</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体";
        }

        .box {
            border: rgb(48, 0, 0) solid thin;
            padding: 5px;
        }

        #contents p {
            line-height: 1.8em;
            text-indent: 2em;
            font-size: 18px;
            padding: 10px 0 10px 0;
            font-weight: normal;
            text-align: justify;
            color: #333;
            font-family: "宋体";
        }

        #contents footer {
            line-height: 1.8em;
            text-indent: 2em;
            font-size: 18px;
            padding: 10px 0 10px 0;
            font-weight: normal;
            text-align: right;
            color: #333;
            font-family: "宋体";
        }

        section ul li {
            font-size: 16px;
            color: #424242;
            list-style: none;
            line-height: 28px;
            display: inline-block;
            white-space: nowrap;
            width: 100%;
            overflow: hidden;
            text-overflow: ellipsis;
        }
    </style>
</head>

<body>
<%@ include file="WEB-INF/include/menu-utf-8.jsp" %>
<div id="middle-part" class="container">
    <div class="row">
        <s:if test="#item!=null" >
        <s:push value="#item">
            <div id="contents" class="col-lg-12">
                <h3 id="title" class="text-center"><s:property value="title"/></h3>
                <h4 id="subTitle" class="text-center"><s:property value="subTitle"/></h4>
                <s:generator separator="\r\n" val="text" var="plist"/>
                <s:iterator value="#plist">
                    <p><s:property/></p>
                </s:iterator>
                <footer><s:property value="author"/></footer>
                <footer><s:property value="publishDate"/></footer>
            </div>
        </s:push>
        </s:if>
    </div>
    <hr style=" height:3px;border:none;border-top:3px double black;"/>
    <div class="row">
        <div class="col-lg-6">
            <section>
                <h4><a href="announce_moreAnnounces" target="_blank">通知</a></h4>
                <ul>
                    <s:iterator value="#announceList">
                        <li><a href="announce_view?id=<s:property value='id' />" target="_blank"><s:property
                                value="title"/> </a></li>
                    </s:iterator>

                </ul>
            </section>

        </div>
        <div class="col-lg-6">

            <section>
                <h4><a href="announce_moreRules" target="_blank">规章</a></h4>
                <ul>
                    <s:iterator value="#rulesList">
                        <li><a href="announce_view?id=<s:property value='id' />" target="_blank"><s:property
                                value="title"/> </a></li>
                    </s:iterator>

                </ul>
            </section>
        </div>
    </div>
</div>

<s:debug/>
<%@ include file="WEB-INF/include/footer.jsp" %>
</body>
</html>
