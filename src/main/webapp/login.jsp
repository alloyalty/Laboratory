<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ page contentType="text/html;charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>实验室开放管理系统登录</title>
    <%@include file="WEB-INF/include/share.txt" %>

    <style type="text/css">
        body, td, th {
            font-size: 16px;
        }

        h1, h2, h3, h4, h5, h6 {
            font-family: "黑体", serif;
        }

        .errorMessage li {
            list-style: none;
            text-align: left;
            vertical-align: bottom;
            padding: 0px 10px 0 0px;
            color: #ee0000;
        }

        #contents {
            width: 100%;
            height: 600px;
            background-color: #eee;
            margin: 0 auto;
            padding-top: 50px;
            padding-bottom: 50px;
        }
    </style>
</head>
<body onload="setMinHeight()">
<div id="header">
    <div id="brand"><img src="images/logo.png" width="475" height="130" alt="河南工业大学"/> <span
            class="systitle">实验室开放管理系统</span></div>
</div>
<div class="clearfloat"></div>
<div id="middle-part">
    <div id="contents">
        <div id="login" class="container" style="width:360px;margin: 0 auto">
            <form class="form-signin" action="user_login.action" method="post" id="loginForm">
                <h3 class="form-signin-heading">实验室开放预约管理系统</h3>
                <br/>
                <div class="input-group"> <span class="input-group-addon"> <span
                        class="glyphicon glyphicon-user"></span></span>
                    <label for="inputUsername" class="sr-only">用户名</label>
                    <input
                            name="id" type="text" id="inputUsername" class="form-control"
                            placeholder="用户名" style="width: 300px" required autofocus>
                </div>
                <label class="sr-only" for="inputPassword">密码</label>
                <div class="input-group">
                    <div class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></div>
                    <input class="form-control" id="inputPassword" name="password"
                           type="password" placeholder="密码" required style="width: 300px">
                </div>
                <br/>
                <%--        <div class="checkbox">--%>
                <%--          <label>--%>
                <%--            <input type="checkbox" value="remember-me"--%>
                <%--					name="rememberMe" >--%>
                <%--            记住密码 </label>--%>
                <%--        </div>--%>
                <div>
                    <label class="radio-inline">
                        <input type="radio" name="role"
                               value="student" checked="checked">
                        学生 </label>

                    <label class="radio-inline">
                        <input type="radio"
                               name="role" value="teacher">
                        教师 </label>

                    <label class="radio-inline">
                        <input type="radio"
                               name="role" value="duty">
                        值班员 </label>


                    <label class="radio-inline">
                        <input type="radio"
                               name="role" value="labManager">
                        管理员 </label>

                </div>
                <br/>
                <s:actionerror cssClass="errorMessage" />
                <button class="btn btn-lg btn-primary btn-block" type="submit"
                        style="width: 350px">登录
                </button>
                <%--                findBackPwd_sendEmail--%>
                <a class="btn btn-lg btn-primary btn-block" type="button" id="findBackPwd"
                   style="width: 350px">找回密码
                </a>
                <s:fielderror fieldName="id"/>
            </form>
        </div>
        <div class="clearfloat"></div>
    </div>
</div>

<%@ include file="WEB-INF/include/footer.jsp" %>
<script>

    $(document).ready(function () {
        //同步请求
        /*
        $("#findBackPwd").click(function(){
            let val = $("#inputUsername").val();
            if (val==null || val== undefined || val.trim()===""){
                alert("找回密码必须输入用户名。");
                return;
            } else {
                $("#loginForm").attr("action","findBackPwd_sendEmail");
                $("#loginForm").submit();
            }
        });
        */

        //异步请求
        $("#findBackPwd").click(function () {
            let val = $("#inputUsername").val();
            if (val === null || val === undefined || val.trim() === "") {
                alert("找回密码必须输入用户名。");
                return;
            } else {
                $.post("findBackPwdAsyn_sendEmail", $("#loginForm").serializeArray(),
                    function (data, status) {
                        // alert("数据: \n" + data + "\n状态: " + status);
                        if (data["succ"] == "succ") {
                            alert(data["backMsg"]);
                        } else {
                            alert(data["backMsg"]);
                        }
                    }, "json");
            }
        });
    });


</script>
</body>
</html>