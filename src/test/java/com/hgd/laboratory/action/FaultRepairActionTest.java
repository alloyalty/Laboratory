package com.hgd.laboratory.action;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.*;

public class FaultRepairActionTest extends StrutsSpringTestCase {
    private FaultRepairAction action;
    private ActionProxy proxy;


    @Override
    protected String[] getContextLocations() {
        return super.getContextLocations();
    }


    @Override
    protected void initServletMockObjects() {
        super.initServletMockObjects();
        request = new MockHttpServletRequest();
    }




    public void testMappingAndActionName(String url, String actionName, String methodName) {
        request.setParameter("param", "test...");
        ActionMapping mapping = getActionMapping(url);
        ActionProxy proxy = getActionProxy(url);

        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals(actionName, mapping.getName());
        assertEquals(methodName,proxy.getMethod());
    }






    @Test
    public void testGetActionMapping() {
        request.setParameter("param", "test...");
        ActionMapping mapping = getActionMapping("/faultRepair_reqfill.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("faultRepair_reqfill", mapping.getName());
    }




    /**
     * 输入: lid,fid,faultDFesp ,且写入faultRepair 之中
     * 输出：faultRepair,faultFillList
     * @return
     */
    @Test
    public void test_fillFaultSubmit() throws Exception {
        final String url = "/faultRepair_fillFaultSubmit.action";
        final String actionName = "faultRepair_fillFaultSubmit";
        final String methodName = "fillFaultSubmit";

        testMappingAndActionName(url, actionName, methodName);

    }

    @Test
    public void test_queryFaultByLid() {
        final String url = "/faultRepair_queryFaultByLid.action";
        final String actionName = "faultRepair_queryFaultByLid";
        final String methodName = "queryFaultByLid";

        testMappingAndActionName(url, actionName, methodName);
    }

    @Test
    public void test_updateMaintance() throws Exception {
        final String url = "/faultRepair_updateMaintance.action";
        final String actionName = "faultRepair_updateMaintance";
        final String methodName = "updateMaintance";

        testMappingAndActionName(url, actionName, methodName);


    }
}