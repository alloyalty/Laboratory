package com.hgd.laboratory.action;

import com.hgd.laboratory.util.DateUtil;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class AnAppointmentQueryActionTest {

    @Test
    public void queryHistory() {
    }

    @Test
    public void queryCurrent() {
    }

    @Test
    public void stdDate() throws ParseException {

        check( "2018-05-11 00:00:00","2018-05-11 11:23:53" );
        check( "2018-05-11 00:00:00","2018-05-11 23:41:53" );
        check( "2018-05-11 00:00:00","2018-05-11 00:00:01" );

    }

    private void check(String expected,String s) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
        final Date date = sdf.parse( s );
        final Date mydate = DateUtil.stdDate( date );

        assertEquals( expected ,sdf.format( mydate ) );
    }


}