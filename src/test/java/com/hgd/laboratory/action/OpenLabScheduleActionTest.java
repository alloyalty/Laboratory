package com.hgd.laboratory.action;

import com.hgd.laboratory.service.LabService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Test;

import static org.junit.Assert.*;

public class OpenLabScheduleActionTest extends StrutsSpringTestCase {
    private OpenLabScheduleAction action;
    private ActionProxy proxy;


    @Override
    protected String[] getContextLocations() {
        return super.getContextLocations();
    }

    public void testexecute() {

        final String url = "/openLabSchedule_view.action";
        final String actionName = "openLabSchedule_view";
        final String methodName = "execute";

        testMappingAndActionName(url, actionName, methodName);
    }


    public void testMappingAndActionName(String url, String actionName, String methodName) {
        request.setParameter("param", "test...");
        ActionMapping mapping = getActionMapping(url);
        ActionProxy proxy = getActionProxy(url);

        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals(actionName, mapping.getName());
        assertEquals(methodName,proxy.getMethod());
    }




}