package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.util.OpenPlanUtil;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Test;

import java.util.Map;


public class TestTempOpenPlanAction extends StrutsSpringTestCase {

    private TempOpenPlanAction action;
    private ActionProxy proxy;



    @Override
    protected String[] getContextLocations() {
        return super.getContextLocations();
    }

    @Test
    public void testDelete() {

        final String url = "/tempOpenPlan_delete.action";
        final String actionName = "tempOpenPlan_delete";
        final String methodName = "delete";

        testMappingAndActionName(url, actionName, methodName);
    }

    @Test
    public void testmanagep() {

        final String url = "/tempOpenPlan_managetp.action";
        final String actionName = "tempOpenPlan_managetp";
        final String methodName = "managetp";

        testMappingAndActionName(url, actionName, methodName);
    }
    @Test
    public void testpublist() {

        final String url = "/tempOpenPlan_publish.action";
        final String actionName = "tempOpenPlan_publish";
        final String methodName = "publish";

        testMappingAndActionName(url, actionName, methodName);
    }
    public void testview() {

        final String url = "/tempOpenPlan_view.action";
        final String actionName = "tempOpenPlan_view";
        final String methodName = "view";

        testMappingAndActionName(url, actionName, methodName);
    }


    public void testMappingAndActionName(String url, String actionName, String methodName) {
        request.setParameter("param", "test...");
        ActionMapping mapping = getActionMapping(url);
        ActionProxy proxy = getActionProxy(url);

        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals(actionName, mapping.getName());
        assertEquals(methodName,proxy.getMethod());
    }







    /**
     * 输入方式一：tempPlanId
     * 输出：tempOpenPlan的六个属性, tempOpenPlanDetailMap是一个Map
     * @throws Exception
     */

    @Test
    public void testworkout() throws Exception {

        final String url = "/tempOpenPlan_workout.action";
        final String actionName = "tempOpenPlan_workout";
        final String methodName = "workout";

        testMappingAndActionName(url, actionName, methodName);


    }

    /**
     * 表单或url输入：
     *      方式二：term,weekOfTerm,labId
     * 输出：给workoutTempOpenPlans.jsp使用
     * tempOpenPlan的六个属性, tempOpenPlanDetailMap是一个Map
     * --与view 相同
     */


    //保存一个空白的临时计划测试===>第一次Insert，不是Update
    @Test
    public void testsave() throws Exception {
        final String url = "/tempOpenPlan_save.action";
        final String actionName = "tempOpenPlan_save";
        final String methodName = "save";

        testMappingAndActionName(url, actionName, methodName);

    }


}