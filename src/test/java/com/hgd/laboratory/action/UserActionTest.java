package com.hgd.laboratory.action;

import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.TermService;
import com.hgd.laboratory.service.UserService;
import com.hgd.laboratory.util.TermUtil;
import com.hgd.laboratory.util.TestUtil;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Test;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserActionTest extends StrutsSpringTestCase {

    @Test
    public void testLoginMapping() {
        ActionMapping mapping = getActionMapping("/user_login.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("user_login", mapping.getName());

    }

    public void testlogin_UserNameAndPasswordIsCorrect_ReturnSuccess() throws Exception {
        //stub TermUtil 以便隔离TermUtil
        TestUtil.stubTermUtil();

        //set parameters before calling getActionProxy
        request.setParameter("id", "student");
        request.setParameter("password", "123456");
        request.setParameter("role", "student");

        //stub userService 以便隔离Service
        UserService userService = mock(UserService.class);
        User user = new User("student","123456","student","");
        when(userService.checkUser("student","123456","student")).thenReturn(user);
        TermService termService = mock(TermService.class);
        when(termService.termIsRunning()).thenReturn(true);

        ActionProxy proxy = getActionProxy("/user_login.action");
        assertNotNull(proxy);

        UserAction action = (UserAction) proxy.getAction();
        action.setUserService(userService);
        action.setTermService(termService);
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
        assertEquals("student",action.getModel().getId());
    }



    public void testlogin_UserNameOrPasswordNotCorrect_ReturnInput() throws Exception {
        //stub TermUtil 以便隔离TermUtil
        TestUtil.stubTermUtil();

        //set parameters before calling getActionProxy
        request.setParameter("id", "student");
        request.setParameter("password", "123");
        request.setParameter("role", "student");

        //stub userService 以便隔离Service
        UserService userService = mock(UserService.class);
        User user = new User("student","123","student","");
        when(userService.checkUser("student","123","student")).thenReturn(null);

        ActionProxy proxy = getActionProxy("/user_login.action");
        assertNotNull(proxy);

        UserAction action = (UserAction) proxy.getAction();
        action.setUserService(userService);
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
        assertEquals("student",action.getModel().getId());
    }



    @Test
    public void changePwd_原密码错_ReturnInput() throws Exception {
        //stub TermUtil 以便隔离TermUtil
        TestUtil.stubTermUtil();

        //set parameters before calling getActionProxy
        request.setParameter("logName", "student");
        request.setParameter("oldPassword", "654321");
        request.setParameter("newPassword", "654321");
        request.setParameter("reNewPassword", "654321");

        //stub userService 以便隔离Service
        UserService userService = mock(UserService.class);
        User user = new User("student","123456","student","");
        when(userService.findOne(any())).thenReturn(user);

        //stub session 构建环境，session保存有登录信息
        Map<String ,Object > sessionMap =  new HashMap<>();
        createLoginStatus(sessionMap,user);


        ActionProxy proxy = getActionProxy("/user_changePwd.action");
        assertNotNull(proxy);

        UserAction action = (UserAction) proxy.getAction();
        action.setSessionMap(sessionMap);  //注入stub
        action.setUserService(userService); //注入stub
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
//        assertTrue("密码更新成功。".equals());
    }

    @Test
    public void changePwd_二次输入密码不同_ReturnInput() throws Exception {
        //stub TermUtil 以便隔离TermUtil
        TestUtil.stubTermUtil();

        //set parameters before calling getActionProxy
        request.setParameter("logName", "student");
        request.setParameter("oldPassword", "123456");
        request.setParameter("newPassword", "666666");
        request.setParameter("reNewPassword", "654321");

        //stub userService 以便隔离Service
        UserService userService = mock(UserService.class);
        User user = new User("student","123456","student","");
        when(userService.findOne(any())).thenReturn(user);

        //stub session 构建环境，session保存有登录信息
        Map<String ,Object > sessionMap =  new HashMap<>();
        createLoginStatus(sessionMap,user);


        ActionProxy proxy = getActionProxy("/user_changePwd.action");
        assertNotNull(proxy);

        UserAction action = (UserAction) proxy.getAction();
        action.setSessionMap(sessionMap);  //注入stub
        action.setUserService(userService); //注入stub
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.INPUT, result);
//        assertTrue("密码更新成功。".equals());
    }

    @Test
    public void testChangePwd_DataIsCorrect_ReturnSuccess() throws Exception {
        //stub TermUtil 以便隔离TermUtil
        TestUtil.stubTermUtil();

        //set parameters before calling getActionProxy
        request.setParameter("logName", "student");
        request.setParameter("oldPassword", "123456");
        request.setParameter("newPassword", "654321");
        request.setParameter("reNewPassword", "654321");

        //stub userService 以便隔离Service
        UserService userService = mock(UserService.class);
        User user = new User("student","123456","student","");
        when(userService.findOne(any())).thenReturn(user);

        //stub session 构建环境，session保存有登录信息
        Map<String ,Object > sessionMap =  new HashMap<>();
        createLoginStatus(sessionMap,user);


        ActionProxy proxy = getActionProxy("/user_changePwd.action");
        assertNotNull(proxy);

        UserAction action = (UserAction) proxy.getAction();
        action.setSessionMap(sessionMap);  //注入stub
        action.setUserService(userService); //注入stub
        assertNotNull(action);

        String result = proxy.execute();
        assertEquals(Action.SUCCESS, result);
//        assertTrue("密码更新成功。".equals());

    }

    public void createLoginStatus(Map<String, Object> sessionMap, User user) {
        Term currentTermObj = TermUtil.getCurrentTerm();
        String termStr = currentTermObj.getYear() + "-" + currentTermObj.getTerm();

        sessionMap.put("user",user);
        sessionMap.put("currentTermObj",currentTermObj);
        sessionMap.put("currentTerm",termStr);
    }


}