package com.hgd.laboratory.action;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionProxy;
import org.apache.struts2.StrutsSpringTestCase;
import org.apache.struts2.dispatcher.mapper.ActionMapping;
import org.junit.Test;

import javax.servlet.http.HttpSession;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class LabOpenPlanActionTest extends StrutsSpringTestCase {
    private LabOpenPlanAction action;
    private ActionProxy proxy;




    @Override
    protected String[] getContextLocations() {
        return super.getContextLocations();
    }


    @Test
    public void testGetActionMapping() {
        request.setParameter("param", "test...");
        ActionMapping mapping = getActionMapping("/labOpenPlan_view.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("labOpenPlan_view", mapping.getName());
    }




    public void testview() throws Exception {
        ActionMapping mapping = getActionMapping("/labOpenPlan_view.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("labOpenPlan_view", mapping.getName());


    }

    /**
     * @param 无
     * 以登录后的当前学期为参数
     * 输出: labOpenPlanList
     */
    @Test
    public void testmanage() throws Exception {
        ActionMapping mapping = getActionMapping("/labOpenPlan_manage.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("labOpenPlan_manage", mapping.getName());

    }


    /** 从表单form 或者 url请求来的输入参数
     * input:
     * @param srch_openPlanId   Case1 已知 openPlanID
     *
     * @param  srch_labId   Case 2 openPlanID未知，但term,labID 已知
     * output:
     *  model, labOpenPlanDetailMap
     *
     * @return
     */

    @Test
    public void testworkout_存在srch_openPlanId参数() throws Exception {
        //set parameters before calling getActionProxy
        request.setParameter("srch_openPlanId", "1");
        //stub LabOpenPlanService
        LabOpenPlanService labOpenPlanService = mock(LabOpenPlanService.class);
        when(labOpenPlanService.findLabOpenPlan(1)).thenReturn(any());

        ActionProxy proxy = getActionProxy("/labOpenPlan_workout.action");
        assertNotNull(proxy);
        //检查其方法的正确性
        assertEquals("workout",proxy.getMethod());

        LabOpenPlanAction action = (LabOpenPlanAction) proxy.getAction();
        action.setLabOpenPlanService(labOpenPlanService);
        //检查所执行的Action类名的正确性
        assertTrue("Spring 关于action配置出错，导致错误：Wrong method was defined as an action method",
                proxy.getInvocation().getAction() instanceof  LabOpenPlanAction);

        //检查action的执行结果
        String result = proxy.execute();
        assertEquals("workout", result);

        //检查action 输出属性
        verify(labOpenPlanService,times(1)).findLabOpenPlan(1);

    }

  @Test
    public void testworkout_不存在srch_openPlanId参数() throws Exception {
      //mockito 未完全把握，todo

      //set parameters before calling getActionProxy
      String term="2018-2019-1";
      request.setParameter("srch_labId","6333");
      request.getSession(true).setAttribute("currentTerm",term);
      assertTrue(term.equals(request.getSession().getAttribute("currentTerm")));
      //stub LabOpenPlanService
      LabOpenPlanService labOpenPlanService = mock(LabOpenPlanService.class);
      when(labOpenPlanService.findLabOpenPlan(any(),any())).thenReturn(any());

      ActionProxy proxy = getActionProxy("/labOpenPlan_workout.action");
      assertNotNull(proxy);
      LabOpenPlanAction action = (LabOpenPlanAction) proxy.getAction();
      action.setLabOpenPlanService(labOpenPlanService);

      //检查其方法的正确性
      assertEquals("workout",proxy.getMethod());

      //检查所执行的Action类名的正确性
      assertTrue("Spring 关于action配置出错，导致错误：Wrong method was defined as an action method",
              proxy.getInvocation().getAction() instanceof  LabOpenPlanAction);

      //检查action的执行结果
      String result = proxy.execute();
      assertEquals("workout", result);

      //检查action 输出属性,此处term参数置为空，是因为Strut2测试框架对session模拟失败，不得不设置为null
      //LabOpenPlanAction 第148行    String term = (String)request.getSession().getAttribute("currentTerm");结果为空
      //可以认为是StrutsSpringTestCase的一个bug
      final LabOpenPlanTuple labOpenPlan = verify(labOpenPlanService, times(1)).findLabOpenPlan(any(), any());


  }




    @Test
    public void testdelete() {
        ActionMapping mapping = getActionMapping("/labOpenPlan_delete.action");
        assertNotNull(mapping);
        assertEquals("/", mapping.getNamespace());
        assertEquals("labOpenPlan_delete", mapping.getName());

    }

    @Test
    public void test可以通过request创建会话() {
        request.setParameter("a","123");
        HttpSession x = request.getSession(true);
        assertNotNull(x);

        x.setAttribute("currentTerm","2018-2019-1");
        assertEquals("2018-2019-1",(String)x.getAttribute("currentTerm"));

    }
}