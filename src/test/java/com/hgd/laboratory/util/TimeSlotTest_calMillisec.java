package com.hgd.laboratory.util;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class TimeSlotTest_calMillisec {
    //单元测试发现3个错误 用时1h

    @Parameters(name = "{index}:{0}= calMillisec(\"{1}\")")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {0, "0:00"},
                {60 * 60 * 1000, "0:60"},
                {60 * 60 * 1000, "01:00"},

                {60 * 60 * 1000 + 1 * 60 * 1000, "01:01"},
                {2 * 60 * 60 * 1000, "02:00"},
                {24 * 60 * 60 * 1000, "24:00"},

                {2 * 60 * 60 * 1000, "01:60"},
                {2 * 60 * 60 * 1000 + 15 * 60 * 1000, "02:15"},
                {23 * 60 * 60 * 1000 + 59 * 60 * 1000, "23:59"},

        });
    }

    private int expected;
    private String timeStr;

    public TimeSlotTest_calMillisec(int expected, String timeStr) {
        this.expected = expected;
        this.timeStr = timeStr;
    }

    @Test
    public void calMiliec() {
        TimeSlot timeSlot = TimeSlot.getInstance();

        assertEquals(expected, timeSlot.calMiliec(timeStr));
    }


}