package com.hgd.laboratory.util;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TimeSlotTest {
    //单元测试发现3个错误 用时1h



    private int expectedTimeSlot;
    private String timeStr;

    public TimeSlotTest(int expectedTimeSlot, String timeStr) {
        this.expectedTimeSlot = expectedTimeSlot;
        this.timeStr = timeStr;
    }

    @Parameters(name = "{index}: calTimeslot(\"{1}\")=>{0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                { 0,"2019-09-12 7:00:00" },
                { 0,"2019-09-12 8:29:00" },
                { 1,"2019-09-12 8:30:00" },
                { 1,"2019-09-12 9:30:00" },
                { 1,"2019-09-12 10:05:00" },
                { 2,"2019-09-12 10:06:00" },
                { 2,"2019-09-12 10:24:59" },
                { 2,"2019-09-12 10:25:00" },
                { 2,"2019-09-12 12:00:00" },
                { 3,"2019-09-12 12:00:01" },
                { 3,"2019-09-12 12:01:00" },
                { 3,"2019-09-12 12:10:00" },
                { 3,"2019-09-12 14:20:00" },
                { 4,"2019-09-12 14:30:00" },
                { 5,"2019-09-12 16:25:00" },
                { 5,"2019-09-12 18:00:00" },
                { 6,"2019-09-12 18:01:00" },
                { 6,"2019-09-12 19:30:00" },
                { 6,"2019-09-12 20:00:00" },
                { 6,"2019-09-12 21:10:00" },
                { 7,"2019-09-12 21:10:01" },
                { 7,"2019-09-12 21:11:00" },
        });

    }
    @AfterClass
    public static void teardown() {
        TimeSlot.reset();
    }

    @Test
    public void calTimeslot() throws ParseException {
        /*                  2019-09-12 7:00,8:29      .....0
        {{"8:30","10:05"},  2019-09-12 8:30, 9:30, 10:05  ......1
        {"10:25","12:00"},  2019-09-12 10:06,10:24:59,10:25, 12:00  .......2
        {"12:10","14:20"},  12:00:01,12:01,12:10,14:20  ..........3
        {"14:30","16:05"},  14:30 ......4
        {"16:25","18:00"},  16:25,18:00 ......5
        {"19:30","21:10"}}; 19:30,20:00,21:10 ........6
         */
        String[][] timeslot = new String[][]{{"8:30", "10:05"}, {"10:25", "12:00"},
                {"12:10", "14:20"}, {"14:30", "16:05"}, {"16:25", "18:00"},
                {"19:30", "21:10"}};
        TimeSlot.setCustomTimeTable(timeslot);
        TimeSlot timeSlot = TimeSlot.getInstance();
        Date currDateTime = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" ).parse( timeStr );

        int ts = timeSlot.calTimeslot( currDateTime );

        assertEquals( this.expectedTimeSlot,ts );


    }


}