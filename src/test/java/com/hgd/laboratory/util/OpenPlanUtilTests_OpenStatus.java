package com.hgd.laboratory.util;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.junit.Test;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class OpenPlanUtilTests_OpenStatus {

    @Parameters(name = "{index}: openStatus({1},\"{2}\")={0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"1", 1, "1,3,5"},
                {"1", 2, "1,2,5"},
                {"1", 3, "1,4,3"},
                {"1", 5, "1,3,5-7"},
                {"1", 6, "1,3-7"},
                {"0", 1, "3,5,8"},
                {"0", 2, "1,3-5"},
                {"0", 3, "1,5-12"},
                {"0", 11, "1-10,3,5"},
                {"0", 13, "1-12,14"},
                {"0", 13, ""},
        });

    }

    private String expected;
    private int wOfTerm;
    private String content;

    public OpenPlanUtilTests_OpenStatus(String expected, int wOfTerm, String content) {
        this.expected = expected;
        this.wOfTerm = wOfTerm;
        this.content = content;
    }

    @Test
    public void openPlan_OpenStatus_ReturnStatus() throws Exception {
        int weeksOfTerm = 22;

        final String actual = OpenPlanUtil.openPlan_OpenStatus(wOfTerm, content, weeksOfTerm);

        assertEquals(expected, actual);
    }


}
