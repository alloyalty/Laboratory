package com.hgd.laboratory.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiService {

    @Autowired
    private TestApiService testApiService;

    public String test() {
        String connect = testApiService.connect();
        connect += "test";//test自己的业务
        return connect;
    }
}
