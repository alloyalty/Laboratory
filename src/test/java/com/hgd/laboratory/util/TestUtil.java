package com.hgd.laboratory.util;

import com.hgd.laboratory.po.Term;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class TestUtil {
    public static void stubTermUtil() throws ParseException {
        Date theDate = new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2020-2-18").getTime());
        Term stubTerm = new Term("2018-2019", 1, 21, theDate);
        TermUtil.reset();
        TermUtil.setCustomTerm(stubTerm);
    }
}
