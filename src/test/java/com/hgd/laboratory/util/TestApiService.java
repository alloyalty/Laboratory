package com.hgd.laboratory.util;

import org.springframework.stereotype.Component;

@Component
public class TestApiService {
    public String connect() {
        return "error";
    }

    public String findFromDb() {
        return "db_data";
    }
}
