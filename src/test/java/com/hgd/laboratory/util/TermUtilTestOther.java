package com.hgd.laboratory.util;

import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.service.TermService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class TermUtilTestOther {
    @Autowired
    private TermService termService;
    @Autowired
    private SessionFactory sessionFactory;


    //在已经存在正在运行的学期时，返回学期串
    //在没有学期运行时，应返回空串
    @Test
    public void getTermStr_学期正在运行_Return学期串() throws ParseException {
        //首先stub TremUtil
        TermUtil.reset();
        Date theDate = new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2020-2-18").getTime());
        Term stubTerm = new Term("2018-2019", 1, 21, theDate);
        stubTerm.setStatus(1);
        TermUtil.setCustomTerm(stubTerm);

        //执行
        String s = TermUtil.getTermStr();

        //断言
        TermUtil.reset();
        System.out.println(s);
        assertTrue("2018-2019-1".equals(s));
    }

    @Test
    public void getTermStr_学期没有运行时_Return空串() throws ParseException {
        //首先stub TremUtil
        TermUtil.reset();
        Date theDate = new Date(new SimpleDateFormat("yyyy-MM-dd").parse("2020-2-18").getTime());
        Term stubTerm = new Term("2018-2019", 1, 21, theDate);
        stubTerm.setStatus(0);
        TermUtil.setCustomTerm(stubTerm);

        //执行
        String s = TermUtil.getTermStr();

        //断言
        TermUtil.reset();
        System.out.println(s);
        assertTrue("".equals(s));
    }

}