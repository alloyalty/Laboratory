package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.Term;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(Parameterized.class)
public class TermUtilTest_calWeekOfTerm extends TermUtilTest_Base {

    @Parameters(name = "{index}: calWeekOfTerm(\"{0}\")={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"20190826", 1},
                {"20190827", 1},
                {"20190828", 1},
                {"20190829", 1},
                {"20190901", 1},
                {"20190902", 2},
                {"20190923", 5},

        });

    }


    private String dateStr;
    private Integer expected;

    public TermUtilTest_calWeekOfTerm(String dateStr, Integer expected) {
        this.dateStr = dateStr;
        this.expected = expected;
    }

    @Test
    public void calWeekOfTerm() throws InvalidDateInTerm, ParseException {

        stubTerm();

        Date currDate = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
        final Integer weekOfTerm = TermUtil.calWeekOfTerm(currDate, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday());

        assertEquals(expected, weekOfTerm);


    }

    public void testInstance(String data, int expected) throws ParseException, InvalidDateInTerm {
        TermUtil termUtil = TermUtil.getInstamce();
        Date date = new SimpleDateFormat("yyyyMMdd").parse(data);
        assertTrue(termUtil.calWeekOfTerm(date, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday()) == expected);
    }


}