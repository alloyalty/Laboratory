package com.hgd.laboratory.util;

import org.junit.Test;

import static org.junit.Assert.*;

public class PageTest {

    @Test
    public void getMaxPanelNo() {
        Page page = new Page(20);  //每页20条记录
        page.setTotalRows(999L);
        page.setPerPanelPages(10);

        long currentPage = page.getTotalPages();
        page.setCurrentPage(currentPage);
        int  currPanelNo = page.getCurrentPanelNo();

        int expected_CurreentPanelNo = 5;
        assertEquals(expected_CurreentPanelNo,currPanelNo);

    }


}