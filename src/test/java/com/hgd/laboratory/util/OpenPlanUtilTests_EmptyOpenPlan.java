package com.hgd.laboratory.util;

import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.service.LabService;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OpenPlanUtilTests_EmptyOpenPlan {
      int weeksOfTerm = 22;

    @Test
    public void construct_empty() throws Exception{
        //预先设置optDetailMap数据
        //手工计算expected
        //电脑计算actual
        //比较
        int wOfTerm = 7;
        Map<String, String> optDetailMap = new HashMap<>();
//        0 0 0 0 0 0 1
//        0 0 0 0 0 0 1
//        0 0 0 0 0 0 1
//        0 0 0 0 0 0 1
//        0 0 0 0 0 0 1
//        0 0 0 0 0 0 1
//        1 1 1 1 1 1 1
        Map<String,String> expectedMap = new HashMap<>();
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <=7 ; j++) {
                String key ="t"+i+"w"+j;
                boolean b = (wOfTerm==i ) || (wOfTerm==j);
                String v  = b?"1":"0";
                optDetailMap.put(key,""+i+"-"+j);
                expectedMap.put(key,v);
//                System.out.print(" "+v);
            }
//            System.out.println();
        }

        Map<String, TempItem> actual = OpenPlanUtil.construct_empty(wOfTerm,optDetailMap, weeksOfTerm);
        for (int i = 1; i <=LabService.TIMESLOTNUM ; i++) {
            for (int j = 1; j <=7 ; j++) {
                String key ="t"+i+"w"+j;
                assertEquals(expectedMap.get(key),actual.get(key).getOrign());
                assertEquals("",actual.get(key).getTemp());
                //语义不是很清楚
            }

        }


    }





}
