package com.hgd.laboratory.util;

import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import static org.junit.Assert.*;

public class ReservationRules_ConstructTest {


    @Test
    public void readConfig() {
        //文件的类路径为输入参数
        //读取文件,构造一个ReservationRules对象
        //断言

        ReservationRules expect = new ReservationRules(1,3);
        ReservationRules reservationRules = ReservationRules.buildRR("/labopen.properties");

        assertEquals(expect,reservationRules);
    }



    @Test
    public void testPath() {
        System.out.println(ReservationRules_ConstructTest.class.getResource("").getPath());
        System.out.println(ReservationRules_ConstructTest.class.getResource("/").getPath());
        System.out.println(ReservationRules_ConstructTest.class.getResource("/com/hgd/laboratory/util/").getPath());
        System.out.println();
        System.out.println( Thread.currentThread().getContextClassLoader().getResource("").getPath());
        System.out.println( Thread.currentThread().getContextClassLoader().getResource("com/hgd/laboratory/util/").getPath());
        System.out.println();
        final ClassLoader classLoader = this.getClass().getClassLoader();
        System.out.println( classLoader.getResource("").getPath());
        System.out.println( this.getClass().getClassLoader().getResource("com/hgd/laboratory/util/").getPath());


    }

}