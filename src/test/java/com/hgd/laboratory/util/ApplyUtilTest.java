package com.hgd.laboratory.util;

import com.hgd.laboratory.po.OpenRes;
import org.junit.Test;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

public class ApplyUtilTest {

    @Test
    public void fillResMess() throws ParseException {
        //新建一对象
        //程序执行
        //手工执行结果
        //比较
        Date openDate =new java.sql.Date(new  SimpleDateFormat("yyyyMMdd").parse( "20190825" ).getTime());

        OpenRes openRes = new OpenRes( 1,openDate,"6312" );
        ApplyUtil.fillResMess( openDate,"6312",1,openRes,39 );

        OpenRes openResExpect = new OpenRes( 1,openDate,"6312" );
        openResExpect.setResConsumed( 0 );
        openResExpect.setResTotalNum( 39 );
        openResExpect.setResRest( 39 );
        openResExpect.setDayOfWeek( 7 );
        openResExpect.setCurrentAvailApplyCode( "20190825#6312#1001" );
        assertEquals(openResExpect, openRes);


    }



    @Test
    public void parseApplyIds() {
        assertEqualsList( Arrays.asList(1L,3L,5L),ApplyUtil.parseApplyIds( new String[] {"1","3","5"} ) );
        assertEqualsList( Arrays.asList(1L,3L),ApplyUtil.parseApplyIds( new String[] {"1","3"} ) );
        assertEqualsList( Arrays.asList(1L),ApplyUtil.parseApplyIds( new String[] {"1"} ) );
        assertEqualsList( Arrays.asList(new Long[] {}),ApplyUtil.parseApplyIds( new String[] {} ) );
    }

    private void assertEqualsList(List<Long> asList, List<Long> parseApplyIds) {
        boolean succ = true;
        if (asList.size() == parseApplyIds.size()) {
            for (int i=0;i<asList.size();i++) {
                if (!asList.get(i).equals(parseApplyIds.get(i))) {
                    succ =false;
                }
            }
        } else {
            succ =false;
        }
        if (!succ) {
            System.out.println("expected:"+asList.toString());
            System.out.println("actual:"+parseApplyIds.toString());

        }
        assertTrue(succ);
    }
}