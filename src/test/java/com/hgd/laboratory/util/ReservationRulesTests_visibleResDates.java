package com.hgd.laboratory.util;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ReservationRulesTests_visibleResDates {
    public static final int EXPECTED_LENGTH = 3;

    /*
    未来哪些天资源可见
    currentDate是 20180801
    可见天是20180803,20180804,20180805
    不可见天是20180801,20180802
 */

    /*       输入   输出
月中   20180615 [20180617,20180618,20180619]
月未   20190831 [20190902，2019093，20190904]
月初   20190901 [20190903,~04,~05]
跨月   20190227 [20190301,~2,~3]
跨年   20191230 [20200101,~2,~3]
 */

    private String currDate;
    private String firstDate;
    private String endDate;

    public ReservationRulesTests_visibleResDates(String currDate, String firstDate, String endDate) {
        this.currDate = currDate;
        this.firstDate = firstDate;
        this.endDate = endDate;
    }

    @Parameters(name = "{index}: openStatus({1},\"{2}\")={0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                { "20180615","20180617","20180619" },
                { "20190831","20190902","20190904" },
                { "20190901","20190903","20190905" },
                { "20190227","20190301","20190303" },
                { "20191230","20200101","20200103" },
        });


    }

    @Test
    public void visibleResDates() throws ParseException {
        {
            Data data = new Data(currDate, firstDate, endDate);
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");

            Date date = df.parse(data.getCurrDate());
            Date date1 = df.parse(data.getFirstDate());
            Date date2 = df.parse(data.getEndDate());

            ReservationRules reservationRules = new ReservationRules(1, 3 );
            List<Date> dateList = reservationRules.visibleResDates(date);

            boolean pass = true;
            pass = pass && (EXPECTED_LENGTH == dateList.size());
            pass = pass && yyyymmddCompare(date1, dateList.get(0));
            pass = pass && yyyymmddCompare(date2, dateList.get(2));
            assertTrue(pass);
        }


    }

    private boolean yyyymmddCompare(Date date0, Date date) {
        Calendar calendar0 = Calendar.getInstance();
        calendar0.setTime(date0);
        calendar0.set(Calendar.MILLISECOND, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        boolean ret = true;
        ret = calendar0.get(Calendar.YEAR) == calendar.get(Calendar.YEAR)
                && calendar0.get(Calendar.MONTH) == calendar.get(Calendar.MONTH)
                && calendar0.get(Calendar.DAY_OF_MONTH) == calendar.get(Calendar.DAY_OF_MONTH)
                && calendar0.get(Calendar.DATE) == calendar.get(Calendar.DATE)
        ;
        return ret;

    }

    class Data {
        String currDate;
        String firstDate;
        String endDate;

        public Data(String currDate, String firstDate, String endDate) {
            this.currDate = currDate;
            this.firstDate = firstDate;
            this.endDate = endDate;
        }

        public String getCurrDate() {
            return currDate;
        }

        public void setCurrDate(String currDate) {
            this.currDate = currDate;
        }

        public String getFirstDate() {
            return firstDate;
        }

        public void setFirstDate(String firstDate) {
            this.firstDate = firstDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }


}