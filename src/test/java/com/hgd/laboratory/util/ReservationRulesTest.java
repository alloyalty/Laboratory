package com.hgd.laboratory.util;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class ReservationRulesTest {

    /*
    未来哪些天资源可见
    currentDate是 20180801
    可见天是20180803,20180804,20180805
    不可见天是20180801,20180802
 */
    @Test
    public void visibleResDates() throws ParseException {
        /*       输入   输出
        月中   20180615 [20180617,20180618,20180619]
        月未   20190831 [20190902，2019093，20190904]
        月初   20190901 [20190903,~04,~05]
        跨月   20190227 [20190301,~2,~3]
        跨年   20191230 [20200101,~2,~3]
         */

        {
            Data data = new Data( "20180615","20180617","20180619" );
            SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );

            Date date = df.parse( data.getCurrDate() );
            Date date0 = df.parse( data.getFirstDate() );
            Date date2 = df.parse( data.getEndDate() );

            ReservationRules reservationRules = new ReservationRules( 1,3 );
            List<Date> dateList = reservationRules.visibleResDates(date);

            assertEquals( 3,dateList.size() );
            assertTrue( yyyymmddCompare( date0,dateList.get( 0 ) ) );
            assertTrue( yyyymmddCompare( date2,dateList.get( 2 ) ) );
        }

        {//  月未   20190831 [20190902，2019093，20190904]
            Data data = new Data( "20190831","20190902","20190904" );
            SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );

            Date date = df.parse( data.getCurrDate() );
            Date date0 = df.parse( data.getFirstDate() );
            Date date2 = df.parse( data.getEndDate() );

            ReservationRules reservationRules = new ReservationRules( 1,3 );
            List<Date> dateList = reservationRules.visibleResDates(date);
            assertEquals( 3,dateList.size() );
            assertTrue( yyyymmddCompare( date0,dateList.get( 0 ) ) );
            assertTrue( yyyymmddCompare( date2,dateList.get( 2 ) ) );
        }

        {//月初   20190901 [20190903,~04,~05]
            Data data = new Data( "20190901","20190903","20190905" );
            SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );

            Date date = df.parse( data.getCurrDate() );
            Date date0 = df.parse( data.getFirstDate() );
            Date date2 = df.parse( data.getEndDate() );

            ReservationRules reservationRules = new ReservationRules( 1,3 );
            List<Date> dateList = reservationRules.visibleResDates(date);
            assertEquals( 3,dateList.size() );
            assertTrue( yyyymmddCompare( date0,dateList.get( 0 ) ) );
            assertTrue( yyyymmddCompare( date2,dateList.get( 2 ) ) );
        }

        {// 跨月   20190227 [20190301,~2,~3]
            Data data = new Data( "20190227","20190301","20190303" );
            SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );

            Date date = df.parse( data.getCurrDate() );
            Date date0 = df.parse( data.getFirstDate() );
            Date date2 = df.parse( data.getEndDate() );

            ReservationRules reservationRules = new ReservationRules( 1,3 );
            List<Date> dateList = reservationRules.visibleResDates(date);
            assertEquals( 3,dateList.size() );
            assertTrue( yyyymmddCompare( date0,dateList.get( 0 ) ) );
            assertTrue( yyyymmddCompare( date2,dateList.get( 2 ) ) );
        }

        {// 跨年   20191230 [20200101,~2,~3]
            Data data = new Data( "20191230","20200101","20200103" );
            SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd" );

            Date date = df.parse( data.getCurrDate() );
            Date date0 = df.parse( data.getFirstDate() );
            Date date2 = df.parse( data.getEndDate() );

            ReservationRules reservationRules = new ReservationRules( 1,3 );
            List<Date> dateList = reservationRules.visibleResDates(date);
            assertEquals( 3,dateList.size() );
            assertTrue( yyyymmddCompare( date0,dateList.get( 0 ) ) );
            assertTrue( yyyymmddCompare( date2,dateList.get( 2 ) ) );
        }


    }

    private boolean yyyymmddCompare(Date date0,Date date) {
        Calendar calendar0 = Calendar.getInstance();
        calendar0.setTime( date0 );
        calendar0.set(Calendar.MILLISECOND,0);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime( date );
        calendar.set(Calendar.MILLISECOND,0);
        boolean ret = true;
        ret = calendar0.get( Calendar.YEAR ) == calendar.get( Calendar.YEAR )
                && calendar0.get( Calendar.MONTH ) == calendar.get( Calendar.MONTH )
                && calendar0.get( Calendar.DAY_OF_MONTH ) == calendar.get( Calendar.DAY_OF_MONTH )
                && calendar0.get( Calendar.DATE ) == calendar.get( Calendar.DATE )
        ;
        return ret;

    }

    class Data {
        String currDate;
        String firstDate;
        String endDate;

        public Data(String currDate,String firstDate,String endDate) {
            this.currDate = currDate;
            this.firstDate = firstDate;
            this.endDate = endDate;
        }

        public String getCurrDate() {
            return currDate;
        }

        public void setCurrDate(String currDate) {
            this.currDate = currDate;
        }

        public String getFirstDate() {
            return firstDate;
        }

        public void setFirstDate(String firstDate) {
            this.firstDate = firstDate;
        }

        public String getEndDate() {
            return endDate;
        }

        public void setEndDate(String endDate) {
            this.endDate = endDate;
        }
    }

    @Test
    public void obeyRule() throws ParseException {
                /*
        currentDate是 20180801
        specDate 是20180803,20180804,20180805是符合规定的

        依据20180801 计算出合理的区间startdate.enddDate，如果在此区间之内，则合规；否则不规。
         */

        SimpleDateFormat df = new SimpleDateFormat( "yyyyMMdd hh:mm:ss" );

        Date date = df.parse( "20180801 12:57:01" );

        Date date0 = df.parse( "20180803 00:00:01" );
        Date date1 = df.parse( "20180804 12:00:00" );
        Date date2 = df.parse( "20180805 23:59:99" );

        ReservationRules reservationRules = new ReservationRules( 1,3 );
        boolean pass = reservationRules.obeyRule( date0, date);
        pass =pass && reservationRules.obeyRule( date1, date);
        pass = pass && !reservationRules.obeyRule( date2, date);

        assertTrue(pass );


    }
}