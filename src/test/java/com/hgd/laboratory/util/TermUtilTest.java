package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.Term;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.*;

public class TermUtilTest  extends TermUtilTest_Base {

    @Test(expected = Exception.class)
    public void calWeekOfTerm_BadDate_ThrowException() throws InvalidDateInTerm, ParseException {

        stubTerm();

        TermUtil termUtil = TermUtil.getInstamce();
        Date date = new SimpleDateFormat("yyyyMMdd").parse("20190825");
        termUtil.calWeekOfTerm(date, TermUtil.getCurrentTerm().getDayOfFirstWeekMonday());

    }


}