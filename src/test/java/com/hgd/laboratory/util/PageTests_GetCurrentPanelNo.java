package com.hgd.laboratory.util;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class PageTests_GetCurrentPanelNo {

    private long currentPage;
    private int expected_curreentPanelNo;
    
    @Parameters(name = "{index}: checkCurrPanelNo(currentPage={0},currentPanelNo={1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {1, 1},
                {2, 1},
                {3, 1},
                {4,1 },
                {5, 1},
                {6, 2},
        });

    }

    public PageTests_GetCurrentPanelNo(long currentPage, int expected_curreentPanelNo) {
        this.currentPage = currentPage;
        this.expected_curreentPanelNo = expected_curreentPanelNo;
    }

    @Test
    public void getCurrentPanelNo() {
        Page page = new Page(20);  //每页20条记录
        page.setTotalRows(999L);                  //总记录数999
        page.setPerPanelPages(5);                 //每个panel上显示5个连续的页超链标签
        page.setCurrentPage(this.currentPage);

        int  actual = page.getCurrentPanelNo();

        assertEquals(this.expected_curreentPanelNo,actual);


    }
    public void checkCurrPanelNo(long currentPage, int expected_curreentPanelNo, Page page) {

        page.setCurrentPage(currentPage);
        int expected_CurreentPanelNo = expected_curreentPanelNo;
        int  currPanelNo = page.getCurrentPanelNo();
        assertEquals(expected_CurreentPanelNo,currPanelNo);

    }


}