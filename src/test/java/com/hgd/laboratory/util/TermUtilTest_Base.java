package com.hgd.laboratory.util;

import com.hgd.laboratory.po.Term;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TermUtilTest_Base {
    public void stubTerm() throws ParseException {
        TermUtil.reset();
        Date firstDateOfTerm = new SimpleDateFormat("yyyyMMdd").parse("20190826");
        java.sql.Date firstDate = new java.sql.Date(firstDateOfTerm.getTime());
        final Term termStub = new Term("2018-2019", 1, 22, firstDate);
        TermUtil.setCustomTerm(termStub);
        TermUtil termUtil = TermUtil.getInstamce();
    }
}
