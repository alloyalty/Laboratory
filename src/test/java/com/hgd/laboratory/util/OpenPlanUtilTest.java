package com.hgd.laboratory.util;

import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.service.LabService;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class OpenPlanUtilTest {
      int weeksOfTerm = 22;


    @Test(expected = Exception.class)
    public void openPlan_含错内容_ThrowException1() throws Exception {
        //异常测试
        assertEquals("1",OpenPlanUtil.openPlan_OpenStatus(1,"1,3,18-a", weeksOfTerm));
    }
    @Test(expected = Exception.class)
    public void openPlan_含错内容_ThrowException2() throws Exception {
        //异常测试
        assertEquals("1",OpenPlanUtil.openPlan_OpenStatus(1,"1,a,11", weeksOfTerm));
    }


    //-1,0,23,24 皆异常
    @Test(expected = Exception.class)
    public void validWeek_周次为0_ThrowException() throws Exception {
        OpenPlanUtil.validWeek(0, weeksOfTerm);
    }

    @Test(expected = Exception.class)
    public void validWeek_周次为负1_ThrowException()  throws Exception {
        OpenPlanUtil.validWeek(-1, weeksOfTerm);
    }
    @Test(expected = Exception.class)
    public void validWeek_大于最大周次22_ThrowException()  throws Exception{
        OpenPlanUtil.validWeek(23, weeksOfTerm);
    }
    @Test(expected = Exception.class)
    public void validWeek_大于最大周次22_ThrowException1()  throws Exception{
        OpenPlanUtil.validWeek(24, weeksOfTerm);
    }

    @Test
    public void validWeek_第1周_ReturnTrue()  throws Exception{
        boolean actual =OpenPlanUtil.validWeek(1, weeksOfTerm);
        assertTrue(actual);

    }
    @Test
    public void validWeek_第22周_ReturnTrue()  throws Exception{
        boolean actual =OpenPlanUtil.validWeek(22, weeksOfTerm);
        assertTrue(actual);

    }
    @Test
    public void validWeek_第21周_ReturnTrue()  throws Exception{
        boolean actual =OpenPlanUtil.validWeek(21, weeksOfTerm);
        assertTrue(actual);
    }


}
