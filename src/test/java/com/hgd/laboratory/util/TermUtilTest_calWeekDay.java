package com.hgd.laboratory.util;

import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TermUtilTest_calWeekDay {
    /* 一般性测试，不使用DB */

    /*
    2019.11.11 ~2019.11.17 返回1，2，3，4，5，6，7
     */

    @Parameters(name = "{index}: openStatus({1},\"{2}\")={0}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {"2019-11-11", 1},
                {"2019-11-12", 2},
                {"2019-11-13", 3},
                {"2019-11-17", 7},
        });


    }

    private String dateStr;
    private int expect;

    public TermUtilTest_calWeekDay(String dateStr, int expect) {
        this.dateStr = dateStr;
        this.expect = expect;
    }

    @Test
    public void calWeekDay() throws ParseException {

        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);

        int weekDay = TermUtil.calWeekDay(date);

        assertEquals(expect, weekDay);

    }

}