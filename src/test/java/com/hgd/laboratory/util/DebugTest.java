package com.hgd.laboratory.util;

import com.hgd.laboratory.po.FaultRepair;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class DebugTest {

    //测试获取bean的属性名集合
    @Test
    public void getEntityPropNames() {
        FaultRepair fm = new FaultRepair();
        List<String> list = Debug.getEntityPropNames(fm);

        System.out.println(list);
        assertEquals(8,list.size());
    }



}