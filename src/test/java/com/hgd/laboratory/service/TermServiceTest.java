package com.hgd.laboratory.service;

import com.hgd.laboratory.po.Term;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class TermServiceTest {
    Term  term = Term.termBuilder("2030-2031",1,18,"2030-09-02");
    Term  newTerm = Term.termBuilder("2030-2031",2,22,"2031-03-12");

    @Autowired
    private TermService termService;
    @Autowired
    private SessionFactory sessionFactory;

    public TermServiceTest() throws ParseException {
    }

    @Before
    public void setup() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Term").executeUpdate();

    }

    //初始化
    //1在学期处于关闭的条件下，可以按指定参数初始化学期
    //2在学期处于未关闭的条件下，按指定参数初始化学期失败，返回false，且系统当前学期不变

    //关闭学期
    //1在学期处于关闭的条件下，关闭学期无任何动作。数据中当前学期不变。
    //2在学期处于未关闭的条件下，按指定参数关闭学期，无返回值。数据库中找不到正在工作的学期

    @Test
    @Transactional
    public void init_学期关闭条件下() {
        //执行之前学期数据已经删除
        boolean result = termService.init(term.getYear(), term.getTerm(), term.getWeeksOfTerm(), term.getDayOfFirstWeekMonday());

        Session session = sessionFactory.getCurrentSession();
        Term act = (Term)session.createQuery("from Term where year=:year and term=:term")
                .setParameter("year", term.getYear())
                .setParameter("term", term.getTerm())
                .uniqueResult();
        assertEquals(1,(int)act.getStatus());
        assertTrue(result);

    }

    @Test
    @Transactional
    public void init_学期未关闭条件下() {
        boolean result0 = termService.init(term.getYear(), term.getTerm(), term.getWeeksOfTerm(), term.getDayOfFirstWeekMonday());
        boolean result = termService.init(newTerm.getYear(), newTerm.getTerm(), newTerm.getWeeksOfTerm(), newTerm.getDayOfFirstWeekMonday());

        Session session = sessionFactory.getCurrentSession();
        Term act = (Term)session.createQuery("from Term where year=:year and term=:term")
                .setParameter("year", term.getYear())
                .setParameter("term", term.getTerm())
                .uniqueResult();
        assertEquals(1,(int)act.getStatus());
        assertFalse(result);

    }

    @Test
    @Transactional
    public void closure_学期未关闭条件下_正常关闭() {
        boolean result0 = termService.init(term.getYear(), term.getTerm(), term.getWeeksOfTerm(), term.getDayOfFirstWeekMonday());

        termService.closure();

        Session session = sessionFactory.getCurrentSession();
        Term act = (Term)session.createQuery("from Term where year=:year and term=:term")
                .setParameter("year", term.getYear())
                .setParameter("term", term.getTerm())
                .uniqueResult();
        assertTrue(result0);
        assertEquals(0,(int)act.getStatus());


    }

    @Test
    @Transactional
    public void closure_学期已经关闭条件下_还处于关闭状态() {
        boolean result0 = termService.init(term.getYear(), term.getTerm(), term.getWeeksOfTerm(), term.getDayOfFirstWeekMonday());
        termService.closure();

        termService.closure();

        Session session = sessionFactory.getCurrentSession();
        Term act = (Term)session.createQuery("from Term where year=:year and term=:term")
                .setParameter("year", term.getYear())
                .setParameter("term", term.getTerm())
                .uniqueResult();
        assertTrue(result0);
        assertEquals(0,(int)act.getStatus());


    }

    @Test
    @Transactional
    public void termIsRunning() throws ParseException {
        Term term = new Term("2020-2021",1,22,"2020-02-17");
        term.setStatus(1);
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Term").executeUpdate();
        session.save(term);

        assertTrue(this.termService.termIsRunning());


    }
}