package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.util.ApplyUtil;
import org.junit.Test;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.*;

public class AnAppointmentServiceImplTest {

    @Test
    public void query() {
    }



    @Test
    public void caldayOfWeek() throws ParseException {
        AnAppointmentServiceImpl anAppointmentService = new AnAppointmentServiceImpl();
        {//星期1
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 1,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }

        {//星期2
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,1 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 2,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }
        {//星期3
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,2 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 3,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }

        {//星期4
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,3 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 4,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }

        {//星期5
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,4 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 5,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }


        {//星期六
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,5 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 6,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }

        {//星期日
            SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
            java.util.Date date = ft.parse( "2019-08-26" );

            Calendar calendar = Calendar.getInstance();
            calendar.setTime( date );
            calendar.set(Calendar.MILLISECOND,0);
            calendar.add( Calendar.DAY_OF_MONTH,6 );

            // 星期1，返回1，....,星期日 返回7
            assertEquals( 7,ApplyUtil.calDayOfWeek( new Date( calendar.getTime().getTime() ) ) );
        }





    }

    @Test
    public void initialAvailApplyCode() throws ParseException {
        AnAppointmentServiceImpl anAppointmentService = new AnAppointmentServiceImpl();

        SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
        java.util.Date date = ft.parse( "2019-08-27" );

        String  s = ApplyUtil.initialAvailApplyCode( "6312",new Date( date.getTime() ),2 );
        assertTrue( s.equals( "20190827#6312#2001" ) );
    }

    @Test
    public void addOne() {

        AnAppointmentServiceImpl anAppointmentService = new AnAppointmentServiceImpl();
        {
            String s = anAppointmentService.addOne(     "20180830#6312#3002");
            assertTrue( s.equals("20180830#6312#3003"   ) );
        }

        {
            String s = anAppointmentService.addOne(     "20180830#6312#0002");
            assertTrue( s.equals("20180830#6312#0003"   ) );
        }

        {
            String s = anAppointmentService.addOne(     "20180830#6312#0000");
            assertTrue( s.equals("20180830#6312#0001"   ) );
        }
    }

}