package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class UserServiceImpl_ATest {

    @Autowired
    private UserService userService;

    @Autowired
    private SessionFactory sessionFactory;

    private String stu = "stuDuty";
    private String  stupass="123456";
    private String  tea="teaLab";
    private String  teapass = "123456";
    private String  erritem="000000";

    @Before
    @Transactional
    public void setup() {
        //建立学生stuDuty、老师teaLab
        //建立值班人员stuDuty,实验室 管理人员 teaLab
        Student student = new Student(stu,"STUTEST","TTT","rj","1601",0,0,0);
        DutyStudent dutyStudent = new DutyStudent(student);
        Teacher teacher = new Teacher(tea,"TEATEST","XX");
        LabTeacher labTeacher = new LabTeacher(teacher);

        Session session =sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();
        session.createQuery("delete from UserRole").executeUpdate();
        session.createQuery("delete from User").executeUpdate();
        session.createQuery("delete from Student ").executeUpdate();
        session.createQuery("delete from Teacher ").executeUpdate();

        session.save(student);
        session.save(dutyStudent);
        session.save(teacher);
        session.save(labTeacher);
        User ustu = session.get(User.class,student.getsId());
        User utea = session.get(User.class,teacher.gettId());


    }

    @Test
    @Transactional
    public void checkUser_stuDuty是位学生_返回学生id和角色学生() {
        User user = userService.checkUser(stu,stupass,"student");
        assertTrue( user!=null && user.getId().equals(stu) && user.getRole().equals("student"));
    }
    @Test
    @Transactional
    public void checkUser_stuDuty是位学生_返回学生id和角色值班员() {
        User user = userService.checkUser(stu,stupass,"duty");
        System.out.println(user);
        assertTrue( user!=null && user.getId().equals(stu) && user.getRole().equals("duty"));
    }
    @Test
    @Transactional
    public void checkUser_stuLab是位老师_返回老师id和角色老师() {
        User user = userService.checkUser(tea,teapass,"teacher");
        assertTrue( user!=null && user.getId().equals(tea) && user.getRole().equals("teacher"));
    }
    @Test
    @Transactional
    public void checkUser_stuLab是位实验室管理员_返回实验室管理员id和角色实验室管理员() {
        User user = userService.checkUser(tea,teapass,"labManager");
        assertTrue( user!=null && user.getId().equals(tea) && user.getRole().equals("labManager"));
    }
    @Test
    @Transactional
    public void checkUser_stuDuty是位学生但id或密码或角色有一个不对_返回null() {
        assertNull( userService.checkUser(stu,stupass,erritem));
        assertNull( userService.checkUser(stu,erritem,"student"));
        assertNull( userService.checkUser(erritem,stupass,"student"));
        assertNull( userService.checkUser(stu,erritem,"duty"));

        ;
    }
    @Test
    @Transactional
    public void checkUser_stuLab是位老师但id或密码或角色有一个不对_返回null() {
        assertNull( userService.checkUser(tea,teapass,erritem));
        assertNull( userService.checkUser(tea,erritem,"teacher"));
        assertNull( userService.checkUser(erritem,teapass,"teacher"));
        assertNull( userService.checkUser(tea,erritem,"labManager"));
    }
}