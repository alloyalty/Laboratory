package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.ImportDataService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class ImportDataServiceImpl_EntityTest {
    @Autowired
    private ImportDataService importDataService;

    @Autowired
    private SessionFactory sessionFactory;

    public ImportDataServiceImpl_EntityTest() {
        System.out.println("测试类实例化");
    }
    Student student = new Student("201616040123","黄鹏","信息科学与工程学院","软件专业","1601",0,0,3 );
    Student student1 = new Student("201616040123","黄鹏","信息科学与工程学院","软件专业","1602",0,0,3 );

    Teacher teacher =  new Teacher("2004664","王春清","软件工程");
    Teacher teacher1 =  new Teacher("2004664","王春清1","软件工程");

    Laboratory laboratory = new Laboratory("6311","莲6301","莲花街",0,0,1,"九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色" );
    Laboratory laboratory1 = new Laboratory("6311","莲6301","莲花街1",0,0,1,"九代英特尔酷睿i9-9900K/Windows 10 家庭中文版/16G/2T+512G SSD/GeForce RTX 2080-8GB/黑色" );

    Facility facility = new Facility("FAULT1101","台式计算机",0,"config6311","6311");
    Facility facility1 = new Facility("FAULT1101","台式计算机1",0,"config6311","6311");

    SoftwareDetails softwareDetails = new SoftwareDetails();
    SoftwareConfig softwareConfig = new SoftwareConfig("软件工程实验软件","主要用于软件工程专业");

    @Before
    public void setup() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();
        session.createQuery("delete from UserRole ").executeUpdate();
        session.createQuery("delete from User ").executeUpdate();
        session.createQuery("delete from Student").executeUpdate();
        session.createQuery("delete from Teacher").executeUpdate();
        session.createQuery("delete from Laboratory").executeUpdate();
        session.createQuery("delete from Facility").executeUpdate();
        session.createQuery("delete from SoftwareDetails").executeUpdate();
        session.createQuery("delete from SoftwareConfig").executeUpdate();

    }

    //对于每一个需要导入的实体，都应删去实体中有的记录
    //(1)测试导入一个新实体成功，（2）导入一个已经存在的实体，

    @Test
    public void importSoftwareDetails() {
    }

    @Test
    public void importSoftwareConfig() {
    }

    @Test
    public void importFacility_导入新设备() {
        this.importDataService.importFacility(facility);

        Session session = sessionFactory.getCurrentSession();
        Facility act = session.get(Facility.class,facility.getfId());
        assertEquals(facility,act);
    }

    @Test
    public void importFacility_导入已经存在的设备() {
        this.importDataService.importFacility(facility);
        this.importDataService.importFacility(facility1);

        Session session = sessionFactory.getCurrentSession();
        Facility act = session.get(Facility.class,facility1.getfId());
        assertEquals(facility1,act);
    }

    @Test
    public void importStudent_导入新学生() {
        this.importDataService.importStudent(student);

        Session session = sessionFactory.getCurrentSession();
        Student act = session.get(Student.class,student.getsId());
        assertEquals(student,act);

    }

    @Test
    public void importStudent_import导入已经存在的学生() {
        this.importDataService.importStudent(student);
        this.importDataService.importStudent(student1);

        Session session = sessionFactory.getCurrentSession();
        Student act = session.get(Student.class,student1.getsId());
        assertEquals(student1,act);

    }

    @Test
    public void importTeacher_导入新教师() {
        this.importDataService.importTeacher(teacher);

        Session session = sessionFactory.getCurrentSession();
        Teacher act = session.get(Teacher.class,teacher.gettId());
        assertEquals(teacher,act);

    }
    @Test
    public void importTeacher_导入已经存在的教师() {
        this.importDataService.importTeacher(teacher);
        this.importDataService.importTeacher(teacher1);

        Session session = sessionFactory.getCurrentSession();
        Teacher act = session.get(Teacher.class,teacher1.gettId());
        assertEquals(teacher1,act);

    }

    @Test
    public void importLaboratory_导入新实验室() {
        this.importDataService.importLaboratory(laboratory);

        Session session = sessionFactory.getCurrentSession();
        Laboratory act = session.get(Laboratory.class,laboratory.getLabId());
        assertEquals(laboratory,act);

    }
    @Test
    public void importLaboratory_导入已经存在的实验室() {
        this.importDataService.importLaboratory(laboratory);
        this.importDataService.importLaboratory(laboratory1);

        Session session = sessionFactory.getCurrentSession();
        Laboratory act = session.get(Laboratory.class,laboratory1.getLabId());
        assertEquals(laboratory1,act);
    }

}