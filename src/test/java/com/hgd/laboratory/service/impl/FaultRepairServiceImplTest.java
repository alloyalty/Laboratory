package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.FaultRepair;
import com.hgd.laboratory.service.FaultRepairService;
import com.hgd.laboratory.util.Page;
import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class FaultRepairServiceImplTest {

    @Autowired
    private FaultRepairService faultRepairService;

    public FaultRepairService getFaultRepairService() {
        return faultRepairService;
    }

    public void setFaultRepairService(FaultRepairService faultRepairService) {
        this.faultRepairService = faultRepairService;
    }

    @Test
    @Transactional
    public void fillFault() {
                /*
        新建一个对象 faultRepair;执行申报；
        依faultRepair.id从数据库中读取对象faultRepair1，
        验证faultRepair1 == faultRepair2
        */
        Session session = this.faultRepairService.getCurrentSession();

        FaultRepair fr = new FaultRepair("6320", "FACILITY001", "启动异常。");
        FaultRepair fra = new FaultRepair("6320", "FACILITY001", "启动异常。");
        this.faultRepairService.fillFault(fr);

        FaultRepair fr1 = session.get(FaultRepair.class, fr.getId());

        fra.setId(fr1.getId());
        boolean match = true;
        match = match && fr1.getLid().equals(fra.getLid());
        match = match && fr1.getFid().equals(fra.getFid());
        match = match && fr1.getFaultDFesp().equals(fra.getFaultDFesp());

        assertTrue(match);
    }

    @Test
    @Transactional
    public void updateMaintance() {
        FaultRepair fr = new FaultRepair("6320", "FACILITY002", "启动异常。");
        this.faultRepairService.fillFault(fr);

        String msg = "故障已经修复。TEST DATA";
        this.faultRepairService.updateMaintance(fr.getId(), msg);

        FaultRepair fr1 = new FaultRepair("6320", "FACILITY002", "启动异常。");
        fr1.setFstate(FaultRepairService.RECOVERY);
        fr1.setMaintenceRecs(msg);
        fr1.setId(fr.getId());
        assertTrue(fr.equalsInfo(fr1));
    }

    @Test
    @Transactional
    public void queryStatusFaultList() {
        //应先删除所有记录
        Session session = this.faultRepairService.getCurrentSession();

        session.createQuery("Delete from FaultRepair").executeUpdate();
        FaultRepair fr1 = new FaultRepair("6320", "FACILITY001", "启动异常1。");
        FaultRepair fr2 = new FaultRepair("6320", "FACILITY002", "启动异常2。");
        FaultRepair fr3 = new FaultRepair("6320", "FACILITY003", "启动异常3。");

        this.faultRepairService.fillFault(fr1);
        this.faultRepairService.fillFault(fr2);
        this.faultRepairService.fillFault(fr3);
        this.faultRepairService.updateMaintance(fr3.getId(), "故障排除TEST");

        PaginationData<FaultRepair> paginationData = this.faultRepairService.queryStatusFaultList(new Page(20));
        assertEquals(2, paginationData.getDataList().size());
    }

    @Test
    public void queryStatusFaultListByLid() {
        //查指定lid故障记录列表
        //应先删除所有记录
        Session session = this.faultRepairService.getCurrentSession();

        session.createQuery("Delete from FaultRepair").executeUpdate();
        FaultRepair fr1 = new FaultRepair("6320", "FACILITY001", "启动异常1。");
        FaultRepair fr2 = new FaultRepair("6320", "FACILITY002", "启动异常2。");
        FaultRepair fr3 = new FaultRepair("6320", "FACILITY003", "启动异常3。");
        FaultRepair fr4 = new FaultRepair("6321", "FACILITY001", "启动异常1。");
        FaultRepair fr5 = new FaultRepair("6321", "FACILITY002", "启动异常2。");
        FaultRepair fr6 = new FaultRepair("6323", "FACILITY003", "启动异常3。");
        this.faultRepairService.fillFault(fr1);
        this.faultRepairService.fillFault(fr2);
        this.faultRepairService.fillFault(fr3);
        this.faultRepairService.fillFault(fr4);
        this.faultRepairService.fillFault(fr5);
        this.faultRepairService.fillFault(fr6);
        this.faultRepairService.updateMaintance(fr3.getId(), "故障排除TEST");
        this.faultRepairService.updateMaintance(fr5.getId(), "故障排除TEST");

        List<FaultRepair> frList = this.faultRepairService.queryStatusFaultListByLid("6320");
        assertEquals(2, frList.size());
    }
}