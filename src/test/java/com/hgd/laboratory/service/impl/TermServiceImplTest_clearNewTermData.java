package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.service.TermService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class TermServiceImplTest_clearNewTermData {


    @Autowired
    private TermService termService;



    @Test
    public void clearNewTermData() {
        boolean succ = termService.clearNewTermData("2018-2019-1");
        assertTrue(succ);
    }
}