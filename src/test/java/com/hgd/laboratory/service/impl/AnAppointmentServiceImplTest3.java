package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.exception.InvalidDateInTerm;
import com.hgd.laboratory.po.OpenRes;
import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.util.TermUtil;
import com.hgd.laboratory.util.TestUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class AnAppointmentServiceImplTest3 {
    @Autowired
    private AnAppointmentService anAppointmentService;



    @Test
    public void queryDateInfo () throws InvalidDateInTerm, ParseException {
        java.sql.Date theDate = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2019-08-26").getTime());
        Term stubTerm = new Term("2018-2019", 1, 21, theDate);
        TermUtil.reset();
        TermUtil.setCustomTerm(stubTerm);
        testInstance("2019-08-26", "第1周，星期1");
        testInstance("2019-09-01", "第1周，星期7");
        testInstance("2019-09-09", "第3周，星期1");
        testInstance("2019-09-15", "第3周，星期7");

    }


    @Test
    public void queryDateInfo_InDB () throws InvalidDateInTerm, ParseException {
        java.sql.Date theDate = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2020-01-13").getTime());
        Term stubTerm = new Term("2019-2020", 2, 21, theDate);
        TermUtil.reset();
        TermUtil.setCustomTerm(stubTerm);
        testInstance("2020-01-13", "第1周，星期1");

    }
    public void testInstance(String data, String expected) throws ParseException, InvalidDateInTerm {
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse(data);
        String s = anAppointmentService.queryDateInfo(date);
        assertEquals(expected,s);
    }

    public AnAppointmentService getAnAppointmentService() {
        return anAppointmentService;
    }

    public void setAnAppointmentService(AnAppointmentService anAppointmentService) {
        this.anAppointmentService = anAppointmentService;
    }
}