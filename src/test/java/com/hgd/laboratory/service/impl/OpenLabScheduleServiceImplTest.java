package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.OpenLabScheduleTuple;
import com.hgd.laboratory.po.OpenLabSchedule;
import com.hgd.laboratory.po.OpenLabScheduleDetail;
import com.hgd.laboratory.service.FaultRepairService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.OpenLabScheduleService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class OpenLabScheduleServiceImplTest {

    @Autowired
    @Getter     @Setter
    private OpenLabScheduleService openLabScheduleService;
    @Autowired
    private SessionFactory sessionFactory;



    @Test
    @Transactional
    public void findFirstOpenLabSchedule() {
        /*
        1.先删去全部记录
        2.插入二条记录，其中第二条lid（TLIB6000）<第一条lid(TLIB6001)
        3.执行
        4.与第二条记录相比，应正确
        */

        Session session = sessionFactory.getCurrentSession();
        String hql = " delete  from OpenLabSchedule";
        session.createQuery( hql ).executeUpdate();


        OpenLabSchedule openLabSchedule = OpenLabSchedule.mockInstance();
        session.save( openLabSchedule );
        Map<String, String> map = OpenLabScheduleDetail.mockScheduleDetailMap();
        Long tempLong = 100000000L;
        saveDetail( session,openLabSchedule,map,tempLong );


        OpenLabSchedule openLabSchedule1 = OpenLabSchedule.mockInstance1();
        session.save( openLabSchedule1 );
        Map<String, String> map1 = OpenLabScheduleDetail.mockScheduleDetailMap1();
        Long tempLong1 = 200000000L;
        saveDetail( session,openLabSchedule1,map1,tempLong1 );

        OpenLabScheduleTuple openLabScheduleTuple = this.openLabScheduleService.findFirstOpenLabSchedule( openLabSchedule1.getTerm() );

        //验证
        assertEquals( openLabSchedule1,openLabScheduleTuple.getOpenLabSchedule() );
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertEquals( map1.get( key ),openLabScheduleTuple.getOpenLabScheduleDetailMap().get( key ) );

            }

        }
    }

    private void saveDetail(Session session,OpenLabSchedule openLabSchedule,Map<String, String> map,Long tempLong) {

        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                int timeSlot = i;
                int dayOfWeek = j;

                OpenLabScheduleDetail openLabScheduleDetail = new OpenLabScheduleDetail( tempLong,timeSlot,dayOfWeek,map.get( key ),openLabSchedule );
                session.save( openLabScheduleDetail );
                tempLong++;

            }

        }

        for (String key : map.keySet()) {

        }
    }

    @Test
    @Transactional
    public void findFirstOpenLabSchedule1() {
        /*
        1.先删去全部记录
        2.插入一条记录
        3.执行
        4.与插入记录相比，应正确
        */
        Session session = sessionFactory.getCurrentSession();

        String hql = " delete  from OpenLabSchedule";
        session.createQuery( hql ).executeUpdate();


        OpenLabSchedule openLabSchedule = OpenLabSchedule.mockInstance();
        session.save( openLabSchedule );
        Map<String, String> map = OpenLabScheduleDetail.mockScheduleDetailMap();
        Long tempLong = 100000000L;
        saveDetail( session,openLabSchedule,map,tempLong );


        OpenLabScheduleTuple openLabScheduleTuple = this.openLabScheduleService.findOpenLabSchedule( openLabSchedule.getTerm(),openLabSchedule.getLabId() );

        //验证
        assertEquals( openLabSchedule,openLabScheduleTuple.getOpenLabSchedule() );
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertEquals( map.get( key ),openLabScheduleTuple.getOpenLabScheduleDetailMap().get( key ) );

            }

        }
    }
}