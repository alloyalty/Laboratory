package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.StudentExpService;
import com.hgd.laboratory.util.DateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class StudentExpServiceImplTest {

    @Autowired
    StudentExpService studentExpService;
    @Autowired
    SessionFactory sessionFactory;


    private void readyData() throws ParseException {
        //删去所有数据
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();

        List<ApplyForLab> applyForLabList = this.prepareData();
        //构造数据
        for (ApplyForLab applyForLab : applyForLabList) {
            session.save(applyForLab);
        }
    }

    @Test
    public void queryUniqueApply() throws ParseException {
        readyData();
        Timestamp today = getTimestamp("2019-10-06");
        Long applyNo = studentExpService.queryUniqueApply("student", today, 6, "6312", "2018-2019-1").getApplyNumber();
        //assertEquals(1124,applyNo.intValue());
        assertNotNull(applyNo);
        applyNo = studentExpService.queryUniqueApply("student", today, 5, "6312", "2018-2019-1").getApplyNumber();
//        assertEquals(1124,applyNo.intValue());
        assertNotNull(applyNo);
    }

    public Timestamp getTimestamp(String sDate) throws ParseException {
        java.util.Date uDate = new SimpleDateFormat("yyyy-MM-dd").parse(sDate);
        return new Timestamp(DateUtil.stdDate(uDate).getTime());
    }

    @Test
    public void queryUniqueApply2() throws ParseException {
        readyData();
        Timestamp today = getTimestamp("2019-10-06");
        Long applyNo = studentExpService.queryUniqueApply("student", today, 5, "6312", "2018-2019-1").getApplyNumber();
        //assertEquals(1124,applyNo.intValue());
        assertNotNull(applyNo);
    }

    public void queryUniqueApply3() throws ParseException {
        readyData();
        java.sql.Timestamp today = new java.sql.Timestamp(DateUtil.stdDate(new java.util.Date()).getTime());
        Long applyNo = studentExpService.queryUniqueApply("student", today, 1, "6312", "2018-2019-1").getApplyNumber();
        //assertEquals(1126,applyNo.intValue());
        assertNotNull(applyNo);
    }

    public void queryUniqueApply4() throws ParseException {
        readyData();
        java.sql.Timestamp today = new java.sql.Timestamp(DateUtil.stdDate(new java.util.Date()).getTime());
        Long applyNo = studentExpService.queryUniqueApply("student", today, 2, "6312", "2018-2019-1").getApplyNumber();
        assertNull(applyNo);

    }

    public void queryUniqueApply5() throws ParseException {
        readyData();
        java.sql.Timestamp today = new java.sql.Timestamp(DateUtil.stdDate(new java.util.Date()).getTime());
        Long applyNo = studentExpService.queryUniqueApply("student", today, 7, "6312", "2018-2019-1").getApplyNumber();
        assertNull(applyNo);

    }

    private List<ApplyForLab> prepareData() throws ParseException {
        List<ApplyForLab> list = new ArrayList<>();
        ApplyForLab temp = new ApplyForLab();
        list.add(temp.genApplyForLab(1123L, "2019-10-06 00:00:00.0", "student", "TEST2", "6312", "", "b1", "a1", "c1", "Dev-C,C++", 7, 7, 1, "", "null", "2018-2019-1", "20191006#6312#7001"));
        list.add(temp.genApplyForLab(1124L, "2019-10-06 00:00:00.0", "student", "TEST2", "6312", "", "d3", "d3", "dd", "interllij idea 2019", 5, 6, 2, "", "null", "2018-2019-1", "20191006#6312#6001"));
        list.add(temp.genApplyForLab(1125L, "2019-10-06 00:00:00.0", "student", "TEST2", "6312", "", "bb", "aa", "de", "Dev-C,C++###eclipse for web", 2, 2, 1, "", "null", "2018-2019-1", "20191006#6312#2001"));
        list.add(temp.genApplyForLab(1126L, "2019-10-06 00:00:00.0", "student", "TEST2", "6312", "", "AA", "A", "asa", "", 1, 1, 2, "", "null", "2018-2019-1", "20191006#6312#1001"));
        return list;
    }


    @Test
    public void queryTheMachineId() {
    }

    @Test
    public void confirmApply() throws ParseException {
        readyData();
        Session session = sessionFactory.getCurrentSession();
        List<ApplyForLab> applyForLabList = session.createQuery("from ApplyForLab ").list();
        long applyNo = applyForLabList.get(0).getApplyNumber();

        studentExpService.confirmApply("FACITEST004", applyNo);

        ApplyForLab applyForLab = sessionFactory.getCurrentSession().get(ApplyForLab.class, applyNo);
        assertNotNull(applyForLab);
        assertEquals(3, (int) applyForLab.getState());
        assertEquals("FACITEST004", applyForLab.getFaciId());

    }

    @Test
    public void queryConfirmedApply() {
    }


    public StudentExpService getStudentExpService() {
        return studentExpService;
    }

    public void setStudentExpService(StudentExpService studentExpService) {
        this.studentExpService = studentExpService;
    }
}