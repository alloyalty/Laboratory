package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.LabOpenPlan;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class LabOpenPlanServiceImplTest {
    @Setter
    @Getter
    @Autowired
    private LabOpenPlanService labOpenPlanService;

    @Autowired
    private SessionFactory sessionFactory;

    LabOpenPlan lop1 = new LabOpenPlan(1, "2018-2019-1", "6320", 0, "实验室开放计划");
    LabOpenPlan lop2 = new LabOpenPlan(2, "2018-2019-1", "6321", 1, "实验室开放计划");
    LabOpenPlan lop3 = new LabOpenPlan(3, "2018-2019-1", "6318", 0, "实验室开放计划");
    List<LabOpenPlan> lopList = new ArrayList<>();

    @Before
    @Transactional
    public void setup() {
        //测试之前，删除LabOpenPlan全部数据
        final Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from LabOpenPlan").executeUpdate();

    }

    @Test
    @Transactional
    public void queryAllLabOpenPlansTest() {
        //插入三条新LabOpenPlan记录进去
        //查询返回三条记录
        //执行功能返lkd三条记录为真。
        lopList.clear();
        lopList.addAll(Arrays.asList(lop1, lop2, lop3));
        for (Iterator<LabOpenPlan> iterator = lopList.iterator(); iterator.hasNext(); ) {
            LabOpenPlan next = iterator.next();
            sessionFactory.getCurrentSession().save(next);
        }

        List<LabOpenPlan> qlist = this.labOpenPlanService.queryAllLabOpenPlans("2018-2019-1");
        assertEquals(lopList.size(), qlist.size());
    }


    @Test
    @Transactional
    @SuppressWarnings("unchecked")
    public void deleteTest() throws OpenPlanExpressinException {
        //新插入一个开放计划 LabOpenPlan lop
        //依据lop.id, 删除之
        //验证已经删除

        LabOpenPlan lop = mockLabOpenPlan("2018-2019-2", "64444");
        LabOpenPlanTuple labOpenPlanTuple;
        Map<String, String> map = mockLabOpenPlanDetails(45333L);
        this.labOpenPlanService.saveUpdate(lop, map, LabService.SCRATCH, LabService.SAVE);

        //执行
        this.labOpenPlanService.delete(lop.getOpenPlanId());

        //验证
        Session session = sessionFactory.getCurrentSession();
        List<LabOpenPlan> list = session.createQuery("from LabOpenPlan where openPlanId=:openPlanId")
                .setParameter("openPlanId",lop.getOpenPlanId())
                .list();
        assertTrue(list==null || list.size()==0);

    }


    @Test
    @Transactional
    public void findLabOpenPlanAndSaveUpdae_Test() throws OpenPlanDataIncompleteException, OpenPlanExpressinException {
        //新插入一个开放计划 LabOpenPlan lop
        //依据lop.id读出一个lopRead
        //两者应完全一致

        LabOpenPlan lop = mockLabOpenPlan("2018-2019-1", "6333");
        {
            LabOpenPlanTuple labOpenPlanTuple;
            Map<String, String> map = mockLabOpenPlanDetails(24323L);
            this.labOpenPlanService.saveUpdate(lop, map, LabService.SCRATCH, LabService.SAVE);

            //执行
            labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan(lop.getOpenPlanId());

            //验证
            assertEquals(lop.getOpenPlanId(), labOpenPlanTuple.getLabOpenPlan().getOpenPlanId());
            assertEquals((int) lop.getStatus(), LabService.SCRATCH);//状态验证
            Map<String, String> map1 = labOpenPlanTuple.getLabOpenPlanDetailMap();
            assertLabOpenPlanDetailsEqulas(map, map1);
        }

        {
            LabOpenPlanTuple labOpenPlanTuple;
            lop.setTerm("2018-2019-2");
            Map<String, String> map = mockLabOpenPlanDetails(532323L);

            this.labOpenPlanService.saveUpdate(lop, map, LabService.PUBLISH, LabService.UPDATE);
            labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan(lop.getOpenPlanId());
            assertEquals(lop.getOpenPlanId(), labOpenPlanTuple.getLabOpenPlan().getOpenPlanId());
            assertEquals((int) lop.getStatus(), LabService.PUBLISH);//状态验证
            Map<String, String> map1 = labOpenPlanTuple.getLabOpenPlanDetailMap();
            assertLabOpenPlanDetailsEqulas(map, map1);
        }

    }

    public void assertLabOpenPlanDetailsEqulas(Map<String, String> map, Map<String, String> map1) {
        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertEquals(map.get(key), map1.get(key));
            }
        }
    }

    @Test
    @Transactional
    public void createBlankLabOpenPlan() throws OpenPlanDataIncompleteException {
        //首先创建空的，
        //读出
        //检查
        this.labOpenPlanService.createBlankLabOpenPlan("2018-2019-1", "TESTXXX");

        //验证
        LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan("2018-2019-1", "TESTXXX");
        LabOpenPlan lop = labOpenPlanTuple.getLabOpenPlan();
        Map<String, String> map = labOpenPlanTuple.getLabOpenPlanDetailMap();

        assertEquals("2018-2019-1", lop.getTerm());
        assertEquals("TESTXXX", lop.getLabId());
        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertTrue("".equals(map.get(key)));
            }
        }
    }

    public static LabOpenPlan mockLabOpenPlan(String term, String labId) {
        LabOpenPlan lop = new LabOpenPlan(null, term, labId, 0, "实验室开放计划");
        return lop;
    }

    public static Map<String, String> mockLabOpenPlanDetails(final long seed) {
        java.util.Random rnd = new Random();
        rnd.setSeed(seed);
        Map<String, String> lopDetailMap = new HashMap<>();
        for (int i = 1; i < LabService.TIMESLOTNUM + 1; i++) {
            for (int week = 1; week <= 7; week++) {
                String key = "t" + i + "w" + week;

               int w1 = rnd.nextInt(15)+1;
               int w2 = rnd.nextInt(15)+1;
                String value = "" + w1+"-"+w2;
                lopDetailMap.put(key, value);
            }

        }
        return lopDetailMap;

    }

}