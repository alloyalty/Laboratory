package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.ImportResult;
import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.ImportDataService;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class ImportDataServiceImplTest {
    @Autowired
    private ImportDataService importDataService;

    @Autowired
    private SessionFactory sessionFactory;


    @Before
    @Transactional
    public void setup() {
        //导入过程会自动更新系统的已经有的数据
        //进行测试
        //测试完成后，回滚


    }

    @Test
    @Transactional
    public void importDataServiceImpl_学生信息导入() throws IOException, URISyntaxException {
        final String name = "/学生信息导入表.xlsx";

        ImportResult importResult = getImportResult(name, Student.class);

        assertEquals(5,importResult.getImportedDataRows());
        assertEquals(0, importResult.getUnImportDatas().size());



    }

    public ImportResult getImportResult(String name, Class tClass) throws URISyntaxException, IOException {
        final URL url = ImportDataServiceImplTest.class.getResource(name);
        System.out.println(url.getFile());
        File file = new File(url.toURI());
        String fileName = url.getFile();
        return importDataService.importData(file, tClass, fileName.substring(fileName.lastIndexOf('.')));
    }


    @Test
    @Transactional
    public void importDataServiceImpl_教师信息导入() throws IOException, URISyntaxException {
        final String name = "/教师信息导入表.xlsx";

        ImportResult importResult = getImportResult(name, Teacher.class);

        assertEquals(5,importResult.getImportedDataRows());
        assertEquals(0, importResult.getUnImportDatas().size());

    }

    @Test
    @Transactional
    public void importDataServiceImpl_实验室信息导入() throws IOException, URISyntaxException {
        final String name = "/实验室信息导入表.xlsx";

        ImportResult importResult = getImportResult(name, Laboratory.class);

        assertEquals(3,importResult.getImportedDataRows());
        assertEquals(0, importResult.getUnImportDatas().size());
    }

    @Test
    @Transactional
    public void importDataServiceImpl_设备信息导入表() throws IOException, URISyntaxException {
        final String name = "/设备信息导入表.xlsx";

        ImportResult importResult = getImportResult(name, Facility.class);

        assertEquals(60,importResult.getImportedDataRows());
        assertEquals(0, importResult.getUnImportDatas().size());

    }
  @Test
  @Transactional
    public void importDataServiceImpl_软件主信息导入() throws IOException, URISyntaxException {
      final String name = "/软件主信息导入表.xlsx";

      ImportResult importResult = getImportResult(name, SoftwareConfig.class);

      assertEquals(2,importResult.getImportedDataRows());
      assertEquals(0, importResult.getUnImportDatas().size());
    }


    @Test
    @Transactional
    public void importDataServiceImpl_软件详细信息导入() throws IOException, URISyntaxException {
        final String name = "/软件详细信息导入表.xlsx";

        ImportResult importResult = getImportResult(name, SoftwareDetails.class);

        assertEquals(24,importResult.getImportedDataRows());
        assertEquals(0, importResult.getUnImportDatas().size());

    }


    @Test
    public void queryConfigId_已经存在配置名_returnInt7() {
        Integer  configId = this.importDataService.queryConfigId("软件工程实验软件");
        assertTrue(7==configId);
    }

@Test
    public void queryConfigId_已经存在配置名_returnInt8() {
        Integer  configId = this.importDataService.queryConfigId("计算机科学实验软件");
        assertTrue(8==configId);
    }

@Test
    public void queryConfigId_已经存在配置名_returnInt0() {
        Integer  configId = this.importDataService.queryConfigId("XXXX");
        assertTrue(0==configId);
    }


}