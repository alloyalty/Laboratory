package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.service.ReservationRulesService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class RRServiceTest {
    @Autowired
    ReservationRulesService reservationRulesService;
    @Test
    public void testRRBean() {
        assertNotNull(reservationRulesService);
    }
}
