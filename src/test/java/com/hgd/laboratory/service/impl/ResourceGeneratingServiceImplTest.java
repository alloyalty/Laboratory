package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.*;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.LaboratoryService;
import com.hgd.laboratory.service.ResourceGeneratingService;
import com.hgd.laboratory.util.TermUtil;
import com.hgd.laboratory.util.TermUtilTest_Base;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class ResourceGeneratingServiceImplTest {
    @Autowired
    @Getter
    @Setter
    SessionFactory sessionFactory;

    @Autowired
    @Getter
    @Setter
    @InjectMocks
    ResourceGeneratingService resourceGeneratingService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }


    @Test
    public void resGeneratingPer24Hours() {
    }

    @Test
    @Transactional
    public void calResTotalNum() {
        //删去Facilicy、Laboratory全部数据
        //增加一条 Laboratory记录
        //增加10条Facilicy记录，其中有二条状态为2（表示故障），应有8条记录正常

        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Facility").executeUpdate();
        session.createQuery("delete from Laboratory").executeUpdate();
        Laboratory laboratory = new Laboratory("6320", "Test", "123号", 0, 0, 7, "computer");
        Facility f1 = new Facility("f001", "A01", 0, "config6320", "6320");
        Facility f2 = new Facility("f002", "A02", 0, "config6320", "6320");
        Facility f3 = new Facility("f003", "A03", 0, "config6320", "6320");
        Facility f4 = new Facility("f004", "A04", 0, "config6320", "6320");
        Facility f5 = new Facility("f005", "A05", 0, "config6320", "6320");
        Facility f6 = new Facility("f006", "A06", 0, "config6320", "6320");
        Facility f7 = new Facility("f007", "A07", 0, "config6320", "6320");
        Facility f8 = new Facility("f008", "A08", 2, "config6320", "6320");
        Facility f9 = new Facility("f009", "A09", 2, "config6320", "6320");
        Facility f10 = new Facility("f0010", "A010", 3, "config6320", "6320");
        List<Facility> facilityList = new ArrayList<>();
        facilityList.addAll(Arrays.asList(f1, f2, f3, f4, f5, f6, f7, f8, f9, f10));
        session.save(laboratory);
        for (Facility f : facilityList) {
            session.save(f);
        }

        int act = this.resourceGeneratingService.calResTotalNum("6320",session);
        System.out.println(act);
        assertEquals(8,act);

    }

    @Test
    public void resGeneratingImmediate() {
    }

    @Test
    public void todayResIsReady_已经生成_ReturnTrue() throws ParseException {
        //todayResIsReady
        //删去全部记录
        //"20190827"新建记录
        //以日期20190827, 返回真
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ResourceGenerationDate").executeUpdate();
        ResourceGenerationDate resourceGenerationDate = new ResourceGenerationDate("20190827");
        session.save(resourceGenerationDate);

        Date date = new SimpleDateFormat("yyyyMMdd").parse("20190827");
        assertTrue(resourceGeneratingService.todayResGenTaskIsCompleted(date));


    }

    @Test
    public void todayResIsReady_没有生成_returnFalse() throws ParseException {
        //todayResIsReady
        //删去全部记录

        //以日期20190827, 返回假
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ResourceGenerationDate").executeUpdate();

        Date date = new SimpleDateFormat("yyyyMMdd").parse("20190827");
        assertFalse(resourceGeneratingService.todayResGenTaskIsCompleted(date));
    }

    @Test
    @Transactional
    public void generatingResForLab_指定日期实验室_生成开放资源() throws Exception {
        //准备
        //1 stub设置学期
        //2.批定日期、指定实验室
        //3.模拟实验室的最大可用机器数 60
        //4.模拟星期一（其它的全部为空）
        //5.人工计算得到全部预期的资源

        new TermUtilTest_Base().stubTerm();//2018-2019,1,22,2019-08-26

        java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2019-09-02");
        java.sql.Date sDate = new java.sql.Date(date.getTime());
        String labId = "6311";
        int dayOfweek = 1;  //星期一
        int weekOfTerm = 2; //第二周

        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from LabOpenPlan").executeUpdate();
        session.createQuery("delete from OpenLabSchedule").executeUpdate();
        session.createQuery("delete from TempOpenPlan").executeUpdate();
        session.createQuery("delete from OpenRes").executeUpdate();


        //取出resourceGeneratingService代理的目标对象target
        //对此对象进行spy
        //将spy后对象，要求spring代理spy


        //模拟星期一（其它的全部为空）
        LabOpenPlan labOpenPlan = new LabOpenPlan(null, "2018-2019-1", labId, 1, "TEST02");
        session.save(labOpenPlan);

        Integer openPlanId = labOpenPlan.getOpenPlanId();
        OpenLabSchedule openLabSchedule = new OpenLabSchedule(openPlanId, "2018-2019-1", labId, 1, "TEST02");
        Map<String, String> map = mockScheduleDetailMap();  ////第1，3，5，7节开放，其它关闭

        createOpenLabSchedule(session, openLabSchedule, map);


        //执行
        this.resourceGeneratingService.generateResForLab(TermUtil.getTermStr(), session, date, dayOfweek, weekOfTerm, labId, 60);

        //验证
        OpenRes expectOpenRes1 = new OpenRes(1, sDate, dayOfweek, labId, 60, 0, 60, "20190902#6311#1001");
        OpenRes expectOpenRes3 = new OpenRes(3, sDate, dayOfweek, labId, 60, 0, 60, "20190902#6311#3001");
        OpenRes expectOpenRes5 = new OpenRes(5, sDate, dayOfweek, labId, 60, 0, 60, "20190902#6311#5001");
        OpenRes expectOpenRes7 = new OpenRes(7, sDate, dayOfweek, labId, 60, 0, 60, "20190902#6311#7001");
        Set<OpenRes> expectOpenResSet = new HashSet<>();
        expectOpenResSet.addAll(Arrays.asList(new OpenRes[]{expectOpenRes1, expectOpenRes3, expectOpenRes5, expectOpenRes7}));

        List<OpenRes> openResList = session.createQuery("from OpenRes where openDate=:sDate")
                .setParameter("sDate", sDate)
                .list();
        assertEquals(4, openResList.size());
        Set<OpenRes> actOpenResSet = openResList.stream().map(x -> {
            x.setId(null);
            return x;
        })
                .collect(Collectors.toSet());

        for (OpenRes res : actOpenResSet) {
            System.out.println(res);
            assertTrue(expectOpenResSet.contains(res));
        }


    }

    private void createOpenLabSchedule(Session session, OpenLabSchedule openLabSchedule, Map<String, String> map) {
        session.save(openLabSchedule);
        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;

                final String content = map.get(key);
                OpenLabScheduleDetail openLabScheduleDetail = new OpenLabScheduleDetail(null, i, j, content, openLabSchedule);
                session.save(openLabScheduleDetail);

            }

        }
    }

    private static Map<String, String> mockScheduleDetailMap() {
        Map<String, String> map = new HashMap<>();

        for (int i = 1; i <= LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                String value;
                if (j != 1) {
                    value = "";
                } else {
                    if (i == 1 || i == 3 || i == 5 || i == 7) //第1，3，5，7节开放，其它关闭
                        value = "1-20";
                    else
                        value = "";
                }
                map.put(key, value);
            }

        }
        return map;
    }

}