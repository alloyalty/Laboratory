package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.AnAppointmentService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class AnAppointmentServiceImplTest_isRepeatedApp {
    @Autowired
    private AnAppointmentService anAppointmentService;

    @Autowired
    private SessionFactory sessionFactory;
    ApplyForLab app1,app2,app3,app4,app5,app6,app7,app8,app9,app10,app11,app12,app13,app14,app15,app16;
    ApplyForLab temp = new ApplyForLab();

    public AnAppointmentServiceImplTest_isRepeatedApp() throws ParseException {
         app1 = temp.genApplyForLab(805L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 2, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#2001");
         app2 = temp.genApplyForLab(806L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 3, 7, AnAppointmentService.AUDIT_NOPASS, "", "null", "2018-2019-1", "20190928#6312#7001");
        //29
         app3 = temp.genApplyForLab(807L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 1, 3, AnAppointmentService.AUDITPASS_USED, "", "null", "2018-2019-1", "20190929#6312#3001");
         app4 = temp.genApplyForLab(808L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 1, 1, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#1002");
         app5 = temp.genApplyForLab(809L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 2, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002");
         app6 = temp.genApplyForLab(810L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 3, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001");
        //30
         app7 = temp.genApplyForLab(804L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "DDD", "AAA", "CCC", "Dev-C,C++", 2, 3, AnAppointmentService.CANCEL_APPLY, "", "null", "2018-2019-1", "20190925#6312#1001");
         app8 = temp.genApplyForLab(805L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 4, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#2001");
         app9 = temp.genApplyForLab(806L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 1, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#7001");
        //10月1日
         app10 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
         app11 = temp.genApplyForLab(808L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 3, 3, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#1002");
         app12 = temp.genApplyForLab(809L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 1, 1, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002");
         app13 = temp.genApplyForLab(810L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001");
        //10月2日 已经审核通过
         app14 = temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002");
         app15 = temp.genApplyForLab(810L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001");

        //10月2日 已经审核未通过
         app16 = temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDIT_NOPASS, "", "null", "2018-2019-1", "20190929#6312#2002");

    }


    @Before
    public void setup() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab").executeUpdate();


    }

    @Test
    public void isRepeatedApplication_与已经审核通过未使用重叠_RetrunTrue() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app1);

        String dateStr = "20190928";//对应记录1
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C",
                "D", 1, 3, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertTrue(x);

    }

    public java.sql.Date getSqlDate(String dateStr) throws ParseException {
        Date date = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
        return new java.sql.Date(date.getTime());
    }

    @Test
    public void isRepeatedApplication_与审核未通过的重叠_ReturnFalse() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app2);

        String dateStr = "20190928";  //对应记录2
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C", "D",
                2, 3, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }

    @Test
    public void isRepeatedApplication_与审核通过且已经使用的重叠_ReturnFalse() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app3);

        //既然已经使用过了，就可以重复使用 //对应记录3
        String dateStr = "20190929";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C", "D",
                2, 2, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }

    @Test
    public void isRepeatedApplication_与审核通过且已经使用的不重叠__其它皆不重叠_ReturnFalse() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app4);

        //既然已经使用过了，就可以重复使用 //对应记录4
        String dateStr = "20190929";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C", "D",
                6, 7, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }

    @Test
    public void isRepeatedApplication_与取消的重叠_ReturnFalse() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app7);

        //既然已经取消过了，就可以重复使用//对应记录7
        String dateStr = "20190930";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C", "D",
                2, 2, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }


    @Test
    public void isRepeatedApplication_与待审核的申请重叠_ReturnTrue() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app10);

        //对应记录10
        String dateStr = "20191001";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "XXX", "6312", "", "A", "B", "C", "D",
                2, 2, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertTrue(x);

    }

    @Test
    public void isRepeatedApplication_与待审核的申请不重叠_其它皆不重叠_ReturnTrue() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app11);

        //先审的，优先占用。//对应记录11
        String dateStr = "20191001";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student", "", "6312", "", "A", "B", "C", "D", 1, 1, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }

    @Test
    public void 验证写入的ApplyForLab与读出的完全相同() throws ParseException {
        ApplyForLab temp = new ApplyForLab();
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab").executeUpdate();

        ApplyForLab app1 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
        ApplyForLab app2 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
        //以上二个完全相同。
        //插入app1,读出后，与app2进行比较
        session.save(app1);
        session.clear();
        ApplyForLab x = session.get(ApplyForLab.class, app1.getApplyNumber());
        app2.setApplyNumber(app1.getApplyNumber());
        assertEquals(x, app2);

    }

    @Test
    public void 验证申请时间查询_输入yyyyMMdd__应返回查询记录() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab").executeUpdate();

        ApplyForLab app1 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
        //插入一条记录
        //使用yyyyMMdd应能找到此记录
        session.save(app1);
//        java.sql.Date d = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01").getTime());
//        System.out.println(d);
        List<ApplyForLab> applyForLabList = session.createQuery("from ApplyForLab  where applyTime=:applyTime")
                .setParameter("applyTime", new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2019-10-01").getTime()))
                .list();

        System.out.println(applyForLabList.get(0));
        assertTrue(applyForLabList != null && applyForLabList.size() == 1);


    }

    @Test
    public void isRepeatedApplication_与已经审核通过未使用重叠但学生不同_RetrunFalse() throws ParseException {
        Session session = sessionFactory.getCurrentSession();
        session.save(app12);

        //先审的，优先占用。//对应记录12
        String dateStr = "20191001";
        java.sql.Date applyTime = getSqlDate(dateStr);
        ApplyForLab applyForLab = new ApplyForLab(applyTime, "student1", "", "6312", "", "A", "B", "C", "D", 1, 1, "2018-2019-1");
        boolean x = anAppointmentService.isRepeatedApplication(applyForLab);
        assertFalse(x);

    }

    public AnAppointmentService getAnAppointmentService() {
        return anAppointmentService;
    }

    public void setAnAppointmentService(AnAppointmentService anAppointmentService) {
        this.anAppointmentService = anAppointmentService;
    }

    private List<ApplyForLab> prepareAuditedDate() throws ParseException {
        List<ApplyForLab> list = new ArrayList<>();
        //28
        list.add(temp.genApplyForLab(805L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 2, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#2001"));
        list.add(temp.genApplyForLab(806L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 3, 7, AnAppointmentService.AUDIT_NOPASS, "", "null", "2018-2019-1", "20190928#6312#7001"));
        //29
        list.add(temp.genApplyForLab(807L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 1, 3, AnAppointmentService.AUDITPASS_USED, "", "null", "2018-2019-1", "20190929#6312#3001"));
        list.add(temp.genApplyForLab(808L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 1, 1, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#1002"));
        list.add(temp.genApplyForLab(809L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 2, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 3, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001"));
        //30
        list.add(temp.genApplyForLab(804L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "DDD", "AAA", "CCC", "Dev-C,C++", 2, 3, AnAppointmentService.CANCEL_APPLY, "", "null", "2018-2019-1", "20190925#6312#1001"));
        list.add(temp.genApplyForLab(805L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 4, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#2001"));
        list.add(temp.genApplyForLab(806L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 1, 2, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190928#6312#7001"));
        //10月1日
        list.add(temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001"));
        list.add(temp.genApplyForLab(808L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 3, 3, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#1002"));
        list.add(temp.genApplyForLab(809L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 1, 1, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001"));
        //10月2日 已经审核通过
        list.add(temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001"));

        //10月2日 已经审核未通过
        list.add(temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDIT_NOPASS, "", "null", "2018-2019-1", "20190929#6312#2002"));

        return list;
    }


    @Test
    public void findProblem() throws ParseException {

        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();

        ApplyForLab app1 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
        session.save(app1);

        final String hql = "from ApplyForLab where userId=?1 and applyTime=?2 and labId=?3 and ?4>=timeslotStart and ?5<=timeslotFinish and (state=0 or state=2)";
        List list = session.createQuery(hql)
                .setParameter(1, app1.getUserId())
                .setParameter(2, app1.getApplyTime())
                .setParameter(3, app1.getLabId())
                .setParameter(4, app1.getTimeslotStart())
                .setParameter(5, app1.getTimeslotFinish())
                .list();
        assertTrue(list != null && list.size() == 1);

    }

    @Test
    public void findProblem_验证时间() throws ParseException {

        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();

        ApplyForLab app1 = temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 2, 2, AnAppointmentService.WAITFOR_AUDIT, "", "null", "2018-2019-1", "20190929#6312#3001");
        session.save(app1);

        java.sql.Date date = new java.sql.Date(new SimpleDateFormat("yyyyMMdd").parse("20191001").getTime());
        final String hql = "from ApplyForLab where userId=?1 and applyTime=?2 and labId=?3 and ?4>=timeslotStart and ?5<=timeslotFinish and (state=0 or state=2)";
        List list = session.createQuery(hql)
                .setParameter(1, app1.getUserId())
                .setParameter(2, date)
                .setParameter(3, app1.getLabId())
                .setParameter(4, app1.getTimeslotStart())
                .setParameter(5, app1.getTimeslotFinish())
                .list();
        assertTrue(list != null && list.size() == 1);

    }
}