package com.hgd.laboratory.service.impl;

import org.junit.Test;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.*;

public class FindBackPwdServiceImplTest {

    public Timestamp ignoreMillSecond(Timestamp outDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(outDate.getTime());
        calendar.set(Calendar.MILLISECOND,0);
        return new Timestamp(calendar.getTimeInMillis());

    }

    @Test
    public void ignoreMillSecond() {
        {
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
            testInstance(new Timestamp(new Date().getTime()));
        }



    }

    public void testInstance(Timestamp x) {
        final Timestamp x1 = ignoreMillSecond(x);
        System.out.println(x1);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(x1.getTime());
        assertEquals(0,calendar.get(Calendar.MILLISECOND));
    }
}