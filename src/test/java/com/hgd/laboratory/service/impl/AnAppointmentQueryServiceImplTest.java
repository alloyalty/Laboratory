package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.ApplyForLabAudit;
import com.hgd.laboratory.dto.PaginationData;
import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.po.Term;
import com.hgd.laboratory.service.AnAppointmentQueryService;
import com.hgd.laboratory.service.AnAppointmentService;
import com.hgd.laboratory.util.AuditQueryReqParameter;
import com.hgd.laboratory.util.Page;
import com.hgd.laboratory.util.TermUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class AnAppointmentQueryServiceImplTest {
    @Autowired
    AnAppointmentQueryService anAppointmentQueryService;
    @Autowired
    SessionFactory sessionFactory;


    /*
    入口参数
    Date currDateTime,int tx,Page page
     */
    @Test
    @Transactional
    public void queryHistory1() throws ParseException {
        queryHistoryTestTemplate("20190831", 1, 2L, 1, 3);
    }

    @Test
    @Transactional
    public void queryHistory2() throws ParseException {
        queryHistoryTestTemplate("20190831", 2, 2L, 1, 3);

    }


    @Test
    @Transactional
    public void queryHistory3() throws ParseException {
        queryHistoryTestTemplate("20190831", 3, 2L, 2, 4);
    }

    @Test
    @Transactional
    public void queryHistory4() throws ParseException {
        queryHistoryTestTemplate("20190831", 4, 2L, 2, 4);

    }

    @Test
    @Transactional
    public void queryHistory5() throws ParseException {
        queryHistoryTestTemplate("20190902", 0, 2L, 2, 4);
    }

    @Test
    @Transactional
    public void queryHistory6() throws ParseException {
        queryHistoryTestTemplate("20190902", 1, 2L, 2, 4);
    }


    @Test
    @Transactional
    //20190902,2,3L,1,3
    public void queryHistory7() throws ParseException {
        queryHistoryTestTemplate("20190902", 2, 4L, 1, 7);
    }

    @Test
    @Transactional
    public void queryHistory8() throws ParseException {
        queryHistoryTestTemplate("20190902", 3, 4L, 1, 7);
    }

    @Test
    @Transactional
    public void queryHistory9() throws ParseException {
        queryHistoryTestTemplate("20190902", 4, 4L, 2, 8);
    }

    @Test
    @Transactional
    public void queryHistory10() throws ParseException {
        queryHistoryTestTemplate("20190903", 4, 4L, 2, 8);
    }

    private void queryHistoryTestTemplate(String dateStr, int tx, long currentPage, int maxRecInCurrPage, int maxRecs) throws ParseException {
        this.stubTermUtil();
        //删去所有数据
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();

        Date currDate = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
        Page page = new Page(2);
        page.setCurrentPage(currentPage);
        List<ApplyForLab> applyForLabList = prepareQueryDate();
        //构造数据
        for (ApplyForLab applyForLab : applyForLabList) {
            session.save(applyForLab);
        }
        PaginationData<ApplyForLab> x = anAppointmentQueryService.queryHistory(currDate, tx, page);
        assertEquals(maxRecInCurrPage, x.getDataList().size());
        assertEquals(maxRecs, x.getPage().getTotalRows().intValue());
    }

    private void queryCurrentTestTemplate(String dateStr, int tx, long currentPage, int maxRecInCurrPage, int maxRecs) throws ParseException {
        stubTermUtil();
        //删去所有数据
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();
        Date currDate = new SimpleDateFormat("yyyyMMdd").parse(dateStr);
        Page page = new Page(2);
        page.setCurrentPage(currentPage);
        List<ApplyForLab> applyForLabList = prepareQueryDate();
        //构造数据
        for (ApplyForLab applyForLab : applyForLabList) {
            session.save(applyForLab);
        }
        PaginationData<ApplyForLab> x = anAppointmentQueryService.queryCurrent(currDate, tx, page);
        assertEquals(maxRecInCurrPage, x.getDataList().size());
        assertEquals(maxRecs, x.getPage().getTotalRows().intValue());
    }


    private List<ApplyForLab> prepareQueryDate() throws ParseException {
        ApplyForLab temp = new ApplyForLab();
        List<ApplyForLab> list = new ArrayList<>();
        list.add(temp.genApplyForLab(4L, "2018-08-30 00:00:00.0", "student", "TEST2", "6312", "", "B", "A", "D", "C", 1, 1, 0, "", "null", "2018-2019-1", "20180830#6312#1001"));
        list.add(temp.genApplyForLab(5L, "2018-08-30 00:00:00.0", "student", "TEST2", "6312", "", "B", "A", "D", "C", 1, 1, 0, "", "null", "2018-2019-1", "20180830#6312#1002"));
        list.add(temp.genApplyForLab(6L, "2018-08-30 00:00:00.0", "student", "TEST2", "6312", "", "B", "A", "D", "C", 1, 1, 0, "", "null", "2018-2019-1", "20180830#6312#1003"));
        list.add(temp.genApplyForLab(7L, "2019-08-31 00:00:00.0", "student", "TEST2", "6312", "", "B", "A", "D", "C", 2, 2, 0, "", "null", "2018-2019-1", "20190831#6312#2001"));
        list.add(temp.genApplyForLab(8L, "2019-09-02 00:00:00.0", "student", "TEST2", "6312", "", "B", "A", "D", "Dev-C,C++###Visual Studio Community 2017", 1, 1, 0, "", "null", "2018-2019-1", "20190902#6312#1001"));
        list.add(temp.genApplyForLab(9L, "2019-09-02 00:00:00.0", "student", "TEST2", "6312", "", "DSDS", "ASA", "D", "Visual C++###mysq###sql server ", 3, 3, 0, "", "null", "2018-2019-1", "20190902#6312#3001"));
        list.add(temp.genApplyForLab(10L, "2019-09-02 00:00:00.0", "student", "TEST2", "6312", "", "sds", "dfd", "D", "Visual Studio Community 2017###eclipse for web", 1, 1, 0, "", "null", "2018-2019-1", "20190902#6312#1002"));
        list.add(temp.genApplyForLab(11L, "2019-09-02 00:00:00.0", "student", "TEST2", "6312", "", "ere", "ere", "asa", "Dev-C,C++###eclipse for web", 1, 1, 0, "", "null", "2018-2019-1", "20190902#6312#1003"));

        return list;
    }

    private List<ApplyForLab> prepareAuditDate() throws ParseException {
        List<ApplyForLab> list = new ArrayList<>();
        //28
        ApplyForLab temp = new ApplyForLab();
        list.add(temp.genApplyForLab(805L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 2, 2, 0, "", "null", "2018-2019-1", "20190928#6312#2001"));
        list.add(temp.genApplyForLab(806L, "2019-09-28 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 5, 7, 4, "", "null", "2018-2019-1", "20190928#6312#7001"));
        //29
        list.add(temp.genApplyForLab(807L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 1, 3, 4, "", "null", "2018-2019-1", "20190929#6312#3001"));
        list.add(temp.genApplyForLab(808L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 1, 1, 0, "", "null", "2018-2019-1", "20190929#6312#1002"));
        list.add(temp.genApplyForLab(809L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 2, 2, 0, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-09-29 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 3, 4, 0, "", "null", "2018-2019-1", "20190929#6312#4001"));
        //30
        list.add(temp.genApplyForLab(804L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "DDD", "AAA", "CCC", "Dev-C,C++", 6, 6, 4, "", "null", "2018-2019-1", "20190925#6312#1001"));
        list.add(temp.genApplyForLab(805L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 4, 4, 0, "", "null", "2018-2019-1", "20190928#6312#2001"));
        list.add(temp.genApplyForLab(806L, "2019-09-30 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "X2", "Dev-C,C++", 1, 2, 0, "", "null", "2018-2019-1", "20190928#6312#7001"));
        //10月1日
        list.add(temp.genApplyForLab(807L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "Visual Studio Community 2017", 1, 1, 4, "", "null", "2018-2019-1", "20190929#6312#3001"));
        list.add(temp.genApplyForLab(808L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B1", "A1", "C1", "Dev-C,C++", 3, 3, 4, "", "null", "2018-2019-1", "20190929#6312#1002"));
        list.add(temp.genApplyForLab(809L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, 0, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-10-01 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, 0, "", "null", "2018-2019-1", "20190929#6312#4001"));
        //10月2日 已经审核通过
        list.add(temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#2002"));
        list.add(temp.genApplyForLab(810L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B3", "A3", "C3", "interllij idea 2019", 5, 6, AnAppointmentService.AUDITPASS_UNUSE, "", "null", "2018-2019-1", "20190929#6312#4001"));

        //10月2日 已经审核未通过
        list.add(temp.genApplyForLab(809L, "2019-10-02 00:00:00.0", "student", "TEST", "6312", "", "B2", "A2", "C2", "Visual Studio Community 2017", 4, 4, AnAppointmentService.AUDIT_NOPASS, "", "null", "2018-2019-1", "20190929#6312#2002"));

        return list;
    }

    @Test
    @SuppressWarnings("unchecked")
    public void genData() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from ApplyForLab";
        List<ApplyForLab> applyForLabList = (List<ApplyForLab>) session.createQuery(hql).list();

        StringBuffer sb = new StringBuffer();
        sb.append("List<ApplyForLab> list= new ArrayList<>();\n");
        for (ApplyForLab applyForLab : applyForLabList) {
            String construct = applyForLab.genConstructCode();
            sb.append(String.format("list.add(%s);\n", construct));
        }
        System.out.println(new String(sb));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void genaAuditData() {
        Session session = sessionFactory.getCurrentSession();
        String hql = "from ApplyForLab where applyNumber>=804";
        List<ApplyForLab> applyForLabList = (List<ApplyForLab>) session.createQuery(hql).list();

        StringBuffer sb = new StringBuffer();
        sb.append("List<ApplyForLab> list= new ArrayList<>();\n");
        for (ApplyForLab applyForLab : applyForLabList) {
            String construct = applyForLab.genConstructCode();
            sb.append(String.format("list.add(%s);\n", construct));
        }
        System.out.println(new String(sb));
    }

    @Test
    @Transactional
    public void queryCurrent1() throws ParseException {
        queryCurrentTestTemplate("20180830", 0, 4, 2, 8);
        queryCurrentTestTemplate("20180830", 1, 4, 2, 8);
        queryCurrentTestTemplate("20180830", 2, 3, 1, 5);
        queryCurrentTestTemplate("20190831", 0, 3, 1, 5);
        queryCurrentTestTemplate("20190831", 1, 3, 1, 5);
        queryCurrentTestTemplate("20190831", 2, 3, 1, 5);
        queryCurrentTestTemplate("20190831", 3, 2, 2, 4);

    }


    private void stubTermUtil() throws ParseException {
        java.sql.Date theDate = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2019-08-26").getTime());
        Term stubTerm = new Term("2018-2019", 1, 22, theDate);
        TermUtil.reset();
        TermUtil.setCustomTerm(stubTerm);

    }
    @Test
    @Transactional
    public void queryWaitForAudit1() throws ParseException {
        //删去所有数据
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();
        Date currDate = new SimpleDateFormat("yyyy-MM-dd").parse("2019-09-28");


        List<ApplyForLab> applyForLabList = this.prepareAuditDate();
        //构造数据
        for (ApplyForLab applyForLab : applyForLabList) {
            session.save(applyForLab);
        }
        String dataStr = "2019-09-28";
        //2019-09-28  申请的全部记录（不论其状态），共计16 {8条待审，5条取消预约，2条审核通过,1条审核未通过}
        auditTestTemplate(dataStr, 2, 6, 16, new AuditQueryReqParameter(null, (Date) null, null, null));
        auditTestTemplate(dataStr, 1, 8, 8, new AuditQueryReqParameter(null, (Date) null, null, 0));
        auditTestTemplate(dataStr, 1, 5, 5, new AuditQueryReqParameter(null, (Date) null, null, 4));
        auditTestTemplate(dataStr, 1, 2, 2, new AuditQueryReqParameter(null, (Date) null, null, AnAppointmentService.AUDITPASS_UNUSE));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter(null, (Date) null, null, AnAppointmentService.AUDIT_NOPASS));

        auditTestTemplate(dataStr, 2, 6, 16, new AuditQueryReqParameter("6312", (Date) null, null, null));
        auditTestTemplate(dataStr, 1, 8, 8, new AuditQueryReqParameter("6312", (Date) null, null, 0));
        auditTestTemplate(dataStr, 1, 5, 5, new AuditQueryReqParameter("6312", (Date) null, null, 4));
        auditTestTemplate(dataStr, 1, 2, 2, new AuditQueryReqParameter("6312", (Date) null, null, AnAppointmentService.AUDITPASS_UNUSE));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter("6312", (Date) null, null, AnAppointmentService.AUDIT_NOPASS));

        auditTestTemplate(dataStr, 1, 4, 4, new AuditQueryReqParameter("6312", "2019-09-29", null, null));
        auditTestTemplate(dataStr, 1, 3, 3, new AuditQueryReqParameter("6312", "2019-09-29", null, 0));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter("6312", "2019-09-29", null, 4));
        auditTestTemplate(dataStr, 1, 0, 0, new AuditQueryReqParameter("6312", "2019-09-29", null, AnAppointmentService.AUDITPASS_UNUSE));
        auditTestTemplate(dataStr, 1, 0, 0, new AuditQueryReqParameter("6312", "2019-09-29", null, AnAppointmentService.AUDIT_NOPASS));

        auditTestTemplate(dataStr, 1, 4, 4, new AuditQueryReqParameter("6312", (Date) null, 1, null));
        auditTestTemplate(dataStr, 1, 2, 2, new AuditQueryReqParameter("6312", (Date) null, 2, 0));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter("6312", (Date) null, 3, 4));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter("6312", (Date) null, 4, 2));
        auditTestTemplate(dataStr, 1, 1, 1, new AuditQueryReqParameter("6312", (Date) null, 4, 1));


    }


    private void auditTestTemplate(String dateStr, long currentPage, int maxRecInCurrPage, int maxRecs, AuditQueryReqParameter auditQueryParam) throws ParseException {
        Page page = new Page(10);
        page.setCurrentPage(currentPage);
        java.sql.Date today = new java.sql.Date(new SimpleDateFormat("yyyy-MM-dd").parse("2019-09-28").getTime());
        PaginationData<ApplyForLabAudit> paginationData = anAppointmentQueryService.queryWaitForAudit(auditQueryParam, page, today);
        assertEquals(maxRecInCurrPage, paginationData.getDataList().size());
    }

    @Test
    public void auditAll() {
    }

    @Test
    public void queryApplyList() {
    }

    @Test
    public void cancelAll() {
    }


    public AnAppointmentQueryService getAnAppointmentQueryService() {
        return anAppointmentQueryService;
    }

    public void setAnAppointmentQueryService(AnAppointmentQueryService anAppointmentQueryService) {
        this.anAppointmentQueryService = anAppointmentQueryService;
    }
}