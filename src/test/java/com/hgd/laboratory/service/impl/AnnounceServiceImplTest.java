package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.Notice;
import com.hgd.laboratory.service.AnnounceService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class AnnounceServiceImplTest {

    @Autowired
    private SessionFactory sessionFactory;

    @Autowired
    private AnnounceService announceService;

    Notice n1 = new Notice(AnnounceService.ANNOUNCE, "897 title", "2109-10-2", "正文", 0, "", 0, "");
    Notice n1a = new Notice(AnnounceService.ANNOUNCE, "897 title", "2109-10-2", "正文", 0, "", 0, "");
    Notice n2 = new Notice(AnnounceService.ANNOUNCE, "899 title", "2109-10-3", "正文3333", 0, "", 0, "");
    Notice n2a = new Notice(AnnounceService.ANNOUNCE, "899 title", "2109-10-3", "正文3333", 0, "", 0, "");

    @Test
    public void getMainArticleId() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Notice ").executeUpdate();
        session.save(n1);
        session.save(n2);

        Long id = announceService.getMainArticleId();
        assertEquals(n2.getId(), id);
        System.out.println("找到" + id);

    }

    @Test
    public void findOne() {
        Session session = sessionFactory.getCurrentSession();
        session.save(n1);
        long id = n1.getId();

        Notice notice = announceService.findOne(id);

        n1a.setId(n1.getId());
        assertEquals(n1a, notice);
        System.out.println("找到" + notice.toString());
    }

    @Test
    public void query() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Notice ").executeUpdate();
        session.save(n1);
        session.save(n2);

        List<Notice> noticeList = announceService.query(AnnounceService.ANNOUNCE, 100);
        List<Notice> noticeList1 = announceService.query(AnnounceService.RULES, 100);

        for (Notice n : noticeList ) {
            System.out.println(n.getTitle());
        }
        assertEquals(2, noticeList.size());
        assertEquals(0, noticeList1.size());
        System.out.println("query 测试成功");

    }

    public AnnounceService getAnnounceService() {
        return announceService;
    }

    public void setAnnounceService(AnnounceService announceService) {
        this.announceService = announceService;
    }
}