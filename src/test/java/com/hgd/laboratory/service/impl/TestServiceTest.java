package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.Laboratory;
import com.hgd.laboratory.util.SpringUils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class TestServiceTest {
    @Autowired
    private SessionFactory sessionFactory;

    @Test
    @Transactional
    public void getAllLabList() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from Laboratory ").executeUpdate();
        Laboratory lab1 = new Laboratory("6321", "莲6321", "address", 0, 0, 1, "");
        Laboratory lab2 = new Laboratory("6322", "莲6322", "address", 0, 0, 1, "");
        Laboratory lab3 = new Laboratory("6323", "莲6323", "address", 0, 0, 1, "");
        session.save(lab1);
        session.save(lab2);
        session.save(lab3);

        List<String> list = new TestService().getAllLabList();
        assertTrue(3 == list.size());
    }
}