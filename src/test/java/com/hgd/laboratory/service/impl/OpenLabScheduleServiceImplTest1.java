package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.service.OpenLabScheduleService;
import com.hgd.laboratory.util.OpenPlanUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class OpenLabScheduleServiceImplTest1 {
    @Autowired
    private OpenLabScheduleService openLabScheduleService;

    @Test
    public void mergeToLabOpenSchedule() {
    }

    @Test
    public void reverseToString() {
        //将一个集合，写成一个表达式
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();

        {
            Set<Integer> set = null;
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "".equals( s ) );

        }

        {
            Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {5}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "5".equals( s ) );

        }

        {
            Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {5,7}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "5,7".equals( s ) );

        }


        {   Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {1,5,6,7,2,12,10}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "1-2,5-7,10,12".equals( s ) );
        }

        {
            Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {1,7,8,9,2}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "1-2,7-9".equals( s ) );

        }
        {
            Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {10,9,8,7,6,5,4,3}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "3-10".equals( s ) );

        }


        {
            Set<Integer> set = new HashSet<>();
            set.addAll( Arrays.asList( new Integer[] {}) );
            String s = OpenPlanUtil.reverseToString( set );
            assertTrue( "".equals( s ) );

        }



    }

    @Test
    public void valid_tempPlan() {
        Set<Integer> set = new HashSet<>();
        set.add( 1 );
        set.add( 3 );
        set.add( 5 );
        set.add( 7 );
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        assertTrue( OpenPlanUtil.valid_tempPlan( set,2,"0","" ));

        assertTrue( OpenPlanUtil.valid_tempPlan( set,3,"1","" ));

        assertTrue( OpenPlanUtil.valid_tempPlan( set,4,"0","1" ));

        assertTrue( OpenPlanUtil.valid_tempPlan( set,5,"1","0" ));

        assertFalse( OpenPlanUtil.valid_tempPlan( set,4,"1","1" ));

        assertFalse( OpenPlanUtil.valid_tempPlan( set,5,"0","0" ));

        assertFalse( OpenPlanUtil.valid_tempPlan( set,6,"0","0" ));

    }

    @Test
    public void transferToSet() throws OpenPlanExpressinException {

        Set<Integer> set;
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        set = OpenPlanUtil.transferToSet( "" );
        assertTrue( set.isEmpty() );

        set = OpenPlanUtil.transferToSet( "1" );
        assertTrue( set.size() == 1 );
        assertTrue( set.contains( 1 ) );

        set = OpenPlanUtil.transferToSet( "1-5" );
        assertTrue( set.size() == 5 );
        HashSet<Integer> set1 = new HashSet<Integer>();
        Integer[] arr1 = new Integer[]{1,2,3,4,5};
        set1.addAll( Arrays.asList( arr1 ) );
        assertTrue( set.containsAll( set1 ) );


        {
            set = OpenPlanUtil.transferToSet( "1-2, 7-9 " );
            assertTrue( set.size() == 5 );
            HashSet<Integer> set2 = new HashSet<Integer>();
            Integer[] arr2 = new Integer[]{1,2,7,8,9};
            set2.addAll( Arrays.asList( arr2 ) );
            assertTrue( set.containsAll( set2 ) );
        }

        {
            set = OpenPlanUtil.transferToSet( "1, 2, 5-7, 10,12 " );
            assertTrue( set.size() == 7 );
            HashSet<Integer> set3 = new HashSet<Integer>();
            Integer[] arr3 = new Integer[]{1,2,5,6,7,10,12};
            set3.addAll( Arrays.asList( arr3 ) );
            assertTrue( set.containsAll( set3 ) );
        }

        {
            set = OpenPlanUtil.transferToSet( " ,  " );
            assertTrue( set.size() == 0 );
            assertTrue( set.isEmpty() );
        }
        {
            set = OpenPlanUtil.transferToSet( " , , " );
            assertTrue( set.size() == 0 );
            assertTrue( set.isEmpty() );
        }


    }

    @Test(expected = Exception.class)
    public void transferToSet1() throws OpenPlanExpressinException {
        Set<Integer> set;
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        set = OpenPlanUtil.transferToSet( "0" );

    }


    @Test(expected = Exception.class)
    public void transferToSet2() throws OpenPlanExpressinException {
        Set<Integer> set;
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        set = OpenPlanUtil.transferToSet( "-1" );

    }

    @Test(expected = Exception.class)
    public void transferToSet3() throws OpenPlanExpressinException {
        Set<Integer> set;
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        set = OpenPlanUtil.transferToSet( "30" );

    }


    @Test(expected = Exception.class)
    public void transferToSet5() throws OpenPlanExpressinException {
        Set<Integer> set;
        OpenLabScheduleServiceImpl openLabScheduleServiceImpl = new OpenLabScheduleServiceImpl();
        set = OpenPlanUtil.transferToSet( "1+2" );

    }


    public OpenLabScheduleService getOpenLabScheduleService() {
        return openLabScheduleService;
    }

    public void setOpenLabScheduleService(OpenLabScheduleService openLabScheduleService) {
        this.openLabScheduleService = openLabScheduleService;
    }
}