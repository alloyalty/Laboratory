package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.po.User;
import com.hgd.laboratory.service.UserService;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Rollback(value=true)
@Transactional
public class UserServiceImplTest {
    @Autowired
    private UserService userService;

    @Autowired
    private SessionFactory sessionFactory;

    User user1 = new User("idTest1","pass001","student","");
    User user2 = new User("idTest2","pass002","student","");
    User user3 = new User("idTest3","pass003","student","");

    @Before
    @Transactional
    public void setup() {
        //如果希望与db系统的已经有的数据相关的话，则应删去user表中的所有的数据
        //再插入三条数据User,再进行测试
        //测试完成后，回滚
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from ApplyForLab ").executeUpdate();
        session.createQuery("delete from UserRole").executeUpdate();
        session.createQuery("delete from User").executeUpdate();

        session.save(user1);
        session.save(user2);
        session.save(user3);

    }

    @Test
    @Transactional
    public void checkUser() {
        User user = this.userService.checkUser("idTest1","pass001","student");
        assertTrue(isSameUser(user,user1));
    }

    @Test
    @Transactional
    public void checkUser_用户角色不正确_ReturnNull() {
        User user = this.userService.checkUser("idTest","pass001","student1");
        assertNull(user);

    }

    @Test
    @Transactional
    public void checkUser_用户名不存在_ReturnNull() {
        User user = this.userService.checkUser("idTest","pass001","student");
        assertNull(user);

    }


    @Test
    @Transactional
    public void checkUser_密码不正确_ReturnNull() {
        User user = this.userService.checkUser("idTest1","pass00e","student");
        assertNull(user);
    }



    public boolean isSameUser(User user,User userx) {
        return user.getId().equals(userx.getId())
          && user.getPassword().equals(userx.getPassword())
          && user.getRole().equals(userx.getRole());
    }

    @Test
    @Transactional
    public void findOne() {
        User user = this.userService.findOne("idTest1");
        assertTrue(isSameUser(user,user1)    );
    }

    @Test
    @Transactional
    public void findOne_未找到_ReturnNull() {
        User user = this.userService.findOne("idTest");
        assertNull(user);
    }

    @Test
    @Transactional
    public void update() {
        User user = this.userService.findOne("idTest2");
        user.setPassword("newPass");
        this.userService.update(user);

        User updatedUser = this.userService.findOne("idTest2");
        assertTrue(isSameUser(updatedUser,user)    );
    }
}