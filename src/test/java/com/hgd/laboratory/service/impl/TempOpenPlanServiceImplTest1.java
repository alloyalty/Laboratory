package com.hgd.laboratory.service.impl;

import com.hgd.laboratory.dto.LabOpenPlanTuple;
import com.hgd.laboratory.dto.TempItem;
import com.hgd.laboratory.dto.TempOpenPlanTuple;
import com.hgd.laboratory.exception.OpenPlanDataIncompleteException;
import com.hgd.laboratory.exception.OpenPlanExpressinException;
import com.hgd.laboratory.po.LabOpenPlan;
import com.hgd.laboratory.po.TempOpenPlan;
import com.hgd.laboratory.service.LabOpenPlanService;
import com.hgd.laboratory.service.LabService;
import com.hgd.laboratory.service.TempOpenPlanService;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.hgd.laboratory.service.impl.LabOpenPlanServiceImplTest.mockLabOpenPlanDetails;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContext.xml")
@Transactional
public class TempOpenPlanServiceImplTest1 {
    @Autowired @Getter    @Setter
    private TempOpenPlanService tempOpenPlanService;
    @Autowired @Getter    @Setter
    private LabOpenPlanService labOpenPlanService;
    @Autowired @Getter    @Setter
    private SessionFactory sessionFactory;


    @Test
    @Transactional
    public void deleteAllRec() {
        /**
         * 删除全部记录
         * 查询全部记录为0条
         *
         */
        this.tempOpenPlanService.deleteAllRec();

        Session session = sessionFactory.getCurrentSession();
        List l = session.createQuery( "select count(*) from TempOpenPlan" ).list();
        assertEquals( 0L,(long) l.get(0) );

    }

    @Test
    public void queryAllTempOpenPlans() {
        //获取本学期全部临时计划测试

        //测试前删除全部数据
        //插入三条记录进去
        //查询返回三条记录
        //执行功能返lkd三条记录为真。
        deleteAllTempPlanAndLabOpenPlan();

        List<TempOpenPlan> topList = mockTempOpenPlanList();
        for (int i = 0; i < topList.size(); i++) this.tempOpenPlanService.getCurrentSession().save(topList.get(i));

        List<TempOpenPlan> qlist = this.tempOpenPlanService.queryAllTempOpenPlans("2018-2019-1");
        assertEquals(3,qlist.size());
    }

    @Test
    @Transactional
    public void delete() {
        /**
         * 删除指定记录
         * 验证查询指定记录为nul
         *
         */

        deleteAllTempPlanAndLabOpenPlan();
        Session session = sessionFactory.getCurrentSession();

        List<TempOpenPlan> topList = mockTempOpenPlanList();
        for (int i = 0; i < topList.size(); i++) this.tempOpenPlanService.getCurrentSession().save(topList.get(i));

        TempOpenPlan tempOpenPlan = new  TempOpenPlan(0,"2018-2019-1","TESTYYYY",4,0,"");
        session.save( tempOpenPlan );


        this.tempOpenPlanService.delete(tempOpenPlan.getTempPlanId());
        TempOpenPlan ret = session.get( TempOpenPlan.class,tempOpenPlan.getTempPlanId() );
        assertEquals( null,ret );
    }

    @Test
    @Transactional
    public void findTempOpenPlan( ) throws OpenPlanDataIncompleteException, OpenPlanExpressinException {
        //删除全部记录
        //插入相对应的LabOpenPlan lop记录
        //新插入一个TempOpenPlan top实体，它依赖的LabOpenPlan是lop实体
        //依据top.id读出一个topRead
        //校验topRead 应与 top和top 相匹配

        deleteAllTempPlanAndLabOpenPlan();

        //构造一个LabOpenPlan计划
        String term="2018-2019-1";
        String labId="TESTXXXX";
        LabOpenPlan lop = new LabOpenPlan(null,term,labId,LabOpenPlanService.PUBLISH,"TEST");
        Map<String, String> lopmap = mockLabOpenPlanDetails(632323L);
        this.labOpenPlanService.saveUpdate(lop, lopmap, LabOpenPlanService.PUBLISH, LabService.SAVE);
        LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan( term,labId );

        //构造一个空白的TempOpenPlan计划
        int weekOfTerm =5;
        this.tempOpenPlanService.createBlankTempOpenPlan( term,labId ,weekOfTerm,labOpenPlanTuple);

        //修改临时计划
        TempOpenPlanTuple tempOpenPlanTuple = tempOpenPlanService.findTempOpenPlan( term,labId,weekOfTerm );
        TempOpenPlan topRead = tempOpenPlanTuple.getTempOpenPlan();
        Map<String,TempItem> mapRead = tempOpenPlanTuple.getTempOpenPlanDetailMap();
        modifyTempPlan(mapRead);
        this.tempOpenPlanService.saveUpdate(topRead,mapRead,LabService.PUBLISH,LabService.UPDATE);

        //读出结果
        TempOpenPlanTuple tempConcOpenPlanTuple = tempOpenPlanService.findTempOpenPlan( term,labId,weekOfTerm );
        TempOpenPlan topConcRead = tempOpenPlanTuple.getTempOpenPlan();
        Map<String,TempItem> mapConcRead = tempOpenPlanTuple.getTempOpenPlanDetailMap();


        assertEquals( topConcRead.getTerm(),topRead.getTerm() );
        assertEquals( topConcRead.getLabId(),topRead.getLabId() );
        assertEquals( topConcRead.getWeekOfTerm(),topRead.getWeekOfTerm() );

        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertTrue(mapConcRead.get(key).equals(mapRead.get(key)));

            }
        }

    }

    public void deleteAllTempPlanAndLabOpenPlan() {
        Session session = sessionFactory.getCurrentSession();
        session.createQuery("delete from LabOpenPlan").executeUpdate();
        session.createQuery("delete from TempOpenPlan").executeUpdate();
    }

    public void modifyTempPlan(Map<String, TempItem> mapRead) {
        long seed=34454L;
        Random rnd = new Random();
        rnd.setSeed(seed);
        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                String temp =""+rnd.nextInt(2);
                String orgin = mapRead.get(key).getOrign();
                mapRead.put( key,new TempItem( orgin,temp ) );

            }
        }
    }

    public Map<String, TempItem> mockTempOpenPlanDetails(long seed) {
        java.util.Random rnd = new Random();
        rnd.setSeed(seed);

        Map<String,TempItem> topmap = new HashMap<>(  );
        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                String orgin =""+i+"-"+"j";
                String temp =""+rnd.nextInt();
                topmap.put( key,new TempItem( orgin,temp ) );

            }
        }
        return topmap;
    }


    @Test
    @Transactional
    public void createBlankTempOpenPlan() throws OpenPlanDataIncompleteException, OpenPlanExpressinException {
        //首先创建空的，
        //读出
        //检查
        deleteAllTempPlanAndLabOpenPlan();

        String term = "2018-2019-1";
        String labId = "TESTZZZ";
        int weekOfTerm =5;
        //新建被依赖的实体lop
        LabOpenPlan lop = new LabOpenPlan(null,term,labId,LabOpenPlanService.PUBLISH,"TEST");
        Map<String, String> lopDetailsMap = mockLabOpenPlanDetails(632323L);
        this.labOpenPlanService.saveUpdate(lop, lopDetailsMap, LabOpenPlanService.PUBLISH, LabService.SAVE);
        LabOpenPlanTuple labOpenPlanTuple = this.labOpenPlanService.findLabOpenPlan( term,labId );

        //创建空白对象top
        this.tempOpenPlanService.createBlankTempOpenPlan( term,labId ,weekOfTerm,labOpenPlanTuple);

        TempOpenPlanTuple tempOpenPlanTuple = this.tempOpenPlanService.findTempOpenPlan( term,labId ,weekOfTerm);
        TempOpenPlan temp = tempOpenPlanTuple.getTempOpenPlan();
        Map<String, TempItem> map = tempOpenPlanTuple.getTempOpenPlanDetailMap();

        //断言
        assertEquals( term,temp.getTerm() );
        assertEquals( labId,temp.getLabId() );
        assertEquals((Integer)weekOfTerm,temp.getWeekOfTerm());

        for (int i = 1; i < LabService.TIMESLOTNUM; i++) {
            for (int j = 1; j <= 7; j++) {
                String key = "t" + i + "w" + j;
                assertEquals( "",map.get( key ).getTemp() );
                final String tempOrgin = map.get(key).getOrign(); //{0,1}
                final String lopOrgin = lopDetailsMap.get(key);//4-5
                assertTrue(match(lopOrgin,tempOrgin,weekOfTerm));
            }
        }

    }

    @Test
    public void match_Test() {
        assertTrue(match("1-4","0",5));
        assertTrue(match("4-1","0",5));
        assertTrue(match("1-4","1",4));
        assertTrue(match("4-1","1",4));

    }

    private boolean match(String lopOrgin, String tempOrgin,int weekOfTerm) {
        String[] weeks = lopOrgin.split("-");
        int w1 = Integer.parseInt(weeks[0]);
        int w2 = Integer.parseInt(weeks[1]);
        if (w1>w2) {
            int temp = w1;
            w1=w2;
            w2 = temp;
        }
        String conc="";
        if (weekOfTerm>=w1 && weekOfTerm<=w2)
            conc = "1";
        else
            conc ="0";

        return conc.equals(tempOrgin);


    }



    public static List<TempOpenPlan> mockTempOpenPlanList() {
        List<TempOpenPlan> topList = new ArrayList<>();
        TempOpenPlan top1 = new TempOpenPlan(1,"2018-2019-1","6320",5,0,"临时开放计划");
        TempOpenPlan top2 = new TempOpenPlan(2,"2018-2019-1","6318",6,1,"临时开放计划");
        TempOpenPlan top3 = new TempOpenPlan(3,"2018-2019-1","6321",8,0,"临时开放计划");
        topList.add(top1);
        topList.add(top2);
        topList.add(top3);
        return topList;


    }
}