package test;

import com.hgd.laboratory.po.ApplyForLab;
import com.hgd.laboratory.service.ReservationRulesService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;

import static org.junit.Assert.*;

public class ApplyForLabTest {

    @Test
    public void stateDisplay() {
        assertEquals("待审核", ApplyForLab.stateDisplay(0));
        assertEquals("审核未通过", ApplyForLab.stateDisplay(1));
        assertEquals("审核通过但未使用", ApplyForLab.stateDisplay(2));
        assertEquals("审核通过且已使用", ApplyForLab.stateDisplay(3));
        assertEquals("被取消预约", ApplyForLab.stateDisplay(4));


    }



}